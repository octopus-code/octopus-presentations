<!-- .slide: data-background="#ffffff" data-transition="none" -->

<div class="talk-title">
  <h2>State of the union of the Octopus project</h2>
  <p style="font-size: 0.7em; display: grid;">
    <a href="https://octopus-code.gitlab.io/octopus-presentations/2021-03-25-MPSD-Theory-Seminar/static/">Heiko Appel </a>
  </p>

  <p style="display: grid;">
    Max Planck Institute for the Structure and Dynamics of Matter
    <br>
    Hamburg, Germany
  </p>

  <img  style="height:200px;" src="figures/Octopus_logo.png" />

  For more content visit: <a href="https://gitlab.com/octopus-code/octopus-presentations">https://gitlab.com/octopus-code/octopus-presentations</a>
</div>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

  <p style="font-size: 0.8em; font-weight: bold;" align="center">Collaboration - Thanks!</p>

  <div align="left">
   <table>
   <tr>
   <td style="padding-left:10px;" align="center">
     <img class="r-stretch" style="height:200px; box-shadow: 5px 5px 5px grey;" src="figures/collaborators/franco.jpg" alt="Franco Bonafe" title="Franco Bonafe"/>
     <small>Franco Bonaf&eacute;</small></td>
   <td style="padding-left:10px;" align="center">
     <img class="r-stretch" style="height:200px; box-shadow: 5px 5px 5px grey;" src="figures/collaborators/martin.jpg" alt="Martin L&uuml;ders" title="Martin L&uuml;ders"/>
     <small>Martin L&uuml;ders</small></td>
   <td style="padding-left:10px;" align="center">
     <img class="r-stretch" style="height:200px; box-shadow: 5px 5px 5px grey;" src="figures/collaborators/portrait-sohlmann2.jpg" alt="Sebastian Ohlmann" title="Sebastian Ohlmann"/>
     <small>Sebastian Ohlmann</small></td>
   <td style="padding-left:10px;" align="center">
     <img class="r-stretch" style="height:200px; box-shadow: 5px 5px 5px grey;" src="figures/collaborators/micael.jpg" alt="Micael Oliveira" title="Micael Oliveira"/>
     <small>Micael Oliveira</small></td>
   <td style="padding-left:10px;" align="center">
     <img class="r-stretch" style="height:200px; box-shadow: 5px 5px 5px grey;" src="figures/collaborators/nicolas.jpg" alt="Nicolas Tancogne-Dejean" title="Nicolas Tancogne-Dejean"/>
     <small>Nicolas Tancogne-Dejean</small></td>
   </tr>
   </table>
  </div>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Octopus developments not covered in this talk
- Casida implementation for photons
- Sternheimer implementation for photons
- Multi-trajectory Ehrenfest dynamics
- Real-time TDDFT with Hubbard U 
- Real-time TDDFT with hybrid functionals
- Magnons from real-time TDDFT
- ...

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->
<p align="center" style="color: #003399; font-size: 0.9em;">Coupling of time-dependent Schr&ouml;dinger/Kohn-Sham and Maxwell's equations</p>
<hr style="height:10px; visibility:hidden;" />
<p align="center">
  <img style="margin-bottom:20px; margin-right:10px; box-shadow: 5px 5px 5px grey; border-radius: 8px;border:2px;" align="center" src="figures/maxwell_schroedinger.png" width="420px"/>
</p>
<hr style="height:10px; visibility:hidden;" />
<p align="center" style="font-size: 0.9em; color: #c70039" class="fragment">or: how everything got started</p>


---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

<h3>Maxwell's equations in Schr&ouml;dinger form</h3>
<p align="left">Riemann-Silberstein (RS) vector</p>
<p>
  \begin{align*}
    \vec{F}(\vec{r},t) = \sqrt{\epsilon_0/2} \hspace{1ex} \vec{E}(\vec{r},t) + i \sqrt{1/(2\mu_0)} \hspace{1ex} \vec{B}(\vec{r},t)
  \end{align*}
</p>

<p align="left">Maxwell's equations in microscopic form</p>
<p align="center">
<table>
<tr>
  <td>
       \begin{align*}
         \vec{\nabla} \cdot  \vec{E}   &= \frac{1}{\epsilon_0} \rho      \\
         \vec{\nabla} \cdot  \vec{B}   &= 0
       \end{align*}
  </td>
  <td>
\begin{align*}
        \vec{\nabla} \times \vec{E}  &= - \partial_t \vec{B}            \\
        \vec{\nabla} \times \vec{B}  &= \mu_0 \epsilon_0 \partial_t \vec{E} + \mu_0 \vec{j}
      \end{align*}
  </td>
</tr>
<tr class="fragment">
  <td align="center">
        $\Big\Downarrow$
  </td>
  <td>&emsp;&emsp;</td>
  <td align="center">
        $\Big\Downarrow$
  </td>
</tr>
<tr class="fragment">
  <td>
     \begin{align*}
      \vec{\nabla} \cdot \vec{F} = \frac{1}{\sqrt{2 \epsilon_0}} \rho
     \end{align*}
  </td>
  <td>
     \begin{align*}
      i \partial_t \vec{F}(\vec{r},t) = \pm c_0 \vec{\nabla} \times \vec{F}(\vec{r},t) - \frac{i}{\sqrt{2 \epsilon_0}} \vec{j}
    \end{align*}
  </td>
</tr>
</table>
</p>


<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Maxwell implementation
 - New interface for arbitrary linear media shapes (based on CGAL library)
 - GPU kernel for curl
 - Batchification of critical operations (exponential, curl)

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Maxwell implementation
 <div align="center">
   <video width="800" onclick="this.paused ? this.play() : this.pause()">
     <source src="movies/azulene_maxwell+tdks.webm">
   </video>
   <div class="anim-play slide" data-what="video"></div>
   <div class="anim-pause slide" data-what="video"></div>
 </div>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->
<p align="center" style="color: #003399; font-size: 0.9em;">New Multi-System Framework</p>
<hr style="height:10px; visibility:hidden;" />
<p align="center">
  <img style="margin-bottom:20px; margin-right:10px; box-shadow: 0px 0px 0px grey; border-radius: 0px;border:0px;" align="center" src="figures/octopus-interactions1.svg" width="620px"/>
</p>


---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Multi-System Implementation
 - New framework to handle calculations of coupled systems
 - Allows to define many physical systems simultaneously:
   - Electrons, ions, lasers, Maxwell, DFTB+, PCM, etc.
 - Systems are coupled through interactions:
   - Electron-ion, Lorentz force, dipole coupling, ...
 - Calculations modes are now "algorithms": a set of state machine atomic operations
 - The code automatically handles all the interactions/systems: very general and robust
 - New parallelization level: systems
 - Current efforts focused on porting SCF and time propagation to new framework

Note: speaker note

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Multi-System Framework Design
 - Focus on extendability and maintainability:
   - Adding new systems, interactions and algorithms should be as simple as possible
 - Flexible algorithms:
   - Time-propagation using different propagators and time-steps for each system
   - Nested SCF loops
 - Framework is independent of existing systems and interactions
 - Systems do not know about each other directly, instead they know interactions
 - Heavy use of object-oriented programming

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

<pre><code data-trim data-noescape data-line-numbers="1-5|7-9">
%Systems
 "Sun"   | classical_particle
 "Earth" | classical_particle
 "Moon"  | classical_particle
%

%Interactions
 gravity | all_partners
%
</code></pre>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

<pre><code data-trim data-noescape data-line-numbers="1-3|5-9|11-13">
%Systems
 "SolarSystem" | multisystem
%

%SolarSystem.Systems
 "Sun"   | classical_particle
 "Earth" | classical_particle
 "Moon"  | classical_particle
%

%Interactions
 gravity | all_partners
%
</code></pre>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

<pre><code data-trim data-noescape data-line-numbers="1-3|5-8|10-13|15-17">
%Systems
 "SolarSystem" | multisystem
%

%SolarSystem.Systems
 "Sun"   | classical_particle
 "Earth" | multisystem
%

%Earth.Systems
 "Terra" | classical_particle
 "Luna"  | classical_particle
%
</code></pre>

---


---

<!-- .slide: data-background="#ffffff" data-transition="none" -->
<p align="center" style="color: #003399; font-size: 0.9em;">New website for the octopus documentation</p>
<hr style="height:10px; visibility:hidden;" />
<p align="center">
  <img style="margin-bottom:20px; margin-right:10px; box-shadow: 0px 0px 0px grey; border-radius: 0px;border:0px;" align="center" src="figures/octopus-documentation.svg" width="620px"/>
</p>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->
### New Website - Live Demo!
<img class="r-stretch" style="border: 0;" src="figures/octopus-new-webpage.png" />
<a href="https://octopus-code.org/new-site/develop/">https://octopus-code.org/new-site/develop/</a>

Notes:
 - Tutorials
 - Manual
 - Variable definitions
 - Improved cross-linking

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Continuous integration, Gitlab, and Buildbot
<img class="r-stretch" style="border: 0;" src="figures/overview-ci.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Pipelines

- Different architectures
  - x86
  - PPC
  - Nvidia GPUs
- Different toolchains
  - Foss (gcc + openmpi + openblas)
  - Intel (Intel compiler + Intel MPI + MKL)
- Different combinations:
  - with/without MPI
  - with/without OpenMP
- Valgrind builder
- Warnings checker
- about 30 different pipelines

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Testsuite
- Goal: avoid feature regression
- Mostly integration tests (full runs)
- Some unit tests, some validation tests

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Implementation
- Custom framework in perl
  - Single test
  - Series of runs with different input files
  - Match specified values in files against reference values
- Driver script
  - Several tests at once (also with MPI)
  - Collect all data in a yaml file (~ 20000 matches)

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Testsuite web application
- Motivation:
  - about 15000 matches per toolchain
  - about 400000 matches per commit
  -> how to identify and correct failed tests?
- Use web application to display results from testsuite
- Help in adapting failed tests
  - Adjusting tolerance
  - Recentering
- Analyze tests → improve testsuite
- Integration with buildbot
- At https://octopus-code.org/testsuite

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Overview
<img class="r-stretch" style="border: 0;" src="figures/testsuite-app-overview.png" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### One commit
<img class="r-stretch" style="border: 0;" src="figures/testsuite-app-commit.png" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Comparison of one input file
<img class="r-stretch" style="border: 0;" src="figures/testsuite-app-inputfile.png" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Summary
- Quite some data accumulated
  - ~2900 commits
  - ~75000 runs (i.e. toolchains)
  - ~930M matches
- Very helpful for fixing failing tests

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Performance testsuite
- Goal: track performance over time, avoid regression
- Unit tests for a wide parameter space
- Future: add full runs

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Implementation
- Create tests using python
  - Template input files
  - Yaml configuration for parameters (number of states, number of grid points, ...)
  - Run for full hypercube of combinations
- Several thousand tests
- Parallel execution: make + srun
  - Several tests in parallel with make -jN
  - Start test with srun → no overbooking of node

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Comparison of two runs
<img class="r-stretch" style="border: 0;" src="figures/performance-app-comparison-plots.png" />


---

<!-- .slide: data-background="#ffffff" data-transition="none" -->
<p align="center" style="color: #003399; font-size: 0.9em;">Performance Optimizations</p>
<hr style="height:10px; visibility:hidden;" />
<p align="center">
  <img style="margin-bottom:20px; margin-right:10px; box-shadow: 5px 5px 5px grey; border-radius: 8px;border:2px;" align="center" src="figures/octopus+racecar.svg" width="620px"/>
</p>


---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### GS improvements

- Improve eigensolvers
  - Conjugate gradients
    - Corrected implementation: order of steps
    - Introduce normalizations
    - Adapt convergence criteria of eigensolver loop
  - RMMDIIS: small corrections
- Improve preconditioners
  - Filter preconditioner
    - Theoretical understanding in terms of Jacobi iterations
    - Generalization to non-orthogonal cells
    - Adapt default
  - Generalized multigrid preconditioner
- Improve Broyden mixing
  - Remove normalization of intermediate quantities
  - Fix numerical issues
  - Enable fast convergence to very high accuracy
  - Implement restarting: mixing history is reset after a while,
    helps finding the right minimum
- Improvement in convergence
  - Before, difficulties below 10^-8 in relative densities
  - Now, convergence to 10^-15 possible for many systems
- Bugfixes for the LCAO

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Improved convergence
<img class="r-stretch" style="border:0;" src="figures/convergence_comparison.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Optimizations
- Casida for huge matrices (up to 100k), used for photon coupling
- Improvements of the OpenMP parallelization
  - More pragmas for loops, some are more complicated (e.g. norm)
  - Good scaling up to about 12 threads per rank
  - Only good for large grids, can substitute domain parallelization
- Application of phase for periodic systems
  - only once at beginning and end of time step
  - use phase correction for boundary points
  - can save up to 50-80% of the computing time
- Allocate aligned memory for better vectorization, introduce AVX512 instructions
- Improve finite-difference kernels by using non-temporal store instructions
  - directly store to memory without cache
  - improve performance of kernel

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Evolution over time - GS run
  <img class="r-stretch" style="border: 0;" src="figures/comparison-octopus-versions-gs.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Evolution over time - GS run
  <img class="r-stretch" style="border: 0;" src="figures/comparison-octopus-versions-gs-speedup.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Evolution over time - TD run
  <img class="r-stretch" style="border: 0;" src="figures/comparison-octopus-versions-td.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Evolution over time - TD run
  <img class="r-stretch" style="border: 0;" src="figures/comparison-octopus-versions-td-speedup.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->
<p align="center" style="color: #003399; font-size: 0.9em;">Octopus on GPUs</p>
<hr style="height:10px; visibility:hidden;" />
<p align="center">
  <img style="margin-bottom:20px; margin-right:10px; box-shadow: 5px 5px 5px grey; border-radius: 8px;border:0px;" align="center" src="figures/octopus-on-gpus.svg" width="520px"/>
</p>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### GPU developments
- batches:
  - a set of wave functions, stored on CPU or GPU
  - unpacked and packed format (innermost index: mesh vs state)

<img class="r-stretch" style="border:0;" src="figures/batch_layout.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### GPU developments
- Refactor batch implementation and handling; now easier to use
- Extend use of batches in many places in the code, thus also
  the use of GPUs
- Use all GPUs on a node
- Port more features and algorithms to the GPU, implement some kernels
- Performance improvements
  - Handling of memory:
    - Avoid unnecessary allocations
    - Use packed format on CPU, thus avoid transposition during transfer
    - Use pinned memory for faster transfer
  - Use CUDA-aware MPI for faster communication
    - mostly for ghost exchange (parallel in domains)
    - Transfer through network directly from GPU memory
    - Avoid copy to CPU memory
  - Overlap computation and communication
    - For Laplace operator: communicate ghost points while computing inner part
      of Laplace operator
    - Load next batch in GPU memory while processing current one (only if not
      all batches fit in GPU memory), pipelining approach

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### How to use the GPU efficiently
- Avoid domain parallelization, use state and k point parallelization
- Large enough grids (>10k grid points)
- Enough states (>32 per k point)
- Thus, more suited for large molecules than small solids with many k points
- Enable fft on GPUs (PoissonSolver = fft, FFTLibrary = accel)
- Set StatesPack = yes

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### GPU Speedup (large molecule)
<img class="r-stretch" style="border:0;" src="figures/comparison-octopus-gpu-td.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### GPU Speedup (large molecule)
<img class="r-stretch" style="border:0;" src="figures/comparison-octopus-gpu-td-speedup.svg" />

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Maxwell 1: Wi-Fi
 - Add openscad snapshot

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Maxwell 2: Octopus in Octopus
 - Add video of the Octopus simulation

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Octopus-DFTB interface: benzene
 - Show comparison of input files to run same simulation with TDDFT and TDDFTB

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Octopus-DFTB interface: benzene
 - Show comparison of results from both simulations (2x dipole moment plots + 2x animations of breathing oscillations)

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

### Outlook
 - Octopus Forum as meeting point
 - Are you ready for GPUs? Send us your inputs. We provide help.
 - Plans for the future
 - Octopus Basics and Advances lectures in September 2021


