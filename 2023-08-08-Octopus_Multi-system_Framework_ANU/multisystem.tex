%\documentclass[palatino,aspectratio=1610]{beamer}
\documentclass[palatino,aspectratio=1610,handout]{beamer}

%%%%%%%%%%%%%% MACROS AND STUFF %%%%%%%%%%%%%%
\mode<presentation>

\usetheme{Madrid}

\definecolor{mycolor}{RGB}{67,130,176}
\usecolortheme[named=mycolor]{structure}
\setbeamercolor{normal text}{fg=violet!20!black}

\useoutertheme[footline=authortitle,subsection=false]{miniframes}
%\useoutertheme{default}
\setbeamertemplate{navigation symbols}{} 

\usefonttheme[onlymath]{serif}


\usepackage[english]{babel}

\usepackage{graphicx,amsmath,amsfonts,amssymb,mathrsfs,bm}
\usepackage{pgfpages}
\usepackage{tikz}
\usepackage{ulem}
\usepackage{algpseudocode}
\usetikzlibrary{arrows,shadows,decorations.pathmorphing,backgrounds,positioning,fit,petri,calc,shapes}
\usepackage{smartdiagram}

\input{macros.tex}

\newcommand{\mysection}[1]{
%\frame{
%\begin{center}
%{\LARGE #1}
%\end{center}
%}
}

\usepackage{listings}
\lstloadlanguages{C,[90]Fortran}
\newenvironment{specialframe}{\begin{frame}[fragile,environment=specialframe]}{\end{frame}}
\lstset{% general command to set parameter(s)
    basicstyle=\tiny,     % print whole listing small
    keywordstyle=\color{darkblue},
                                % underlined bold black keywords
    identifierstyle=\color{darkgreen},           % nothing happens
    commentstyle=\color{darkred},   % white comments
    stringstyle=\color{red}\ttfamily,      % typewriter type for strings
    showstringspaces=false}     % no special string spaces


\definecolor{darkblue}{rgb}{0,0,0.5}
\definecolor{darkgreen}{rgb}{0,0.5,0}
\definecolor{darkred}{rgb}{0.5,0,0}




%%%%%%%%%%%%%%%% TITLE PAGE %%%%%%%%%%%%%%
\title[]{The Octopus Multi-system Framework \\ (or how to write a coupler from scratch)}

\author[]{Micael Oliveira and the Octopus developers\inst{1}}

\institute[]{\inst{1} \textit{Max Planck Institute for the Structure and Dynamics of Matter, Hamburg, Germany}}

\pgfdeclareimage[height=4.0cm]{oct_logo}{img/octopus_logo}
\date[CFP 2023]{\vskip-0.2cm\pgfuseimage{oct_logo}}


\pgfdeclareimage[height=0.7cm]{logo_2}{img/cosima-768x768}
\pgfdeclareimage[height=0.7cm]{logo_1}{img/mpsd_logo}
\logo{\pgfuseimage{logo_1}\hskip0.1cm\pgfuseimage{logo_2}}

\setbeamertemplate{sidebar left}


\begin{document}

\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\frametitle{Motivation}

\tikzstyle{every picture}+=[remember picture]

  At its core, Octopus [1] solves a set of differential equations describing the dynamics of a system of electrons and nuclei:
 
  \begin{align}
    &i \frac{\partial}{\partial t}\varphi_i(\mvec{r}, t) = \left\{
           \tikz[baseline]{
             \node[anchor=base]  (t_ps) {$v_\ext(\mvec{r}; \mvec{R})$};
           } + v_{\Hartree\xc}[n](\mvec{r},t)\right\}\varphi_i(\mvec{r},t)\nonumber
           \\
    &m_I \frac{\partial^2}{\partial t^2} \mvec{R}_I(t) = \sum_{J \neq I} \mvec{F}_{IJ}(\mvec{R}_I, \mvec{R}_J) + 
        \tikz[baseline]{
         \node[anchor=base]  (t_fie) {$ \mvec{F}_{Ie}(\mvec{R}_I; n)$};
         }\nonumber
  \end{align}
  
% \onslide<2->
 \begin{itemize}
  \item Electronic orbitals $\varphi_i(\mvec{r}, t)$ are discretized on a grid
  \item Nuclei are treated classically as point charges
  \item The equations are coupled by the nuclear coordinates $\mvec{R}$ and by the electronic density $n(\mvec{r}, t) = \sum_i |\varphi_i(\mvec{r}, t)|^2$
 \end{itemize}

 \vskip0.5cm
 \begin{footnotesize}
  [1] \url{www.octopus-code.org}
 \end{footnotesize}

}

\frame{\frametitle{Motivation}

 \begin{columns}
  \begin{column}{0.60\textwidth}
  Developers wanted to couple new types of systems with electrons and ions:
  \begin{itemize}
   \item Classical electromagnetic fields (Maxwell equations)
   \item Quantized EM fields (quantum electrodynamics)
   \item Solvent models
   \item ...
  \end{itemize}
  \end{column}
  \begin{column}{0.3\textwidth}
   \includegraphics[width=1.0\textwidth]{img/octopus-interactions}
  \end{column}
 \end{columns}

 \vskip0.7cm

 This can be challenging:
  \begin{itemize}
  \item Very different numerical methods are used to solve each subset of equations
  \item Time-scales can be very different (e.g., electrons move faster than nuclei)
 \end{itemize}
 
  \vskip0.5cm

  \onslide<2->
 \begin{center}
 Not so different from coupled climate models!  
 \end{center}
 
}



\frame{\frametitle{Motivation}

 The new framework should be able to:
 \begin{itemize}
  \item Handle arbitrary number of systems and system types
  \item Implement different types of interactions between systems
  \item Implement several different algorithms for each system type
  \item Decide what systems/interactions/algorithms to run from the input file
  \item Allow users to mix different types of algorithms (e.g., time-propagation and minimization)
  \item Give the user complete control over any possible approximations
  \item Allow for parallelization over systems
 \end{itemize}

 \vskip0.5cm
 
 \onslide<2->
 \begin{center}
  Very ambitious and not trivial to implement!
 \end{center}

}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{specialframe}\frametitle{How to design this}

 
 \begin{enumerate}
  \item Clearly state the problem we are trying to solve
  \item Write down all requirements
  \item Choose a suitable programming paradigm (object-oriented, functional, etc)
  \item Develop and test the code using a simple, well understood example application that covers most of the intended use-cases 
 \end{enumerate}

 \end{specialframe}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{specialframe}\frametitle{What problem are we trying to solve}


 \begin{itemize}
  \item \only<1|handout:0>{Framework to simulate interacting physical systems}\only<2-|handout:1>{\sout{Framework to simulate interacting physical systems}}
 \end{itemize}
 \pause
 \vskip0.2cm
 \begin{itemize}
  \item \only<2|handout:0>{Framework to solve systems of coupled differential equations}\only<3-|handout:1>{\sout{Framework to solve systems of coupled differential equations}}
 \end{itemize}
 \pause
 \vskip0.2cm
 \begin{itemize}
  \item Framework to handle one or more iterative algorithms that need to exchange information at specific iterations
 \end{itemize}

 \pause
 \vskip0.5cm

 The framework needs to:
 \begin{itemize}
  \item Implement a mechanism for information exchange
  \item Implement conditions for information exchange
  \item Keep track of the internal state of systems, couplings, and algorithms
 \end{itemize}
 
\end{specialframe}


\begin{specialframe}\frametitle{What problem are we trying to solve}

 Some terminology:
 \begin{itemize}
  \item {\bf System}: physical system characterized by some internal quantities (e.g, positions, densities, temperatures, etc) that are updated by some iterative algorithm
  \item {\bf Coupling}: an internal quantity of a system that is required to execute the algorithm of another system (e.g., the position of a particle)
  \item {\bf Interaction}: a term required to execute the algorithm of a system that, in general, requires internal quantities from the system and some couplings to be evaluated (e.g., gravitational force)
  \item {\bf Interaction partner}: some entity that contains couplings needed by other systems. All systems can be interaction partners, but not all interaction partners are systems (e.g., data models)
 \end{itemize}
 
\end{specialframe}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{specialframe}\frametitle{Design requirements}

 \begin{center}
  {\bf Main requirement}: framework plus all existing systems, interactions and algorithms should be easy to maintain and extend.
 \end{center}
  \pause
 \begin{itemize}%[<+->]
  \item Framework should be completely independent of existing systems, interactions and algorithms
  \item Adding new systems should not require modifying existing systems, interactions or algorithms
  \item Adding new interactions should only require to modify systems that want to use those interactions
  \item Adding new algorithms should only require to modify systems that want to use those algorithms
  \item Modifications to the framework should only be required when adding new {\bf features}, not when adding new systems/interactions/algorithms
 \end{itemize}

\end{specialframe}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{specialframe}\frametitle{Programming paradigm and design choices}
 
 The way \alert{NOT} to do it:
 \vskip0.2cm
 
 \lstset{language=Fortran}
 \begin{lstlisting}
if (system_A%is_electrons) then
  ...
  select case (system_A%propagator)
  case (AETRS)
    ...
    if (system_A%has_interaction_X_with_system_B) then
      ...
    end if
  end select
  ...
else if (system_A%is_ions) then
  if ((system_A%has_interaction_Y_with_system_B) then
    ...
  end if
  ...
end if
 \end{lstlisting}

\end{specialframe}


\begin{specialframe}\frametitle{Programming paradigm and design choices}

  \begin{itemize}%[<+->]
  \item Object Oriented Programming
  \item Framework defines several abstract classes for systems, interactions and algorithms
  \begin{itemize}
   \item Actual systems, interactions and algorithms provide implementations for the required deferred methods
   \item Clean separation between the framework and the math/physics
  \end{itemize}
  \item Systems do not know about each other directly, instead they know interactions
  \item An interaction connects a system with an interaction partner
  \item Interactions are uni-directional
  \item Algorithms are implemented as a set of state machine atomic operations (algorithmic operations)
  \item Systems do not inherit from the algorithms, instead, they implement algorithmic operations
  \begin{itemize}
   \item Fortran does not allow multiple inheritance! But we wouldn't use it anyway...
  \end{itemize}
 \end{itemize}

 
\end{specialframe}

\begin{specialframe}\frametitle{UML Class diagram}

 \begin{center}
  \includegraphics[width=1.\textwidth]{img/multisystem_uml}
 \end{center}
  \includegraphics[width=0.28\textwidth]{img/key_uml}

 
\end{specialframe}


\begin{specialframe}\frametitle{The general algorithm}

 \begin{small}

 \begin{algorithmic}[1] % The number tells where the line numbering should start
  \Repeat
    \ForAll {systems}
     \State $algo\_op \gets $ next algorithmic operation
     \State $break \gets false$
     \While{not $break$}
      \If {$algo\_op \neq$ update interactions}
        \State execute algorithmic operation
        \State $algo\_op \gets $ next algorithmic operation
      \Else
        \State try updating interactions
        \If{interactions updated}
          \State $algo\_op \gets$ next algorithmic operation 
        \EndIf
        \State $break \gets true$
      \EndIf
     \EndWhile
    \EndFor
  \Until{all algorithms finished}
 \end{algorithmic}
 \end{small}

\end{specialframe}


\frame{\frametitle{Conditions for interaction update}

 When a system request an interaction to be updated, the following conditions must be met for a successful update:
 \begin{itemize}%[<+->]
  \item The necessary system quantities must be at the exact requested time
  \item The partner's algorithm clock must be at the requested time or is going to reach the requested time in the next time-step
  \item The necessary partner quantities must be either:
  \begin{itemize}
   \item at the exact requested time (if user requested the interaction timing to be exact)
   \item at the closest possible time in the past (if the user allowed for retarded interactions)
   \item at the closest possible time in the future (if the user allowed for time interpolation)
  \end{itemize} 
 \end{itemize}

}

\frame{\frametitle{Updating clocks}

  \begin{itemize}%[<+->]
   \item The algorithm's clock is tentatively advanced when interactions are being updated and rewound if failed
   \item The algorithm's clock is advanced when interactions are successfully updated
   \item The system's clock is advanced when a time-step/iteration is finished
   \item A quantity's clock is updated whenever the quantity is updated
  \end{itemize}

}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\frame{\frametitle{Testing the framework: celestial dynamics}

  \begin{center}
  \includegraphics[width=0.3\textwidth]{img/Moon_apsidal_precession}
 \end{center}

  \begin{itemize}%[<+->]
   \item System of planets and moons as point particles interacting with gravity
   \item Numerical integration of orbits with different algorithms
   \item Easy to validate results
   \item Fast turnover for code development
  \end{itemize}

}

\frame{\frametitle{Testing the framework: the velocity Verlet propagator}

 \begin{enumerate}%[<+->]
  \item Update positions
   \begin{displaymath}
    \mvec{x}(t + \Delta t) = \mvec{x}(t) + \mvec{v}(t) \Delta t + \frac{1}{2}\mvec{a}(t)\Delta t^2 
   \end{displaymath}
  \item Update interactions with all partners (compute $\mvec{F}(\mvec{x}(t+\Delta t))$)
  \item Compute acceleration $\mvec{a}(t + \Delta t)$
  \item Compute velocity
   \begin{displaymath}
    \mvec{v}(t + \Delta t) = \mvec{v}(t) + \frac{1}{2} (\mvec{a}(t) + \mvec{a}(t+\Delta t)) \Delta t
   \end{displaymath}
 \end{enumerate}

}

\begin{specialframe}\frametitle{Testing the framework: a simple case}

 \begin{block}{\texttt{Input}}
   \begin{tiny}
    \begin{verbatim}
# List of systems to simulate, giving each one a name and declaring what type of system it is
%Systems  # (I am a block. Each line can have multiple columns. Columns are separated by a vertical bar)
 "Sun"   | classical_particle
 "Earth" | classical_particle
 "Moon"  | classical_particle
%

# Each system will interact through gravity with all possible partners
%Interactions
 gravity | all_partners
%

# We use velocitiy verlet for all systems
TDSystemPropagator = verlet

# Time step and total simulation time
TDTimeStep = 3600  # one hour
TDPropagationTime = 3*24*3600  # three days

# Next come the initial conditions, masses, etc
...
   \end{verbatim}
  \end{tiny}
 \end{block}

\end{specialframe}

\frame{\frametitle{Testing the framework: a simple case}
 
 \begin{center}
  \includegraphics[width=0.6\textwidth]{img/celestial-orbits}
 \end{center}
 \pause 
 \begin{center}
  But how does this work in practice?
 \end{center}
}

\frame{\frametitle{Visualizing the multi-system time-stepping algorithm}

 \url{https://octopus-code.org/documentation/main/developers/code_documentation/propagators/custom_diagram/}

 \begin{center}
  \includegraphics[width=0.8\textwidth]{img/multisystem-diagram}
 \end{center}
 
}

\begin{specialframe}\frametitle{Testing the framework: different time-steps}

 \begin{block}{\texttt{Input}}
   \begin{tiny}
    \begin{verbatim}
# List of systems to simulate, giving each one a name and declaring what type of system it is
%Systems
 "Sun"   | classical_particle
 "Earth" | classical_particle
 "Moon"  | classical_particle
%

# Each system will interact through gravity with all possible partners
%Interactions
 gravity | all_partners
%

# We use velocitiy verlet for all systems
TDSystemPropagator = verlet

# Time step and total simulation time
Sun.TDTimeStep   = 3600   # one hour
Earth.TDTimeStep = 3600/2 # 30 minutes
Moon.TDTimeStep  = 3600/4 # 15 minutes
TDPropagationTime = 3*24*3600  # three days

# We need to allow for the interactions to use information that is behind in time
# Another way to handle this would be to use interpolation
InteractionTiming = timing_retarded

# Next come the initial conditions, masses, etc
...
   \end{verbatim}
  \end{tiny}
 \end{block}

\end{specialframe}


\begin{specialframe}\frametitle{Testing the framework: multi-systems and nesting}

 \begin{block}{\texttt{Input}}
   \begin{tiny}
    \begin{verbatim}
# Top-level list of systems
%Systems
 "Sun"   | classical_particle
 "Earth" | multisystem   # This is a system of systems
%

# Here we specify the systems contained in the "Earth" system
%Earth.Systems
 "Terra" | classical_particle
 "Luna"  | classical_particle
%

# Each system will interact through gravity with all possible partners
%Interactions
 gravity | all_partners
%

# We use velocitiy verlet for all systems
TDSystemPropagator = verlet

# Time step and total simulation time
TDTimeStep = 3600  # one hour

TDPropagationTime = 3*24*3600  # three days

# Next come the initial conditions, masses, etc
...
   \end{verbatim}
  \end{tiny}
 \end{block}

\end{specialframe}


%\begin{specialframe}\frametitle{Testing the framework: multi-systems and nesting}
%
% \begin{itemize}
%  \item Unlimited nesting
%  \item Remember that multi-systems are also systems:
%  \begin{itemize}
%   \item Each multi-system has its own namespace
%   \item Multi-systems can have their own algorithms and interactions
%  \end{itemize}
% \end{itemize}
%
%\end{specialframe}


\begin{specialframe}\frametitle{Testing the framework: different algorithms}

 \begin{block}{\texttt{Input}}
   \begin{tiny}
    \begin{verbatim}
# Top-level list of systems
%Systems
 "Sun"   | classical_particle
 "Earth" | multisystem   # This is a system of systems
%

# Here we specify the systems contained in the "Earth" system
%Earth.Systems
 "Terra" | classical_particle
 "Luna"  | classical_particle
%

# Each system will interact through gravity with all possible partners
%Interactions
 gravity | all_partners
%

# Use exponential-midpoint for Sun (NB: this propagator requires the evaluation of the forces at dt/2 and at dt)
Sun.TDSystemPropagator = exp_mid_2step
Earth.TDSystemPropagator = verlet

# Time step and total simulation time
Sum.TDTimeStep   = 3600   # one hour
Earth.TDTimeStep = 3600/2 # 30 minutes

TDPropagationTime = 3*24*3600  # three days

# Next come the initial conditions, masses, etc
...
   \end{verbatim}
  \end{tiny}
 \end{block}

\end{specialframe}



\begin{specialframe}\frametitle{Classical dynamics class diagram}

 \begin{center}
  \includegraphics[width=0.7\textwidth]{img/classical_particles_uml}
 \end{center}
 
\end{specialframe}


\end{document}


\frame{\frametitle{Design principles (continued)}

 Some key properties to keep in mind when designing new algorithms:
  \begin{itemize}[<+->]
  \item Results cannot depend on the order in which systems are specified in the input file
  \item When using the exact same algorithm for all the systems, the results should be independent of the way in which the systems are partitioned
 \end{itemize}

}



\frame{\frametitle{Design decisions}

 \begin{itemize}[<+->]
  \item Framework should be completely independent of existing systems, interactions and algorithms
  \item Systems do not know about each other directly, instead they know the interactions
  \item Interactions are uni-directional
  \item No central puppeteer controlling the actions of all the systems
 \end{itemize}

}


\frame{\frametitle{Design in practice (continued)}


  \begin{itemize}[<+->]
  \item Three general types of algorithmic operations:
  \begin{itemize}
   \item System and algorithmic generic: implemented in the framework
   \item Algorithm specific and system generic: implemented in the algorithm classes
   \item System specific: implemented in the system classes
  \end{itemize}
  \item The framework keeps track of time (iterations) with clocks (counters)
  \begin{itemize}
   \item Systems, algorithms and quantities all have clocks attached
  \item The algorithm's clock is advanced when interactions are being updated
  \item The system's clock is advanced when a time-step/iteration is finished
  \item A quantity's clock is updated whenever the quantity is updated
    \end{itemize}
 \end{itemize}

}

\frame{\frametitle{More on interactions}

 \begin{itemize}[<+->]
  \item Each system has an arbitrary number of interactions
  \item Each interaction connects a system with an interaction partner
  \item To evaluate the interaction, one needs, in general, quantities from both the system and the partners
  \item Interactions are updated at specific times when executing an algorithm
  \item Updating an interaction requires that the system and the partner be at the same ``time'', or at the best possible ``time''
  \item All systems are connected through at least one interaction (``ghost interaction'')
  \item It is only possible to update the interactions once all systems are at the same ``time''
  \item Updating interactions is a barrier in the execution flow of the algorithm
 \end{itemize}

}

