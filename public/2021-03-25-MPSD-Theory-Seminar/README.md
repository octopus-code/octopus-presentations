# Update git submodules

```shell
git submodule update --init --recursive
```

# Install node packages
```
npm install
```

# Tidy up HTML

```
tidy -m -i presentation.html
```
