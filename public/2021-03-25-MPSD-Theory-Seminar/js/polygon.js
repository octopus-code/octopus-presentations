    var width = 1200;
    var height = 500;
    var lineLength = 220;
    var legendWidth = 360;
    var svg = d3.select("#mypolygon").append("svg")
      .attr("width", width)
      .attr("height", height)

    var poly = undefined;
    var angle = undefined;
    var angleRad = undefined;
    var angleOut = undefined;
    var angleOutRad = undefined;

    function drawPoly(count) {
      if(poly) {
        poly.remove();
      }
      poly = svg.append('g');
      angle = 360 / count;
      angleRad = Math.PI * angle / 180;
      angleOut = (180 - angle) / 2;
      angleOutRad = Math.PI * angleOut / 180;

      for(var i = 0; i < count; i++) {

        poly
          .append('circle')
          .attr('stroke', '#69a3b2')
          .attr('fill', '#69a3b2')
          .attr("r", 20)
          .attr("cx", Math.cos(i*angleRad) * lineLength)
          .attr("cy", -Math.sin(i*angleRad) * lineLength);



         for(var j = 0; j < i; j++) {
        poly
          .append('line')
          .attr('stroke', 'red')
          .attr('x1', Math.cos(i*angleRad) * lineLength + 8)
          .attr('y1', -Math.sin(i*angleRad) * lineLength)
          .attr('x2', Math.cos(j*angleRad) * lineLength + 8)
          .attr('y2', -Math.sin(j*angleRad) * lineLength)
        poly
          .append('line')
          .attr('stroke', 'blue')
          .attr('x1', Math.cos(i*angleRad) * lineLength - 8)
          .attr('y1', -Math.sin(i*angleRad) * lineLength )
          .attr('x2', Math.cos(j*angleRad) * lineLength - 8)
          .attr('y2', -Math.sin(j*angleRad) * lineLength)
         }


      }

      poly
       .attr(
         'transform',
         `translate(${(width - legendWidth)/2},${height/2})`
       )
    }

    drawPoly(10)

    d3.csv('data/polyNames.csv', function(data){
      var nameG = svg.append('g')
      var labels = nameG
       .selectAll('g')
       .data(data)
       .enter()
       .append('text')
       .text(function(d, i){
          return (i+3)+" - "+d.name+" - "+ 2* ((i+3)*(i+2))/2
        })
       .attr('transform', (d,i)=>{
          return `translate(60, ${i * 28})`
        })
       .style('font-size', 12)
       .style('font-family', 'Arial')
       .style('cursor', 'pointer')
       .on('click', function(d,i){
          drawPoly(i+3)
          labels.style('fill', 'black')
          d3.select(this).style('fill', 'red')
        })

      labels.filter(function(d,i){
        if(i===0) {
          d3.select(this).on('click').call(this, d,i);
        }
      })

      nameG
       .attr(
         'transform',
         `translate(
      ${width - legendWidth},
      ${(height - data.length * 13)/2}
     )`
       )


    });


