function tagify(name) {
    let re1=/ /g
    let re2=/\./g
    return name.replace(re1,'-').replace(re2,'-')
}


// ----------------------------------------------------------------------------------------

function prepare_systems(systems, constants) {

    Object.keys(systems).forEach( function(key) {

        var current = systems[key];
        var tmp = key.split('.');

        current.name = tmp[tmp.length-1]; // discard parent names;

        current.id = 'system_'+tagify(key);
        current.status = 'expand';
        current.minHeight = constants.systemHeight;
        current.isContainer = current['children'].length>0?true:false;


        current.interactions.forEach( function(i) {
            i.owner = key;
            i.name = i.type+':'+i.owner+'=>'+i.partner;
            i.width = constants.interactionWidth;
            i.show = true;
            i.isGhost = i.type == 'ghost';
            current[i.type+'-'+i.partner] = i
        });
    });

}

// ----------------------------------------------------------------------------------------

function prepare_events(events, constants, top_events, index) {

    var counter = 0;
    Object.keys(events).forEach( function(key) {

        index[counter] = key;
        current = events[key];
        current.step = counter;
        current.id = 'step_'+counter++;
        current.name = current.function;
        current.isInteraction = false;
        current.color = "rgba(255,255,0,0.2)"
        current.textcolor = "black"
        current.isGhost=false;
        current.minHeight = constants.minHeight;
        current.show = false;
        current.canCollapse = false;

        switch(current.function){
            case 'dt_operation':
                current.name = current.operation;
                if(current.operation == "Propagation step - Updating interactions") {
                    current.color = "darkgreen";
                    current.textcolor = "darkgreen";
                } else {
                    current.color = "green";
                    current.textcolor = "green";
                }
            break;
            case 'multisystem_dt_operation':
                current.canCollapse = true;
                if (current.system=='root') top_events.push(key)
            break;
            case 'interaction_with_partner_update':
                current.isInteraction = true;
                current.color = "blue";
                current.textcolor = "blue";
                if( current.target.split('-')[0] == 'ghost') {
                    current.isGhost = true;
                    current.color = "lightblue";
                    current.textcolor = "blue";
                    }
            break;
            case 'system_update_exposed_quantities':
                if (step_is_ghost(events, key, '')) {
                    current.color = "pink";
                    current.textcolor = "red";
                } else {
                    current.color = "red";
                    current.textcolor = "red";
                }
            break;
            }

        current.status = 'expand';

        constants.max_steps = counter;

    });

    return top_events;
}

function expand_all(data) {
    Object.keys(data.events).forEach( function(key) {
        data.events[key].show = true;
    })
    update(data);
}

function collapse_all(data) {
    Object.keys(data.events).forEach( function(key) {
        data.events[key].show = false;
    })
    update(data);
}

// ----------------------------------------------------------------------------------------

function create_step_groups(events, top_events, top_group) {

    for(step of top_events) {
        var current = events[step];
        var compound_steps = [];

        // console.log('create step groups: ',step);

        top_group.append('g').attr('id',tagify(current.id));

        var svg_sub_group = top_group.selectAll('g#'+current.id);

        for (child of current.children) {
            // if(events[child].children.length>0)
            compound_steps.push(child)
        }
        create_step_groups(events, compound_steps, svg_sub_group);


    }
}

// ----------------------------------------------------------------------------------------

function event_get_height(events, key, constants, viewGhosts) {

    var current = events[key];
    var height;

    switch(current.status){
    case 'expand':
        height = (viewGhosts || ! current.isGhost)?current.minHeight:0;
        for( child of current.children ) {
             height += event_get_height(events, child, constants, viewGhosts) + constants.stepMargin;
        }
        if (current.parent.length==0) height +=10;
    break;
    case 'collapse':
        height  = constants.minHeight+10;
    break;
    case 'hide':
        height = 0;
        for( child of current.children ) {
            height += event_get_height(events, child, constants, viewGhosts) + constants.stepMargin;
        }
        if (current.parent.length==0) height +=10;
    break;
    }

    return height;
}

function system_get_width(systems, key) {

    var current = systems[key];
    var width = 50;


    if(current.show) {
        for( child of current['children'] ) {
            width += system_get_width(systems, child) + 5;
        }
        for( interaction of current['interactions'] ) {
            width += interaction.width;
        }
    }


    return width;
}

function system_get_y_offset(systems, key) {

    var current = systems[key];
    var offset = 0;

    for (parent of current['parent'])  {
        offset += system_get_y_offset(systems, parent) + systems[parent].height + 5;
    }
    return offset;
}

function all_finished(events, key) {

    var current = events[key];
    var result = false;

    if( current.function == 'multisystem_dt_operation') {
        result = true;
        for( child of current['children']) {
            result = all_finished(events, child) && result;
        }
    }
    else if (current.operation == 'Propagation step finished') {
        result = true;
    }
    return result;
}

function contains_highlight(events, key, highlight) {

    var current = events[key];
    var result = false;

    if(current.step == highlight) {
        result = true;
    } else {
        for (child of current.children) {
            result = result || contains_highlight(events, child, highlight);
        }
    }

    return result;
}

function step_is_ghost(events, key, prefix) {

    var current = events[key];
    var result;

    if( current.isGhost ) {
        result = true;
    } else {
        result = false;
        for(parent of current.parent) {
            result = result || step_is_ghost(events, parent, prefix+' ');
        }
    }

    return result;
}

function step_is_visible(data, key) {


        if( !Object.keys(data.events).includes(key) ) return false;

        var current = data.events[key];

        var result = data.viewContainers || !data.systems[current.system].isContainer;
        result = result && (data.viewGhosts || !step_is_ghost(data.events, key,''));

        return result;
}

function run(data, my_global_tag, viewGhosts, viewContainers, initial_step) {

    var systems = data.systems;
    var events = data.events;

    data.constants = Object();

    data.constants.minHeight  = 30;
    data.constants.minWidth = 50;
    data.constants.stepHeight = 40; // we still might want to make this more flexible, depending on the content of the steps.
    data.constants.stepMargin = 10;
    data.constants.systemMargin = 10;
    data.constants.systemHeight = 30;
    data.constants.systemMinWidth = 200;
    data.constants.interactionWidth = 50; // this should be determined from the text width

    data.viewContainers = viewContainers;
    data.viewGhosts = viewGhosts;

    data.highlight = initial_step;

    console.log('run with '+my_global_tag)

    var top_div = d3.select("div#"+my_global_tag)

    var header_div = top_div.append('div').attr('id','header'+my_global_tag).attr('class','headcontainer').style('position','relative')
    var main_div = top_div.append('div').attr('id','main'+my_global_tag).attr('class','stepcontainer')


    header_div
    .append('svg')
    .attr('id','head'+my_global_tag)
    .attr('width','1500px')

    var display = header_div.append('div').attr('class','display')

    main_div
    .append('svg')
    .attr('id','main'+my_global_tag)
    .attr('width','1500px')

    data.top_div = main_div;

    // declare the arrow heads:
    var svg = main_div.select("svg#main"+my_global_tag);
    var svg_head = top_div.select("svg#head"+my_global_tag);

    var arrowPoints = [[0, 0], [0, 10], [10, 5]];

    svg
    .append('defs')
    .append('marker')
    .attr('id', 'arrow')
    .attr('viewBox', [0, 0, 10,10])
    .attr('refX', 10)
    .attr('refY', 5)
    .attr('markerWidth', 10)
    .attr('markerHeight', 10)
    .attr('orient', 'auto-start-reverse')
    .append('path')
    .attr('d', d3.line()(arrowPoints))
    .attr('stroke', 'black');


    var control = svg_head.append('g').attr('id','control').attr('transform','translate(30,0)');
    var header = svg_head.append('g').attr('id','header').attr('transform','translate( 50, 0 )');
    var main   = svg.append('g').attr('id','main').attr('transform','translate( 50, 100 )');
    var footer = svg.append('g').attr('id','footer').attr('transform','translate( 50, 0 )');

    var tooltip = main_div.append('div').attr('id','tooltip').attr('class','tooltip')


    control.append('rect')
    .attr('x',0)
    .attr('y',5)
    .attr('rx',10)
    .attr('ry',10)
    .attr('width',180)
    .attr('height',40)
    .attr('id','ContainerSwitch')
    .attr('fill','lightBlue')
    .attr('stroke','black')
    .on("click", function(event) { toggle_container_view(data); })

    control.append('text').attr('id','ContainerSwitch')
    .text(data.viewContainers?'Hide Containers':'Show Containers')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',30)
    .on("click", function(event) { toggle_container_view(data); })

    control.append('rect')
    .attr('x',0)
    .attr('y',55)
    .attr('rx',10)
    .attr('ry',10)
    .attr('width',180)
    .attr('height',40)
    .attr('id','GhostSwitch')
    .attr('fill','lightBlue')
    .attr('stroke','black')
    .on("click", function(event) { toggle_ghost_view(data); })

    control.append('text').attr('id','GhostSwitch')
    .text(data.viewGhosts?'Hide Ghosts':'Show Ghosts')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',80)
    .on("click", function(event) { toggle_ghost_view(data); })

    control.append('rect')
    .attr('x',0)
    .attr('y',105)
    .attr('rx',10)
    .attr('ry',10)
    .attr('width',180)
    .attr('height',40)
    .attr('id','ExpandAllSwitch')
    .attr('fill','lightBlue')
    .attr('stroke','black')
    .on("click", function(event) { expand_all(data); })

    control.append('text').attr('id','ExpandAllSwitch')
    .text('Expand All')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',130)
    .on("click", function(event) { expand_all(data); })


    control.append('rect')
    .attr('x',0)
    .attr('y',155)
    .attr('rx',10)
    .attr('ry',10)
    .attr('width',180)
    .attr('height',40)
    .attr('id','CollapseAllSwitch')
    .attr('fill','lightBlue')
    .attr('stroke','black')
    .on("click", function(event) { collapse_all(data); })

    control.append('text').attr('id','CollapseAllSwitch')
    .text('Collapse All')
    .attr('class','sequence-normal')
    .attr('x',15)
    .attr('y',180)
    .on("click", function(event) { collapse_all(data); })



    control.append('rect')
    .attr('x',200)
    .attr('y',5)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',40)
    .attr('height',40)
    .attr('id','ForwardSwitch')
    .attr('fill','lightBlue')
    .attr('stroke','black')
    .on("click", function(event) { forward(data); })

    control.append('text').attr('id','ForwardSwitch')
    .text('+')
    .attr('class','sequence-normal')
    .attr('x',215)
    .attr('y',30)
    .on("click", function(event) { forward(data); })

    control.append('rect')
    .attr('x',200)
    .attr('y',55)
    .attr('rx',20)
    .attr('ry',20)
    .attr('width',40)
    .attr('height',40)
    .attr('id','BackwardSwitch')
    .attr('fill','lightBlue')
    .attr('stroke','black')
    .on("click", function(event) { backward(data); })

    control.append('text').attr('id','BackwardSwitch')
    .text('-')
    .attr('class','sequence-normal')
    .attr('x',215)
    .attr('y',80)
    .on("click", function(event) { backward(data); })


/*
    control.append('text').attr('id','StepDisplay')
    .attr('class','sequence-normal')
    .text('bla')
    .attr('x',300)
    .attr('y',15)
*/

    // prepare global time arrow:

    svg.append('line')
    .attr('x1',10)
    .attr('y1',50)
    .attr('x2',10)
    .attr('y2',50)
    .attr('id','global_time_line')
    .attr('marker-end', 'url(#arrow)')
    .attr('stroke', 'black');


    console.log("running main script.");

    prepare_systems(systems, data.constants);

    var top_events = [];

    data.index = [];
    prepare_events(events, data.constants, top_events, data.index);

    create_step_groups(events, top_events, main);

    data.top_events = top_events;


    // collect svg areas in data.svg:

    data.svg = Object();
    data.svg.all = svg;
    data.svg.head = svg_head;
    data.svg.control = control;
    data.svg.header = header;
    data.svg.main = main;
    data.svg.footer = footer;
    data.tooltip = tooltip;
    data.display = display;

    update(data);

}

function collect_operations(events, start, operations) {

    var current = events[start];

    if( current.function == 'dt_operation' ) {
        operations.push( {system:current.system, op:current.operation.substring(17).replace('- ',''), step:current.step});
    } else {
        for( child of current.children ) collect_operations(events, child, operations);
    }
}

function drawInteraction(systems, key, constants, header, viewGhosts) {

    system_ = systems[key];
    var tag = tagify(system_.id);

    // console.log('draw_interaction: tag: '+tag+' status = '+system_.status);

    var filtered_interactions = [];

    if (system_.status == 'expand') {
        for(interaction of system_.interactions) {
            if( viewGhosts || interaction['type']!='ghost') {
                filtered_interactions.push(interaction)
            }
        }
    }

    header.selectAll('rect#'+tag).remove();
    header.selectAll('text#'+tag+'_1').remove();
    header.selectAll('text#'+tag+'_2').remove();


    var interactions      = header.selectAll('rect#'+tag).data(filtered_interactions);
    var interactions_txt1 = header.selectAll('text#'+tag+'_1').data(filtered_interactions);
    var interactions_txt2 = header.selectAll('text#'+tag+'_2').data(filtered_interactions);


    interactions.enter().append('rect')
    .attr('id',tag)
    .attr('x', s => s.x )
    .attr('y', s => s.y)
    .attr('rx',2)
    .attr('ry',2)
    .attr('width', s=>s.width)
    .attr('height', 40)
    .attr('stroke', 'black')
    .attr('fill', 'rgba(0,255,0,0.2)')

    interactions
    .attr('x', s => s.x )
    .attr('y', s => s.y)
    .attr('width', s=>s.width)


    if(system_.status == 'expand' || system_.status == 'collapse' ) {

//        console.log('drawing interaction for '+key)

        interactions_txt1.enter().append('text')
        .text( function(d) {return d.type})
        .attr('class','sequence-interactions')
        .attr('id',tag+'_1')
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 10)
        .attr('stroke','black')

        interactions_txt1
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 10)

        interactions_txt1.exit().remove()

        interactions_txt2.enter().append('text')
        .text( function(d) {tmp = d.partner.split('.'); return tmp[tmp.length-1]})
        .attr('class','sequence-interactions')
        .attr('id',tag+'_2')
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 25)
        .attr('stroke','black')

        interactions_txt2
        .attr('x', s => s.x + 5)
        .attr('y', s => s.y + 25)

        interactions_txt2.exit().remove()

    } else {
            // header.selectAll('rect#'+tag).remove()
        header.selectAll('text#'+tag+'_1').remove()
        header.selectAll('text#'+tag+'_2').remove()
    }
}

function clock_string(current, clock_name) {

    var clock = current[clock_name+'_clock'];
    var display = '';

    if(clock.length>0) {
        if( clock[1] > clock[0] ) {
            display += '<tr><th>'+clock_name+':</th> <th>'+clock[0]+'</th>  <th>'+clock[1]+"</th></tr>"
        } else {
            display += '<tr><th>'+clock_name+':</th> <td>'+clock[0]+'</td>  <td>'+clock[1]+"</td></tr>"
        }
    }
    return display;
}

function tooltip_string(current) {

    var display_string = current.step + ': <b><i>'+current.name+"</i></b><br/>"+"<br/>";

    display_string += "<b>System:</b> "+current.system+"</br>";
    display_string += "<table class='display'>"
    display_string += "<tr><th>Clocks:</th><td>In</td><td>Out</td></tr>";
    display_string += clock_string(current, 'system');
    display_string += clock_string(current, 'prop');
    display_string += clock_string(current, 'interaction');
    display_string += clock_string(current, 'partner');
    display_string += clock_string(current, 'requested');
    display_string += "</table>"
    if( typeof(current.updated)!=='undefined' ) display_string += "<b>updated:</b>  "+current.updated+"</br>";

    return display_string;
}

function drawStep(data, key, group, step_y, x_label, prefix) {

    var systems = data.systems;
    var events = data.events;
    var constants = data.constants;
    var viewGhosts = data.viewGhosts;

    var current = events[key];
    var step_id = '#'+current.id;
    var system = systems[current.system];

    var x=current.x;
    var y=0;
    var height = event_get_height(events, key, constants, viewGhosts) ;
    var width=current.width;
    var delta_y = 0; //constants.minHeight;


    // console.log(prefix+'drawStep: ',key, current.function, current.system );

    var step_group = group.selectAll(step_id)
    step_group.attr('transform','translate(0,'+step_y+')');
    step_group.selectAll('rect').remove()
    step_group.selectAll('text').remove()

//    console.log('START draw ',key, height, step_y, current.status);

    switch(current.status) {

        case 'expand':
            delta_y = constants.minHeight;
            step_y += height + constants.stepMargin;
            step_group.insert('rect','g')
            .attr('id',current.id)
            .attr('x', x)
            .attr('y', y)
            .attr('width', width)
            .attr('height', height)
            .attr('fill',current.color)
            .attr('stroke','black')
            .attr('stroke-width', current.step==data.highlight?6:1)
            .on('click', function(event) { clickStep(event, current, data); } )
            .on("mouseover", function(event) {
                var display_string = tooltip_string(current)
                data.tooltip.transition().duration(500)
                    .style("opacity",1.0);
                data.tooltip
                    .html(display_string + "<br/>"  )
                    .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")
                    .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");
                })
            .on("mousemove", function(event) {
                data.tooltip
                .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")
                .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");
            })
            .on("mouseout", function(event) {
                data.tooltip.transition().duration(500)
                    .style("opacity", 0);
            })
//            .on('mouseover', function(event) { data.svg.control.selectAll('text#StepDisplay').text(current.name); })

            step_group.insert('text','g')
            .text(current.name)
            .attr('class','sequence-normal')
            .attr('id',current.id)
            .attr('x', x_label)
            .attr('y', 15)
            .attr('fill',current.textcolor)
            .attr('font-weight',current.step==data.highlight?'bold':'normal')
            current.children.forEach( function(child) { delta_y = drawStep(data, child, group, delta_y, x_label, prefix+'  ' )} )
            if(current.parent.length>0 && events[current.parent].function!='multisystem_dt_operation') {
                var par_x = events[current.parent].x
                var stroke_width = current.updated=="true"?"2px":"1px"

                if(par_x > x) {

                    step_group.append('rect','g')
                    .attr('id',current.id)
                    .attr('x', x + current.width )
                    .attr('y', y)
                    .attr('width', par_x - x - events[current.parent].width)
                    .attr('height', height )
                    .attr('fill','none')
                    .attr('stroke','black')
                    .attr('stroke-width',stroke_width)

                } else {

                    step_group.append('rect','g')
                    .attr('id',current.id)
                    .attr('x', par_x + events[current.parent].width )
                    .attr('y', y)
                    .attr('width', x - par_x - current.width)
                    .attr('height', height)
                    .attr('fill','none')
                    .attr('stroke','black')
                    .attr('stroke-width',stroke_width)

                }

            }

            if(current.operation == 'Propagation step finished') {
                console.log(current.system_clock);

            }
        break;

        case 'collapse':
            step_y += constants.minHeight+10;
            step_group.insert('rect','g')
            .attr('id',current.id)
            .attr('x', x)
            .attr('y', 0)
            .attr('width', width)
            .attr('height', constants.minHeight+10)
            .attr('fill',current.color)
            .attr('stroke','black')
            .attr('stroke-width', current.step==data.highlight?4:1)
            .on('click', function(event) { clickStep(event, current, data); } )
            .on("mouseover", function(event) {
                var display_string = tooltip_string(current); display_string += "<b>updated:</b>  "+current.updated+"</br>";
                data.tooltip.transition().duration(500)
                    .style("opacity",1.0);
                data.tooltip
                    .html(display_string + "<br/>"  )
                    .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")
                    .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");
                    })
            .on("mousemove", function(event) {
                data.tooltip
                .style("left", (d3.pointer(event, data.svg.main)[0] + 10) + "px")
                .style("top",  (d3.pointer(event, data.svg.main)[1] - 28) + "px");
            })
            .on("mouseout", function(event) {
                data.tooltip.transition().duration(500)
                    .style("opacity", 0);
            })

//            step_group.insert('text','g')
//            .text(current.id+' '+current.name)
//            .attr('x', x_label)
//            .attr('y', 15)
//            .attr('fill',current.textcolor)
            ops = [];
            collect_operations(events, key, ops);
            // console.log(ops);
            for(operation of ops) {
                step_group.insert('text','g')
                .attr('id',current.id)
                .attr('class','sequence-normal')
                .text(operation.op)
                .attr('x', systems[operation.system].x)
                .attr('y', y+20)
                .attr('font-weight',operation.step==data.highlight?'bold':'normal')
                .attr('fill',operation.textcolor)

            }
        break;

        case 'hide':
            step_y += height;
            current.children.forEach( function(child) { delta_y = drawStep(data, child, group, delta_y, x_label, prefix+'  ' )} )
        break;

    }

//    console.log('STOP  draw ',key, height, step_y);

    return step_y;

}

function process_system_geometry(systems, key, offset_x, offset_y, constants, viewContainers, viewGhosts) {

    // console.log('processing ',key,' with', offset_x, offset_y)

    var current = systems[key]

    var result = Object();
    var delta_y = 0;
    var max_x = 0;
    var max_y = 0;
    var new_max_y = 0;

    current.x = offset_x;
    current.y = offset_y;
    current.height = constants.systemHeight;

    var offset = offset_x

    if( viewContainers || ! current.isContainer ) {
        offset += constants.interactionWidth + 5;
        for (interaction of current.interactions) {
            if (!interaction.isGhost || viewGhosts ) {
                interaction.x = offset;
                offset += interaction.width+5;
            }
        }
        delta_y = current.height+constants.stepMargin;
        offset = Math.max(offset, offset_x+constants.systemMinWidth);
    } else {
        delta_y = 0;
    }


    for (child of current.children) {
        tmp_result = process_system_geometry(systems, child, offset, offset_y+delta_y, constants, viewContainers, viewGhosts);
        offset += tmp_result.width + 10;
        max_x = Math.max(offset, tmp_result.max_x);
        new_max_y = Math.max(max_y, tmp_result.max_y );
    }


    if( !viewContainers && current.isContainer ) {

        result.width = 0
        result.max_x = max_x;
        result.max_y = new_max_y;
        current.x = offset_x;
        current.y = offset_y;
        current.height = constants.systemHeight;
        current.width = offset - offset_x;

    } else {

        current.width = offset - offset_x;
        result.width  = offset - offset_x;
        result.max_x = max_x;
        result.max_y = new_max_y + current.height + constants.stepMargin;

    }

    return result;
}

function update(data) {

    // update offsets:

    var events     = data.events;
    var systems    = data.systems;
    var constants  = data.constants;
    var top_events = data.top_events;

    var svg    = data.svg.all;
    var svg_head = data.svg.head;
    var header = data.svg.header;
    var main   = data.svg.main;
    var footer = data.svg.footer;

    var x = 0;
    var y = 0;


    // start with 'root':

    var geom = process_system_geometry(systems, 'root', 0, 5, constants, data.viewContainers, data.viewGhosts);

    var max_y = geom.max_y + 20;
    var max_x = geom.max_x + 100;

    var active_systems = [];
    var active_events = [];

    Object.keys(systems).forEach( function(key) {
        d = systems[key];
        d.interactions.forEach( function(i) {
            i.y = max_y;
        });

        if ( data.viewContainers || !d.isContainer ) {
            active_systems.push(d);
            d.status = 'expand';
        } else {
            d.status = 'hide';
        }
        if (d.show == false) d.status='hide'
    });


    main.attr('transform','translate(50,0)')

    y = max_y + 20;


    Object.keys(events).forEach( function(d) {

        var current = events[d];
        var system  = systems[current['system']];
        var isMulti = current.function=="multisystem_dt_operation";

        current.status = 'expand';

        if (current.isGhost && !data.viewGhosts) current.status = 'hide';
        if (systems[current.system].isContainer && !data.viewContainers) current.status = 'hide';

        if (current.function == "system_update_exposed_quantities") {
            var parent = events[current.parent[0]];
            if( parent.isGhost && !data.viewGhosts) current.status = 'hide';
            if( systems[parent.system].isContainer && !data.viewContainers) current.status = 'hide';
        }


        if ( current.parent.length==0 && isMulti) y+=20;

        current.y = y;

        current.height = event_get_height(events, d, constants, data.viewGhosts);
        if (current.isInteraction) {
            current.x = system[current.target].x;
        } else {
            current.x = system.x;
        }
        if( isMulti ) {
            current.x -= 5;
            current.height += 5;
        }

        current.width = isMulti?system.width+10:constants.minWidth;

        if( (data.viewContainers || !system.isContainer) && (data.viewGhosts || !current.isGhost) ) {
            active_events.push(current);
            y+=constants.minHeight+5;
        }
        if (current.show == false && current.canCollapse && !contains_highlight(events, d, data.highlight) && current.status!='hide') current.status='collapse'

    });


    var system_groups_header = header.selectAll('g.system_t').data(active_systems);

    system_groups_header.enter()
      .append('g')
        .attr('class','system_t')
        .attr('id',function(d) {return d.id;})

    var system_groups_footer = footer.selectAll('g.system_b').data(active_systems);

    system_groups_footer.enter()
      .append('g')
        .attr('class','system_b')
        .attr('id',function(d) {return d.id;})
        .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
    system_groups_footer
        .attr("transform",function(d) {return 'translate(0, '+d.y+')';})


    var sysline1 = header.selectAll('g.system_t').selectAll('rect').data(active_systems);
    var sysline1_txt = header.selectAll('g.system_t').selectAll('text').data(active_systems);

    sysline1.enter().append('rect')
        .attr('x', d => d.x )
        .attr('y', d => d.y )
        .attr('rx',5)
        .attr('ry',5)
        .attr('width', d=>d.width)
        .attr('height', d => d.height)
        .attr('stroke', 'black')
        .attr('fill', 'none')
//           .on("click", function(event, d) { console.log(event); clickSystem(event, systems[d], data); })

    sysline1
        .attr('x', d => d.x )
        .attr('y', d => d.y )
        .attr('width', d=>d.width)

    sysline1.exit().remove()


    sysline1_txt.enter().append('text')
        .text( d => d.name )
        .attr('x', d => 20 + d.x )
        .attr('y', d => 20 + d.y )
        .attr('stroke', 'black')
        .attr('visibility', d => (d.status!='hide'?'visible':'hidden'))
//            .on("click", function(event, d) { console.log(event); clickSystem(event, systems[d], data); })


    sysline1_txt
    .text( d => d.name )
    .attr('x', d => 20 + d.x )
    .attr('y', d => 20 + d.y )
    .attr('visibility', d => (d.status!='hide'?'visible':'hidden'))

    sysline1_txt.exit().remove()

    Object.keys(systems).forEach( function(key) {drawInteraction(systems, key, constants, header, data.viewGhosts)} )




    // var step_groups = main.selectAll('g.step').data(top_events);

    main.selectAll('line#finished').remove()

    var step_y = 0
    top_events.forEach( function(key) {
        step_y = 10 + drawStep(data, key, main, step_y, max_x, '' );
        if (all_finished(events, key )) {
            main.append('line')
            .attr('id','finished')
            .attr('x1',-10)
            .attr('y1',step_y+5)
            .attr('x2',max_x+400)
            .attr('y2',step_y+5)
            .attr('stroke','black')
            .attr('stroke-width',2)
            step_y += 20;
        }
    });


    data.display
    .html(tooltip_string(events[data.index[data.highlight]]) + "<br/>"  )


    data.svg.control.attr('transform','translate('+max_x+',5)');
    svg.select('#global_time_line')
       .attr('y2',180+step_y)

    svg.attr('viewBox','0 0 '+(max_x+500)+' '+ (Math.max(step_y+200, 400))+'')
    svg_head.attr('viewBox','0 0 '+(max_x+500)+' '+ (Math.max(250,max_y+60)) )


}


function clickStep(event, d, data){
//    console.log('click Step', d.id, d.name, event);
    d.show = !d.show;
    event.stopPropagation();
    update(data)
}

function clickSystem(event, d, data) {
//    console.log('click System', d.name);
    d.show = !d.show;
    d.width = d.show?(minWidth + d.interactions.length * interactionWidth + 2*systemMargin):minWidth;
    update()
}


function toggle_container_view(data) {

    data.viewContainers = !data.viewContainers; // move that into data.systems?
    data.svg.control.selectAll('text#ContainerSwitch').text(data.viewContainers?'Hide Containers':'Show Containers')
    update(data);

}

function toggle_ghost_view(data) {

    data.viewGhosts = !data.viewGhosts;
    data.svg.control.selectAll('text#GhostSwitch').text(data.viewGhosts?'Hide Ghosts':'Show Ghosts')
    update(data);

}

function forward(data) {
    do {
        data.highlight += 1;
        if (data.highlight > data.constants.max_steps) data.highlight = 0;
    } while(!step_is_visible(data, data.index[data.highlight]))
    update(data)
}

function backward(data) {
    do {
        data.highlight -= 1;
        if(data.highlight < 0) data.highlight = data.constants.max_steps;
    }
    while(!step_is_visible(data, data.index[data.highlight]))
    update(data)
}

function entry(filename, global_tag, viewGhosts, viewContainers, initial_step) {
        console.log('trying to load: '+filename);
        console.log(viewGhosts, viewContainers)
        d3.json(filename).then( function(d) {run(d, global_tag, viewGhosts, viewContainers, initial_step)} );
}

function set_display(data, key) {
    data.dislplay = key;
    update(data)
}

entry(filename, global_tag, viewGhosts, viewContainers, initial_step)

