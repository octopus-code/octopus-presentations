<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Basic input options - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Basic_input_options","wgTitle":"Tutorial:Basic input options","wgCurRevisionId":11461,"wgRevisionId":11461,"wgArticleId":1322,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Basic","Ground State","Molecule","Pseudopotentials","DFT","Total Energy"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Basic_input_options","wgRelevantArticleId":1322,"wgRequestId":"eae02694b8c841d8ed7edcb1","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Basic_input_options rootpage-Tutorial_Basic_input_options skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Basic input options</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Now we will move to a more complicated (and realistic) input file. We will obtain the ground state of the nitrogen atom. We will introduce several basic input variables and will give a more detailed description of the output for this example.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#The_input_files"><span class="tocnumber">1</span> <span class="toctext">The input files</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Output"><span class="tocnumber">2</span> <span class="toctext">Output</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Species"><span class="tocnumber">2.1</span> <span class="toctext">Species</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Grid"><span class="tocnumber">2.2</span> <span class="toctext">Grid</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Mixing"><span class="tocnumber">2.3</span> <span class="toctext">Mixing</span></a></li>
<li class="toclevel-2 tocsection-6"><a href="#Eigensolver"><span class="tocnumber">2.4</span> <span class="toctext">Eigensolver</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#LCAO"><span class="tocnumber">2.5</span> <span class="toctext">LCAO</span></a></li>
<li class="toclevel-2 tocsection-8"><a href="#Wavefunction_kind"><span class="tocnumber">2.6</span> <span class="toctext">Wavefunction kind</span></a></li>
<li class="toclevel-2 tocsection-9"><a href="#SCF"><span class="tocnumber">2.7</span> <span class="toctext">SCF</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-10"><a href="#Restarting"><span class="tocnumber">3</span> <span class="toctext">Restarting</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="The_input_files">The input files</span></h2>
<p>This sample input file lets us obtain the ground state of the nitrogen atom, within the LDA approximation, in a closed-shell (unpolarized) configuration (as explained below, you need an auxiliary <b><tt>.xyz</tt></b> input). Note that this is not the correct ground state of the nitrogen atom! However, it will permit us to describe some of the most important input variables:
</p>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = gs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsOutput"><code>UnitsOutput</code></a></span> = eV_Angstrom

Nitrogen_mass = 14.0

%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Species"><code>Species</code></a></span>
 'N' | species_pseudo | set | standard | lmax | 1 | lloc | 0 | mass | Nitrogen_mass
%

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span> = 'N.xyz'

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> = 1
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=Occupations"><code>Occupations</code></a></span>
  2 | 1 | 1 | 1
%

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=BoxShape"><code>BoxShape</code></a></span> = sphere
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 5.0*angstrom
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.18*angstrom
</pre>
<p>We have introduced here several new variables:
</p>
<ul><li><tt><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsOutput"><code>UnitsOutput</code></a></span> = eV_Angstrom</tt>: Two different unit systems may be used for output: the usual atomic units (which is the default, and the ones used internally in the code); and the system in which the Ångström is substituted for the atomic unit of length, and the electronvolt is substituted for the atomic unit of energy. You can find a more detailed description of units in Octopus in the <a href="/wiki/Manual:Units" title="Manual:Units">Units</a> page of the <a href="/wiki/Manual" title="Manual">manual</a>.</li></ul>
<ul><li>The following entry in the input file is not a variable that Octopus will read directly, but rather illustrates the possibility of writing "user-defined" values and expressions to simplify the input file. In this case, we define the nitrogen  mass (<tt>Nitrogen_mass = 14.0</tt>) (note that in this case, as an exception, the value is expected to be in the so-called "atomic mass units", rather than in "atomic units"). This definition may be used elsewhere in the input file.</li></ul>
<ul><li>The <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Species"><code>Species</code></a></span> block should contain the list of species that are present in the system to be studied. In this case we have only one species: nitrogen. The first field is a string that defines the name of the species, "N" in this case. The second field defines the type of species, in this case  <tt> species_pseudo </tt>. Then a list of parameters follows. The parameters are specified by a first field with the parameter name and the field that follows with the value of the parameter. Some parameters are specific to a certain species while others are accepted by all species. In our example <tt> set </tt> instructs Octopus to use a pseudopotential for nitrogen from the <tt> standard </tt> set. This happens to be a Troullier-Martins pseudopotential defined in the <b><tt>N.psf</tt></b> file found in the directory <b><tt>share/octopus/pseudopotentials/PSF</tt></b>. Then come maximum <tt> lmax </tt> - component of the pseudopotential to consider in the calculation, and the <tt> lloc </tt> - component to consider as local. Generally, you want to set the maximum <i>l</i> to the highest available in the pseudopotential and the local <i>l</i> equal to the maximum <i>l</i>. Finally, the mass of the species can also be modified from the default values by setting <tt> mass </tt> parameter.</li></ul>
<ul><li><tt><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span> = 'N.xyz'</tt>: The geometry of the molecule (in this case, a single atom in the grid origin) is described in this case in a file with the well known <tt>XYZ</tt> format. The file for this outrageously simple case is given by:</li></ul>
<pre>1
This is a comment line
N 0 0 0
</pre>
<ul><li><tt><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> = 1</tt>: By default, Octopus performs spin-unpolarized calculations (restricted closed-shell, in Hartree-Fock terminology). It then places two electrons in each orbital. The number of orbitals, or Kohn-Sham states, is then calculated by counting the number of valence electrons present in the system, and dividing by two. In this case, since we have five valence electrons, the code would use three orbitals. However, we know beforehand that the HOMO orbital has a three-fold degeneracy, and as a consequence we need to put each one of the three <i>p</i> electrons in a different orbital. We therefore need one more orbital, which we get with this line in the input file.</li></ul>
<ul><li><tt>%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=Occupations"><code>Occupations</code></a></span></tt> block: Generally, the occupations of the Kohn-Sham orbitals are automatically decided by the code, filling the lowest-energy orbitals. However, if we have degeneracies in the LUMO as in this case, the user may want to accommodate the electrons in a certain predefined way. In this example, the obvious way to fill the orbitals of the nitrogen atom is to put two electrons in the first and deepest orbital (the <i>s</i> orbital), and then one electron on each of the second, third and fourth orbitals (the <i>p</i> orbitals, which should be degenerate).</li></ul>
<ul><li><tt><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Grid&amp;name=BoxShape"><code>BoxShape</code></a></span> = sphere</tt>: This is the choice of the shape of the simulation box, which in this case is set to be a sphere (other possible choices are <tt>minimum</tt>, <tt>cylinder</tt>, or <tt>parallelepiped</tt>).</li></ul>
<ul><li><tt><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 5.0*angstrom</tt>: The radius of the sphere that defines the simulation box.</li></ul>
<ul><li><tt><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.18*angstrom</tt>: As you should know, Octopus works in a real-space regular cubic mesh. This variable defines the spacing between points, a key numerical parameter, in some ways equivalent to the energy cutoff in plane-wave calculations.</li></ul>
<h2><span class="mw-headline" id="Output">Output</span></h2>
<p>Once you have constructed the input file and created the <b><tt>N.xyz</tt></b> file, you may unleash Octopus on it. Lets now go over some of the sections of the output.
</p>
<h4><span class="mw-headline" id="Species">Species</span></h4>
<pre>****************************** Species *******************************
  Species 'N'
    type             : pseudopotential
    file             : '/opt/share/octopus/pseudopotentials/PSF/N.psf'
    file format      : PSF
    valence charge   : 5.0
    atomic number    :   7
    form on file     : semilocal
    orbital origin   : calculated
    lmax             : 1
    llocal           : 0
    projectors per l&#160;: 1
    total projectors&#160;: 1
    application form&#160;: kleinman-bylander
    orbitals         : 16
    bound orbitals   : 16

**********************************************************************
</pre>
<p>Here the code searches for the needed pseudopotential files, and informs the user about its success or failure. In this case, only the <b><tt>N.psf</tt></b> file is required. Once that file has been processed, some information about it is written to the output. One of the most important pieces of information to be found here is the valence charge, which tells us how many electrons from this species will be considered in the calculation.
</p>
<h4><span class="mw-headline" id="Grid">Grid</span></h4>
<pre>******************************** Grid ********************************
Simulation Box:
  Type = sphere
  Radius  [A] =   5.000
Main mesh:
  Spacing [A] = ( 0.180, 0.180, 0.180)    volume/point [A^3] =      0.00583
  # inner mesh =      89727
  # total mesh =     127183
  Grid Cutoff [eV] =  1160.586810    Grid Cutoff [Ry] =    85.301565
**********************************************************************
</pre>
<p>This step is about the construction of the mesh. As requested in the input file, a sphere of radius 5 Å is used, which contains a cubic regular real-space grid with spacing 0.18 Å. This implies 89727 points (<tt>inner mesh =  89727</tt>). For the sake of comparison with plane-wave-based codes, this is more or less equivalent to a plane-wave calculation that imposes a density cutoff of 1160.595 eV = 42.6 Hartree (except that in this case there is no artificial periodic repetition of the system).
</p>
<h4><span class="mw-headline" id="Mixing">Mixing</span></h4>
<pre>Input: [MixField = potential] (what to mix during SCF cycles)
Input: [MixingScheme = broyden]
Info: Mixing uses    4 steps and restarts after   20 steps.
</pre>
<p>During the self-consistent procedure one has to use a <a href="/wiki/Manual:Ground_State#Mixing" title="Manual:Ground State">mixing scheme</a> to help convergence. One can mix either the density or the potential, and there are several mixing schemes available.
</p>
<h4><span class="mw-headline" id="Eigensolver">Eigensolver</span></h4>
<pre>**************************** Eigensolver *****************************
Input: [Eigensolver = cg]
Input: [Preconditioner = pre_filter]
Info: Generating weights for finite-difference discretization of Preconditioner
Input: [PreconditionerFilterFactor = 0.5000]
Input: [SubspaceDiagonalization = standard]
**********************************************************************
</pre>
<p>Here we see that the <a href="/wiki/Manual:Ground_State#Eigensolver" title="Manual:Ground State">eigensolver</a> used will be simple conjugate gradients (cg), and a preconditioner is used to speed up its convergence.
</p>
<h4><span class="mw-headline" id="LCAO">LCAO</span></h4>
<p>After some output you should see something like:
</p>
<pre>Info: Performing initial LCAO calculation with      4 orbitals.
Info: Getting Hamiltonian matrix elements.
ETA: .......1......2.......3......4......5.......6......7.......8......9......0

Eigenvalues [eV]
 #st  Spin   Eigenvalue      Occupation
   1   --   -17.398866       2.000000
   2   --    -6.414013       1.000000
   3   --    -6.414013       1.000000
   4   --    -6.414013       1.000000
</pre>
<p>This is the first step of a ground-state calculation: obtaining a reasonably good starting density and Kohn-Sham orbitals to feed in the self-consistent (SCF) procedure. For this purpose, Octopus performs an initial calculation restricted to the basis set of atomic orbitals (<a href="/wiki/Manual:Ground_State#LCAO" title="Manual:Ground State">Linear Combination of Atomic Orbitals</a>, LCAO). The resulting eigenvalues of this calculation are written to standard output.
</p>
<h4><span class="mw-headline" id="Wavefunction_kind">Wavefunction kind</span></h4>
<pre>Info: SCF using real wavefunctions.
</pre>
<p>Very often one can work with real wave-functions. This is particularly helpful as calculations with real wave-functions are much faster than with complex ones. However, if a magnetic field is present, if the system is periodic, or if spin-orbit coupling is present, complex wave-functions are mandatory. But don't worry: the program is able to figure out by itself what to use.
</p>
<h4><span class="mw-headline" id="SCF">SCF</span></h4>
<pre>*********************** SCF CYCLE ITER #    1 ************************
 etot  = -2.54719191E+02 abs_ev   =  2.08E-01 rel_ev   =  3.84E-03
 ediff =        9.36E+00 abs_dens =  2.86E-01 rel_dens =  5.72E-02
Matrix vector products:     28
Converged eigenvectors:      0

#  State  Eigenvalue [eV]  Occupation    Error
      1      -17.420335    2.000000   (1.1E-02)
      2       -6.469134    1.000000   (2.1E-02)
      3       -6.469134    1.000000   (2.1E-02)
      4       -6.469134    1.000000   (2.1E-02)

Density of states:

---------------------------------------------------------------------%
---------------------------------------------------------------------%
---------------------------------------------------------------------%
---------------------------------------------------------------------%
---------------------------------------------------------------------%
---------------------------------------------------------------------%
---------------------------------------------------------------------%
%--------------------------------------------------------------------%
%--------------------------------------------------------------------%
%--------------------------------------------------------------------%
                                                                     ^


Elapsed time for SCF step     1:          0.18
**********************************************************************
</pre>
<p>Now the SCF cycle starts. For every step, Octopus outputs several pieces of information:
</p>
<ul><li>The values <tt>abs_dens</tt> and <tt>rel_dens</tt> are to monitor the absolute and relative convergence of the density, while <tt>rel_ev</tt> and <tt>abs_ev</tt> are two alternative measures of the convergence, based on measuring the difference between input and output eigenvalues. The SCF procedure, by default, is stopped when <tt>rel_dens</tt> is smaller than <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/70b5788b8e24bbc50c5c6fc7b5ef4953cb2291e0" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:4.658ex; height:2.676ex;" alt="{\displaystyle 10^{-6}}" />. This may be altered with the appropriate input variables (see in the manual the variables <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=SCF&amp;name=ConvAbsDens"><code>ConvAbsDens</code></a></span>, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=SCF&amp;name=ConvRelDens"><code>ConvRelDens</code></a></span>, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=SCF&amp;name=ConvAbsEv"><code>ConvAbsEv</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=SCF&amp;name=ConvRelEv"><code>ConvRelEv</code></a></span>).</li></ul>
<ul><li>The line <tt>Matrix vector products:    28</tt> tells us that the Hamiltonian was applied 28 times. This gives us an idea of the computational cost.</li></ul>
<ul><li>The line <tt>Converged eigenvectors:      0</tt> tells us that upon completion of the diagonalization procedure, none of the orbitals met the required precision criterion for the wavefunctions. In a following example, we will modify this criterion in the input file.</li></ul>
<ul><li>The list of eigenvalues is then printed, along with their errors: how much they deviate from "exact" eigenvalues of the current Hamiltonian. This number is the so-called "residue".</li></ul>
<p>You can now take a look at the file <b><tt>static/info</tt></b> that will hold a summary of the calculation.
</p>
<h2><span class="mw-headline" id="Restarting">Restarting</span></h2>
<p>Any ground-state calculation may be restarted later (to refine it if it did not converge properly, or with any other purpose), provided that the contents of the <tt>restart</tt> directory are preserved. You can try this now, just by running Octopus again. You will notice that Octopus did not give any warning after the line
</p>
<pre>Info: Ground-state restart information will be read from 'restart/gs'.
</pre>
<p>This is useful if you change slightly the parameters of the simulation (for example the XC functional or the convergence criteria). If you change the grid parameters Octopus will not be able to restart from the previous calculation. If you do not want Octopus to try to restart a calculation, you can set the variable <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=FromScratch"><code>FromScratch</code></a></span>.
</p><p>In case you ware wondering what the restart information looks like, you can have a look at the contents of the <b><tt>restart</tt></b> directory. This is where the files needed to restart a calculation are stored. It may contain several sub-directories depending on the calculations previously performed. In this case, it just contains one:
</p>
<pre>% ls restart
gs
% ls restart/gs
0000000001.obf  0000000003.obf  density      df_010101.obf  df_010103.obf  dv_010101.obf  dv_010103.obf  f_old_0101.obf  indices.obf  occs    vhxc      vin_old_0101.obf
0000000002.obf  0000000004.obf  density.obf  df_010102.obf  df_010104.obf  dv_010102.obf  dv_010104.obf  grid            mixing       states  vhxc.obf  wfns
</pre>
<p>Octopus stores each individual state in a different binary (yet platform-independent) file. In this case, we only have four states (files <b><tt>0000000001.obf</tt></b> to <b><tt>0000000004.obf</tt></b>). Some other useful quantities, like the density, are also stored in binary form. The other files are text files that contain diverse control information. It is unlikely that you will ever have to work directly with these files, but you may take a look around if you are curious. 
</p><p><br />
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Getting_started" title="Tutorial:Getting started">Getting started</a> - Next <a href="/wiki/Tutorial:Total_energy_convergence" title="Tutorial:Total energy convergence">Total energy convergence</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Octopus_basics" title="Tutorial Series:Octopus basics">Octopus basics</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155049
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.183 seconds
Real time usage: 0.610 seconds
Preprocessor visited node count: 741/1000000
Preprocessor generated node count: 1581/1000000
Post‐expand include size: 5066/2097152 bytes
Template argument size: 1994/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 3706/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  537.532      1 -total
 10.27%   55.213     22 Template:Variable
  5.31%   28.554      1 Template:Tutorial_foot
  5.18%   27.853      4 Template:If
  4.68%   25.175     22 Template:Octopus_version
  3.59%   19.271      4 Template:P2
  2.03%   10.925      4 Template:P1
  1.73%    9.305     22 Template:Code
  1.04%    5.582      9 Template:File
  0.91%    4.901     22 Template:Octopus_major_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1322-0!canonical!math=0 and timestamp 20230412155048 and revision id 11461
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Basic_input_options&amp;oldid=11461">http:///mediawiki/index.php?title=Tutorial:Basic_input_options&amp;oldid=11461</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Basic" title="Category:Basic">Basic</a></li><li><a href="/wiki/Category:Ground_State" title="Category:Ground State">Ground State</a></li><li><a href="/wiki/Category:Molecule" title="Category:Molecule">Molecule</a></li><li><a href="/wiki/Category:Pseudopotentials" title="Category:Pseudopotentials">Pseudopotentials</a></li><li><a href="/wiki/Category:DFT" title="Category:DFT">DFT</a></li><li><a href="/wiki/Category:Total_Energy" title="Category:Total Energy">Total Energy</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ABasic+input+options" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Basic_input_options" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Basic_input_options&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Basic_input_options">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Basic_input_options&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Basic_input_options&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Basic_input_options" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Basic_input_options" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Tutorial:Basic_input_options&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Basic_input_options&amp;oldid=11461" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Basic_input_options&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 21 September 2021, at 16:42.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.183","walltime":"0.610","ppvisitednodes":{"value":741,"limit":1000000},"ppgeneratednodes":{"value":1581,"limit":1000000},"postexpandincludesize":{"value":5066,"limit":2097152},"templateargumentsize":{"value":1994,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":3706,"limit":5000000},"timingprofile":["100.00%  537.532      1 -total"," 10.27%   55.213     22 Template:Variable","  5.31%   28.554      1 Template:Tutorial_foot","  5.18%   27.853      4 Template:If","  4.68%   25.175     22 Template:Octopus_version","  3.59%   19.271      4 Template:P2","  2.03%   10.925      4 Template:P1","  1.73%    9.305     22 Template:Code","  1.04%    5.582      9 Template:File","  0.91%    4.901     22 Template:Octopus_major_version"]},"cachereport":{"timestamp":"20230412155049","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":754});});</script>
</body>
</html>
