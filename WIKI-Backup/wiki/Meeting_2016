<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Meeting 2016 - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Meeting_2016","wgTitle":"Meeting 2016","wgCurRevisionId":8741,"wgRevisionId":8741,"wgArticleId":2299,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Meeting_2016","wgRelevantArticleId":2299,"wgRequestId":"35ce6224d53f82f0f4a760be","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Meeting_2016 rootpage-Meeting_2016 skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Meeting 2016</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Octopus_developers_meeting_-_9-11_November_2016"><span class="tocnumber">1</span> <span class="toctext">Octopus developers meeting - 9-11 November 2016</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#List_of_participants"><span class="tocnumber">1.1</span> <span class="toctext">List of participants</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Program"><span class="tocnumber">1.2</span> <span class="toctext">Program</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Practical_information"><span class="tocnumber">1.3</span> <span class="toctext">Practical information</span></a></li>
</ul>
</li>
</ul>
</div>

<h1><span class="mw-headline" id="Octopus_developers_meeting_-_9-11_November_2016">Octopus developers meeting - 9-11 November 2016</span></h1>
<p>The third Octopus developers meeting took place at the Max Planck Institute for the Dynamics and Structure of Matter, in Hamburg, Germany from the 9 to the 11 of November 2016.
</p>
<h2><span class="mw-headline" id="List_of_participants">List of participants</span></h2>
<table>
<tbody><tr>
<td>
<ol><li>Angel Rubio</li>
<li>Micael Oliveira</li>
<li>Nicolas Tancogne-Dejean</li>
<li>Shunsuke A. Sato</li>
<li>Thibault J.-Y. Derrien</li>
<li>Christian Schaefer</li>
<li>Umberto De Giovannini</li>
<li>Philipp Wopperer</li>
<li>Lorenzo Stella</li>
<li>Theophilou Iris</li>
<li>Florian Eich</li>
<li>Hannes Huebener</li>
<li>Soeren Nielsen</li>
<li>Matthieu Verstraete (99.99999% chance)</li>
<li>Miguel Marques (not for the 3 days... I'll let you know later)</li>
<li>Gabriel Gil (he will be developing next year with Carlo)</li>
<li>Carlo A. Rozzi</li>
<li>Kyung-Min Lee</li>
<li>Johannes Flick</li>
<li>Rene Jestaedt</li>
<li>Nicole Helbig (until Thursday evening)</li>
<li>Norah Hoffmann</li>
<li>Thomas Brumme</li>
<li>Fabio Covito</li>
<li>Heiko Appel</li>
<li>Raison Dsouza</li>
<li>Cesar A. Rodriguez Rosario</li></ol>
</td></tr></tbody></table>
<h2><span class="mw-headline" id="Program">Program</span></h2>
<table>
<tbody><tr>
<th>
</th>
<th>Wednesday
</th>
<th>Thursday
</th>
<th>Friday
</th></tr>
<tr style="vertical-align:top;">
<th>Morning
</th>
<td>
<p><br />
</p><p><br />
10:00 Angel (Welcome)
</p><p>10:10 Micael
</p><p>10:30 Coffee break
</p><p>11:00 Discussion
</p>
</td>
<td>09:00 Christian
<p>09:30 Johannes
</p><p>10:00 Rene
</p><p><br />
</p><p>10:30 Coffee break
</p><p>11:00 Heiko
</p><p>12:00 Shunsuke
</p><p><br />
</p>
</td>
<td>09:00 Coding/Discussions
<p><br />
</p><p><br />
</p><p><br />
</p><p>10:30 Coffee break
</p><p>11:00 Coding/Discussions
</p>
</td></tr>
<tr style="vertical-align:top;">
<th>Afternoon
</th>
<td>13:30 Carlo
<p>13:50 Fabio
</p><p>14:10 Iris
</p><p>14:40 Coding/Discussions
</p><p>16:00 Coffee break
</p><p>16:30 Coding/Discussions
</p>
</td>
<td>13:30 Nicolas
<p>14:00 Thibault
</p><p>14:30 Phillip
</p><p>15:00 Lorenzo
</p><p>16:00 Coffee break
</p><p>17:00 Brainstorming:
</p><p>The future of Octopus
</p>
</td>
<td>Departure
</td></tr>
<tr style="vertical-align:top;">
<th>Evening
</th>
<td>
</td>
<td>Workshop dinner
</td>
<td>
</td></tr></tbody></table>
<p>Talks:
</p>
<ol><li>Micael Oliveira, "Migration to git, gitlab, and the future of the testfarm"</li>
<li>Philipp Wopperer, "Methods for calculation of photoemission observables with octopus (t-surff)"</li>
<li>Hannes Huebener, "Pump-probe in solids"</li>
<li>Shunsuke A. Sato, "Lattice dynamics + Comparison with Yabana group's program"</li>
<li>Nicolas Tancogne-Dejean, "Working with solids in Octopus"</li>
<li>Iris Theophilou, "Basis set implementation in Octopus and current stage of RDMFT development"</li>
<li>Fabio Covito, "A novel non-equilibrium Green’s function approach to carriers dynamics in many body interacting systems"</li>
<li>Carlo A. Rozzi, "From PCM to real-time PCM: we have a cunning plan"</li>
<li>Johannes Flick, "Cavity-QED beyond model systems and OEP in octopus"</li>
<li>Lorenzo Stella, "Ideas for computational nanoplasmonics using OCTOPUS: Jellium pseudo-potential generator and projected equations of motion"</li>
<li>Rene Jestaedt, "TBA"</li>
<li>Heiko Appel, "In-situ visualization in octopus, open problems in QEDFT, outlook and wishlist"</li>
<li>Thibault J.-Y. Derrien, "Defining the excited electron density and the hole density in TD-DFT"</li>
<li>Christian Schaefer, "TBA"</li></ol>
<h2><span class="mw-headline" id="Practical_information">Practical information</span></h2>
<p>The meeting will be at the <a rel="nofollow" class="external text" href="https://goo.gl/maps/pVDNKmmgEDK2">MPSD</a> in Seminar Room SR V. You can find information how to reach it <a rel="nofollow" class="external text" href="http://www.mpsd.mpg.de/en/institute/directions">here</a>.
</p>
<!-- 
NewPP limit report
Cached time: 20230412154602
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.026 seconds
Real time usage: 0.028 seconds
Preprocessor visited node count: 14/1000000
Preprocessor generated node count: 20/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2299-0!canonical and timestamp 20230412154602 and revision id 8741
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Meeting_2016&amp;oldid=8741">http:///mediawiki/index.php?title=Meeting_2016&amp;oldid=8741</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Meeting+2016" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Meeting_2016" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Meeting_2016&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Meeting_2016">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Meeting_2016&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Meeting_2016&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Meeting_2016" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Meeting_2016" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Meeting_2016&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Meeting_2016&amp;oldid=8741" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Meeting_2016&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 11 November 2016, at 12:25.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.026","walltime":"0.028","ppvisitednodes":{"value":14,"limit":1000000},"ppgeneratednodes":{"value":20,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412154602","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":164});});</script>
</body>
</html>
