<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers Manual:ExchangeCorrelation - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers_Manual:ExchangeCorrelation","wgTitle":"Developers Manual:ExchangeCorrelation","wgCurRevisionId":7633,"wgRevisionId":7633,"wgArticleId":1679,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers_Manual:ExchangeCorrelation","wgRelevantArticleId":1679,"wgRequestId":"8ee1c97d03f3f464b5605335","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Manual_ExchangeCorrelation rootpage-Developers_Manual_ExchangeCorrelation skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers Manual:ExchangeCorrelation</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>This information is obsolete. Refer to libxc documentation instead.
</p>
<h2><span class="mw-headline" id="Designation_Table">Designation Table</span></h2>
<p>The key citations to the <a href="/wiki/Developers#Pseudopotentials" title="Developers">Pseudopotential Generation Programs</a> listed bellow.
</p>
<table>
<tbody><tr>
<td colspan="4"><hr />
</td></tr>
<tr>
<th>Designation</th>
<th>Approx.</th>
<th>Spin</th>
<th>Cite
</th></tr>
<tr>
<td colspan="4"><hr />
</td></tr>
<tr>
<td>Slater</td>
<td>LDA</td>
<td>-</td>
<td>-
</td></tr>
<tr>
<td>FermiAmaldi1934</td>
<td>LDA</td>
<td>-</td>
<td>E. Fermi and E. Amaldi, Le orbite s degli elementi, Mem. Accad. Ital. Rome, 6, 1934.
</td></tr>
<tr>
<td>Wigner1934</td>
<td>LDA</td>
<td>-</td>
<td>E. Wigner, On the Interaction of Electrons in Metals, Phys. Rev., 46:(11), 1934.
</td></tr>
<tr>
<td>HedinLundqvist1971</td>
<td>LDA</td>
<td>-</td>
<td>L. Hedin and B. I. Lundqvist, Explicit local exchange-correlation potentials, J. Phys. C, 4:(14), 1971.
</td></tr>
<tr>
<td>PerdewZunger1981</td>
<td>LDA</td>
<td>-</td>
<td>D. M. Ceperley and B. J. Alder, Ground State of the Electron Gas by a Stochastic Method, Phys. Rev. Lett., 45:(7), 1980; J. P. Perdew and A. Zunger, Self-interaction correction to density-functional approximations for many-electron systems, Phys. Rev. B, 23:(10), 1981.
</td></tr>
<tr>
<td>PerdewWang1992</td>
<td>GGA</td>
<td>-</td>
<td>J. P. Perdew and Y. Wang, Accurate and simple analytic representation of the electron-gas correlation energy, Phys. Rev. B, 45:(23), 1992.
</td></tr>
<tr>
<td>Becke1988</td>
<td>GGA</td>
<td>-</td>
<td>A. D. Becke, Density-functional exchange-energy approximation with correct asymptotic behavior, Phys. Rev. A, 38:(6), 1988.
</td></tr>
<tr>
<td>Perdew1986</td>
<td>GGA</td>
<td>-</td>
<td>J. P. Perdew, Density-functional approximation for the correlation energy of the inhomogeneous electron gas, Phys. Rev. B, 33:(12), 1986;J. P. Perdew, Erratum: Density-functional approximation for the correlation energy of the inhomogeneous electron gas, Phys. Rev. B, 34:(10), 1986.
</td></tr>
<tr>
<td>PerdewBurkeErnzerhof1996</td>
<td>GGA</td>
<td>-</td>
<td>J. P. Perdew, K. Burke and M. Ernzerhof, Generalized Gradient Approximation Made Simple, Phys. Rev. Lett., 77:(18), 1996.
</td></tr>
<tr>
<td>ZhaoParr1992</td>
<td>LDA</td>
<td>-</td>
<td>Q. Zhao  and R. G. Parr, Local exchange-correlation functional: Numerical test for atoms and ions, Phys. Rev. A, 46:(9), 1992.
</td></tr>
<tr>
<td>MacDonaldVosko1979</td>
<td>LDA</td>
<td>-</td>
<td>A. H. MacDonald and S. H. Vosko, A relativistic density functional formalism, J. Phys. C, 12:(15), 1979.
</td></tr>
<tr>
<td>PerdewBurkeWang1996</td>
<td>LDA</td>
<td>-</td>
<td>J. P. Perdew, K. Burke, and Y. Wang, Generalized gradient approximation for the exchange-correlation hole of a many-electron system, Phys. Rev. B, 54:(23), 1996;J. P. Perdew, J. A. Chevary, S. H. Vosko, K. A. Jackson, A. Koblar, M. R. Pederson, D. J. Singh, and C. Fiolhais, Atoms, molecules, solids, and surfaces: Applications of the generalized gradient approximation for exchange and correlation, Phys. Rev. B, 46:(11), 1992.
</td></tr>
<tr>
<td>LeeYangParr1988</td>
<td>GGA</td>
<td>-</td>
<td>C. Lee, W. Yang, and R. G. Parr, Development of the Colle-Salvetti correlation-energy formula into a functional of the electron density, Phys. Rev. B, 37:(2), 1988.
</td></tr>
<tr>
<td>BarthHedin1972</td>
<td>LDA</td>
<td>-</td>
<td>U. von Barth and L. Hedin, A local exchange-correlation potential for the spin polarized case. i, J. Phys. C, 5:(13), 1972
</td></tr>
<tr>
<td>GunnarssonLundqvist1976</td>
<td>LDA</td>
<td>-</td>
<td>O. Gunnarsson and B. I. Lundqvist, Exchange and correlation in atoms, molecules, and solids by the spin-density-functional formalism, Phys. Rev. B, 13:(10), 1976.
</td></tr>
<tr>
<td>KriegerLiIafrate1990</td>
<td>OEP</td>
<td>-</td>
<td>J. B. Krieger, Y. Li and G. J. Iafrate, Derivation and application of an accurate Kohn-Sham potential with integer discontinuity, Phys. Lett. A, 146:(146) 1990; J. B. Krieger, Y. Li and G. J. Iafrate, Accurate local spin-polarized exchange potential: Reconciliation of generalized Slater and Kohn-Sham methods, Int. J. Quantum Chem., 41:(3), 1992.
</td></tr>
<tr>
<td>HammerHansenNorskov1999</td>
<td>GGA</td>
<td>-</td>
<td>B. Hammer, L. B. Hansen and J. K. N&#248;rskov, Improved adsorption energetics within density-functional theory using revised Perdew-Burke-Ernzerhof functionals, Phys. Rev. B, 59:(11), 1999.
</td></tr>
<tr>
<td>ZhangYang1998</td>
<td>GGA</td>
<td>-</td>
<td>Y. Zhang and W. Yang, Comment on "Generalized Gradient Approximation Made Simple", Phys. Rev. Lett. 80:(4), 1998.
</td></tr>
<tr>
<td>Perdew1999</td>
<td>MGGA</td>
<td>-</td>
<td>J. P. Perdew, S. Kurth, A. Zupan and P. Blaha, Accurate Density Functional with Correct Formal Properties: A Step Beyond the Generalized Gradient Approximation, Phys. Rev. Lett., 82:(12), 1999.
</td></tr>
<tr>
<td>Hamprecht1998</td>
<td>GGA</td>
<td>-</td>
<td>F. A. Hamprecht, A. J. Cohen, D. J. Tozer and N. C. Handy, Development and assessment of new exchange-correlation functionals, J. Chem. Phys., 109:(15), 1998.
</td></tr>
<tr>
<td>TeterPade1993</td>
<td>LDA</td>
<td>-</td>
<td>S. Goedecker, M. Teter and J. Hutter, Separable dual-space Gaussian pseudopotentials, Phys. Rev. B, 54:(3), 1996.
</td></tr>
<tr>
<td>Teter1991</td>
<td>LDA</td>
<td>-</td>
<td>M. P. Teter, Umpublished, 1991.
</td></tr>
<tr>
<td>VoskoWilkNusair1980</td>
<td>LDA</td>
<td>-</td>
<td>S. H. Vosko L. Wilk and M. Nusair, Accurate spin-dependent electron liquid correlation energies for local spin density calculations: a critical analysis, Can. J. Phys., 58, 1980.
</td></tr>
<tr>
<td>Attaccalite2002</td>
<td>LDA</td>
<td>-</td>
<td>C. Attaccalite, S. Moroni, P. Gori-Giorgi and G. B. Bachelet, Correlation Energy and Spin Polarization in the 2D Electron Gas, Phys. Rev. Lett., 88:(25), 2002; C. Attaccalite, S. Moroni P. Gori-Giorgi and G. B. Bachelet, Erratum: Correlation Energy and Spin Polarization in the 2D Electron Gas [Phys. Rev. Lett. 88, 256601 (2002), Phys. Rev. Lett., 91:(10), 2003.
</td></tr>
<tr>
<td>GellMannBrueckner1957</td>
<td>RPA</td>
<td>-</td>
<td>M. Gell-Mann and K. A. Brueckner, Correlation Energy of an Electron Gas at High Density, Phys. Rev., 106:(2), 1957.
</td></tr>
<tr>
<td>LeeuwenBaerends1994</td>
<td>GGA</td>
<td>-</td>
<td>R. van Leeuwen and E. J. Baerends, Exchange-correlation potential with correct asymptotic behavior, Phys. Rev. A, 49:(4), 1994.
</td></tr>
<tr>
<td colspan="4"><hr />
</td></tr>
</tbody></table>
<!-- 
NewPP limit report
Cached time: 20230412155851
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.029 seconds
Real time usage: 0.033 seconds
Preprocessor visited node count: 6/1000000
Preprocessor generated node count: 14/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1679-0!canonical and timestamp 20230412155851 and revision id 7633
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;oldid=7633">http:///mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;oldid=7633</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers+Manual%3AExchangeCorrelation" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers_Manual:ExchangeCorrelation" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers_Manual:ExchangeCorrelation&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers_Manual:ExchangeCorrelation">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers_Manual:ExchangeCorrelation" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers_Manual:ExchangeCorrelation" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;oldid=7633" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers_Manual:ExchangeCorrelation&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 20 March 2015, at 22:32.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.029","walltime":"0.033","ppvisitednodes":{"value":6,"limit":1000000},"ppgeneratednodes":{"value":14,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412155851","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":171});});</script>
</body>
</html>
