<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Centering a geometry - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Centering_a_geometry","wgTitle":"Tutorial:Centering a geometry","wgCurRevisionId":11322,"wgRevisionId":11322,"wgArticleId":2207,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Basic","Molecule","Oct-center-geom"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Centering_a_geometry","wgRelevantArticleId":2207,"wgRequestId":"e99050b60bc68d19fd9ccae3","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Centering_a_geometry rootpage-Tutorial_Centering_a_geometry skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Centering a geometry</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Before running an Octopus calculation of a molecule, it is always a good idea to run the <a href="/wiki/Manual:External_utilities:oct-center-geom" title="Manual:External utilities:oct-center-geom"><code>oct-center-geom</code></a> utility, which will translate the center of mass to the origin, and align the molecule, by default so its main axis is along the <i>x</i>-axis. Doing this is often helpful for visualization purposes, and making clear the symmetry of the system, and also it will help to construct the simulation box efficiently in the code. The current implementation in Octopus constructs a parallelepiped containing the simulation box and the origin, and it will be much larger than necessary if the system is not centered and aligned. For periodic systems, these considerations are not relevant (at least in the periodic directions).
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Input"><span class="tocnumber">1</span> <span class="toctext">Input</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#inp"><span class="tocnumber">1.1</span> <span class="toctext"><b>inp</b></span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#tAB.xyz"><span class="tocnumber">1.2</span> <span class="toctext"><b>tAB.xyz</b></span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-4"><a href="#Centering_the_geometry"><span class="tocnumber">2</span> <span class="toctext">Centering the geometry</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Input">Input</span></h2>
<p>For this example we need two files.
</p>
<h4><span class="mw-headline" id="inp"><b><tt>inp</tt></b></span></h4>
<p>We need only a very simple input file, specifying the coordinates file.
</p>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span> = "tAB.xyz"
</pre>
<h4><span class="mw-headline" id="tAB.xyz"><b><tt>tAB.xyz</tt></b></span></h4>
<p>We will use this coordinates file, for the molecule <a rel="nofollow" class="external text" href="http://en.wikipedia.org/wiki/Azobenzene"><i>trans</i>-azobenzene</a>, which has the interesting property of being able to switch between <i>trans</i> and <i>cis</i> isomers by absorption of light.
</p>
<pre>24

C   -0.520939   -1.29036    -2.47763
C   -0.580306    0.055376   -2.10547
C    0.530312    0.670804   -1.51903
C    1.71302    -0.064392   -1.30151
C    1.76264    -1.41322    -1.67813
C    0.649275   -2.02387    -2.26419
H   -1.38157    -1.76465    -2.93131
H   -1.48735     0.622185   -2.27142
H    2.66553    -1.98883    -1.51628
H    0.693958   -3.06606    -2.55287
H    0.467127    1.71395    -1.23683
N    2.85908     0.52877    -0.708609
N    2.86708     1.72545    -0.355435
C    4.01307     2.3187      0.237467
C    3.96326     3.66755     0.614027
C    5.0765      4.27839     1.20009
C    6.2468      3.54504     1.41361
C    6.30637     2.19928     1.04153
C    5.19586     1.58368     0.455065
H    5.25919     0.540517    0.17292
H    3.06029     4.24301     0.4521
H    5.03165     5.32058     1.48872
H    7.10734     4.01949     1.86731
H    7.21349     1.63261     1.20754
</pre>
<h2><span class="mw-headline" id="Centering_the_geometry">Centering the geometry</span></h2>
<p><a href="/wiki/Manual:External_utilities:oct-center-geom" title="Manual:External utilities:oct-center-geom"><code>oct-center-geom</code></a> is a serial utility, and it will be found in the <code>bin</code> directory after installation of Octopus.
</p><p>When you run the utility, you should obtain a new coordinates file <code>adjusted.xyz</code> for use in your calculations with Octopus. The output is not especially interesting, except for perhaps the moment of inertia tensor, which comes at the end of the calculation:
</p>
<pre>Using main axis        1.000000       0.000000       0.000000
Input: [AxisType = inertia]
Center of mass [b] =        5.410288       2.130154      -1.005363
Moment of inertia tensor [amu*b^2]
      6542962.320615     -4064048.884840     -3486905.049193
     -4064048.884840      8371776.892023     -2789525.719087
     -3486905.049193     -2789525.719087     10730451.796958
Isotropic average      8548397.003199
Eigenvalues:            1199576.606657          11623018.898148          12822595.504791
Found primary   axis        0.707107       0.565686       0.424264
Found secondary axis       -0.624695       0.780869      -0.000000
</pre>
<p>You can now visualize the original and new coordinates files with <code>xcrysden</code> or your favorite visualization program (<i>e.g.</i> Jmol, Avogadro, VMD, Vesta, etc.), and see what transformations the utility performed. You can see more options about how to align the system at <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Utilities&amp;name=AxisType"><code>AxisType</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Utilities&amp;name=MainAxis"><code>MainAxis</code></a></span>.
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Visualization" title="Tutorial:Visualization">Visualization</a> - Next <a href="/wiki/Tutorial:Time-dependent_propagation" title="Tutorial:Time-dependent propagation">Time-dependent propagation</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Octopus_basics" title="Tutorial Series:Octopus basics">Octopus basics</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155050
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.075 seconds
Real time usage: 0.091 seconds
Preprocessor visited node count: 236/1000000
Preprocessor generated node count: 755/1000000
Post‐expand include size: 1807/2097152 bytes
Template argument size: 1296/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 648/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   61.727      1 -total
 41.87%   25.844      1 Template:Tutorial_foot
 40.68%   25.109      4 Template:If
 31.49%   19.437      3 Template:Variable
 28.16%   17.384      4 Template:P2
 20.51%   12.663      3 Template:Octopus_version
 16.23%   10.021      4 Template:P1
  8.98%    5.543      8 Template:Code
  6.37%    3.930      4 Template:Octopus
  6.35%    3.921      2 Template:File
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2207-0!canonical and timestamp 20230412155050 and revision id 11322
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;oldid=11322">http:///mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;oldid=11322</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Basic" title="Category:Basic">Basic</a></li><li><a href="/wiki/Category:Molecule" title="Category:Molecule">Molecule</a></li><li><a href="/wiki/Category:Oct-center-geom" title="Category:Oct-center-geom">Oct-center-geom</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ACentering+a+geometry" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Centering_a_geometry" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Centering_a_geometry&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Centering_a_geometry">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Centering_a_geometry" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Centering_a_geometry" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;oldid=11322" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Centering_a_geometry&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 30 August 2021, at 15:57.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.075","walltime":"0.091","ppvisitednodes":{"value":236,"limit":1000000},"ppgeneratednodes":{"value":755,"limit":1000000},"postexpandincludesize":{"value":1807,"limit":2097152},"templateargumentsize":{"value":1296,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":648,"limit":5000000},"timingprofile":["100.00%   61.727      1 -total"," 41.87%   25.844      1 Template:Tutorial_foot"," 40.68%   25.109      4 Template:If"," 31.49%   19.437      3 Template:Variable"," 28.16%   17.384      4 Template:P2"," 20.51%   12.663      3 Template:Octopus_version"," 16.23%   10.021      4 Template:P1","  8.98%    5.543      8 Template:Code","  6.37%    3.930      4 Template:Octopus","  6.35%    3.921      2 Template:File"]},"cachereport":{"timestamp":"20230412155050","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":231});});</script>
</body>
</html>
