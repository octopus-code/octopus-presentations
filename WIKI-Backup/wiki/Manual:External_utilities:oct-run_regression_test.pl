<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:External utilities:oct-run regression test.pl - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:External_utilities:oct-run_regression_test.pl","wgTitle":"Manual:External utilities:oct-run regression test.pl","wgCurRevisionId":8450,"wgRevisionId":8450,"wgArticleId":1665,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:External_utilities:oct-run_regression_test.pl","wgRelevantArticleId":1665,"wgRequestId":"7557425efa5dedf243c87510","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_External_utilities_oct-run_regression_test_pl rootpage-Manual_External_utilities_oct-run_regression_test_pl skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:External utilities:oct-run regression test.pl</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#NAME"><span class="tocnumber">1</span> <span class="toctext">NAME</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#SYNOPSIS"><span class="tocnumber">2</span> <span class="toctext">SYNOPSIS</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#DESCRIPTION"><span class="tocnumber">3</span> <span class="toctext">DESCRIPTION</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#EXAMPLES"><span class="tocnumber">4</span> <span class="toctext">EXAMPLES</span></a></li>
</ul>
</div>

<h3><span class="mw-headline" id="NAME">NAME</span></h3>
<p>oct-run_regression_test.pl - Script to create and run octopus regression tests
</p>
<h3><span class="mw-headline" id="SYNOPSIS">SYNOPSIS</span></h3>
<p><tt><i>oct-run_regression_test.pl [ option ] ... [ file ] ...</i></tt>
</p>
<h3><span class="mw-headline" id="DESCRIPTION">DESCRIPTION</span></h3>
<p>This script is one of the octopus utilities.
</p><p>This script can be used to generate and run the octopus regression
tests.
</p><p><code>-n</code>
</p>
<dl><dd>Dry run. Don't run the test. Only print what would be executed.</dd></dl>
<p><code>-v</code>
</p>
<dl><dd>Run in verbose mode. Useful for debugging the script.</dd></dl>
<p><code>-h</code>
</p>
<dl><dd>Show a brief summary of command line options.</dd></dl>
<p><code>-e *file*</code>
</p>
<dl><dd>Specify the name of the Octopus executable. The default is octopus.</dd></dl>
<p><code>-c *file*</code>
</p>
<dl><dd>Create a template for a Octopus regression test. This should always be used if you create a new regression test.</dd></dl>
<p><code>-f *file*</code>
</p>
<dl><dd>This option specifies the input file for a regression test.</dd></dl>
<p><code>-d *directory*</code>
</p>
<dl><dd>Use *directory* as working directory. The default is a randomly generated directory in /tmp.</dd></dl>
<p><code>-i</code>
</p>
<dl><dd>Print content of the octopus inputfile which is used for the current test to stdout.</dd></dl>
<p><code>-p</code>
</p>
<dl><dd>Preserve working directory. Don't clean up the temporary working directory after the run.</dd></dl>
<h3><span class="mw-headline" id="EXAMPLES">EXAMPLES</span></h3>
<p>To create a template for a regression test run e.g.
</p>
<pre> <tt><i>$ oct-run_regression_test.pl -c finite_systems_3d/sodium.test</i></tt>
</pre>
<p>Running a test can be achieved with
</p>
<pre> <tt><i>$ oct-run_regression_test.pl -f finite_systems_3d/sodium.test</i></tt>
</pre>
<p>If a test run fails it might be useful for bug tracing to preserve the
working directory of this run and to print out the input file
</p>
<pre> <tt><i>$ oct-run_regression_test.pl -pi -f finite_systems_3d/sodium.test</i></tt>
</pre>
<p>Run a MPI test with a local directory as working directory
</p>
<pre> <tt><i>$ oct-run_regression_test.pl -d./tmp -f benzene.test -e `pwd`/../src/octopus-mpi</i></tt>
</pre>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:External_utilities:oct-run_periodic_table" title="Manual:External utilities:oct-run periodic table">Manual:External utilities:oct-run_periodic_table</a> - Next <a href="/wiki/Manual:External_utilities:oct-run_testsuite.sh" title="Manual:External utilities:oct-run testsuite.sh">Manual:External utilities:oct-run_testsuite.sh</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412154057
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.052 seconds
Real time usage: 0.061 seconds
Preprocessor visited node count: 162/1000000
Preprocessor generated node count: 394/1000000
Post‐expand include size: 1406/2097152 bytes
Template argument size: 876/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   34.537      1 -total
 36.82%   12.715      9 Template:Flag
 24.61%    8.500      1 Template:Manual_foot
 17.28%    5.967      4 Template:Command_line
 16.99%    5.867      9 Template:Code
 14.67%    5.066      5 Template:Command
 12.30%    4.248      1 Template:Foot
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1665-0!canonical and timestamp 20230412154057 and revision id 8450
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;oldid=8450">http:///mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;oldid=8450</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AExternal+utilities%3Aoct-run+regression+test.pl" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:External_utilities:oct-run_regression_test.pl" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:External_utilities:oct-run_regression_test.pl&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:External_utilities:oct-run_regression_test.pl">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:External_utilities:oct-run_regression_test.pl" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:External_utilities:oct-run_regression_test.pl" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;oldid=8450" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 7 September 2016, at 13:39.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.052","walltime":"0.061","ppvisitednodes":{"value":162,"limit":1000000},"ppgeneratednodes":{"value":394,"limit":1000000},"postexpandincludesize":{"value":1406,"limit":2097152},"templateargumentsize":{"value":876,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   34.537      1 -total"," 36.82%   12.715      9 Template:Flag"," 24.61%    8.500      1 Template:Manual_foot"," 17.28%    5.967      4 Template:Command_line"," 16.99%    5.867      9 Template:Code"," 14.67%    5.066      5 Template:Command"," 12.30%    4.248      1 Template:Foot"]},"cachereport":{"timestamp":"20230412154057","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":190});});</script>
</body>
</html>
