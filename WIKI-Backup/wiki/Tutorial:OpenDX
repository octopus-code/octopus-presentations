<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:OpenDX - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:OpenDX","wgTitle":"Tutorial:OpenDX","wgCurRevisionId":11356,"wgRevisionId":11356,"wgArticleId":2951,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Obsolete"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:OpenDX","wgRelevantArticleId":2951,"wgRequestId":"ec6db4a3948b4b8edf347c11","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.page.gallery.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.page.gallery.styles%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_OpenDX rootpage-Tutorial_OpenDX skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:OpenDX</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p><i>Note: the OpenDX program is no longer supported and the following tutorial should be considered obsolete. We recommend you to use one of the alternatives mentioned in this <a href="/wiki/Tutorial:Visualization" title="Tutorial:Visualization">tutorial</a>.</i>
</p>
<h2><span class="mw-headline" id="Input">Input</span></h2>
<p>At this point, the input should be quite familiar to you:
</p>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = gs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsOutput"><code>UnitsOutput</code></a></span> = eV_Angstrom

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 5*angstrom
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.15*angstrom

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=Output"><code>Output</code></a></span> = wfs + density + elf + potential
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=OutputFormat"><code>OutputFormat</code></a></span> = cube + xcrysden + dx + axis_x + plane_z

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span> = "benzene.xyz"
</pre>
<p>Coordinates are in this case given in the file <b><tt>benzene.xyz</tt></b> file. This file should look like:
</p>
<pre>12
   Geometry of benzene (in Angstrom)
C  0.000  1.396  0.000
C  1.209  0.698  0.000
C  1.209 -0.698  0.000
C  0.000 -1.396  0.000
C -1.209 -0.698  0.000
C -1.209  0.698  0.000
H  0.000  2.479  0.000
H  2.147  1.240  0.000
H  2.147 -1.240  0.000
H  0.000 -2.479  0.000
H -2.147 -1.240  0.000
H -2.147  1.240  0.000
</pre>
<p>Note that we asked octopus to output the wavefunctions (<tt>wfs</tt>), the density, the electron localization function (<tt>elf</tt>) and the Kohn-Sham potential. We ask Octopus to generate this output in several formats, that are requires for the different visualization tools that we mention below. If you want to save some disk space you can just keep the option that corresponds to the program you will use. Take a look at the documentation on variables <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=Output"><code>Output</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=OutputFormat"><code>OutputFormat</code></a></span> for the full list of possible quantities to output and formats to visualize them in.
</p>
<h3><span class="mw-headline" id="OpenDX">OpenDX</span></h3>
<p><a rel="nofollow" class="external text" href="http://www.opendx.org">OpenDX</a> is a very powerful, although quite complex, program for the 3D visualization of data. It is completely general-purpose, and uses a visual language to process the data to be plotted. Don't worry, as you will probably not need to learn this language. We have provided a program (<b><tt>mf.net</tt></b> and <b><tt>mf.cfg</tt></b>) that you can get from the <b><tt>SHARE/util</tt></b> directory in the Octopus installation (probably it is located in <b><tt>/usr/share/octopus/util</tt></b>). 
</p><p>Before starting, please make sure you have it installed together with the Chemistry extensions. You may find some instructions <a href="/wiki/Releases#OpenDX" title="Releases">here</a>. Note that openDX is far from trivial to install and to use. However, the outcomes are really spectacular, so if you are willing to suffer a little at the beginning, we are sure that your efforts will be compensated.
</p><p>If you run Octopus with this input file, afterward you will find in the <b><tt>static</tt></b> directory several files with the extension <b><tt>.dx</tt></b>. These files contain the wave-functions, the density, etc. in a format that can be understood by <a rel="nofollow" class="external text" href="http://www.opendx.org/">Open DX</a> (you will need the chemistry extensions package installed on top of DX, you can find it in our <a href="/wiki/Releases" title="Releases">releases</a> page). 
</p><p>So, let us start. First copy the files <b><tt>mf.net</tt></b> and <b><tt>mf.cfg</tt></b> to your open directory, and open dx with the command line
</p>
<pre> &gt; dx
</pre>
<p>If you followed the instructions given in <a href="/wiki/Releases#OpenDX" title="Releases">here</a> you should see a window similar to the one shown on the right.
</p>
<ul class="gallery mw-gallery-traditional">
		<li class="gallerybox" style="width: 155px"><div style="width: 155px">
			<div class="thumb" style="width: 150px;"><div style="margin:15px auto;"><a href="/wiki/File:Tutorial_dx_1.png" class="image"><img alt="" src="/mediawiki/images/thumb/2/24/Tutorial_dx_1.png/86px-Tutorial_dx_1.png" decoding="async" width="86" height="120" srcset="/mediawiki/images/thumb/2/24/Tutorial_dx_1.png/130px-Tutorial_dx_1.png 1.5x, /mediawiki/images/thumb/2/24/Tutorial_dx_1.png/173px-Tutorial_dx_1.png 2x" /></a></div></div>
			<div class="gallerytext">
<p>Screenshot of the Open DX main menu
</p>
			</div>
		</div></li>
		<li class="gallerybox" style="width: 155px"><div style="width: 155px">
			<div class="thumb" style="width: 150px;"><div style="margin:15px auto;"><a href="/wiki/File:Tutorial_dx_2.png" class="image"><img alt="" src="/mediawiki/images/thumb/a/a9/Tutorial_dx_2.png/110px-Tutorial_dx_2.png" decoding="async" width="110" height="120" srcset="/mediawiki/images/thumb/a/a9/Tutorial_dx_2.png/165px-Tutorial_dx_2.png 1.5x, /mediawiki/images/thumb/a/a9/Tutorial_dx_2.png/221px-Tutorial_dx_2.png 2x" /></a></div></div>
			<div class="gallerytext">
<p>Main dialog of mf.net
</p>
			</div>
		</div></li>
		<li class="gallerybox" style="width: 155px"><div style="width: 155px">
			<div class="thumb" style="width: 150px;"><div style="margin:26px auto;"><a href="/wiki/File:Tutorial_dx_3.png" class="image"><img alt="" src="/mediawiki/images/thumb/4/42/Tutorial_dx_3.png/120px-Tutorial_dx_3.png" decoding="async" width="120" height="98" srcset="/mediawiki/images/thumb/4/42/Tutorial_dx_3.png/180px-Tutorial_dx_3.png 1.5x, /mediawiki/images/thumb/4/42/Tutorial_dx_3.png/240px-Tutorial_dx_3.png 2x" /></a></div></div>
			<div class="gallerytext">
<p>Isosurfaces dialog
</p>
			</div>
		</div></li>
		<li class="gallerybox" style="width: 155px"><div style="width: 155px">
			<div class="thumb" style="width: 150px;"><div style="margin:15px auto;"><a href="/wiki/File:Tutorial_dx_4.png" class="image"><img alt="" src="/mediawiki/images/thumb/3/3d/Tutorial_dx_4.png/101px-Tutorial_dx_4.png" decoding="async" width="101" height="120" srcset="/mediawiki/images/thumb/3/3d/Tutorial_dx_4.png/152px-Tutorial_dx_4.png 1.5x, /mediawiki/images/thumb/3/3d/Tutorial_dx_4.png/203px-Tutorial_dx_4.png 2x" /></a></div></div>
			<div class="gallerytext">
<p>Geometry dialog
</p>
			</div>
		</div></li>
</ul>
<p>Now select <b><tt>Run Visual Programs...</tt></b>, select the <b><tt>mf.net</tt></b> file, and click on <b><tt>OK</tt></b>. This will open the main dialog of our application. You will no longer need the main DX window, so it is perhaps better to minimize it to get it out of the way. Before continuing, go to the menu <b><tt>Execute &gt; Execute on Change</tt></b>, which instructs to open DX to recalculate out plot whenever we make a change in any parameters.
</p>
<div class="thumb tright"><div class="thumbinner" style="width:352px;"><a href="/wiki/File:Tutorial_dx_5.png" class="image"><img alt="Tutorial dx 5.png" src="/mediawiki/images/thumb/2/2c/Tutorial_dx_5.png/350px-Tutorial_dx_5.png" decoding="async" width="350" height="288" class="thumbimage" srcset="/mediawiki/images/thumb/2/2c/Tutorial_dx_5.png/525px-Tutorial_dx_5.png 1.5x, /mediawiki/images/thumb/2/2c/Tutorial_dx_5.png/700px-Tutorial_dx_5.png 2x" /></a>  <div class="thumbcaption"><div class="magnify"><a href="/wiki/File:Tutorial_dx_5.png" class="internal" title="Enlarge"></a></div></div></div></div>
<p>Now, let us choose a file to plot. As you can see, the field <tt>File</tt> in the mains dialog is <tt>NULL</tt>. If you click on the three dots next to the field a file selector will open. Just select the file <b><tt>static/density-1.dx</tt></b> and press OK. Now we will instruct open DX to draw a iso-surface. Click on <tt>Isosurfaces</tt> and a new dialog box will open (to close that dialog box, please click again on <tt>Isosurfaces</tt> in the main dialog!) In the end you may have lots of windows lying around, so try to be organized. Then click on <tt>Show Isosurface 1</tt> - an image should appear with the density of benzene. You can zoom, rotate, etc the picture if you follow the menu <b><tt>Options &gt; View control...</tt></b> in the menu of the image panel. You can play also around in the isosurfaces dialog with the <tt>Isosurface value</tt> in order to make the picture more appealing, change the color, the opacity, etc. You can even plot two isosurfaces with different values.
</p><p>We hope you are not too disappointed with the picture you just plotted. Densities are usually quite boring (even if the Hohenberg-Kohn theorem proves that they know everything)! You can try to plot other quantities like the electronic localization function, the wave-functions, etc, just by choosing the appropriate file in the main dialog. You can try it later: for now we will stick to the "boring density".
</p><p>Let us now add a structure. Close the isosurfaces dialog <b>by clicking again in <tt>Isosurfaces</tt> in the main dialog box</b>, and open the <tt>Geometry</tt> dialog. In the <tt>File</tt> select the file <b><tt>benzene.xyz</tt></b>, and click on <tt>Show Geometry</tt>. The geometry should appear beneath the isosurface. You can make bonds appear and disappear by using the slide <tt>Bonds length</tt>,  change its colors, etc. in this dialog.
</p><p>To finish, let us put some "color" in our picture. Close the <tt>Geometry</tt> dialog (you know how), and open again the isosurfaces dialog. Choose <tt>Map isosurface</tt> and in the <tt>File</tt> field choose the file <b><tt>static/vh.dx</tt></b>. There you go, you should have a picture of an isosurface of benzene colored according to the value of the electrostatic potential! Cool, no? If everything went right, you should end with a picture like the one on the right.
</p><p>As an exercise you can try to plot the wave-functions, etc, make slabs, etc. Play around. As usual some things do not work perfectly, but most of it does!
</p>
<!-- 
NewPP limit report
Cached time: 20230412155459
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.109 seconds
Real time usage: 0.132 seconds
Preprocessor visited node count: 251/1000000
Preprocessor generated node count: 638/1000000
Post‐expand include size: 1839/2097152 bytes
Template argument size: 456/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 2395/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   83.475      1 -total
 40.05%   33.428      9 Template:Variable
 19.60%   16.360      9 Template:Octopus_version
  8.47%    7.066     13 Template:File
  7.34%    6.123      9 Template:Code
  4.84%    4.040      9 Template:Octopus_major_version
  4.69%    3.919      9 Template:Octopus_minor_version
  4.63%    3.861      2 Template:Octopus
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2951-0!canonical and timestamp 20230412155459 and revision id 11356
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:OpenDX&amp;oldid=11356">http:///mediawiki/index.php?title=Tutorial:OpenDX&amp;oldid=11356</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Obsolete" title="Category:Obsolete">Obsolete</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AOpenDX" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:OpenDX" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:OpenDX&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:OpenDX">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:OpenDX" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:OpenDX" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;oldid=11356" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 1 September 2021, at 11:51.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.109","walltime":"0.132","ppvisitednodes":{"value":251,"limit":1000000},"ppgeneratednodes":{"value":638,"limit":1000000},"postexpandincludesize":{"value":1839,"limit":2097152},"templateargumentsize":{"value":456,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":2395,"limit":5000000},"timingprofile":["100.00%   83.475      1 -total"," 40.05%   33.428      9 Template:Variable"," 19.60%   16.360      9 Template:Octopus_version","  8.47%    7.066     13 Template:File","  7.34%    6.123      9 Template:Code","  4.84%    4.040      9 Template:Octopus_major_version","  4.69%    3.919      9 Template:Octopus_minor_version","  4.63%    3.861      2 Template:Octopus"]},"cachereport":{"timestamp":"20230412155459","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":290});});</script>
</body>
</html>
