<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Projects and Runs - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Projects_and_Runs","wgTitle":"Projects and Runs","wgCurRevisionId":1712,"wgRevisionId":1712,"wgArticleId":1366,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Projects_and_Runs","wgRelevantArticleId":1366,"wgRequestId":"3b101c94cb538ab91e1bbd16","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Projects_and_Runs rootpage-Projects_and_Runs skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Projects and Runs</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Definitions"><span class="tocnumber">1</span> <span class="toctext">Definitions</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Run"><span class="tocnumber">1.1</span> <span class="toctext">Run</span></a>
<ul>
<li class="toclevel-3 tocsection-3"><a href="#Piping_runs"><span class="tocnumber">1.1.1</span> <span class="toctext">Piping runs</span></a>
<ul>
<li class="toclevel-4 tocsection-4"><a href="#Note_on_repeating_runs_.28tuning.29"><span class="tocnumber">1.1.1.1</span> <span class="toctext">Note on repeating runs (tuning)</span></a></li>
</ul>
</li>
</ul>
</li>
<li class="toclevel-2 tocsection-5"><a href="#Project"><span class="tocnumber">1.2</span> <span class="toctext">Project</span></a>
<ul>
<li class="toclevel-3 tocsection-6"><a href="#Common_set_of_parameters"><span class="tocnumber">1.2.1</span> <span class="toctext">Common set of parameters</span></a></li>
</ul>
</li>
</ul>
</li>
<li class="toclevel-1 tocsection-7"><a href="#Files_and_folders"><span class="tocnumber">2</span> <span class="toctext">Files and folders</span></a>
<ul>
<li class="toclevel-2 tocsection-8"><a href="#Runs"><span class="tocnumber">2.1</span> <span class="toctext">Runs</span></a>
<ul>
<li class="toclevel-3 tocsection-9"><a href="#state-running_and_state-finished"><span class="tocnumber">2.1.1</span> <span class="toctext">state-running and state-finished</span></a></li>
<li class="toclevel-3 tocsection-10"><a href="#command_passing_.28poor_man.27s_rpc.29"><span class="tocnumber">2.1.2</span> <span class="toctext">command passing (poor man's rpc)</span></a></li>
</ul>
</li>
<li class="toclevel-2 tocsection-11"><a href="#Project_2"><span class="tocnumber">2.2</span> <span class="toctext">Project</span></a></li>
</ul>
</li>
</ul>
</div>

<h1><span class="mw-headline" id="Definitions">Definitions</span></h1>
<h2><span class="mw-headline" id="Run">Run</span></h2>
<p>A run is a set of files:
</p>
<ul><li>parameter configuration ("variables")</li>
<li>possibly inputs from a previous runs that serve as a starting configuration</li>
<li>results ("outputs") from executing octopus on these "variables" and "inputs"</li>
<li>Analysis and visualization of results</li></ul>
<p>The run should be deterministic. E.g. if the run's results and analysis/visualization
are removed the run should contain all the neccessary information to recreate them.
</p>
<h3><span class="mw-headline" id="Piping_runs">Piping runs</span></h3>
<p>Consider a run a black box that processes "inputs" to "outputs" controlled by the "config" (the variables).
Usually a run by itself will not reveal all the interesting properties of the model we fed it with. It will most likely
need to pass through other "runs" to provide a complete view.
</p><p>We will call taking a run's "output" (the results) and feeding it as "input" (starting configuration)
into another run "piping" the runs together.
</p><p>It doesn't look proper (and determinsitic) to allow for loops of runs (for example run1 -&gt; run2 -&gt; run1). If we restrict ourselves to tree-like dependencies we will be able to define a set of runs into a project.
</p>
<h4><span id="Note_on_repeating_runs_(tuning)"></span><span class="mw-headline" id="Note_on_repeating_runs_.28tuning.29">Note on repeating runs (tuning)</span></h4>
<p>Sometimes (or almost always) a run will be repeated or resumed at the configuration it converged to in the previous execution of this run. This is due to developing a reasonable parameter configuration. For example a first crude run might have very rough convergence criteria to have a first look at the results. Some parameters will be tuned and the run repeated.
</p><p>This is not a loop of runs in the sense used above. These repeated execution of the run are still the same run.
</p>
<h2><span class="mw-headline" id="Project">Project</span></h2>
<p>An arbitrary set of runs in a common top folder will be called a project. The runs may be piped together, or they may be independent runs perhaps even of different physical models.
</p><p>A project will be able to determine the dependencies and generate Makefiles that can be used to have a project computed.
</p>
<h3><span class="mw-headline" id="Common_set_of_parameters">Common set of parameters</span></h3>
<p>Furthermore if the runs are very closely related the project may hold a common base configuration that will be merged into the runs' configuration. That can help keeping the runs' parameter configuration to a managable minimum (at the expense of increasing complexity by having two parameter configuration files for one run). The decision of which organisational model to use will remain with the user.
</p><p>In general projects should be optional. A run should be able to work without being embedded in a project (which is the current mode of operation).
</p>
<h1><span class="mw-headline" id="Files_and_folders">Files and folders</span></h1>
<h2><span class="mw-headline" id="Runs">Runs</span></h2>
<p>A run should cleanly separate
</p>
<ul><li>"input" (optional starting configuration, not parameter configuration)</li>
<li>"config" (parameter configuration/variables a.k.a "inp")</li>
<li>checkpoints (temporary files for uncompleted execution due to CPU time constraints)</li>
<li>"output" (the results from the finished run)</li>
<li>state information (run in progress, run completed)</li>
<li>file-based command passing ("abort run")</li>
<li>log files from run execution</li></ul>
<p>Furthermore we want the communication between a running octopus-core and the user interface to be file based.
</p><p>This ensures portability and even managing a project running on an mpp machine simply by rsyncing forth and back the directory contents.
</p><p>The proposed structure is the following
</p>
<pre>&lt;run folder&gt;
|-input/
|-output/
|-.checkpoint/
|-observables/
|-parameters.conf (previously known as "inp")
|-state-running and state-finished
`-command
</pre>
<h3><span class="mw-headline" id="state-running_and_state-finished">state-running and state-finished</span></h3>
<p>Both files are generated by octopus-core. When octopus starts it will create state-running. When checkpointing the execution state (for being able to resume operation), or maybe even at smaller steps, octopus will rewrite this file with progress information. There are two possible models, either
</p>
<ul><li>a single line of a "done/estimated" ratio, or</li>
<li>multiple timestamped lined of the above, e.g.</li></ul>
<pre>1147695936 10/198
1147697123 21/201
...
1147699212 44/199
</pre>
<p>The latter format will allow for an estimation of the remained time.
</p><p>When octopus finishes it will remove state-running and create an empty state-finished
</p>
<h3><span id="command_passing_(poor_man's_rpc)"></span><span class="mw-headline" id="command_passing_.28poor_man.27s_rpc.29">command passing (poor man's rpc)</span></h3>
<p>In order to be able to abort/suspend a run's execution, octopus-core will have to periodically check (in the same loop when writing out state information) for the existance of a command file. If it exists it should read its contents and perform an action.
</p>
<h2><span class="mw-headline" id="Project_2">Project</span></h2>
<p>In the simplest case the project will have a project description config file ("project.conf") that will contain the dependencies of the the contained runs (in the really simplest case there will be no project at all).
</p><p>There will also be an optional common parameter configuration file shared by all contained runs called "parameters.conf". The runs' parameters.conf will override the project's. E.g. the project's project.conf will serve as defaults for all runs.
</p>
<!-- 
NewPP limit report
Cached time: 20230412161202
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.026 seconds
Real time usage: 0.029 seconds
Preprocessor visited node count: 42/1000000
Preprocessor generated node count: 48/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1366-0!canonical and timestamp 20230412161202 and revision id 1712
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Projects_and_Runs&amp;oldid=1712">http:///mediawiki/index.php?title=Projects_and_Runs&amp;oldid=1712</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Projects+and+Runs" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Projects_and_Runs" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Projects_and_Runs&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Projects_and_Runs">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Projects_and_Runs&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Projects_and_Runs&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Projects_and_Runs" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Projects_and_Runs" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Projects_and_Runs&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Projects_and_Runs&amp;oldid=1712" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Projects_and_Runs&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 15 May 2006, at 15:43.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.026","walltime":"0.029","ppvisitednodes":{"value":42,"limit":1000000},"ppgeneratednodes":{"value":48,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412161202","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":228});});</script>
</body>
</html>
