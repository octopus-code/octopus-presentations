<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers Manual:Open Boundaries/Transport - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers_Manual:Open_Boundaries/Transport","wgTitle":"Developers Manual:Open Boundaries/Transport","wgCurRevisionId":7480,"wgRevisionId":7480,"wgArticleId":1967,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers_Manual:Open_Boundaries/Transport","wgRelevantArticleId":1967,"wgRequestId":"12efeb01bfdd2f445d01d10e","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","ext.cite.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["ext.cite.ux-enhancements","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.cite.styles%7Cext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Manual_Open_Boundaries_Transport rootpage-Developers_Manual_Open_Boundaries_Transport skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers Manual:Open Boundaries/Transport</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>This site contains a few notes and ideas about the open boundaries implementation.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Calculating_the_Hartree_potential_for_the_ground_state"><span class="tocnumber">1</span> <span class="toctext">Calculating the Hartree potential for the ground state</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#A_note_for_2D_systems"><span class="tocnumber">1.1</span> <span class="toctext">A note for 2D systems</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-3"><a href="#Parallelizing_the_calculation_of_the_memory_coefficient_and_the_memory_term"><span class="tocnumber">2</span> <span class="toctext">Parallelizing the calculation of the memory coefficient and the memory term</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#References"><span class="tocnumber">3</span> <span class="toctext">References</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Calculating_the_Hartree_potential_for_the_ground_state">Calculating the Hartree potential for the ground state</span></h2>
<p>In order to do DFT for the transport geometry we have to solve the Poisson equation for a system with broken symmetry. The idea is illustrated by the following figure:
</p>
<div class="center"><div class="floatnone"><a href="/wiki/File:Open_Boundaries_ground_state_hartree.png" class="image"><img alt="Open Boundaries ground state hartree.png" src="/mediawiki/images/3/36/Open_Boundaries_ground_state_hartree.png" decoding="async" width="381" height="445" /></a></div></div>
<p>In order to solve the Poisson equation for a system with potential <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/19f1c5b0af1f30c41cb8bc0ba3bfea98681da268" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.812ex; height:2.509ex;" alt="{\displaystyle v&#39;}" /> and density <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/d215ec5b3d3b48ac8ec46e7131e7b3c091c9114e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.079ex; height:2.509ex;" alt="{\displaystyle n&#39;}" /> at its left and right and density <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/a601995d55609f2d9f5e233e36fbe9ea26011b3b" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.395ex; height:1.676ex;" alt="{\displaystyle n}" /> in the middle we define an auxiliary system with localized density <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/1f7d439671d1289b6a816e6af7a304be40608d64" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:1.202ex; height:2.176ex;" alt="{\displaystyle \rho }" /> in the middle and zero density outside. We choose <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/1f7d439671d1289b6a816e6af7a304be40608d64" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:1.202ex; height:2.176ex;" alt="{\displaystyle \rho }" /> such that <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/943b71b4831e5bba0c6efc6db83267a16559ccf3" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:10.615ex; height:3.009ex;" alt="{\displaystyle n=\rho +n&#39;}" />. We then solve the equations
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/49bf34392364e82361b394fa4250284482da1b6f" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.505ex; width:14.283ex; height:2.843ex;" alt="{\displaystyle \nabla ^{2}v&#39;=-4\pi n&#39;}" /> (with periodic boundary conditions)
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/a7330156d94cb82dafc38f38a4c6a98f34a4b648" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:12.923ex; height:3.176ex;" alt="{\displaystyle \nabla ^{2}u=-4\pi \rho }" /> (with a method suitable for finite charge distributions)
</p><p>and obtain the desired potential <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/e07b00e7fc0847fbd16391c778d65bc25c452597" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.128ex; height:1.676ex;" alt="{\displaystyle v}" /> as <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/05a3ab1cf388a59af08fed210bccdbf7b109776a" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.505ex; width:10.208ex; height:2.676ex;" alt="{\displaystyle v=u+v&#39;}" /> where we have to repeat <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/19f1c5b0af1f30c41cb8bc0ba3bfea98681da268" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.812ex; height:2.509ex;" alt="{\displaystyle v&#39;}" /> to fill up the central region.
</p><p>The calculation of <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/19f1c5b0af1f30c41cb8bc0ba3bfea98681da268" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.812ex; height:2.509ex;" alt="{\displaystyle v&#39;}" />, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/d215ec5b3d3b48ac8ec46e7131e7b3c091c9114e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.079ex; height:2.509ex;" alt="{\displaystyle n&#39;}" /> is taken care of by the periodic mode of Octopus. The second equation can be solved by direct summation (in 1D and 2D), FFT with cutoff, ISF, or multigrid (in 3D).
</p>
<h3><span class="mw-headline" id="A_note_for_2D_systems">A note for 2D systems</span></h3>
<p>We need <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/19f1c5b0af1f30c41cb8bc0ba3bfea98681da268" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.812ex; height:2.509ex;" alt="{\displaystyle v&#39;}" /> and <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/d215ec5b3d3b48ac8ec46e7131e7b3c091c9114e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.079ex; height:2.509ex;" alt="{\displaystyle n&#39;}" /> for a 2D-1D system (in the terminology of <sup id="cite_ref-cutoff_1-0" class="reference"><a href="#cite_note-cutoff-1">&#91;1&#93;</a></sup>). This is currently not implemented in Octopus but it should be possible to calculate the potential this way:
</p><p>We define the auxiliary periodic periodic interaction
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/8220439f9b441e3d190be6db0d50dc5755dcfa34" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.671ex; width:30.227ex; height:8.509ex;" alt="{\displaystyle \varphi (x,y)={\begin{cases}{\frac {1}{\sqrt {x^{2}+y^{2}}}},&amp;|y|\leq R\\0,&amp;|y|&gt;R\end{cases}}}" />
</p><p>so that we can write the Hartree convolution integral as
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/20f5a9db04e38bf8828a0a6cac448afe3df130d3" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.338ex; width:47.25ex; height:5.676ex;" alt="{\displaystyle v&#39;(x,y)=\int dx&#39;\int dy&#39;n&#39;(x&#39;,y&#39;)\varphi (x-x&#39;,y-y&#39;)}" />, with <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/4b0bfb3769bf24d80e15374dc37b0441e2616e33" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.764ex; height:2.176ex;" alt="{\displaystyle R}" /> being twice the size of the box in <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b8a6208ec717213d4317e666f1ae872e00620a0d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:1.155ex; height:2.009ex;" alt="{\displaystyle y}" />-direction.
</p><p>Calculating this integral amounts to finding the Fourier-transform of <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/0d6923e9cd43c9cf9c26e63f072d0cb89f76e781" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:6.849ex; height:2.843ex;" alt="{\displaystyle \varphi (x,y)}" /> which is
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/88401c3471d9e973cb5e95d680ebea33ca3e179c" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.505ex; width:48.301ex; height:6.343ex;" alt="{\displaystyle I(G_{x},G_{y})=\int _{-\infty }^{\infty }dx\int _{-R}^{R}dy{\frac {1}{x^{2}+y^{2}}}e^{-i(G_{x}x+G_{y}y)}.}" />
</p><p>Solving this integral is a bit hard, but Alberto suggested the following way to do it:
</p>
<pre>From: alberto@physik.fu-berlin.de
To: Florian Lorenzen &lt;lorenzen@physik.fu-berlin.de&gt;
Date: Tue, 22 Jul 2008 12:38:25 +0200 (CEST)
Subject: integral


Hi,

I was trying to figure out the integral, and I came up with the following
solution -- which I cannot integrate further. But they are one-dimensional
integrals, which should be fast:

- Assuming Gx .ne. 0:

  I(Gx,Gy) = 4 \int_0^R cos(Gy*y)*K_0(|Gx|y)

    where K_0 is the modified Bessel function of the second kind, order 0.
    I think that it is the correct solution because, if one takes R -&gt;
    infinity, one retrieves

    I(Gx,Gy) = (2*Pi)/|G|

    which is the expected result, that one obtains easily using polar
    coordinates.

- If Gx .eq. 0:

  I(0, Gy) = -4\int_0^R cos(Gy*y)*log(y)

  In particular, if both Gs are zero:

  I(0, 0) = R(-1+log(R))


Note that, strictly speaking, I(0,Gy) is infinite. But these infinites
should cancel out because of charge neutrality, as explained in the
article of Rozzi.

The 1D integrals that have to be done are singular at zero, which makes
them a bit more tricky, but I think that maybe using the GSL integration
capabilities, they should cause no problem.

Cheers,

Alberto.
</pre>
<p><br />
</p>
<h2><span class="mw-headline" id="Parallelizing_the_calculation_of_the_memory_coefficient_and_the_memory_term">Parallelizing the calculation of the memory coefficient and the memory term</span></h2>
<p>One of the bottlenecks of the open boundaries propagator is the calculation of the memory coefficients and the calculation of the memory term which are both of quadratic complexity in the number of timesteps.
</p><p>It should be possible to parallelize their calculation by domain parallelization with a certain partitioning and the usage of <a rel="nofollow" class="external text" href="http://www.netlib.org/scalapack/pblas_qref.html">PBLAS</a> instead of BLAS for all the matrix-matrix and matrix-vector multiplication.
</p><p>The idea is to partition the hrid in such a way that each node gets a share of the interface region, i. e. the part of the wave functions that are affected by the memory term. This is easily obtained by a partitioning in stripes (instead of calling METIS):
</p>
<div class="center"><div class="floatnone"><a href="/wiki/File:Open_boudaries_partition.png" class="image"><img alt="Open boudaries partition.png" src="/mediawiki/images/6/6e/Open_boudaries_partition.png" decoding="async" width="311" height="142" /></a></div></div>
<p>Calculating partitions of this shape for the parallepiped by hand is straightforward and a partitioning of the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/99a86c5231bb3cbb863d9d428ebe9ac8db8d4ffb" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:6.968ex; height:2.176ex;" alt="{\displaystyle N\times N}" /> (with <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/f5e3890c981ae85503089652feb48b191b57aae3" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.064ex; height:2.176ex;" alt="{\displaystyle N}" /> being the number of interface points) follows naturally which should be compatible with PBLAS requirements.
</p><p>Of course, this partitioning is not optimal with respect to ghost point communication but should pay off in the end because the dominant terms are calculated faster.
</p>
<h2><span class="mw-headline" id="References">References</span></h2>
<div class="mw-references-wrap"><ol class="references">
<li id="cite_note-cutoff-1"><span class="mw-cite-backlink"><a href="#cite_ref-cutoff_1-0">↑</a></span> <span class="reference-text">C. A. Rozzi, D. Varsano, A. Marini, E. K. U. Gross, and A. Rubio, "Exact Coulomb cutoff technique for supercell calculations", Phys. Rev. B 73, 205119, (2006); <a rel="nofollow" class="external free" href="http://link.aps.org/abstract/PRB/v73/e205119">http://link.aps.org/abstract/PRB/v73/e205119</a></span>
</li>
</ol></div>
<!-- 
NewPP limit report
Cached time: 20230412154432
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.509 seconds
Real time usage: 9.000 seconds
Preprocessor visited node count: 280/1000000
Preprocessor generated node count: 646/1000000
Post‐expand include size: 33/2097152 bytes
Template argument size: 19/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 8164/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00% 3905.388      1 -total
  0.16%    6.274      4 Template:Name
  0.12%    4.648      2 Template:Octopus
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1967-0!canonical!math=0 and timestamp 20230412154423 and revision id 7480
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;oldid=7480">http:///mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;oldid=7480</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers+Manual%3AOpen+Boundaries%2FTransport" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers_Manual:Open_Boundaries/Transport" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers_Manual:Open_Boundaries/Transport&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers_Manual:Open_Boundaries/Transport">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers_Manual:Open_Boundaries/Transport" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers_Manual:Open_Boundaries/Transport" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;oldid=7480" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers_Manual:Open_Boundaries/Transport&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 28 January 2015, at 11:03.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.509","walltime":"9.000","ppvisitednodes":{"value":280,"limit":1000000},"ppgeneratednodes":{"value":646,"limit":1000000},"postexpandincludesize":{"value":33,"limit":2097152},"templateargumentsize":{"value":19,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":8164,"limit":5000000},"timingprofile":["100.00% 3905.388      1 -total","  0.16%    6.274      4 Template:Name","  0.12%    4.648      2 Template:Octopus"]},"cachereport":{"timestamp":"20230412154432","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":9302});});</script>
</body>
</html>
