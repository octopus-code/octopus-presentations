<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers:Removed Features - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers:Removed_Features","wgTitle":"Developers:Removed Features","wgCurRevisionId":10673,"wgRevisionId":10673,"wgArticleId":1878,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers:Removed_Features","wgRelevantArticleId":1878,"wgRequestId":"4676f9bb02de48f99cd4dd5c","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgRedirectedFrom":"Developers_Manual:Removed_Features","wgInternalRedirectTargetUrl":"/wiki/Developers:Removed_Features"});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.view.redirect","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<link rel="canonical" href="http:///wiki/Developers:Removed_Features"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Removed_Features rootpage-Developers_Removed_Features skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers:Removed Features</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><span class="mw-redirectedfrom">(Redirected from <a href="/mediawiki/index.php?title=Developers_Manual:Removed_Features&amp;redirect=no" class="mw-redirect" title="Developers Manual:Removed Features">Developers Manual:Removed Features</a>)</span></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>This page documents features considered obsolete and thus removed from the code. Please include the revision number(s) of your removal commit, and a brief description of the code's functionality. This way, it is easier to recover if the necessity arises.
</p>
<ul><li><b>Complex scaling</b>: Removed in <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/merge_requests/329">MR:329</a> and <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/merge_requests/344">MR:344</a>. The implementation was too complicated to maintain.</li>
<li><b>Internal blas</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/34272ab">34272ab</a></span>. This was our own implementation of blas functions in Fortran 90, as blas is mandatory it is not necessary to have it.</li>
<li><b>Restart in plain format</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/0917190">0917190</a></span>. This is the old format of binary files using Fortran 90 binary output, replaced by the obf format.</li>
<li><b>oct-operator_prof</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/034fa76">034fa76</a></span>. An utility to profile the different versions of operate, this is now done autmatically by octopus, so it is obsolete.</li>
<li><b>Python code</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/f7ff5b1">f7ff5b1</a></span>. This is part of the GUI project that is not complete.</li>
<li><b>Forces by taking the derivative of the potential</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/7d1d03f">7d1d03f</a></span> Replaced by much more precise forces from the derivative of the wavefunctions.</li>
<li><b>PRE_INCOMPLETE_INVERSE preconditioner</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/9622a64">9622a64</a></span>. Never used, slow, and complicated.</li>
<li><b>ARPACK and JDQZ interfaces</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ae669cc">ae669cc</a></span>, external eigensolvers not really used.</li>
<li><b>TRLAN eigensolver</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ccc2914">ccc2914</a></span></li>
<li><b>td_transport</b> CalculationMode <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/67a3ee2">67a3ee2</a></span></li>
<li><b>Expokit</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/74b849b">74b849b</a></span>, replaced by our own implementation for the exponential of a dense matrix.</li>
<li><b>FFTW2 interface</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/6e20748">6e20748</a></span>, not widely used and outdated.</li>
<li><b>NetCDF restart</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/5f81ee3">5f81ee3</a></span>, not widely used.</li>
<li><b>Visscher propagator</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ff0643e">ff0643e</a></span>.</li>
<li><b>Split/Suzuki exponential/propagators</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/1257609">1257609</a></span>.</li>
<li><b>CDFT Hamiltonian terms</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/816625d">816625d</a></span>.</li>
<li><b>Calculation of <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/ba162c66ca85776c83557af5088cc6f8584d1912" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.637ex; height:2.676ex;" alt="{\displaystyle L^{2}}" /></b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/f7d0f0c">f7d0f0c</a></span>, not used and missing gauge corrections.</li>
<li><b>CUDA FFT interface</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/020dfdb">020dfdb</a></span>.</li>
<li><b>Changes in operate</b>:
<ul><li>Autodetection of the best operate: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/d013779">d013779</a></span>, replaced by static selection.</li>
<li>Blue Gene version of operate: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/b235c56">b235c56</a></span>, replaced by the vec version.</li>
<li>Itanium version of operate: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ad1b0ac">ad1b0ac</a></span>, afterwards an improved version will be included if necessary.</li>
<li>C version of operate: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/5285a02">5285a02</a></span>, replaced by the vec version.</li></ul></li>
<li><b>Scissor operator</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ad04ba4">ad04ba4</a></span>. It was not used.</li>
<li><b>libnbc (MPI non-blocking collective communication)</b>: Removed in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/fd20710">fd20710</a></span>, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/845fd09">845fd09</a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/e80ab25">e80ab25</a></span>.</li>
<li><b>StatesSaveMemory</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/f82af86">f82af86</a></span>.</li>
<li><b>StatesWindowSize</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/9c899ed">9c899ed</a></span>.</li>
<li><b>UseTopology</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/03695e1">03695e1</a></span>.</li>
<li><b>CalculationMode = raman</b>, never really implemented <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/00688ff">00688ff</a></span>.</li>
<li><b>OEP kernel in Sternheimer</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/652440f">652440f</a></span>.</li>
<li><b>Removed the old states parallelization routines</b>, replaced by scalapack versions:
<ul><li>Subspace diagonalization <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/a313948">a313948</a></span>.</li>
<li>Orthogonalization <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/84469fa">84469fa</a></span>.</li></ul></li>
<li><b>Current DFT</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/afd08e4">afd08e4</a></span>.</li>
<li><b>Removed the gridhier_m module</b>, no longer needed <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/c40ca77">c40ca77</a></span>.</li>
<li><b>Deleted nl_operator subroutines</b>: communication subroutines <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/a51ce7a">a51ce7a</a></span> and write and op_to_matrix <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/41022cb">41022cb</a></span> because they are not any longer used.</li>
<li><b>Deleted par_vec subroutines</b>: scatter boundary and selective gather nor scatter functions are not any longer used <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/64b1ffe">64b1ffe</a></span>.</li>
<li><b>Deleted mesh_functions subroutines</b>: copy, partial_integrate nor interpolate functions are not any longer used <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/069a4e2">069a4e2</a></span>.</li>
<li><b>Genetic algorithm mesh partititioner</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/c10cabf">c10cabf</a></span>.</li>
<li><b>translate_point function</b> from src/grid/mesh.F90 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ba1f2b1">ba1f2b1</a></span>.</li>
<li><b>Metis 4.0</b> replaced by Metis 5.1 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/bc41195">bc41195</a></span>.</li>
<li><b>Zoltan</b> no longer required since Metis 5.1 is open source <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/d84c0da">d84c0da</a></span>.</li>
<li><b>PFFT mesh partition</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/411a3b4">411a3b4</a></span>.</li>
<li><b>Phase-only optimal control</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/9075051">9075051</a></span>.</li>
<li><b>states_gather</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/92024f5">92024f5</a></span>.</li>
<li><b>mixing in optimal control</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/188310a">188310a</a></span>.</li>
<li><b>Car-Parrinello</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/83a0889">83a0889</a></span>.</li>
<li><b>CalculationMode = one_shot</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/b3599b6">b3599b6</a></span>.</li>
<li><b>Laguerre root solver</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/dcbd529">dcbd529</a></span>.</li>
<li><b>in oct-run_regression_test.pl:</b> clean.sh <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/4ef38eb">4ef38eb</a></span>, build-stamp <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/b3ff280">b3ff280</a></span>, matches.sh <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/bd90593">bd90593</a></span>.</li>
<li><b>mail report</b> in oct-run_testsuite.sh.in <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/19da204">19da204</a></span></li>
<li><b>Poisson SETE</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/c892f74">c892f74</a></span></li>
<li><b>SLATEC</b> external library, used only for SETE <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/1818ff6">1818ff6</a></span></li>
<li><b>periodic table</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/41c72d6">41c72d6</a></span></li>
<li><b>XCTailCorrection</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/eddfcf4">eddfcf4</a></span></li>
<li><b>Single precision</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/dfb825d">dfb825d</a></span> (configure option), <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/9c70286">9c70286</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/6d8a4ed">6d8a4ed</a></span> (checks for single precision mode), <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/af78d76">af78d76</a></span> (global declarations), <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ac31cb5">ac31cb5</a></span> (test).</li>
<li><b>Open systems</b> not maintained and not very useful <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/af13860">af13860</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/745ebb3">745ebb3</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/b347634">b347634</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/34c2ddf">34c2ddf</a></span>  <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/c2232b6">c2232b6</a></span>  <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/73225a4">73225a4</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/c06bc7f">c06bc7f</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/03fdcf3">03fdcf3</a></span>.</li>
<li><b>Input file datasets</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/d4bcdd5">d4bcdd5</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/945b885">945b885</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/72c203a">72c203a</a></span> (tests) <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/3f54bd4">3f54bd4</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/6a907e9">6a907e9</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/8661b99">8661b99</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/8164f6b">8164f6b</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/d10a30b">d10a30b</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/da476a1">da476a1</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/2df9d17">2df9d17</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/8c74879">8c74879</a></span></li>
<li><b>oct-liquid</b> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/c97ea11">c97ea11</a></span></li>
<li><b>c_pointer module</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ff0fa01">ff0fa01</a></span></li>
<li><b>BoxOffset</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/b5300ac">b5300ac</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/21b0c55">21b0c55</a></span> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/ef500f8">ef500f8</a></span></li>
<li><b>bader external library</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/3dba97d">3dba97d</a></span></li>
<li><b>Levy-Perdew estimation of LB94 energy</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/40afcda">40afcda</a></span></li>
<li><b>photoelectron spectrum phase-spectrum filter</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/9047105">9047105</a></span></li>
<li><b>X(eigensolve_scalapack)</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/5febcef">5febcef</a></span> (other routines elsewhere do eigensolves with ScaLAPACK)</li>
<li><b>QR orthogonalization</b>: slow and never used <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/2de8c18">2de8c18</a></span>.</li>
<li><b>GCM calculation mode</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/d824ea5">d824ea5</a></span> Generator-Coordinates Method (K. Capelle, <i>J. Chem. Phys.</i> <b>119</b>, 1285 (2003)), never used and not tested.</li>
<li><b>Self-consistent time propagation</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/05974c5">05974c5</a></span>. Replaced by a self-consistent version of the ETRS propagator.</li>
<li><b>Multigrid eigensolver</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/07dbadf">07dbadf</a></span> never fully implemented.</li>
<li><b>PAPI support</b>: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://gitlab.com/octopus-code/octopus/commit/6d26862">6d26862</a></span> for obsolete version of this library</li></ul>
<!-- 
NewPP limit report
Cached time: 20230412154220
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.163 seconds
Real time usage: 0.507 seconds
Preprocessor visited node count: 584/1000000
Preprocessor generated node count: 1980/1000000
Post‐expand include size: 9159/2097152 bytes
Template argument size: 1342/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 260/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  424.946      1 -total
  7.43%   31.572     95 Template:Commit
  1.02%    4.346      2 Template:MR
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1878-0!canonical!math=0 and timestamp 20230412154220 and revision id 10673
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers:Removed_Features&amp;oldid=10673">http:///mediawiki/index.php?title=Developers:Removed_Features&amp;oldid=10673</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers%3ARemoved+Features" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers:Removed_Features" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers:Removed_Features&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers:Removed_Features">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:Removed_Features&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:Removed_Features&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers:Removed_Features" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers:Removed_Features" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Developers:Removed_Features&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers:Removed_Features&amp;oldid=10673" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers:Removed_Features&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 29 November 2018, at 21:01.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.163","walltime":"0.507","ppvisitednodes":{"value":584,"limit":1000000},"ppgeneratednodes":{"value":1980,"limit":1000000},"postexpandincludesize":{"value":9159,"limit":2097152},"templateargumentsize":{"value":1342,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":260,"limit":5000000},"timingprofile":["100.00%  424.946      1 -total","  7.43%   31.572     95 Template:Commit","  1.02%    4.346      2 Template:MR"]},"cachereport":{"timestamp":"20230412154220","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":118});});</script>
</body>
</html>
