<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Octopus on GPUs - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Octopus_on_GPUs","wgTitle":"Tutorial:Octopus on GPUs","wgCurRevisionId":11444,"wgRevisionId":11444,"wgArticleId":3072,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Octopus_on_GPUs","wgRelevantArticleId":3072,"wgRequestId":"8eaae2b8967a62204f495bfc","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Octopus_on_GPUs rootpage-Tutorial_Octopus_on_GPUs skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Octopus on GPUs</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Octopus supports using GPUs and this support has been substantially improved in
the last years. It does not yet support all features, but for certain systems,
using GPUs can be very efficient. In this tutorial, you will learn how to run
Octopus on GPUs and also for which systems you can expect the best efficiency.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Supported_Features"><span class="tocnumber">1</span> <span class="toctext">Supported Features</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Implementation"><span class="tocnumber">2</span> <span class="toctext">Implementation</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Run_a_calculation_on_the_GPU"><span class="tocnumber">3</span> <span class="toctext">Run a calculation on the GPU</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Beneficial_use_cases"><span class="tocnumber">4</span> <span class="toctext">Beneficial use cases</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Multi-GPU_runs"><span class="tocnumber">5</span> <span class="toctext">Multi-GPU runs</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Domain_parallelization_for_larger_grids"><span class="tocnumber">6</span> <span class="toctext">Domain parallelization for larger grids</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Compiling_Octopus_for_GPUs"><span class="tocnumber">7</span> <span class="toctext">Compiling Octopus for GPUs</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#Using_NVLink_on_Raven"><span class="tocnumber">8</span> <span class="toctext">Using NVLink on Raven</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#Slurm_scripts"><span class="tocnumber">9</span> <span class="toctext">Slurm scripts</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Supported_Features">Supported Features</span></h2>
<p>In general, the most-often used features should work on GPUs and should be efficient as
well. If you run ground-state calculations, please use the RMMDIIS eigensolver. Time-dependent
calculations are supported and are the most efficient run mode (also the most-used run mode).
Some features do not work when running on GPUs or are quite inefficient, e.g. spin-orbit coupling, DFT+U, hybrid functionals.
Moreover, there can be an issue for periodic systems when spheres of pseudopotentials overlap with themselves due to the periodic boundary conditions.
</p><p>It is really worth trying your production workloads on GPUs - for quite a lot of applications, it is more
efficient than on CPUs and there will be more and more GPU resources available in the future.
</p><p>If you find any issues with functionality or performance, please notify the developers.
</p>
<h2><span class="mw-headline" id="Implementation">Implementation</span></h2>
<p>The implementation is based on CUDA and targets only NVIDIA GPUs at the moment.
As Octopus needs double-precision floating point operations, most consumer-grade
and gaming GPUs will only deliver inferior performance because they focus on
single-precision operations. The high-end HPC GPUs that are usually available in
computing centers deliver much better double-precision performance.
</p><p>When octopus is compiled with GPU support, it will detect if a GPU is available.
If there are several GPUs per node, you should generally use one MPI process per
GPU to be most efficient.
</p><p>In general, systems need to be large enough in number of grid points and number
of states to expose enough parallelism that the GPUs can exploit. If the system
is too small, the overhead of launching computations on the GPUs and of copying
data from and to the GPUs makes the computations less efficient than on CPUs.
For large systems, the performance of running on GPUs can be much larger than
the electricity consumption and the costs of the GPUs compared to CPUs.
</p>
<h2><span class="mw-headline" id="Run_a_calculation_on_the_GPU">Run a calculation on the GPU</span></h2>
<p>For this tutorial, we will use the same input as
in the <a href="/wiki/Tutorial:Scaling" title="Tutorial:Scaling">previous tutorial</a>.
Save the following as <b>inp</b> file:
</p>
<pre>CalculationMode = gs
FromScratch = yes

XYZCoordinates = "1ala.xyz"

Spacing = 0.2*angstrom
Radius = 4.0*angstrom

TDPropagator = aetrs
TDMaxSteps = 20
TDTimeStep = 0.05
</pre>
<p>and the following as "1ala.xyz":
</p>
<pre>  23
 units: A
      N                   -1.801560    0.333315   -1.308298
      C                   -1.692266    1.069227    0.012602
      C                   -0.217974    1.151372    0.425809
      O                    0.256888    2.203152    0.823267
      C                   -2.459655    0.319513    1.077471
      H                   -1.269452    0.827043   -2.046396
      H                   -1.440148   -0.634968   -1.234255
      H                   -2.791116    0.267637   -1.602373
      H                   -2.104621    2.114111   -0.129280
      H                   -2.391340    0.844513    2.046396
      H                   -2.090378   -0.708889    1.234538
      H                   -3.530691    0.246022    0.830204
      N                    0.476130   -0.012872    0.356408
      C                    1.893957   -0.046600    0.735408
      C                    2.681281    0.990593   -0.107455
      O                    3.486946    1.702127    0.516523
      O                    2.498931    1.021922   -1.333241
      C                    2.474208   -1.425485    0.459844
      H                    0.072921   -0.880981    0.005916
      H                    1.975132    0.211691    1.824463
      H                    1.936591   -2.203152    1.019733
      H                    3.530691   -1.461320    0.761975
      H                    2.422706   -1.683153   -0.610313
</pre>
<p>Then, run the ground state calculation or reuse the ground state from the
previous tutorial.
</p><p>You can now run your first calculation using a GPU: change the run mode to td
and use the batch script to run on one GPU as given at the end of this page.
In the output, you will see the following section that shows some information on
the GPU being used:
</p>
<pre>************************** GPU acceleration **************************
Rank 00000 uses device number 00000

Selected device:
      Framework              : CUDA
      Device type            : GPU
      Device vendor          : NVIDIA Corporation
      Device name            : Tesla V100-PCIE-32GB
      Cuda capabilities      : 7.0
      Driver version         : 11000
      Device memory          : 32510.500 MiB
      Local/shared memory    : 48.000 KiB
      Max. group/block size  : 1024

**********************************************************************
</pre>
<p>The parallelization section shows that one process is used because we want one
process per GPU. Because we use half a node, we can still use 20 OpenMP threads
which can be beneficial especially in the initialization where still some
computations are done on CPUs.
</p><p>At the end of the run, there is an additional section on allocations and
deallocations that shows how well the internal caching is used. Normally, you
can ignore this section.
</p><p>Now extract the timing of one timestep as we did in the previous tutorial. As a
reference, I have obtained 0.074s. To compare this using the same resources, but
no GPU, you can set the input variable <tt>DisableAccel = yes</tt> and submit
the same batch script again. This time, it will only run on CPUs. For this, I
get a time of 0.26. We can compute a speed-up from these timings, which is 3.5;
for your runs it might deviate slightly.
</p><p>This means that we get a speed-up factor of 3.5 when comparing half a GPU node
on cobra with half a CPU node for this rather small system. This is already
nice!
</p>
<h2><span class="mw-headline" id="Beneficial_use_cases">Beneficial use cases</span></h2>
<p>TD calculations are most optimized for GPUs, other run modes less so. For GS
runs, e.g., the orthogonalization and subspace diagonalization cannot make full
use of GPUs yet, which leads to smaller benefits for GS runs on GPUs.
</p><p>Moreover, grids need to be large enough to saturate the GPUs which are very
powerful. Each process should have more than <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/d7310d2ac4ddefd259b8a2c7c8fb89c6a9ea11a8" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:3.379ex; height:2.676ex;" alt="{\displaystyle 10^{5}}" /> grid points.
</p><p>NVIDIA GPUs are built such that 32 threads always execute the same instructions.
Because of this, it is most efficient to run octopus in such a way that each
process has a multiple of 32 states. The internal data structures of the code
are designed to expose this parallelism in the threads to the GPU.
</p><p>Parallelization in states (and k points) is more efficient than parallelization
in domains. This is also the case for CPU runs, but the different is more
pronounced on GPUs because the communication of the boundary points necessitates
also transfers between the GPUs or between the GPU and the CPU. We use
CUDA-aware MPI to enable direct transfers between the GPUs, but this cannot
completely compensate the overhead.
</p>
<h2><span class="mw-headline" id="Multi-GPU_runs">Multi-GPU runs</span></h2>
<p>To examine how octopus runs using several GPUs, you can use the second jobscript
at the end of the tutorial to submit a job to a full GPU node, using 2 MPI
processes and 2 GPUs. The output section on GPUs will show:
</p>
<pre>************************** GPU acceleration **************************
Rank 00001 uses device number 00001
Rank 00000 uses device number 00000

Selected device:
      Framework              : CUDA
      Device type            : GPU
      Device vendor          : NVIDIA Corporation
      Device name            : Tesla V100-PCIE-32GB
      Cuda capabilities      : 7.0
      Driver version         : 11000
      Device memory          : 32510.500 MiB
      Local/shared memory    : 48.000 KiB
      Max. group/block size  : 1024

**********************************************************************
</pre>
<p>which confirms that 2 GPUs are used. As you can see in the parallelization
section, be default states parallelization is used, which results in 16 states
per process.
</p><p>Extract the timing from the output. I obtain 0.043s, corresponding to a speed-up
of about 1.72 and a parallel efficiency of 86%. Although 16 states per process
is not optimal, the parallel efficiency is still very good.
</p><p>Now run the same calculation on 2 full nodes and determine the timing and
compute the speed-up and parallel efficiency. What do you see? Is it still
efficient?
</p><p>Next, we will run the same calculation on one node, but using domain
parallelization. For this, add <tt>ParStates = 1</tt> to the input file and
submit the jobscript again. Examine the parallelization section in the output to
ensure that domain parallelization has been used. Extract the timing from the
output and compute the speed-up and parallel efficiency. I obtain about 0.090s
which gives a speed-up of 0.82 - the code becomes slower! This is due to the
small grid in this system which does not expose enough parallelism to distribute
it efficiently to two GPUs - the overhead of the parallelization scheme (e.g.
communicating ghost points) is too large.
</p>
<h2><span class="mw-headline" id="Domain_parallelization_for_larger_grids">Domain parallelization for larger grids</span></h2>
<p>Set the spacing to <tt>0.15</tt> and rerun the ground state calculation (you
need to delete the restart folder before). Then repeat the runs on one GPU and
on 2 GPUs using parallelization in domains. What do you observe? Is this
parallelization scheme more efficient for the larger grid?
</p><p><br />
</p>
<h2><span class="mw-headline" id="Compiling_Octopus_for_GPUs">Compiling Octopus for GPUs</span></h2>
<p>If you compile octopus yourself, you need to specify the flag
"--enable-cuda" to enable CUDA support. If your CUDA installation is in a
non-standard path, you additionally need to specify "--with-cuda-prefix=DIR" to
point to the corresponding CUDA installation directory. If you would like to do
profiling on GPUs using Nsight systems, it is advisable to add the flag "--enable-nvtx" which will add
support for NVTX.
</p><p><br />
</p>
<h2><span class="mw-headline" id="Using_NVLink_on_Raven">Using NVLink on Raven</span></h2>
<p>If the domain parallelization on larger grids is not efficient on cobra, you can try to use a system that features
a fast interconnect (e.g. NVLink), such as the raven supercomputer at MPCDF. If you have a normal account at MPCDF: run the same
example on raven. The GPU nodes on raven have a fast interconnect on the board
(NVLink) which can massively speed up domain parallelization runs which use
CUDA-aware MPI. You need to load the modules "gcc/10 cuda/11.2 openmpi_gpu/4
octopus-gpu/11" in order to use the octopus version specifically built to make
use of CUDA-aware MPI. Run the example on 1, 2, and 4 GPUs with domain
parallelization. What do you observe? Is it more efficient on raven?
</p><p><br />
</p>
<h2><span class="mw-headline" id="Slurm_scripts">Slurm scripts</span></h2>
<p>To run octopus on the cobra supercomputer using one GPU, you can use the
following script:
</p>
<pre>#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
#
# Job Name:
#SBATCH -J octopus_course
#
# Reservation:
#SBATCH --reservation=mpsd_course
#
# Node feature:
#SBATCH --constraint="gpu"
# Specify type and number of GPUs to use:
#SBATCH --gres=gpu:v100:1       # Use only 1 GPU of a shared node
#SBATCH --mem=92500             # Memory is necessary if using only 1 GPU
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
# for OpenMP:
#SBATCH --cpus-per-task=20
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# wall clock limit:
#SBATCH --time=00:05:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=cores

# Run the program:
module purge
module load gcc/9 cuda/11.0 impi/2019.9 octopus-gpu/11
srun octopus
</pre>
<p>It will run on a node with NVIDIA V100 GPUs and use 1 of them.
</p><p>To run on a full cobra node with 2 GPUs, you can use the following script:
</p>
<pre>#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
#
# Job Name:
#SBATCH -J octopus_course
#
# Reservation:
#SBATCH --reservation=mpsd_course
#
# Node feature:
#SBATCH --constraint="gpu"
# Specify type and number of GPUs to use:
#SBATCH --gres=gpu:v100:2       # Use both GPUs of a node
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
# for OpenMP:
#SBATCH --cpus-per-task=20
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# wall clock limit:
#SBATCH --time=00:05:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=cores

# Run the program:
module purge
module load gcc/9 cuda/11.0 impi/2019.9 octopus-gpu/11
srun octopus
</pre>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Scaling" title="Tutorial:Scaling">Scaling</a> - Next <a href="/wiki/Tutorial:Profiling" title="Tutorial:Profiling">Profiling</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Running_Octopus_on_HPC_systems" title="Tutorial Series:Running Octopus on HPC systems">Running Octopus on HPC systems</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155110
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.077 seconds
Real time usage: 0.417 seconds
Preprocessor visited node count: 209/1000000
Preprocessor generated node count: 577/1000000
Post‐expand include size: 924/2097152 bytes
Template argument size: 943/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 4790/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   27.236      1 Template:Tutorial_foot
100.00%   27.236      1 -total
 93.05%   25.343      4 Template:If
 63.92%   17.408      4 Template:P2
 36.54%    9.951      4 Template:P1
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:3072-0!canonical!math=0 and timestamp 20230412155110 and revision id 11444
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;oldid=11444">http:///mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;oldid=11444</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AOctopus+on+GPUs" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Octopus_on_GPUs" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Octopus_on_GPUs&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Octopus_on_GPUs">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Octopus_on_GPUs" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Octopus_on_GPUs" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;oldid=11444" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Octopus_on_GPUs&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 10 September 2021, at 15:57.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.077","walltime":"0.417","ppvisitednodes":{"value":209,"limit":1000000},"ppgeneratednodes":{"value":577,"limit":1000000},"postexpandincludesize":{"value":924,"limit":2097152},"templateargumentsize":{"value":943,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":4790,"limit":5000000},"timingprofile":["100.00%   27.236      1 Template:Tutorial_foot","100.00%   27.236      1 -total"," 93.05%   25.343      4 Template:If"," 63.92%   17.408      4 Template:P2"," 36.54%    9.951      4 Template:P1"]},"cachereport":{"timestamp":"20230412155110","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":567});});</script>
</body>
</html>
