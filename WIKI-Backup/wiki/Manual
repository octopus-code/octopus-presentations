<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual","wgTitle":"Manual","wgCurRevisionId":8434,"wgRevisionId":8434,"wgArticleId":1537,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual","wgRelevantArticleId":1537,"wgRequestId":"38b95e29b78b8b40e75b78c4","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual rootpage-Manual skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><hr class="noprint" />
<p><span class="noprint"><i>
If you are going to collaborate, please check <a href="/wiki/Developers_Manual:Writing_Documentation" class="mw-redirect" title="Developers Manual:Writing Documentation">Writing Documentation</a>.</i><br /></span>
<span class="noprint"><i>
If you want to know what you can do check for open tickets: <a rel="nofollow" class="external autonumber" href="http://www.tddft.org/trac/octopus/query?status=new&amp;status=assigned&amp;status=reopened&amp;group=priority&amp;component=doc&amp;keywords=%7Ewiki&amp;order=priority">[1]</a></i>
</span>
<br class="noprint" />
<span class="noprint">If you want to print or download the whole manual there is a [/mediawiki/index.php?title=Special:PdfPrint&amp;page=The_Octopus_Manual PDF version] available.<br />
</span>
</p>
<hr class="noprint" /><p><br class="noprint" />
</p><dl><dd><a href="/wiki/Manual:About_this_manual" title="Manual:About this manual">About This Manual</a></dd>
<dd><a href="/wiki/Manual:About_Octopus" title="Manual:About Octopus">About Octopus</a></dd>
<dd><a href="/wiki/Manual:Installation" title="Manual:Installation">Installation</a></dd>
<dd>Basics
<dl><dd><a href="/wiki/Manual:Input_file" title="Manual:Input file">The input file</a></dd>
<dd><a href="/wiki/Manual:Running_Octopus" title="Manual:Running Octopus">Running Octopus</a></dd>
<dd><a href="/wiki/Manual:Units" title="Manual:Units">Units</a></dd>
<dd><a href="/wiki/Manual:Physical_System" title="Manual:Physical System">Physical System</a></dd>
<dd><a href="/wiki/Manual:Hamiltonian" title="Manual:Hamiltonian">Hamiltonian</a></dd>
<dd><a href="/wiki/Manual:Discretization" title="Manual:Discretization">Discretization</a></dd>
<dd><a href="/wiki/Manual:Output" title="Manual:Output">Output</a></dd>
<dd><a href="/wiki/Manual:Symmetry" title="Manual:Symmetry">Symmetry</a></dd>
<dd><a href="/wiki/Manual:Troubleshooting" title="Manual:Troubleshooting">Troubleshooting</a></dd></dl></dd>
<dd>Calculations
<dl><dd><a href="/wiki/Manual:Ground_State" title="Manual:Ground State">Ground State</a></dd>
<dd><a href="/wiki/Manual:Time-Dependent" title="Manual:Time-Dependent">Time-Dependent</a></dd>
<dd><a href="/wiki/Manual:Casida" title="Manual:Casida">Casida Linear Response</a></dd>
<dd><a href="/wiki/Manual:Linear_Response" title="Manual:Linear Response">Sternheimer Linear Response</a></dd>
<dd><a href="/wiki/Manual:Optimal_Control" title="Manual:Optimal Control">Optimal Control</a></dd>
<dd><a href="/wiki/Manual:Geometry_Optimization" title="Manual:Geometry Optimization">Geometry Optimization</a></dd></dl></dd>
<dd><a href="/wiki/Manual:Visualization" title="Manual:Visualization">Visualization</a></dd>
<dd><a href="/wiki/Manual:Advanced_ways_of_running_Octopus" title="Manual:Advanced ways of running Octopus">Advanced ways of running Octopus</a></dd>
<dd>Utilities
<dl><dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-analyze_projections&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-analyze projections (page does not exist)">oct-analyze_projections</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-atomic_occupations" title="Manual:External utilities:oct-atomic occupations">oct-atomic_occupations</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-casida_spectrum" title="Manual:External utilities:oct-casida spectrum">oct-casida_spectrum</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-center-geom" title="Manual:External utilities:oct-center-geom">oct-center-geom</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-check_deallocs&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-check deallocs (page does not exist)">oct-check_deallocs</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-conductivity&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-conductivity (page does not exist)">oct-conductivity</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-convert" title="Manual:External utilities:oct-convert">oct-convert</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-dielectric-function&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-dielectric-function (page does not exist)">oct-dielectric-function</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-display_partitions" title="Manual:External utilities:oct-display partitions">oct-display_partitions</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-harmonic-spectrum" title="Manual:External utilities:oct-harmonic-spectrum">oct-harmonic-spectrum</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-help" title="Manual:External utilities:oct-help">oct-help</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-infrared_spectrum&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-infrared spectrum (page does not exist)">oct-infrared_spectrum</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-local_multipoles&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-local multipoles (page does not exist)">oct-local_multipoles</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-oscillator-strength&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-oscillator-strength (page does not exist)">oct-oscillator-strength</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-photoelectron_spectrum&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-photoelectron spectrum (page does not exist)">oct-photoelectron_spectrum</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-propagation_spectrum" title="Manual:External utilities:oct-propagation spectrum">oct-propagation_spectrum</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-run_periodic_table" title="Manual:External utilities:oct-run periodic table">oct-run_periodic_table</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-run_regression_test.pl" title="Manual:External utilities:oct-run regression test.pl">oct-run_regression_test.pl</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-run_testsuite.sh" title="Manual:External utilities:oct-run testsuite.sh">oct-run_testsuite.sh</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-vdW_c6&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-vdW c6 (page does not exist)">oct-vdW_c6</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-vibrational_spectrum" title="Manual:External utilities:oct-vibrational spectrum">oct-vibrational_spectrum</a></dd>
<dd><a href="/wiki/Manual:External_utilities:oct-xyz-anim" title="Manual:External utilities:oct-xyz-anim">oct-xyz-anim</a></dd>
<dd><a href="/wiki/Manual:Deprecated_Utilities" title="Manual:Deprecated Utilities">Deprecated Utilities</a></dd></dl></dd>
<dd>Examples
<dl><dd><a href="/mediawiki/index.php?title=Manual:Examples:Hello_world&amp;action=edit&amp;redlink=1" class="new" title="Manual:Examples:Hello world (page does not exist)">Hello World</a></dd>
<dd><a href="/mediawiki/index.php?title=Manual:Examples:Benzene&amp;action=edit&amp;redlink=1" class="new" title="Manual:Examples:Benzene (page does not exist)">Benzene</a></dd></dl></dd>
<dd>Appendix
<dl><dd><a href="/wiki/Manual:Updating_to_a_new_version" title="Manual:Updating to a new version">Appendix: Updating to a new version</a></dd>
<dd><a href="/wiki/Manual:Building_from_scratch" title="Manual:Building from scratch">Appendix: Building from scratch</a></dd>
<dd><a href="/wiki/Tutorial:Running_Octopus_on_Graphical_Processing_Units_(GPUs)" title="Tutorial:Running Octopus on Graphical Processing Units (GPUs)">Appendix: Octopus with GPU support</a></dd>
<dd><a href="/wiki/Manual:Specific_architectures" title="Manual:Specific architectures">Appendix: Specific architectures</a></dd>
<dd><a href="/wiki/Manual:Appendix:Porting_Octopus_and_Platform_Specific_Instructions" title="Manual:Appendix:Porting Octopus and Platform Specific Instructions">Appendix: Porting Octopus and Platform Specific Instructions</a></dd>
<dd><a href="/wiki/Manual:Appendix:Reference_Manual" title="Manual:Appendix:Reference Manual">Appendix: Reference Manual</a></dd>
<dd><a href="/wiki/Manual:Appendix:Copying" title="Manual:Appendix:Copying">Appendix: Copying</a></dd></dl></dd></dl>
<p><br />
</p>
<span class="noprint"><hr /></span>
<p><span class="noprint">Back to <a href="/wiki/Documentation" title="Documentation">Documentation</a></span>
</p>
<!-- 
NewPP limit report
Cached time: 20230412153807
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.090 seconds
Real time usage: 0.298 seconds
Preprocessor visited node count: 402/1000000
Preprocessor generated node count: 2094/1000000
Post‐expand include size: 3571/2097152 bytes
Template argument size: 2798/2097152 bytes
Highest expansion depth: 3/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  243.184      1 -total
 69.80%  169.754      5 Template:Manual_page
 18.09%   43.989     47 Template:Manual_subpage
  4.73%   11.492      5 Template:Manual_title
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1537-0!canonical and timestamp 20230412153807 and revision id 8434
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual&amp;oldid=8434">http:///mediawiki/index.php?title=Manual&amp;oldid=8434</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk"><span><a href="/wiki/Talk:Manual" rel="discussion" title="Discussion about the content page [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Manual&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual&amp;oldid=8434" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 7 September 2016, at 13:00.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.090","walltime":"0.298","ppvisitednodes":{"value":402,"limit":1000000},"ppgeneratednodes":{"value":2094,"limit":1000000},"postexpandincludesize":{"value":3571,"limit":2097152},"templateargumentsize":{"value":2798,"limit":2097152},"expansiondepth":{"value":3,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%  243.184      1 -total"," 69.80%  169.754      5 Template:Manual_page"," 18.09%   43.989     47 Template:Manual_subpage","  4.73%   11.492      5 Template:Manual_title"]},"cachereport":{"timestamp":"20230412153807","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":440});});</script>
</body>
</html>
