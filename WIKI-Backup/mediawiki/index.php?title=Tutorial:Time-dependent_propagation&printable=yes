<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Time-dependent propagation - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Time-dependent_propagation","wgTitle":"Tutorial:Time-dependent propagation","wgCurRevisionId":11410,"wgRevisionId":11410,"wgArticleId":1332,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Basic","Time-dependent","Molecule","Pseudopotentials","DFT","Laser"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Time-dependent_propagation","wgRelevantArticleId":1332,"wgRequestId":"5f5a1f9d9095718317484bff","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.cite.styles":"ready","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["ext.cite.ux-enhancements","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.cite.styles%7Cext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Time-dependent_propagation rootpage-Tutorial_Time-dependent_propagation skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Time-dependent propagation</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>OK, it is about time to do TDDFT. Let us perform a calculation of the time evolution of the density of the methane molecule. 
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Ground-state"><span class="tocnumber">1</span> <span class="toctext">Ground-state</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Time-dependent"><span class="tocnumber">2</span> <span class="toctext">Time-dependent</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Input"><span class="tocnumber">2.1</span> <span class="toctext">Input</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Output"><span class="tocnumber">2.2</span> <span class="toctext">Output</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#The_time_step"><span class="tocnumber">3</span> <span class="toctext">The time step</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Laser_fields"><span class="tocnumber">4</span> <span class="toctext">Laser fields</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#References"><span class="tocnumber">5</span> <span class="toctext">References</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Ground-state">Ground-state</span></h2>
<p>The reason to do first a ground-state DFT calculation is that this will be the initial state for the real-time calculation.
</p><p>Run the input file below to obtain a proper ground-state (stored in the <tt>restart</tt> directory) as from the <a href="/wiki/Tutorial:Total_energy_convergence" title="Tutorial:Total energy convergence">Total energy convergence</a> tutorial. (Now we are using a more accurate bond length than before.)
</p>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = gs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsOutput"><code>UnitsOutput</code></a></span> = eV_Angstrom

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 3.5*angstrom
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.18*angstrom

CH = 1.097*angstrom
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Coordinates"><code>Coordinates</code></a></span>
  "C" |           0 |          0 |           0
  "H" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)
  "H" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)
  "H" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)
  "H" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)
%
</pre>
<h2><span class="mw-headline" id="Time-dependent">Time-dependent</span></h2>
<h4><span class="mw-headline" id="Input">Input</span></h4>
<p>Now that we have a starting point for our time-dependent calculation, we modify the input file so that it looks like this:
</p>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = td
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsOutput"><code>UnitsOutput</code></a></span> = eV_Angstrom

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 3.5*angstrom
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.18*angstrom

CH = 1.097*angstrom
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Coordinates"><code>Coordinates</code></a></span>
  "C" |           0 |          0 |           0
  "H" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)
  "H" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)
  "H" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)
  "H" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)
%
 
Tf  = 0.1/eV
dt = 0.002/eV

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPropagator"><code>TDPropagator</code></a></span> = aetrs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDMaxSteps"><code>TDMaxSteps</code></a></span> = Tf/dt
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDTimeStep"><code>TDTimeStep</code></a></span> = dt
</pre>
<p>Here we have switched on a new run mode, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> <tt>= td</tt> instead of <tt>gs</tt>, to do a time-dependent run. We have also added three new input variables:
</p>
<ul><li><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPropagator"><code>TDPropagator</code></a></span>: which algorithm will be used to approximate the evolution operator. Octopus has a large number of possible propagators that you can use (see Ref.<sup id="cite_ref-1" class="reference"><a href="#cite_note-1">&#91;1&#93;</a></sup> for an overview).</li></ul>
<ul><li><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDMaxSteps"><code>TDMaxSteps</code></a></span>: the number of time-propagation steps that will be performed.</li>
<li><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDTimeStep"><code>TDTimeStep</code></a></span>: the length of each time step.</li></ul>
<p>Note that, for convenience, we have previously defined a couple of variables, <tt>Tf</tt> and <tt>dt</tt>. We have made use of one of the possible propagators, <tt>aetrs</tt>. The manual explains about the possible options; in practice this choice is usually good except for very long propagation where the <tt>etrs</tt> propagator can be more stable.
</p>
<h4><span class="mw-headline" id="Output">Output</span></h4>
<p>Now run Octopus. This should take a few seconds, depending on the speed of your machine. The most relevant chunks of the standard output are
</p>
<pre>Input: [IonsConstantVelocity = no]
Input: [Thermostat = none]
Input: [MoveIons = no]
Input: [TDIonicTimeScale = 1.000]
Input: [TDTimeStep = 0.2000E-02 hbar/eV]
Input: [TDPropagationTime = 0.1000 hbar/eV]
Input: [TDMaxSteps = 50]
Input: [TDDynamics = ehrenfest]
Input: [TDScissor = 0.000]
Input: [TDPropagator = aetrs]
Input: [TDExponentialMethod = taylor]
</pre>
<p>...
</p>
<pre>********************* Time-Dependent Simulation **********************
  Iter           Time        Energy   SC Steps    Elapsed Time
**********************************************************************

      1       0.002000   -218.785065         1         0.118
      2       0.004000   -218.785065         1         0.121
      3       0.006000   -218.785065         1         0.118
</pre>
<p>...
</p>
<pre>     49       0.098000   -218.785065         1         0.123
     50       0.100000   -218.785065         1         0.130
</pre>
<p>It is worthwhile to comment on a few things:
</p>
<ul><li>We have just performed the time-evolution of the system, departing from the ground-state, under the influence of no external perturbation. As a consequence, the electronic system does not evolve. The total energy does not change (this you may already see in the output file, the third column of numbers), nor should any other observable change. However, this kind of run is useful to check that the parameters that define the time evolution are correct.</li>
<li>As the evolution is performed, the code probes some observables and prints them out. These are placed in some files under the directory <b><tt>td.general</tt></b>, which should show up in the working directory. In this case, only two files show up, the <b><tt>td.general/energy</tt></b>, and the <b><tt>td.general/multipoles</tt></b> files. The <b><tt>td.general/multipoles</tt></b> file contains a large number of columns of data. Each line corresponds to one of the time steps of the evolution (except for the first three lines, that start with a <tt>#</tt> symbol, which give the meaning of the numbers contained in each column, and their units). A brief overview of the information contained in this file follows:
<ul><li>The first column is just the iteration number.</li>
<li>The second column is the time.</li>
<li>The third column is the dipole moment of the electronic system, along the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/87f9e315fd7e2ba406057a97300593c4802b53e4" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.33ex; height:1.676ex;" alt="{\displaystyle x}" />-direction: <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/79c519c227d6d56f73c74670d680c2566fd1879c" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.338ex; width:28.002ex; height:5.676ex;" alt="{\displaystyle \langle \Phi (t)\vert {\hat {x}}\vert \Phi (t)\rangle =\int \!\!{\rm {d}}^{3}r\;x\,n(r)}" />. Next are the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b8a6208ec717213d4317e666f1ae872e00620a0d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:1.155ex; height:2.009ex;" alt="{\displaystyle y}" />- and <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/bf368e72c009decd9b6686ee84a375632e11de98" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.088ex; height:1.676ex;" alt="{\displaystyle z}" />-components of the dipole moment.</li>
<li>The file <b><tt>td.general/energy</tt></b> contains the different components of the total energy.</li></ul></li></ul>
<ul><li>It is possible to restart a time-dependent run. Try that now. Just increase the value of <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDMaxSteps"><code>TDMaxSteps</code></a></span> and rerun Octopus. If, however, you want to start the evolution from scratch, you should set the variable <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=FromScratch"><code>FromScratch</code></a></span> to <tt>yes</tt>.</li></ul>
<h2><span class="mw-headline" id="The_time_step">The time step</span></h2>
<p>A key parameter is, of course, the time step, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDTimeStep"><code>TDTimeStep</code></a></span>. Before making long calculations, it is worthwhile spending some time choosing the largest time-step possible, to reduce the number of steps needed. This time-step depends crucially on the system under consideration, the spacing, on the applied perturbation, and on the algorithm chosen to approximate the evolution operator. 
</p><p>In this example, try to change the time-step and to rerun the time-evolution. Make sure you are using <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDMaxSteps"><code>TDMaxSteps</code></a></span> of at least 100, as with shorter runs an instability might not appear yet. You will see that for time-steps larger than <tt>0.0024</tt> the propagation gets unstable and the total energy of the system is no longer conserved. Very often it diverges rapidly or even becomes NaN.
</p><p>Also, there is another input variable that we did not set explicitly, relying on its default value, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDExponentialMethod"><code>TDExponentialMethod</code></a></span>. Since most propagators rely on algorithms to calculate the action of the exponential of the Hamiltonian, one can specify which algorithm can be used for this purpose.
</p><p>You may want to learn about the possible options that may be taken by <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDExponentialMethod"><code>TDExponentialMethod</code></a></span>, and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPropagator"><code>TDPropagator</code></a></span> -- take a look at the manual. You can now try some exercises:
</p>
<ul><li>Fixing the propagator algorithm (for example, to the default value), investigate how the several exponentiation methods work (Taylor, Chebyshev, and Lanczos). This means finding out what maximum time-step one can use without compromising the proper evolution.</li></ul>
<ul><li>And fixing now the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDExponentialMethod"><code>TDExponentialMethod</code></a></span>, one can now play around with the various propagators.</li></ul>
<h2><span class="mw-headline" id="Laser_fields">Laser fields</span></h2>
<p>Now we will add a time-dependent external perturbation (a laser field) to the molecular Hamiltonian. For brevity, we will omit the beginning of the file, as this is left unchanged. The relevant part of the input file, with the modifications and additions is:
</p>
<pre>Tf  = 1/eV
dt = 0.002/eV
  
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPropagator"><code>TDPropagator</code></a></span> = aetrs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDMaxSteps"><code>TDMaxSteps</code></a></span> = Tf/dt
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDTimeStep"><code>TDTimeStep</code></a></span> = dt

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=FromScratch"><code>FromScratch</code></a></span> = yes
 
amplitude = 1*eV/angstrom
omega = 18.0*eV
tau0 = 0.5/eV
t0 = tau0

%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDExternalFields"><code>TDExternalFields</code></a></span>
  electric_field | 1 | 0 | 0 | omega | "envelope_cos"
%
 
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDFunctions"><code>TDFunctions</code></a></span>
  "envelope_cos" | tdf_cosinoidal | amplitude | tau0 | t0
%

%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDOutput"><code>TDOutput</code></a></span>
  laser
  multipoles
%
</pre>
<div class="thumb tright"><div class="thumbinner" style="width:422px;"><a href="/wiki/File:Tutorial_TD_Laser.png" class="image"><img alt="" src="/mediawiki/images/thumb/c/cd/Tutorial_TD_Laser.png/420px-Tutorial_TD_Laser.png" decoding="async" width="420" height="325" class="thumbimage" srcset="/mediawiki/images/thumb/c/cd/Tutorial_TD_Laser.png/630px-Tutorial_TD_Laser.png 1.5x, /mediawiki/images/thumb/c/cd/Tutorial_TD_Laser.png/840px-Tutorial_TD_Laser.png 2x" /></a>  <div class="thumbcaption"><div class="magnify"><a href="/wiki/File:Tutorial_TD_Laser.png" class="internal" title="Enlarge"></a></div>Laser field used in the tutorial</div></div></div>
<p>The most important variables here are the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDExternalFields"><code>TDExternalFields</code></a></span> block and the associated <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDFunctions"><code>TDFunctions</code></a></span> block. You should carefully read the manual page dedicated to these variables: the particular laser pulse that we have employed is the one whose envelope function is a cosine.
</p><p>Now you are ready to set up a run with a laser field. Be careful to set a total time of propagation able to accommodate the laser shot, or even more if you want to see what happens afterwards. You may also want to consult the meaning of the variable <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDOutput"><code>TDOutput</code></a></span>.
</p><p>A couple of important comments:
</p>
<ul><li>You may supply several laser pulses: simply add more lines to the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDExternalFields"><code>TDExternalFields</code></a></span> block and, if needed, to the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDFunctions"><code>TDFunctions</code></a></span> block.</li>
<li>We have added the <tt>laser</tt> option to <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDOutput"><code>TDOutput</code></a></span> variable, so that the laser field is printed in the file <b><tt>td.general/laser</tt></b>. This is done immediately at the beginning of the run, so you can check that the laser is correct without waiting.</li>
<li>You can have an idea of the response to the field by looking at the dipole moment in the <b><tt>td.general/multipoles</tt></b>. What physical observable can be calculated from the response?</li>
<li>When an external field is present, one may expect that unbound states may be excited, leading to ionization. In the calculations, this is reflected by density reaching the borders of the box. In these cases, the proper way to proceed is to setup absorbing boundaries (variables <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=AbsorbingBoundaries"><code>AbsorbingBoundaries</code></a></span>, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=ABWidth"><code>ABWidth</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=ABCapHeight"><code>ABCapHeight</code></a></span>).</li></ul>
<h2><span class="mw-headline" id="References">References</span></h2>
<div class="mw-references-wrap"><ol class="references">
<li id="cite_note-1"><span class="mw-cite-backlink"><a href="#cite_ref-1">↑</a></span> <span class="reference-text">
A. Castro, M.A.L. Marques, and A. Rubio, <i>Propagators for the time-dependent Kohn–Sham equations</i>, <a rel="nofollow" class="external text" href="http://dx.doi.org/10.1063/1.1774980">J. Chem. Phys.</a> <b>121</b> 3425-3433 (2004) </span>
</li>
</ol></div>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Centering_a_geometry" title="Tutorial:Centering a geometry">Centering a geometry</a> - Next <a href="/wiki/Tutorial:Recipe" title="Tutorial:Recipe">Recipe</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Octopus_basics" title="Tutorial Series:Octopus basics">Octopus basics</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155052
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.310 seconds
Real time usage: 1.744 seconds
Preprocessor visited node count: 1153/1000000
Preprocessor generated node count: 2243/1000000
Post‐expand include size: 8599/2097152 bytes
Template argument size: 3105/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 2532/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00% 1656.578      1 -total
  5.31%   88.025     41 Template:Variable
  2.32%   38.449     41 Template:Octopus_version
  1.69%   28.044      5 Template:If
  1.20%   19.915      6 Template:P2
  1.09%   17.979      1 Template:Tutorial_foot
  0.94%   15.602      1 Template:Article
  0.85%   14.040     41 Template:Code
  0.63%   10.357      4 Template:P1
  0.38%    6.273     41 Template:Octopus_major_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1332-0!canonical!math=0 and timestamp 20230412155051 and revision id 11410
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Time-dependent_propagation&amp;oldid=11410">http:///mediawiki/index.php?title=Tutorial:Time-dependent_propagation&amp;oldid=11410</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Basic" title="Category:Basic">Basic</a></li><li><a href="/wiki/Category:Time-dependent" title="Category:Time-dependent">Time-dependent</a></li><li><a href="/wiki/Category:Molecule" title="Category:Molecule">Molecule</a></li><li><a href="/wiki/Category:Pseudopotentials" title="Category:Pseudopotentials">Pseudopotentials</a></li><li><a href="/wiki/Category:DFT" title="Category:DFT">DFT</a></li><li><a href="/wiki/Category:Laser" title="Category:Laser">Laser</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ATime-dependent+propagation&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Time-dependent_propagation" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Time-dependent_propagation&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Time-dependent_propagation">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Time-dependent_propagation&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Time-dependent_propagation&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Time-dependent_propagation" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Time-dependent_propagation" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Time-dependent_propagation&amp;oldid=11410" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Time-dependent_propagation&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 6 September 2021, at 12:37.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.310","walltime":"1.744","ppvisitednodes":{"value":1153,"limit":1000000},"ppgeneratednodes":{"value":2243,"limit":1000000},"postexpandincludesize":{"value":8599,"limit":2097152},"templateargumentsize":{"value":3105,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":2532,"limit":5000000},"timingprofile":["100.00% 1656.578      1 -total","  5.31%   88.025     41 Template:Variable","  2.32%   38.449     41 Template:Octopus_version","  1.69%   28.044      5 Template:If","  1.20%   19.915      6 Template:P2","  1.09%   17.979      1 Template:Tutorial_foot","  0.94%   15.602      1 Template:Article","  0.85%   14.040     41 Template:Code","  0.63%   10.357      4 Template:P1","  0.38%    6.273     41 Template:Octopus_major_version"]},"cachereport":{"timestamp":"20230412155052","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":113});});</script>
</body>
</html>
