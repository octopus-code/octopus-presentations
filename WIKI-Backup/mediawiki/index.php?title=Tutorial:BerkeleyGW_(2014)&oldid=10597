<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:BerkeleyGW (2014) - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:BerkeleyGW_(2014)","wgTitle":"Tutorial:BerkeleyGW (2014)","wgCurRevisionId":10597,"wgRevisionId":10597,"wgArticleId":2313,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Bulk","GW"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:BerkeleyGW_(2014)","wgRelevantArticleId":2313,"wgRequestId":"5a8be39f8f51e6034da80c51","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_BerkeleyGW_2014 rootpage-Tutorial_BerkeleyGW_2014 skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:BerkeleyGW (2014)</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 12:10, 26 August 2018 by <a href="/wiki/User:Dstrubbe" class="mw-userlink" title="User:Dstrubbe"><bdi>Dstrubbe</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Dstrubbe&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Dstrubbe (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Dstrubbe" class="mw-usertoollinks-contribs" title="Special:Contributions/Dstrubbe">contribs</a>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;diff=prev&amp;oldid=10597" title="Tutorial:BerkeleyGW (2014)">diff</a>) <a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;direction=prev&amp;oldid=10597" title="Tutorial:BerkeleyGW (2014)">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p><b>NOTE</b>: This tutorial page was set up for the Benasque TDDFT school 2014. The specific references to the supercomputer used at that time will have to be adapted for others to use this tutorial. More recent version: <a href="/wiki/Tutorial:BerkeleyGW" title="Tutorial:BerkeleyGW">Tutorial:BerkeleyGW</a>
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Interacting_with_Hopper"><span class="tocnumber">1</span> <span class="toctext">Interacting with Hopper</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Getting_started_in_the_tutorial"><span class="tocnumber">2</span> <span class="toctext">Getting started in the tutorial</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Day_1"><span class="tocnumber">2.1</span> <span class="toctext">Day 1</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Day_2"><span class="tocnumber">2.2</span> <span class="toctext">Day 2</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#General_workflow"><span class="tocnumber">3</span> <span class="toctext">General workflow</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Documentation_and_resources"><span class="tocnumber">4</span> <span class="toctext">Documentation and resources</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Interacting_with_Hopper">Interacting with Hopper</span></h2>
<p>The <a href="/wiki/BerkeleyGW" title="BerkeleyGW">BerkeleyGW</a> tutorial is done on the <a rel="nofollow" class="external text" href="https://www.nersc.gov/users/computational-systems/hopper">Hopper supercomputer</a> (at NERSC in California). There are a few key things you need to know about how to interact with the machine:
</p>
<ul><li>To log in, run <code>ssh trainX@hopper.nersc.gov</code> in your terminal, substituting the actual name of your training account.</li>
<li>Be aware that since this machine is far away, you should not try running X-Windows programs!</li>
<li>You submit jobs by the <code>qsub</code> command, <i>e.g.</i> <code>qsub job.scr</code>, which will put them in the queue for execution when there is free space.</li>
<li>You can see what jobs you currently have in the queue by executing <code>qstat -u $USER</code>, so you can see when your job finishes. A status code will be shown: Q = waiting in the queue, R = running, C = complete.</li>
<li>You can cancel a job by <code>qdel job</code>, where <code>job</code> is the job id as written by <code>qstat</code>.</li>
<li>The job script (<i>e.g.</i> <code>01-calculate_scf.qsub</code>) specifies parameters to the PBS/Torque queuing system about how many cores to use, what commands to run, etc.</li>
<li>To copy files from Hopper to your local machine, in a terminal on your local machine, write <code>scp trainX@hopper.nersc.gov:FULL_PATH_TO_YOUR_FILE .</code> (filling in the username and filename) and enter your password when prompted. For very small ASCII files, you may find cut and paste more convenient.</li></ul>
<h2><span class="mw-headline" id="Getting_started_in_the_tutorial">Getting started in the tutorial</span></h2>
<h3><span class="mw-headline" id="Day_1">Day 1</span></h3>
<p>To obtain the files for the boron nitride and benzene examples for the first day of the tutorial:
</p>
<pre>cd $SCRATCH
/project/projectdirs/m1694/BGW-tddft/copy_day_1.sh
</pre>
<ul><li>In each case, enter your copy of the directory, and look at <code>README</code> and follow instructions given there.</li>
<li>Start by running <code>2-benzene/1-mf/1-scf</code> and then <code>2-benzene/1-mf/2-wfn</code>. This will take a little while, so while this runs, do the BN example.</li></ul>
<h3><span class="mw-headline" id="Day_2">Day 2</span></h3>
<p>To obtain the files for the silicon and benzene examples for the second day of the tutorial:
</p>
<pre>cd $SCRATCH
/project/projectdirs/m1694/BGW-tddft/copy_day_2.sh
</pre>
<p>The solution for the benzene example is available at
</p>
<pre> /project/projectdirs/m1694/BGW-tddft/2-benzene_run
</pre>
<p>You can copy the necessary files (WFNs, bse*mat, eps*mat, eqp*) from there.
</p><p>Other instructions:
</p>
<ul><li>We will work on the following directories: 2-benzene and 3-silicon. We will not work on the 1-boron_nitride example!</li>
<li>Start with the example 3-silicon.</li>
<li>In each case, enter your copy of the directory, and look at <code>README</code> and follow instructions given there.</li>
<li>There is additional example for XCrySDen, which is available in the shared folder on imac01 (see instructions on blackboard). If for some reason you are not able to copy it from there, it can also be downloaded <a rel="nofollow" class="external text" href="http://civet.berkeley.edu/~jornada/files/xct_LiCl.zip">here</a>. Note: to use XCrySDen on the iMacs, run <code>/sw/bin/xcrysden</code>.</li></ul>
<p>The examples are available for download here: <a rel="nofollow" class="external text" href="http://web.mit.edu/~dstrubbe/www/2-benzene.tar.gz">2-benzene.tar.gz</a>, <a rel="nofollow" class="external text" href="http://web.mit.edu/~dstrubbe/www/3-silicon.tar.gz">3-silicon.tar.gz</a>.
</p>
<h2><span class="mw-headline" id="General_workflow">General workflow</span></h2>
<ul><li>Finish all basic goals from both the boron nitride and benzene examples before starting any stretch goal.</li></ul>
<h2><span class="mw-headline" id="Documentation_and_resources">Documentation and resources</span></h2>
<ul><li><a rel="nofollow" class="external text" href="http://www.tddft.org/programs/octopus/doc/generated/html/vars.php?page=sections">Octopus variable reference</a> for the pre-release development version.</li>
<li><a rel="nofollow" class="external text" href="http://www.berkeleygw.org/releases/manual_v1.0.6.html">BerkeleyGW manual</a>.</li>
<li><a rel="nofollow" class="external text" href="http://benasque.org/2014tddft/talks_contr/115_BerkeleyGW_octopus.pptx.pdf">Intro slides from first day</a></li>
<li><a rel="nofollow" class="external text" href="http://arxiv.org/abs/1111.4429">BerkeleyGW implementation paper</a> on arxiv.</li>
<li>More extensive <a rel="nofollow" class="external text" href="http://www.nersc.gov/users/training/nersc-training-events/berkeleygw2013">lecture slides</a> from a longer tutorial devoted solely to BerkeleyGW in November 2012.</li>
<li>The <a rel="nofollow" class="external text" href="http://benasque.org/2014tddft/talks_contr/128_Felipe_BSE_Presentation.pdf">slides for the second day of tutorial</a>.</li></ul>
<p>Note that we are using the pre-release development version of Octopus in this tutorial, rather than the current release 4.1.2 which lacks full support for BerkeleyGW output. There are some small differences in output from 4.1.2.
</p>
<span class="noprint"><hr />
<p>Back to <a href="/wiki/Tutorials" title="Tutorials">Tutorials</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155101
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.039 seconds
Real time usage: 0.044 seconds
Preprocessor visited node count: 108/1000000
Preprocessor generated node count: 288/1000000
Post‐expand include size: 396/2097152 bytes
Template argument size: 214/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   10.100      1 -total
 67.18%    6.785     14 Template:Code
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2313-0!canonical and timestamp 20230412155101 and revision id 10597
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;oldid=10597">http:///mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;oldid=10597</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Bulk" title="Category:Bulk">Bulk</a></li><li><a href="/wiki/Category:GW" title="Category:GW">GW</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ABerkeleyGW+%282014%29&amp;returntoquery=oldid%3D10597" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:BerkeleyGW_(2014)" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:BerkeleyGW_(2014)&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:BerkeleyGW_(2014)">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:BerkeleyGW_(2014)" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:BerkeleyGW_(2014)" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;oldid=10597&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;oldid=10597" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:BerkeleyGW_(2014)&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 26 August 2018, at 12:10.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.039","walltime":"0.044","ppvisitednodes":{"value":108,"limit":1000000},"ppgeneratednodes":{"value":288,"limit":1000000},"postexpandincludesize":{"value":396,"limit":2097152},"templateargumentsize":{"value":214,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   10.100      1 -total"," 67.18%    6.785     14 Template:Code"]},"cachereport":{"timestamp":"20230412155101","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":124});});</script>
</body>
</html>
