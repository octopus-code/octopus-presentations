<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:Appendix:Copying - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Appendix:Copying","wgTitle":"Manual:Appendix:Copying","wgCurRevisionId":5755,"wgRevisionId":5755,"wgArticleId":1566,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Appendix:Copying","wgRelevantArticleId":1566,"wgRequestId":"d3017daddc018db3d23d4f11","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Appendix_Copying rootpage-Manual_Appendix_Copying skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:Appendix:Copying</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#INTRODUCTION"><span class="tocnumber">1</span> <span class="toctext">INTRODUCTION</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#LICENSE_EXCEPTIONS"><span class="tocnumber">2</span> <span class="toctext">LICENSE EXCEPTIONS</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Expokit"><span class="tocnumber">3</span> <span class="toctext">Expokit</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Metis_4.0"><span class="tocnumber">4</span> <span class="toctext">Metis 4.0</span></a>
<ul>
<li class="toclevel-2 tocsection-5"><a href="#METIS_COPYRIGHT_NOTICE"><span class="tocnumber">4.1</span> <span class="toctext">METIS COPYRIGHT NOTICE</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-6"><a href="#qshep"><span class="tocnumber">5</span> <span class="toctext">qshep</span></a>
<ul>
<li class="toclevel-2 tocsection-7"><a href="#QSHEP_COPYRIGHT_NOTICE"><span class="tocnumber">5.1</span> <span class="toctext">QSHEP COPYRIGHT NOTICE</span></a></li>
</ul>
</li>
</ul>
</div>

<h3><span class="mw-headline" id="INTRODUCTION">INTRODUCTION</span></h3>
<p>The real-space TDDFT code octopus ("octopus") is provided under the 
GNU General Public License ("GPL"), Version 2, with exceptions for 
external libraries that are contained solely for convenience in this 
distribution. 
</p><p>You can find a copy of the GPL license <a rel="nofollow" class="external text" href="http://www.gnu.org/copyleft/gpl.html">here. </a>
</p><p>A copy of the exceptions and licenses follow this introduction.
</p>
<h3><span class="mw-headline" id="LICENSE_EXCEPTIONS">LICENSE EXCEPTIONS</span></h3>
<p>Octopus provides several external libraries, which are located in the 
"external_libs" subdirectory of the octopus source distribution. The 
GNU General Public License does not apply to these libraries. Separate 
copyright notices can be found for each library in the respective 
subdirectories of "external_libs". Copyright notices are also contained 
in this document.
</p><p>Currently the following external libraries are provided:
</p>
<h3><span class="mw-headline" id="Expokit">Expokit</span></h3>
<ul><li>Roger B. Sidje</li>
<li>Department of Mathematics, University of Queensland</li>
<li>Brisbane, QLD-4072, Australia</li>
<li>Email: rbs@maths.uq.edu.au</li>
<li>WWW: <a rel="nofollow" class="external free" href="http://www.maths.uq.edu.au/expokit/">http://www.maths.uq.edu.au/expokit/</a></li>
<li>Copyright: <a rel="nofollow" class="external free" href="http://www.maths.uq.edu.au/expokit/copyright">http://www.maths.uq.edu.au/expokit/copyright</a></li></ul>
<p><br />
</p>
<h3><span class="mw-headline" id="Metis_4.0">Metis 4.0</span></h3>
<ul><li>George Karypis</li>
<li>Department of Computer Science &amp; Engineering</li>
<li>Twin Cities Campus</li>
<li>University of Minnesota, Minneapolis, MN, USA</li>
<li>Email: karypis@cs.umn.edu</li>
<li>WWW: <a rel="nofollow" class="external free" href="http://www-users.cs.umn.edu/~karypis/metis/index.html">http://www-users.cs.umn.edu/~karypis/metis/index.html</a></li></ul>
<h4><span class="mw-headline" id="METIS_COPYRIGHT_NOTICE">METIS COPYRIGHT NOTICE</span></h4>
<p>The ParMETIS/METIS package is copyrighted by the Regents of the
University of Minnesota. It can be freely used for educational and
research purposes by non-profit institutions and US government agencies
only.  Other organizations are allowed to use ParMETIS/METIS only for
evaluation purposes, and any further uses will require prior approval.
The software may not be sold or redistributed without prior approval.
One may make copies of the software for their use provided that the
copies, are not sold or distributed, are used under the same terms
and conditions.
</p><p>As unestablished research software, this code is provided on an
``as is<i> basis without warranty of any kind, either expressed or</i>
implied. The downloading, or executing any part of this software
constitutes an implicit agreement to these terms. These terms and
conditions are subject to change at any time without prior notice.
</p>
<h3><span class="mw-headline" id="qshep">qshep</span></h3>
<ul><li>Robert Renka</li>
<li>University of North Texas</li>
<li>(817) 565-2767</li></ul>
<p><br />
</p>
<h4><span class="mw-headline" id="QSHEP_COPYRIGHT_NOTICE">QSHEP COPYRIGHT NOTICE</span></h4>
<p>ACM Software Copyright Notice
</p><p><a rel="nofollow" class="external autonumber" href="http://www.acm.org/pubs/toc/CRnotice.html">[1]</a>
</p><p>Copyright   (c)  1998  Association   for  Computing   Machinery,  Inc.
Permission to  include in application  software or to make  digital or
hard copies  of part or all of  this work is subject  to the following
licensing agreement.
</p><p>ACM Software License Agreement
</p><p>All software, both binary and  source published by the Association for
Computing  Machinery  (hereafter,  Software)  is  copyrighted  by  the
Association  (hereafter, ACM) and  ownership of  all right,  title and
interest in and to the Software remains with ACM.  By using or copying
the Software, User agrees to abide by the terms of this Agreement.
</p><p>Noncommercial Use
</p><p>The ACM  grants to you (hereafter, User)  a royalty-free, nonexclusive
right  to execute,  copy, modify  and distribute  both the  binary and
source  code   solely  for   academic,  research  and   other  similar
noncommercial uses, subject to the following conditions:
</p><p>1. User  acknowledges that the  Software is  still in  the development
stage  and that  it is  being supplied  "as is,"  without  any support
services   from  ACM.    Neither  ACM   nor  the   author   makes  any
representations or warranties,  express or implied, including, without
limitation, any  representations or warranties  of the merchantability
or fitness for any particular  purpose, or that the application of the
software, will not infringe on any patents or other proprietary rights
of others.
</p><p>2. ACM shall  not be held  liable for direct, indirect,  incidental or
consequential  damages arising  from any  claim by  User or  any third
party with respect  to uses allowed under this  Agreement, or from any
use of the Software.
</p><p>3. User agrees  to fully  indemnify and hold  harmless ACM  and/or the
author(s) of  the original work from  and against any  and all claims,
demands, suits, losses, damages, costs and expenses arising out of the
User's use of the Software, including, without limitation, arising out
of the User's modification of the Software.
</p><p>4. User may modify  the Software and distribute that  modified work to
third  parties provided  that: (a)  if posted  separately,  it clearly
acknowledges  that it  contains  material copyrighted  by  ACM (b)  no
charge is associated  with such copies, (c) User  agrees to notify ACM
and the Author(s)  of the distribution, and (d)  User clearly notifies
secondary users that such modified work is not the original Software.
</p><p>5. User agrees that  ACM, the authors of the  original work and others
may enjoy  a royalty-free, non-exclusive license to  use, copy, modify
and redistribute these modifications to  the Software made by the User
and  distributed to  third parties  as  a derivative  work under  this
agreement.
</p><p>6. This agreement will terminate immediately upon User's breach of, or
non-compliance with,  any of its terms.   User may be  held liable for
any  copyright   infringement  or   the  infringement  of   any  other
proprietary rights  in the Software  that is caused or  facilitated by
the User's failure to abide by the terms of this agreement.
</p><p>7. This agreement  will be construed  and enforced in  accordance with
the law  of the  state of New  York applicable to  contracts performed
entirely  within the State.   The parties  irrevocably consent  to the
exclusive jurisdiction of  the state or federal courts  located in the
City of New York for all disputes concerning this agreement.
</p><p>Commercial Use
</p><p>Any User wishing to make a commercial use of the Software must contact
ACM  at   permissions@acm.org  to  arrange   an  appropriate  license.
Commercial use  includes (1) integrating or incorporating  all or part
of the source code into a product for sale or license by, or on behalf
of, User to third parties, or (2) distribution of the binary or source
code  to third  parties  for use  with  a commercial  product sold  or
licensed by, or on behalf of, User.
</p><p>Revised 6/98
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:Appendix:Reference_Manual" title="Manual:Appendix:Reference Manual">Manual:Appendix:Reference Manual</a> - Next <a href="/wiki/Manual" title="Manual">Manual</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412154103
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.032 seconds
Real time usage: 0.038 seconds
Preprocessor visited node count: 46/1000000
Preprocessor generated node count: 112/1000000
Post‐expand include size: 218/2097152 bytes
Template argument size: 82/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    8.654      1 -total
100.00%    8.654      1 Template:Manual_foot
 48.00%    4.154      1 Template:Foot
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1566-0!canonical and timestamp 20230412154103 and revision id 5755
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:Appendix:Copying&amp;oldid=5755">http:///mediawiki/index.php?title=Manual:Appendix:Copying&amp;oldid=5755</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AAppendix%3ACopying&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Appendix:Copying" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:Appendix:Copying&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:Appendix:Copying">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Appendix:Copying&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Appendix:Copying&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Appendix:Copying" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Appendix:Copying" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:Appendix:Copying&amp;oldid=5755" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Appendix:Copying&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 13 April 2010, at 22:38.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.032","walltime":"0.038","ppvisitednodes":{"value":46,"limit":1000000},"ppgeneratednodes":{"value":112,"limit":1000000},"postexpandincludesize":{"value":218,"limit":2097152},"templateargumentsize":{"value":82,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%    8.654      1 -total","100.00%    8.654      1 Template:Manual_foot"," 48.00%    4.154      1 Template:Foot"]},"cachereport":{"timestamp":"20230412154103","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":110});});</script>
</body>
</html>
