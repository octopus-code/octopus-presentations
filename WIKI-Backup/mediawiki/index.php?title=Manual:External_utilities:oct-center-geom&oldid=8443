<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:External utilities:oct-center-geom - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:External_utilities:oct-center-geom","wgTitle":"Manual:External utilities:oct-center-geom","wgCurRevisionId":8443,"wgRevisionId":8443,"wgArticleId":1560,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:External_utilities:oct-center-geom","wgRelevantArticleId":1560,"wgRequestId":"4ccfdbbd141872ff3fe9ca5a","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_External_utilities_oct-center-geom rootpage-Manual_External_utilities_oct-center-geom skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:External utilities:oct-center-geom</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 13:28, 7 September 2016 by <a href="/wiki/User:Micael" class="mw-userlink" title="User:Micael"><bdi>Micael</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Micael&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Micael (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Micael" class="mw-usertoollinks-contribs" title="Special:Contributions/Micael">contribs</a>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;diff=prev&amp;oldid=8443" title="Manual:External utilities:oct-center-geom">diff</a>) <a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;direction=prev&amp;oldid=8443" title="Manual:External utilities:oct-center-geom">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><h3><span class="mw-headline" id="NAME">NAME</span></h3>
<p>oct-center-geom - Centers a molecule's geometry
</p>
<h3><span class="mw-headline" id="SYNOPSIS">SYNOPSIS</span></h3>
<p><tt><i>oct-center-geom</i></tt>
</p><p>[oct-center-geom does not read the standard input: all standard input
will be simply ignored. An input file named <b><tt>inp</tt></b> must be present in the
running directory. Also, oct-center-geom accepts no command-line
arguments, since there is not a standard way to do this with Fortran
90.]
</p>
<h3><span class="mw-headline" id="DESCRIPTION">DESCRIPTION</span></h3>
<p>This program is one of the Octopus utilities.
</p><p>It reads the coordinates defined in the <b><tt>inp</tt></b> file, and constructs an output xyz file, that will be called
<b><tt>adjusted.xyz</tt></b> file, that describes the same system but in which the
atomic coordinates are centered, and (optionally) has the axes aligned.
</p><p>To control the orientation of the centered molecule there are two parameters: <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Utilities&amp;name=MainAxis"><code>MainAxis</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Utilities&amp;name=AxisType"><code>AxisType</code></a></span>.
</p><p>Do not forget then to change your input file to use this file instead of your old geometry (by changing <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span>=<code>'adjusted.xyz'</code>). 
</p><p>Be careful with units, this utility honours the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=Units"><code>Units</code></a></span>, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsInput"><code>UnitsInput</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=UnitsOutput"><code>UnitsOutput</code></a></span> variables, so the <b><tt>adjusted.xyz</tt></b> file will be in the specified output units.
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:External_utilities:oct-casida_spectrum" title="Manual:External utilities:oct-casida spectrum">Manual:External utilities:oct-casida_spectrum</a> - Next <a href="/mediawiki/index.php?title=Manual:External_utilities:oct-check_deallocs&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-check deallocs (page does not exist)">Manual:External utilities:oct-check_deallocs</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412154052
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.061 seconds
Real time usage: 0.074 seconds
Preprocessor visited node count: 172/1000000
Preprocessor generated node count: 483/1000000
Post‐expand include size: 1499/2097152 bytes
Template argument size: 476/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   53.590      1 -total
 50.43%   27.025      6 Template:Variable
 25.99%   13.928      6 Template:Octopus_version
 14.94%    8.004      1 Template:Manual_foot
  9.64%    5.166      7 Template:Code
  7.95%    4.260      4 Template:File
  7.65%    4.097      1 Template:Foot
  7.45%    3.991      1 Template:Value
  6.96%    3.731      1 Template:Command
  6.95%    3.724      6 Template:Octopus_minor_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1560-0!canonical and timestamp 20230412154052 and revision id 8443
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;oldid=8443">http:///mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;oldid=8443</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AExternal+utilities%3Aoct-center-geom&amp;returntoquery=oldid%3D8443" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:External_utilities:oct-center-geom" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:External_utilities:oct-center-geom&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:External_utilities:oct-center-geom">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:External_utilities:oct-center-geom" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:External_utilities:oct-center-geom" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;oldid=8443&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;oldid=8443" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 7 September 2016, at 13:28.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.061","walltime":"0.074","ppvisitednodes":{"value":172,"limit":1000000},"ppgeneratednodes":{"value":483,"limit":1000000},"postexpandincludesize":{"value":1499,"limit":2097152},"templateargumentsize":{"value":476,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   53.590      1 -total"," 50.43%   27.025      6 Template:Variable"," 25.99%   13.928      6 Template:Octopus_version"," 14.94%    8.004      1 Template:Manual_foot","  9.64%    5.166      7 Template:Code","  7.95%    4.260      4 Template:File","  7.65%    4.097      1 Template:Foot","  7.45%    3.991      1 Template:Value","  6.96%    3.731      1 Template:Command","  6.95%    3.724      6 Template:Octopus_minor_version"]},"cachereport":{"timestamp":"20230412154052","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":126});});</script>
</body>
</html>
