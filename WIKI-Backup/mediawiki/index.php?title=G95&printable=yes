<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>G95 - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"G95","wgTitle":"G95","wgCurRevisionId":7367,"wgRevisionId":7367,"wgArticleId":2239,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"G95","wgRelevantArticleId":2239,"wgRequestId":"2174123ba0c16a1284f65034","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-G95 rootpage-G95 skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">G95</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Problems_with_earlier_versions_of_g95"><span class="tocnumber">1</span> <span class="toctext">Problems with earlier versions of g95</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Instructions_for_compiling_g95_0.94_with_gcc_4.2.4_on_Ubuntu"><span class="tocnumber">2</span> <span class="toctext">Instructions for compiling g95 0.94 with gcc 4.2.4 on Ubuntu</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Explanations_of_patches"><span class="tocnumber">3</span> <span class="toctext">Explanations of patches</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#References"><span class="tocnumber">4</span> <span class="toctext">References</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Problems_with_earlier_versions_of_g95">Problems with earlier versions of g95</span></h2>
<p>g95 0.92 crashed with an internal error and segmentation fault compiling some files of octopus development version circa May 2014.
g95 0.93 with gcc 4.1.2 had runtime errors on many tests in the testsuite, like the one below, in situations where a function argument (from a different module, and using global pointers) was passed to a subroutine. No problem, or real explanation of this message, could be found.
</p>
<pre>Using input file&#160;: ../../testsuite/components/04-hartree_3d_cg.01-cg_corrected.inp 

Starting test run ...
Executing: cd /tmp/octopus._PuX8M;  /home/pulpo/buildbot/lascar_x86_64_g95/build/_build/testsuite/../src/main/oct-test &gt; out 
At line 2003 of file solvers_oct.f90
Traceback: (Innermost first)
   Called from line 353 of file poisson_cg_oct.f90
   Called from line 990 of file poisson_oct.f90
   Called from line 1274 of file poisson_oct.f90
   Called from line 224 of file test_oct.f90
   Called from line 191 of file test_oct.f90
Fortran runtime error: Actual assumed-shape array argument does not conform
	Elapsed time:     29.8 s
</pre>
<p>However, g95 0.94 with gcc 4.2.4 on Ubuntu worked. So did 0.93 with gcc 4.2.4 on MacOS (from MacPorts). Compiling with this version of gcc took a bit of work, so I give the instructions below.
</p>
<h2><span class="mw-headline" id="Instructions_for_compiling_g95_0.94_with_gcc_4.2.4_on_Ubuntu">Instructions for compiling g95 0.94 with gcc 4.2.4 on Ubuntu</span></h2>
<p>Choose an installation directory in which to put the resulting g95 executables, which you will use as prefix below for the configure step in g95 and libf95, in place of <tt>$HOME/g95-install</tt>.
A multi-threaded <tt>make -j</tt> should work for gcc, but probably won't for g95.
</p>
<pre>wget <a rel="nofollow" class="external free" href="ftp://gcc.gnu.org/pub/gcc/releases/gcc-4.2.4/gcc-core-4.2.4.tar.gz">ftp://gcc.gnu.org/pub/gcc/releases/gcc-4.2.4/gcc-core-4.2.4.tar.gz</a>
cd gcc-4.2.4
sed -i 's/prune_options (&amp;argc, &amp;argv);//' gcc/gcc.c
mkdir g95
cd g95
../configure --enable-languages=c
make
cd ../..
</pre>
<pre>wget <a rel="nofollow" class="external free" href="http://ftp.g95.org/g95_source.tgz">http://ftp.g95.org/g95_source.tgz</a>
tar xzf g95_source.tgz
cd g95-0.94
./configure --prefix=$HOME/g95-install --with-gcc-dir=$PWD/../gcc-4.2.4
sed -i "s|g95_LDADD = |g95_LDADD = $PWD/../gcc-4.2.4/g95/gcc/driver-i386.o |"  Makefile
sed -i "s|f951_LDADD = |f951_LDADD = $PWD/../gcc-4.2.4/g95/libdecnumber/*.o |"  Makefile
sed -i 's/named_section/get_named_section/' trans-decl.c
echo -e "\n\nfopenmp\nF95 Var(flag_openmp)\nEnable OpenMP\n" &gt;&gt; lang.opt
make install
</pre>
<pre>tar xzf libf95.a-0.94.tar.gz
cd libf95.a-0.94
./configure --prefix=$HOME/g95-install 
make install
cd ../..
</pre>
<p>Finally, you can create a symlink from the resulting executable:
</p>
<pre>ln -s x86_64-unknown-linux-gnu-g95 g95-install/bin/g95
</pre>
<h2><span class="mw-headline" id="Explanations_of_patches">Explanations of patches</span></h2>
<ul><li>Patch 1</li></ul>
<pre>sed -i "s|g95_LDADD = |g95_LDADD = $PWD/../gcc-4.2.4/g95/gcc/driver-i386.o |"  Makefile
</pre>
<p>fixes
</p>
<pre>gcc -DTARGET_NAME=\"x86_64-unknown-linux-gnu\" -DSTANDARD_EXEC_PREFIX=\"/home/dstrubbe/g95-0.94/../g95-install_src/lib/gcc-lib/x86_64-unknown-linux-gnu/4.2.4\"
-DTARGET_SYSTEM_ROOT=\"\" -DDEFAULT_TARGET_VERSION=\"\" -DDEFAULT_TARGET_MACHINE=\"\" -DTOOLDIR_BASE_PREFIX=\"\"
-DSTANDARD_STARTFILE_PREFIX=\"\" -DSTANDARD_STARTFILE_PREFIX_2=\"/usr/lib64\" -DSTANDARD_EXEC_PREFIX=\"/home/dstrubbe/g95-0.94/../g95-install_src/lib/gcc-lib/x86_64-unknown-linux-gnu/4.2.4\"
-DSTANDARD_BINDIR_PREFIX=\"/home/dstrubbe/g95-0.94/../g95-install_src/bin\" -DSTANDARD_LIBEXEC_PREFIX=\"/home/dstrubbe/g95-0.94/../g95-install_src/lib/gcc-lib/x86_64-unknown-linux-gnu/4.2.4\"
-DG95_SPEC=\"\"  -g -O2 -DIN_GCC -Wall -Wmissing-prototypes   -o g95 g95-g95spec.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/prefix.o gcc.o version.o
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/intl.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libiberty/libiberty.a  
gcc.o:(.rodata+0x14d8): undefined reference to `host_detect_local_cpu'
</pre>
<ul><li>Patch 2</li></ul>
<pre>sed -i 's/named_section/get_named_section/' trans-decl.c
</pre>
<p>fixes
</p>
<pre>gcc  -g -O2 -DIN_GCC -Wall -Wmissing-prototypes   -o f951 arith.o array.o bbt.o bigint.o bigreal.o check.o convert.o data.o decl.o dump.o entry.o error.o expr.o f95-lang.o forall.o
format.o imodule.o interface.o intrinsic.o io.o iresolve.o kinds.o lang-options.o match.o matchexp.o misc.o module.o parse.o primary.o resolve.o scanner.o scalarize.o select.o show.o
simplify.o st.o symbol.o trans-array.o trans-decl.o trans-intrinsic.o trans-stmt.o trans.o trans-common.o trans-const.o trans-expr.o trans-io.o trans-types.o options.o opts.o version.o
ggc-page.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/main.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libcpp/libcpp.a /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libiberty/libiberty.a  
trans-decl.o: In function `g95_dump_coarray':
/home/dstrubbe/g95-0.94/trans-decl.c:2882: undefined reference to `named_section'
</pre>
<ul><li>Patch 3</li></ul>
<pre>echo -e "\n\nfopenmp\nF95 Var(flag_openmp)\nEnable OpenMP\n" &gt;&gt; lang.opt
</pre>
<p>fixes
</p>
<pre>gcc  -g -O2 -DIN_GCC -Wall -Wmissing-prototypes   -o f951 arith.o array.o bbt.o bigint.o bigreal.o check.o convert.o data.o decl.o dump.o entry.o error.o expr.o f95-lang.o forall.o
format.o imodule.o interface.o intrinsic.o io.o iresolve.o kinds.o lang-options.o match.o matchexp.o misc.o module.o parse.o primary.o resolve.o scanner.o scalarize.o select.o show.o
simplify.o st.o symbol.o trans-array.o trans-decl.o trans-intrinsic.o trans-stmt.o trans.o trans-common.o trans-const.o trans-expr.o trans-io.o trans-types.o options.o opts.o version.o
ggc-page.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/main.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libcpp/libcpp.a /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libiberty/libiberty.a  
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a(cgraph.o): In function `cgraph_varpool_finalize_decl':
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/cgraph.c:1002: undefined reference to `flag_openmp'
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a(omp-low.o): In function `gate_expand_omp':
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/omp-low.c:3565: undefined reference to `flag_openmp'
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a(omp-low.o): In function `gate_lower_omp':
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/omp-low.c:4439: undefined reference to `flag_openmp'
</pre>
<ul><li>Patch 4</li></ul>
<pre>sed -i "s|f951_LDADD = |f951_LDADD = $PWD/../gcc-4.2.4/g95/libdecnumber/*.o |"  Makefile
</pre>
<p>fixes
</p>
<pre>gcc  -g -O2 -DIN_GCC -Wall -Wmissing-prototypes   -o f951 arith.o array.o bbt.o bigint.o bigreal.o check.o convert.o data.o decl.o dump.o entry.o error.o expr.o f95-lang.o forall.o
format.o imodule.o interface.o intrinsic.o io.o iresolve.o kinds.o lang-options.o match.o matchexp.o misc.o module.o parse.o primary.o resolve.o scanner.o scalarize.o select.o show.o
simplify.o st.o symbol.o trans-array.o trans-decl.o trans-intrinsic.o trans-stmt.o trans.o trans-common.o trans-const.o trans-expr.o trans-io.o trans-types.o options.o opts.o version.o
ggc-page.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/main.o /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libcpp/libcpp.a /home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/libiberty/libiberty.a  
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a(dfp.o): In function `decimal_to_decnumber':
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:112: undefined reference to `decContextDefault'
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:121: undefined reference to `decNumberFromString'
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:125: undefined reference to `decNumberFromString'
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:131: undefined reference to `decimal128ToNumber'
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:127: undefined reference to `decNumberFromString'
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:118: undefined reference to `decNumberZero'
/home/dstrubbe/g95-0.94/../gcc-4.2.4/g95/gcc/libbackend.a(dfp.o): In function `encode_decimal128':
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:252: undefined reference to `decContextDefault'
/home/dstrubbe/gcc-4.2.4/g95/gcc/../../gcc/dfp.c:256: undefined reference to `decimal128FromNumber'
...
</pre>
<h2><span class="mw-headline" id="References">References</span></h2>
<ul><li>G95 instructions for building from source: <a rel="nofollow" class="external free" href="http://www.g95.org/source.shtml">http://www.g95.org/source.shtml</a></li>
<li>MacPorts Portfile for g95: <a rel="nofollow" class="external free" href="https://trac.macports.org/browser/trunk/dports/lang/g95/Portfile">https://trac.macports.org/browser/trunk/dports/lang/g95/Portfile</a></li>
<li><a rel="nofollow" class="external free" href="https://trac.macports.org/browser/trunk/dports/lang/g95/files/patch-gcc.c.diff">https://trac.macports.org/browser/trunk/dports/lang/g95/files/patch-gcc.c.diff</a> (Do not remove target_flags though.)</li>
<li><a rel="nofollow" class="external free" href="https://trac.macports.org/browser/trunk/dports/lang/g95/files/patch-lang.opt.diff">https://trac.macports.org/browser/trunk/dports/lang/g95/files/patch-lang.opt.diff</a></li></ul>
<!-- 
NewPP limit report
Cached time: 20230412155013
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.021 seconds
Real time usage: 0.023 seconds
Preprocessor visited node count: 14/1000000
Preprocessor generated node count: 20/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2239-0!canonical and timestamp 20230412155013 and revision id 7367
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=G95&amp;oldid=7367">http:///mediawiki/index.php?title=G95&amp;oldid=7367</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=G95&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/G95" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:G95&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/G95">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=G95&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=G95&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/G95" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/G95" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=G95&amp;oldid=7367" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=G95&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 15 January 2015, at 18:49.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.021","walltime":"0.023","ppvisitednodes":{"value":14,"limit":1000000},"ppgeneratednodes":{"value":20,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412155013","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":109});});</script>
</body>
</html>
