<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Use of symmetries in optical spectra from time-propagation - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation","wgTitle":"Tutorial:Use of symmetries in optical spectra from time-propagation","wgCurRevisionId":11355,"wgRevisionId":11355,"wgArticleId":2122,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Advanced","Time-dependent","Molecule","Pseudopotentials","DFT","Optical Absorption","Oct-propagation spectrum"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation","wgRelevantArticleId":2122,"wgRequestId":"727d4b15a3e248bfdff1ce6a","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","ext.cite.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["ext.cite.ux-enhancements","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.cite.styles%7Cext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Use_of_symmetries_in_optical_spectra_from_time-propagation rootpage-Tutorial_Use_of_symmetries_in_optical_spectra_from_time-propagation skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Use of symmetries in optical spectra from time-propagation</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>In this tutorial we will see how the spatial symmetries of a molecule can be used to reduce the number of time-propagations required to compute the absorption cross-section.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Introduction"><span class="tocnumber">1</span> <span class="toctext">Introduction</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Methane"><span class="tocnumber">2</span> <span class="toctext">Methane</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Linear_molecule"><span class="tocnumber">3</span> <span class="toctext">Linear molecule</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Planar_molecule"><span class="tocnumber">4</span> <span class="toctext">Planar molecule</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#References"><span class="tocnumber">5</span> <span class="toctext">References</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Introduction">Introduction</span></h2>
<p>The dynamic polarizability (related to optical absorption cross-section via <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/a8615008054212263452ea40b8bed483d71589f6" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -1.838ex; width:13.468ex; height:5.176ex;" alt="{\displaystyle \sigma ={\frac {4\pi \omega }{c}}\mathrm {Im} \alpha }" />) is, in its most general form, a 3x3 tensor. The reason is that we can shine light on the system polarized in any of the three Cartesian axes, and for each of these three cases measure how the dipole of the molecule oscillates along the three Cartesian axes. This usually means that to obtain the full dynamic polarizability of the molecule we usually need to apply 3 different perturbations along <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/5ca2debf12f3e7d40af3096f8259efd5416fdca6" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:6.028ex; height:2.009ex;" alt="{\displaystyle x,y,z\,}" />, by setting <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationDirection"><code>TDPolarizationDirection</code></a></span> to 1, 2, or 3.
</p><p>However, if the molecule has some symmetries, it is in general possible to reduce the total number of calculations required to obtain the tensor from 3 to 2, or even 1.<sup id="cite_ref-1" class="reference"><a href="#cite_note-1">&#91;1&#93;</a></sup>
To use this formalism in Octopus, you need to supply some extra information. The most important thing that the code requires is the information about equivalent axes, that is, directions that are related through some symmetry transformation. Using these axes, we construct a reference frame and specify it with the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarization"><code>TDPolarization</code></a></span> block. Note that these axes need not be orthogonal, but they must be linearly-independent. The <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationEquivAxes"><code>TDPolarizationEquivAxes</code></a></span> tells the code how many equivalent axes there are. Ideally, the reference frame should be chosen to maximize the number of equivalent axes. When using three equivalent axes, an extra input variable, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationWprime"><code>TDPolarizationWprime</code></a></span>, is also required.
</p><p>Let us give a couple of examples, which should make all these concepts easier to understand.
</p>
<h2><span class="mw-headline" id="Methane">Methane</span></h2>
<p>As seen in previous tutorials, the methane molecule has <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/70cadcc969636b990957852f205868f9f7178840" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:2.45ex; height:2.509ex;" alt="{\displaystyle T_{d}}" /> symmetry. This means that it is trivial to find three linearly-independent equivalent axes such that only one time-propagation is needed to obtain the whole tensor. As it happens, we can use the usual <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/87f9e315fd7e2ba406057a97300593c4802b53e4" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.33ex; height:1.676ex;" alt="{\displaystyle x}" />, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b8a6208ec717213d4317e666f1ae872e00620a0d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:1.155ex; height:2.009ex;" alt="{\displaystyle y}" />, and <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/bf368e72c009decd9b6686ee84a375632e11de98" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.088ex; height:1.676ex;" alt="{\displaystyle z}" /> directions, with all of them being equivalent (check the <a rel="nofollow" class="external text" href="https://en.wikipedia.org/wiki/Symmetry_operation">symmetry operations</a> of the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/70cadcc969636b990957852f205868f9f7178840" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:2.45ex; height:2.509ex;" alt="{\displaystyle T_{d}}" />  <a rel="nofollow" class="external text" href="https://en.wikipedia.org/wiki/Molecular_symmetry">point group</a> if you are not convinced). Therefore we can perform just one propagation with the perturbation along the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/87f9e315fd7e2ba406057a97300593c4802b53e4" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.33ex; height:1.676ex;" alt="{\displaystyle x}" /> direction adding the following to the input file used previously: 
</p>
<pre>%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarization"><code>TDPolarization</code></a></span>
 1 | 0 | 0
 0 | 1 | 0
 0 | 0 | 1
%
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationDirection"><code>TDPolarizationDirection</code></a></span> = 1
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationEquivAxes"><code>TDPolarizationEquivAxes</code></a></span> = 3
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationWprime"><code>TDPolarizationWprime</code></a></span>
 0 | 0 | 1
%
</pre>
<p>Note that we had omitted the blocks <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarization"><code>TDPolarization</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationWprime"><code>TDPolarizationWprime</code></a></span> in the previous tutorials, as these are their default 
</p><p>Once the time-propagation is finished, you will find, as usual, a <b><tt>td.general/multipoles</tt></b> file. This time the file contains all the necessary information for Octopus to compute the full tensor, so running the  <b><tt>oct-propagation_spectrum</tt></b> utility will produce two files: <b><tt>cross_section_vector.1</tt></b> and <b><tt>cross_section_tensor</tt></b>. The later should look like this (assuming you used the converged grid parameters from the
<a href="/wiki/Tutorial:Convergence_of_the_optical_spectra" title="Tutorial:Convergence of the optical spectra">Convergence of the optical spectra</a> tutorial):  
</p>
<pre># nspin         1
# kick mode    0
# kick strength    0.005291772086
# dim           3
# pol(1)           1.000000000000    0.000000000000    0.000000000000
# pol(2)           0.000000000000    1.000000000000    0.000000000000
# pol(3)           0.000000000000    0.000000000000    1.000000000000
# direction    1
# Equiv. axes  3
# wprime           0.000000000000    0.000000000000    1.000000000000
# kick time        0.000000000000
#       Energy         (1/3)*Tr[sigma]    Anisotropy[sigma]      sigma(1,1,1)        sigma(1,2,1)        sigma(1,3,1)        ...
#        [eV]               [A^2]               [A^2]               [A^2]               [A^2]               [A^2]            ...
      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00      0.00000000E+00     ...
      0.10000000E-01     -0.23961887E-09      0.69324919E-10     -0.23961887E-09     -0.17164329E-10      0.32468123E-10     ...
      0.20000000E-01     -0.94563522E-09      0.27697039E-09     -0.94563522E-09     -0.68575991E-10      0.12971822E-09     ...
...
</pre>
<p>Try comparing the spectrum for each component of the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/59f59b7c3e6fdb1d0365a494b81fb9a696138c36" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.33ex; height:1.676ex;" alt="{\displaystyle \sigma }" /> tensor.
</p>
<h2><span class="mw-headline" id="Linear_molecule">Linear molecule</span></h2>
<p>Now let us look at a linear molecule. In this case, you might think that we need two calculations to obtain the whole tensor, one for the direction along the axis of the molecule, and another for the axis perpendicular to the molecule. The fact is that we need only one, in a specially chosen direction, so that our field has components both along the axis of the molecule and perpendicular to it. Let us assume that the axis of the molecule is oriented along the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/ab34739435d9d9d99cddf4041740b107343b1398" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.717ex; height:1.676ex;" alt="{\displaystyle x\,}" /> axis. Then we can use
</p>
<pre>%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarization"><code>TDPolarization</code></a></span>
 1/sqrt(2) | -1/sqrt(2) | 0
 1/sqrt(2) |  1/sqrt(2) | 0
 1/sqrt(2) |  0         | 1/sqrt(2)
%
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationDirection"><code>TDPolarizationDirection</code></a></span> = 1
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationEquivAxes"><code>TDPolarizationEquivAxes</code></a></span> = 3
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationWprime"><code>TDPolarizationWprime</code></a></span>
 1/sqrt(2) |  0         | 1/sqrt(2)
%
</pre>
<p>You should try to convince yourself that the three axes are indeed equivalent and linearly independent. The first and second axes are connected through a simple reflection in the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/97005bee6e83614cf6ce64d4e68e5ab2ac280709" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.418ex; height:1.676ex;" alt="{\displaystyle xz}" /> plane, transforming the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b8a6208ec717213d4317e666f1ae872e00620a0d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:1.155ex; height:2.009ex;" alt="{\displaystyle y}" /> coordinate from <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/79010041adce4783505a14713ebdd31aa19aa21d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:7.231ex; height:3.176ex;" alt="{\displaystyle -1/{\sqrt {2}}}" /> into <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/75a0bbdb60fcb73ac67d9970a5eb0808b87fd37d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:5.423ex; height:3.176ex;" alt="{\displaystyle 1/{\sqrt {2}}}" />. <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationWprime"><code>TDPolarizationWprime</code></a></span> should be set to the result obtained by applying the inverse symmetry operation to the third axis. This actually leaves the third axis unchanged.
</p>
<h2><span class="mw-headline" id="Planar_molecule">Planar molecule</span></h2>
<p>Finally, let us look at a general planar molecule (in the <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/c72eb345e496513fb8b2fa4aa8c4d89b855f9a01" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:2.485ex; height:2.009ex;" alt="{\displaystyle xy}" /> plane). In principle we need only two calculations (that is reduced to one if more symmetries are present like, <i>e.g.</i>, in benzene). In this case we chose one of the polarization axes on the plane and the other two rotated 45 degrees:
</p>
<pre>%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarization"><code>TDPolarization</code></a></span>
 1/sqrt(2) | 0 | 1/sqrt(2)
 1/sqrt(2) | 0 |-1/sqrt(2)
 0         | 1 | 0
%

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationEquivAxes"><code>TDPolarizationEquivAxes</code></a></span> = 2
</pre>
<p>In this case, we need two runs, one for <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationDirection"><code>TDPolarizationDirection</code></a></span> equal to 1, and another for it equal to 3. Note that if there are less than 3 equivalent axes, <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPolarizationWprime"><code>TDPolarizationWprime</code></a></span> is irrelevant.
</p>
<h2><span class="mw-headline" id="References">References</span></h2>
<div class="mw-references-wrap"><ol class="references">
<li id="cite_note-1"><span class="mw-cite-backlink"><a href="#cite_ref-1">↑</a></span> <span class="reference-text">
M.J.T. Oliveira, A. Castro, M.A.L. Marques, and A. Rubio, <i>On the use of Neumann's principle for the calculation of the polarizability tensor of nanostructures</i>, <a rel="nofollow" class="external text" href="http://dx.doi.org/10.1166/jnn.2008.142">J. Nanoscience and Nanotechnology</a> <b>8</b> 1-7 (2008) (<a rel="nofollow" class="external text" href="https://arxiv.org/abs/0710.2624v1">arXiv</a>)</span>
</li>
</ol></div>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Triplet_excitations" title="Tutorial:Triplet excitations">Triplet excitations</a> - 
</p><p>Back to <a href="/wiki/Tutorial_Series:Optical_response" title="Tutorial Series:Optical response">Optical response</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155152
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.429 seconds
Real time usage: 6.726 seconds
Preprocessor visited node count: 766/1000000
Preprocessor generated node count: 1705/1000000
Post‐expand include size: 5161/2097152 bytes
Template argument size: 2563/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 5719/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00% 5980.087      1 -total
  0.92%   54.822     19 Template:Variable
  0.49%   29.015      5 Template:If
  0.43%   25.600     19 Template:Octopus_version
  0.37%   22.157      1 Template:Article
  0.34%   20.085      6 Template:P2
  0.24%   14.311      1 Template:Tutorial_foot
  0.17%   10.180      4 Template:P1
  0.15%    8.729     19 Template:Code
  0.09%    5.545     19 Template:Octopus_major_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2122-0!canonical!math=0 and timestamp 20230412155145 and revision id 11355
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;oldid=11355">http:///mediawiki/index.php?title=Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;oldid=11355</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Advanced" title="Category:Advanced">Advanced</a></li><li><a href="/wiki/Category:Time-dependent" title="Category:Time-dependent">Time-dependent</a></li><li><a href="/wiki/Category:Molecule" title="Category:Molecule">Molecule</a></li><li><a href="/wiki/Category:Pseudopotentials" title="Category:Pseudopotentials">Pseudopotentials</a></li><li><a href="/wiki/Category:DFT" title="Category:DFT">DFT</a></li><li><a href="/wiki/Category:Optical_Absorption" title="Category:Optical Absorption">Optical Absorption</a></li><li><a href="/wiki/Category:Oct-propagation_spectrum" title="Category:Oct-propagation spectrum">Oct-propagation spectrum</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AUse+of+symmetries+in+optical+spectra+from+time-propagation&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;oldid=11355" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Use_of_symmetries_in_optical_spectra_from_time-propagation&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 1 September 2021, at 11:47.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.429","walltime":"6.726","ppvisitednodes":{"value":766,"limit":1000000},"ppgeneratednodes":{"value":1705,"limit":1000000},"postexpandincludesize":{"value":5161,"limit":2097152},"templateargumentsize":{"value":2563,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":5719,"limit":5000000},"timingprofile":["100.00% 5980.087      1 -total","  0.92%   54.822     19 Template:Variable","  0.49%   29.015      5 Template:If","  0.43%   25.600     19 Template:Octopus_version","  0.37%   22.157      1 Template:Article","  0.34%   20.085      6 Template:P2","  0.24%   14.311      1 Template:Tutorial_foot","  0.17%   10.180      4 Template:P1","  0.15%    8.729     19 Template:Code","  0.09%    5.545     19 Template:Octopus_major_version"]},"cachereport":{"timestamp":"20230412155152","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":112});});</script>
</body>
</html>
