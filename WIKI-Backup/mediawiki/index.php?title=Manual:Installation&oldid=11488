<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:Installation - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Installation","wgTitle":"Manual:Installation","wgCurRevisionId":11488,"wgRevisionId":11488,"wgArticleId":1344,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Installation","wgRelevantArticleId":1344,"wgRequestId":"0158b4ac47e47e434e73e160","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Installation rootpage-Manual_Installation skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:Installation</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 16:11, 23 September 2021 by <a href="/mediawiki/index.php?title=User:Nicolastd&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:Nicolastd (page does not exist)"><bdi>Nicolastd</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Nicolastd&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Nicolastd (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Nicolastd" class="mw-usertoollinks-contribs" title="Special:Contributions/Nicolastd">contribs</a>)</span> <span class="comment">(<span dir="auto"><span class="autocomment"><a href="#Compiling_and_installing">→‎Compiling and installing</a></span></span>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Manual:Installation&amp;diff=prev&amp;oldid=11488" title="Manual:Installation">diff</a>) <a href="/mediawiki/index.php?title=Manual:Installation&amp;direction=prev&amp;oldid=11488" title="Manual:Installation">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Maybe somebody else installed Octopus for you. In that case, the files
should be under some directory that we can call <b><tt>PREFIX/</tt></b>, the executables
in <b><tt>PREFIX/bin/</tt></b> (e.g. if <b><tt>PREFIX/</tt></b>=<b><tt>/usr/local/</tt></b>, the main Octopus
executable is then in <b><tt>/usr/local/bin/octopus</tt></b>); the pseudopotential files that Octopus will
need in <b><tt>PREFIX/share/octopus/PP/</tt></b>, etc.
</p><p>However, you may be unlucky and that is not the case. In the following we will try
to help you with the still rather unfriendly task of compiling and installing the <tt>octopus</tt>.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Instructions_for_Specific_Architectures"><span class="tocnumber">1</span> <span class="toctext">Instructions for Specific Architectures</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Dowloading"><span class="tocnumber">2</span> <span class="toctext">Dowloading</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Binaries"><span class="tocnumber">2.1</span> <span class="toctext">Binaries</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Source_code"><span class="tocnumber">2.2</span> <span class="toctext">Source code</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-5"><a href="#Building"><span class="tocnumber">3</span> <span class="toctext">Building</span></a>
<ul>
<li class="toclevel-2 tocsection-6"><a href="#Quick_instructions"><span class="tocnumber">3.1</span> <span class="toctext">Quick instructions</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#Slow_instructions"><span class="tocnumber">3.2</span> <span class="toctext">Slow instructions</span></a></li>
<li class="toclevel-2 tocsection-8"><a href="#Long_instructions"><span class="tocnumber">3.3</span> <span class="toctext">Long instructions</span></a>
<ul>
<li class="toclevel-3 tocsection-9"><a href="#Requirements"><span class="tocnumber">3.3.1</span> <span class="toctext">Requirements</span></a></li>
<li class="toclevel-3 tocsection-10"><a href="#Optional_libraries"><span class="tocnumber">3.3.2</span> <span class="toctext">Optional libraries</span></a></li>
<li class="toclevel-3 tocsection-11"><a href="#Unpacking_the_sources"><span class="tocnumber">3.3.3</span> <span class="toctext">Unpacking the sources</span></a></li>
<li class="toclevel-3 tocsection-12"><a href="#Development_version"><span class="tocnumber">3.3.4</span> <span class="toctext">Development version</span></a></li>
<li class="toclevel-3 tocsection-13"><a href="#Configuring"><span class="tocnumber">3.3.5</span> <span class="toctext">Configuring</span></a></li>
<li class="toclevel-3 tocsection-14"><a href="#Compiling_and_installing"><span class="tocnumber">3.3.6</span> <span class="toctext">Compiling and installing</span></a></li>
<li class="toclevel-3 tocsection-15"><a href="#Testing_your_build"><span class="tocnumber">3.3.7</span> <span class="toctext">Testing your build</span></a></li>
<li class="toclevel-3 tocsection-16"><a href="#Fast_recompilation"><span class="tocnumber">3.3.8</span> <span class="toctext">Fast recompilation</span></a></li>
</ul>
</li>
</ul>
</li>
</ul>
</div>

<h3><span class="mw-headline" id="Instructions_for_Specific_Architectures">Instructions for Specific Architectures</span></h3>
<p>See <a href="/wiki/Manual:Specific_architectures" title="Manual:Specific architectures">step-by-step instructions</a> for some specific supercomputers and generic configurations (including Ubuntu and Mac OSX). If the system you are using is in the list, this will be the easiest way to install. Add entries for other supercomputers or generic configurations on which you have successfully built the code.
</p>
<h2><span class="mw-headline" id="Dowloading">Dowloading</span></h2>
<p>Download the latest Octopus version here: <a href="/wiki/Octopus_12" title="Octopus 12">Octopus 12</a>.
</p>
<h3><span class="mw-headline" id="Binaries">Binaries</span></h3>
<p>If you want to install Octopus in a Debian-based Linux box (Debian or Ubuntu, you might not need to compile it;  we release binary packages for some platforms. Keep in mind that these packages are intended to run on different systems and are therefore only moderately optimized. If this is an issue you have to compile the package yourself with the appropriate compiler flags and libraries (see below).
</p><p>Download the appropriate <b><tt>.deb</tt></b> file from the <a href="/wiki/Octopus_12" title="Octopus 12"> downloads page for the current version</a>. Install it (using root access) with the command below, using the appropriate filename you downloaded:
</p>
<pre> <tt><i>$ dpkg -i octopus_package.deb</i></tt>
</pre>
<h3><span class="mw-headline" id="Source_code">Source code</span></h3>
<p>If you have a different system from those mentioned above or you want to compile Octopus you need to get the source code file (<b><tt>.tar.gz</tt></b> file) and follow the compilation instructions below.
</p>
<h2><span class="mw-headline" id="Building">Building</span></h2>
<h3><span class="mw-headline" id="Quick_instructions">Quick instructions</span></h3>
<p>For the impatient, here is the quick-start: 
</p>
<pre> <tt><i>$ tar xzf octopus-12.0.tar.gz</i></tt> 
 <tt><i>$ cd octopus-12.0</i></tt>
 <tt><i>$ ./configure</i></tt>
 <tt><i>$ make</i></tt>
 <tt><i>$ make install</i></tt>
</pre>
<p><br />
This will probably <em>not</em> work, so before giving up, just read
the following paragraphs.
</p>
<h3><span class="mw-headline" id="Slow_instructions">Slow instructions</span></h3>
<p>There is an <a href="/wiki/Manual:Building_from_scratch" title="Manual:Building from scratch">appendix</a> with detailed instructions on how to compile Octopus and the required libraries from scratch -- you only need to do this if you are unable to install from a package manager or use per-built libraries.
</p>
<h3><span class="mw-headline" id="Long_instructions">Long instructions</span></h3>
<p>The code is written in standard Fortran 2003, with some routines written in C (and in bison, if we count the input parser). To build it you will need both a C compiler (gcc works just fine and it is available for almost every piece of silicon), and a
Fortran 2003 compiler. You can check in the <a href="/wiki/Manual:Appendix:Compilers" class="mw-redirect" title="Manual:Appendix:Compilers">Compilers Appendix</a> which compilers Octopus has been tested with. This appendix also contains hints on potential problems with certain platform/compiler combinations and how to fix them.
</p>
<h4><span class="mw-headline" id="Requirements">Requirements</span></h4>
<p><br />
</p><p>Besides the compiler, you will also need:
</p>
<dl><dt>make</dt>
<dd>most computers have it installed, otherwise just grab and install the GNU make.</dd></dl>
<dl><dt>cpp</dt>
<dd>The C preprocessor is heavily used in Octopus to preprocess Fortran code. It is used for both C (from the CPP variable) and Fortran (FCCPP). GNU cpp is the most convenient but others may work too. For more info, see <a href="/wiki/Preprocessors" title="Preprocessors">Preprocessors</a>.</dd></dl>
<dl><dt>Libxc</dt>
<dd>The library of exchange and correlation functionals. It used to be a part of Octopus, but since version 4.0.0 it is a standalone library and needs to be installed independently.  For more information, see the <a rel="nofollow" class="external text" href="http://www.tddft.org/programs/Libxc">Libxc page</a>. Octopus 4.0.0 and 4.0.1 require version 1.1.0 (not 1.2.0 or 1.0.0). Octopus 4.1.2 requires version 2.0.x or 2.1.x, and won't compile with 2.2.x. (Due to bugfixes from libxc version 2.0 to 2.1, there will be small discrepancies in the testsuite for <code>functionals/03-xc.gga_x_pbea.inp</code> and <code>periodic_systems/07-tb09.test</code>). Octopus 5.0.0 supports libxc versions 2.0.x, 2.1.x and 2.2.x. Please note: The Libxc testsuite prior to 2.1 will report some errors in most cases. This is not something to worry about.</dd></dl>
<dl><dt>FFTW</dt>
<dd>We have relied on this great library to perform Fast Fourier Transforms (FFTs). You may grab it from the <a rel="nofollow" class="external text" href="http://www.fftw.org/">FFTW site</a>. You require FFTW version 3.</dd></dl>
<dl><dt>LAPACK/BLAS</dt>
<dd>Our policy is to rely on these two libraries as much as possible on these libraries for linear-algebra operations. If you are running Linux, there is a fair chance they are already installed in your system. The same goes to the more heavy-weight machines (alphas, IBMs, SGIs, etc.). Otherwise, just grab the source from <a rel="nofollow" class="external text" href="http://www.netlib.org">netlib site</a>.</dd></dl>
<dl><dt>GSL</dt>
<dd>Finally someone had the nice idea of making a public scientific library! GSL still needs to grow, but it is already quite useful and impressive. Octopus uses splines, complex numbers, special functions, etc. from GSL, so it is a must! If you don't have it already installed in your system, you can obtain GSL from the <a rel="nofollow" class="external text" href="http://www.gnu.org/software/gsl/">GSL site</a>. You will need version 1.9 or higher. Version 4.0 of Octopus (and earlier) can only use GSL 1.14 (and earlier). A few tests will fail if you use GSL 1.15 or later. Version 5.0.0 of Octopus (and earlier) can only use GSL 1.16 or earlier, due to a bug in our configure script.</dd></dl>
<dl><dt>Perl</dt>
<dd>During the build process Octopus runs several scripts in this language. It's normally available in every modern Unix system.</dd></dl>
<p><br />
</p>
<h4><span class="mw-headline" id="Optional_libraries">Optional libraries</span></h4>
<p><br />
</p><p>There are also some optional packages; without them some parts of Octopus won't work:
<br />
</p>
<dl><dt>MPI</dt>
<dd>If you want to run Octopus in multi-tentacle (parallel) mode, you will need an implementation of MPI. <a rel="nofollow" class="external text" href="http://www-unix.mcs.anl.gov/mpi/mpich/">MPICH</a> or <a rel="nofollow" class="external text" href="http://www.open-mpi.org/">Open MPI</a> work just fine in our Linux boxes.</dd></dl>
<dl><dt>PFFT</dt>
<dd>We rely on this great library for highly scalable parallel Poisson solver, based on Fast Fourier Transforms (FFTs). You may grab it from the <a rel="nofollow" class="external text" href="https://www-user.tu-chemnitz.de/~potts/workgroup/pippig/software.php.en">M. Pippig's site</a>. You also require FFTW version 3.3 compiled with MPI and with a small patch by M. Pippig (also available there).</dd></dl>
<dl><dt>NetCDF</dt>
<dd>The <a rel="nofollow" class="external text" href="http://www.unidata.ucar.edu/software/netcdf/">Network Common Dataform</a> library is needed for writing the binary files in a machine-independent, well-defined format, which can also be read by <a href="/wiki/Manual:Visualization" title="Manual:Visualization">visualization programs</a> such as <a rel="nofollow" class="external text" href="http://www.opendx.org/">OpenDX</a></dd></dl>
<dl><dt>GDLib</dt>
<dd>A library to read graphic files. See <a href="/wiki/Tutorial:Particle_in_an_octopus" title="Tutorial:Particle in an octopus">Tutorial:Particle in an octopus</a>. (The simulation box in 2D can be specified via <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=BoxShapeImage"><code>BoxShapeImage</code></a></span>.) Available from <a rel="nofollow" class="external free" href="http://www.libgd.org/">http://www.libgd.org/</a></dd></dl>
<dl><dt>SPARSKIT</dt>
<dd><a rel="nofollow" class="external text" href="http://www-users.cs.umn.edu/~saad/software/SPARSKIT/">Library for sparse matrix calculations</a>. Used for one propagator technique.</dd></dl>
<dl><dt>ETSF I/O</dt>
<dd>An input/output library implementing the ETSF standardized formats, requiring NetCDF, available at <a rel="nofollow" class="external autonumber" href="http://www.etsf.eu/resources/software/libraries_and_tools">[1]</a>. Versions 1.0.2, 1.0.3, and 1.0.4 are compatible with Octopus (though 1.0.2 will produce a small discrepancy in a filesize in the testsuite). It must have been compiled with the same compiler you are using with Octopus. To use ETSF_IO, include this in the <tt>configure</tt> line, where <tt>$DIR</tt> is the path where the library was installed:</dd></dl>
<pre>--with-etsf-io-prefix="$DIR"
</pre>
<dl><dt>LibISF</dt>
<dd>(version 5.0.0 and later) To perform highly scalable parallel Poisson solver, based on BigDFT 1.7.6, with a cheap memory footprint. You may grab it from the <a rel="nofollow" class="external text" href="http://bigdft.org/">BigDFT site</a>. You require BigDFT version 1.7.6 compiled with MPI, following these instructions: <a rel="nofollow" class="external text" href="http://bigdft.org/Wiki/index.php?title=Installation#Building_the_Poisson_Solver_library_only">installation instructions </a>. Probably, you have to manually copy the files "libwrappers.a" and "libflib.a" to the installation "/lib" directory. To configure Octopus, you have to add this configure line:</dd></dl>
<pre> --with-isf-prefix="$DIR"
</pre>
<h4><span class="mw-headline" id="Unpacking_the_sources">Unpacking the sources</span></h4>
<p>Uncompress and untar it (<tt><i>gzip -cd octopus-12.0.tar.gz </i></tt>). In the following, <b><tt>OCTOPUS-SRC/</tt></b> denotes the source directory of Octopus, created by the <tt><i>tar</i></tt> command.
</p><p><br />
The <b><tt>OCTOPUS-SRC/</tt></b> contains the following subdirectories of interest to users:
</p>
<dl><dt><b><tt>doc/</tt></b></dt>
<dd>The documentation of Octopus, mainly in HTML format.</dd></dl>
<dl><dt><b><tt>liboct_parser/</tt></b></dt>
<dd>The C library that handles the input parsing.</dd></dl>
<dl><dt><b><tt>share/PP/</tt></b></dt>
<dd>Pseudopotentials. In practice now it contains the Troullier-Martins (PSF and UPF formats) and Hartwigsen-Goedecker-Hutter pseudopotential files.</dd></dl>
<dl><dt><b><tt>share/util/</tt></b></dt>
<dd>Currently, the <em>utilities</em> include a couple of IBM OpenDX networks (<b><tt>mf.net</tt></b>), to visualize wavefunctions, densities, etc.</dd></dl>
<dl><dt><b><tt>testsuite/</tt></b></dt>
<dd>Used to check your build. You may also use the files in here as samples of how to do various types of calculations.</dd></dl>
<dl><dt><b><tt>src/</tt></b></dt>
<dd>Fortran90 and C source files. Note that the Fortran90 files have to be preprocessed before being fed to the Fortran compiler, so do not be scared by all the # directives.</dd></dl>
<h4><span class="mw-headline" id="Development_version">Development version</span></h4>
<p>You can get the development version of Octopus by downloading it from the Octopus project on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus">gitlab.com</a>.
</p><p>You can also get the current version with the following command (you need the git package):
</p>
<pre> <tt><i>$ git clone git@gitlab.com:octopus-code/octopus.git</i></tt>
</pre>
<p>Before running the configure script, you will need to run the GNU autotools. This may be done by executing:
</p>
<pre> <tt><i>$ autoreconf -i</i></tt>
</pre>
<p>Note that you need to have working recent versions of the automake and autoconf. In particular, the configure script may fail in the part <tt>checking for Fortran libraries of mpif90</tt> for <tt>autoconf</tt> version 2.59 or earlier. The solution is to update <tt>autoconf</tt> to 2.60 or later, or manually set <tt>FCLIBS</tt> in the <tt>configure</tt> command line to remove a spurious apostrophe.
</p><p>If autoreconf is failing with "aclocal: warning: couldn't open directory 'm4': No such file or directory", create an empty folder named m4 inside external_libs/spglib-1.9.9/.
</p><p>Please be aware that the development version may contain untested changes that can affect the execution and the results of Octopus, especially if you are using new and previously unreleased features. So if you want to use the development version for production runs, you should at least contact Octopus developers.
</p>
<h4><span class="mw-headline" id="Configuring">Configuring</span></h4>
<p>Before configuring you can (should) set up a couple of options. Although
the configure script tries to guess your system settings for you, we recommend 
that you set explicitly the default Fortran compiler and the compiler options.
Note that configure is a standard tool for Unix-style programs and you can find a lot of generic documentation on how it works elsewhere.
</p><p>For example, in bash you would typically do:
</p>
<pre> <tt><i>$ export FC=ifort</i></tt>
 <tt><i>$ export FCFLAGS=&quot;-O2 -xHost&quot;</i></tt>
</pre>
<p>if you are using the Intel Fortran compiler on a linux machine.
</p><p>Also, if you have some of the required libraries in some unusual directories,
these directories may be placed in the variable <code>LDFLAGS</code> (e.g.,
<tt><i>export LDFLAGS=$LDFLAGS:/opt/lib/</i></tt>).
</p><p>The configuration script will try to find out which compiler you are using.
Unfortunately, and due to the nature of the primitive language that Octopus
is programmed in, the automatic test fails very often. Often it is better to set
the variable <code>FCFLAGS</code> by hand, check the <a href="/wiki/Manual:Appendix:Compilers" class="mw-redirect" title="Manual:Appendix:Compilers">Compilers Appendix</a> page for which flags have been reported to work with different Fortran compilers.
</p><p>You can now run the configure script 
</p>
<pre> <tt><i>$ ./configure</i></tt>
</pre>
<p>You can use a fair amount of options to spice Octopus to your own taste. To obtain a full list just type <tt><i>./configure --help</i></tt>. Some 
commonly used options include:
</p>
<dl><dt><code>--prefix=</code><b><tt>PREFIX/</tt></b></dt>
<dd>Change the base installation dir of Octopus to <b><tt>PREFIX/</tt></b>. <b><tt>PREFIX/</tt></b> defaults to the home directory of the user who runs the configure script.</dd></dl>
<dl><dt><code>--with-fft-lib=</code><b><tt>&lt;lib&gt;</tt></b></dt>
<dd>Instruct the configure script to look for the FFTW library exactly in the way that it is specified in the <b><tt>&lt;lib&gt;</tt></b> argument. You can also use the <code>FFT_LIBS</code> environment variable.</dd></dl>
<dl><dt><code>--with-pfft-prefix=</code><b><tt>DIR/</tt></b></dt>
<dd>Installation directory of the PFFT library.</dd></dl>
<dl><dt><code>--with-pfft-lib=</code><b><tt>&lt;lib&gt;</tt></b></dt>
<dd>Instruct the configure script to look for the PFFT library exactly in the way that it is specified in the <b><tt>&lt;lib&gt;</tt></b> argument. You can also use the <code>PFFT_LIBS</code> environment variable.</dd></dl>
<dl><dt><code>--with-blas=</code><b><tt>&lt;lib&gt;</tt></b></dt>
<dd>Instruct the configure script to look for the BLAS library in the way that it is specified in the <b><tt>&lt;lib&gt;</tt></b> argument.</dd></dl>
<dl><dt><code>--with-lapack=</code><b><tt>&lt;lib&gt;</tt></b></dt>
<dd>Instruct the configure script to look for the LAPACK library in the way that it is specified in the <b><tt>&lt;lib&gt;</tt></b> argument.</dd></dl>
<dl><dt><code>--with-gsl-prefix=</code><b><tt>DIR/</tt></b></dt>
<dd>Installation directory of the GSL library. The libraries are expected to be in <b><tt>DIR/lib/</tt></b> and the include files in <b><tt>DIR/include/</tt></b>. The value of <b><tt>DIR/</tt></b> is usually found by issuing the command <tt><i>gsl-config --prefix</i></tt>.</dd></dl>
<dl><dt><code>--with-libxc-prefix=</code><b><tt>DIR/</tt></b></dt>
<dd>Installation directory of the Libxc library.</dd></dl>
<p>If you have problems when the configure script runs, you can find more details of what happened in the file config.log in the same directory.
</p>
<h4><span class="mw-headline" id="Compiling_and_installing">Compiling and installing</span></h4>
<p>Run <tt><i>make</i></tt> and then <tt><i>make install</i></tt>. The compilation may take some time, so you might want to speed it up by running <tt><i>make</i></tt> in parallel (<tt><i>make -j</i></tt>). If everything went fine, you should now be able to taste Octopus. 
</p><p>Depending on the value given to the --prefix=<b><tt>PREFIX/</tt></b> given, the executables will reside in <b><tt>PREFIX/bin/</tt></b>, and the auxiliary files will be copied to <b><tt>PREFIX/share/octopus</tt></b>.
</p><p>Note that starting from <b>GFortran 10</b>, one needs to add in the <tt>FCFLAGS</tt>
</p>
<pre> FCFLAGS =-fallow-argument-mismatch
</pre>
<p>For <b>GFortran 11</b>, one can only use C++17 standard, that one obtains by setting
</p>
<pre> CXXFLAGS=-std=c++17
</pre>
<h4><span class="mw-headline" id="Testing_your_build">Testing your build</span></h4>
<p>After you have successfully built Octopus, to check that your build works as expected there is a battery of tests that you can run.  They will check that Octopus executes correctly and gives the expected results (at least for these test cases). If the parallel version was built, the tests will use up to 6 MPI processes, though it should be fine to run on only 4 cores. (MPI implementations generally permit using more tasks than actual cores, and running tests this way makes it likely for developers to find race conditions.)
</p><p>To run the tests, in the sources directory of Octopus use the command
</p>
<pre> <tt><i>$ make check</i></tt>
</pre>
<p>or if you are impatient,
</p>
<pre> <tt><i>$ make check-short</i></tt>
</pre>
<p>which will start running the tests, informing you whether the tests are passed or not. For examples of job scripts to run on a machine with a scheduler, please see <a href="/wiki/Manual:Specific_architectures" title="Manual:Specific architectures">Manual:Specific_architectures</a>.
</p><p>If all tests fail, maybe there is a problem with your executable (like a missing shared library). 
</p><p>If only some of the tests fail, it might be a problem when calling some external libraries (typically blas/lapack).  Normally it is necessary to compile all Fortran libraries with the same compiler. If you have trouble, try to look for help in the <a rel="nofollow" class="external text" href="http://www.tddft.org/mailman/listinfo/octopus-users">Octopus mailing list</a>.
</p>
<h4><span class="mw-headline" id="Fast_recompilation">Fast recompilation</span></h4>
<p>NOTE: This feature is currently only available in the development version and the plan is to include it in the Octopus 9 release.
</p><p>If you have already compiled the code and if you are changing only one file, you can run
</p>
<pre> <tt><i>$ make NODEP=1</i></tt>
</pre>
<p>to ignore the dependencies due to Fortran module files. This will only compile the files that have changed and link the executables; therefore, it is much faster. If you change, e.g., interfaces of modules or functions, you need to to run <tt><i>make</i></tt> without <tt><i>NODEP=1</i></tt> to ensure a correct handling of the dependencies.
</p><p><br />
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:About_Octopus" title="Manual:About Octopus">Manual:About Octopus</a> - Next <a href="/wiki/Manual:Input_file" title="Manual:Input file">Manual:Input file</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412153808
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.248 seconds
Real time usage: 0.273 seconds
Preprocessor visited node count: 1282/1000000
Preprocessor generated node count: 3548/1000000
Post‐expand include size: 4977/2097152 bytes
Template argument size: 2692/2097152 bytes
Highest expansion depth: 5/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 21/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  157.458      1 -total
 16.29%   25.644     14 Template:Command_line
 12.61%   19.857     25 Template:Command
 10.89%   17.153     69 Template:Name
  7.01%   11.037     10 Template:Inst_file
  6.94%   10.920      8 Template:Flag
  6.71%   10.573     33 Template:File
  6.71%   10.565      1 Template:Manual_foot
  5.99%    9.431      4 Template:Octopus_version
  5.33%    8.394     15 Template:Code
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1344-0!canonical and timestamp 20230412153807 and revision id 11488
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:Installation&amp;oldid=11488">http:///mediawiki/index.php?title=Manual:Installation&amp;oldid=11488</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AInstallation&amp;returntoquery=oldid%3D11488" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Installation" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk"><span><a href="/wiki/Talk:Manual:Installation" rel="discussion" title="Discussion about the content page [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:Installation">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Installation&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Installation&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Installation" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Installation" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Manual:Installation&amp;oldid=11488&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:Installation&amp;oldid=11488" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Installation&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 23 September 2021, at 16:11.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.248","walltime":"0.273","ppvisitednodes":{"value":1282,"limit":1000000},"ppgeneratednodes":{"value":3548,"limit":1000000},"postexpandincludesize":{"value":4977,"limit":2097152},"templateargumentsize":{"value":2692,"limit":2097152},"expansiondepth":{"value":5,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":21,"limit":5000000},"timingprofile":["100.00%  157.458      1 -total"," 16.29%   25.644     14 Template:Command_line"," 12.61%   19.857     25 Template:Command"," 10.89%   17.153     69 Template:Name","  7.01%   11.037     10 Template:Inst_file","  6.94%   10.920      8 Template:Flag","  6.71%   10.573     33 Template:File","  6.71%   10.565      1 Template:Manual_foot","  5.99%    9.431      4 Template:Octopus_version","  5.33%    8.394     15 Template:Code"]},"cachereport":{"timestamp":"20230412153808","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":135});});</script>
</body>
</html>
