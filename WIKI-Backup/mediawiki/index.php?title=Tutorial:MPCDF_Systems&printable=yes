<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:MPCDF Systems - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:MPCDF_Systems","wgTitle":"Tutorial:MPCDF Systems","wgCurRevisionId":11409,"wgRevisionId":11409,"wgArticleId":3067,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:MPCDF_Systems","wgRelevantArticleId":3067,"wgRequestId":"382e1bb4f165004596ecde87","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_MPCDF_Systems rootpage-Tutorial_MPCDF_Systems skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:MPCDF Systems</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>For this course, resources are provided at the MPCDF (Max Planck Computing and
Data Facility), the central computing center of the Max Planck Society.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Connecting_to_MPCDF_systems_via_a_gateway"><span class="tocnumber">1</span> <span class="toctext">Connecting to MPCDF systems via a gateway</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#ssh_config_made_easy"><span class="tocnumber">2</span> <span class="toctext">ssh config made easy</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Connect_to_the_supercomputer"><span class="tocnumber">3</span> <span class="toctext">Connect to the supercomputer</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#Software_environment"><span class="tocnumber">4</span> <span class="toctext">Software environment</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Connecting_to_MPCDF_systems_via_a_gateway">Connecting to MPCDF systems via a gateway</span></h2>
<p>The compute resources at the MPCDF run Linux, and login is only possible via
SSH in combination with two-factor authentication (see below). Any modern
operating system (Linux, MacOS, Windows 10) provides an SSH client which is
typically invoked via the ssh command from a terminal. To access a system at
the MPCDF from the public Internet it is necessary to log into one of the
gateway machines first, and then log from the gateway system into the target
system.
</p><p>You can connect to the gateway machine gatezero.mpcdf.mpg.de and then further
to HPC systems. If you connect for the very first time, you will get a warning
that the host’s authenticity can not be verified. Provided that you can find
the presented “fingerprint” in the list below, it is safe to answer with “yes”
to continue connecting:
</p>
<ul><li>ED25519 key fingerprint is SHA256:qjBJoqcJcCM0LyTqtj09BAxS74u81SizY9zob+XwEOA.</li>
<li>RSA key fingerprint is SHA256:zF/sNLAYqwwRlY3/lhb1A805pGiQiF3GhGP1bBCpvik.</li></ul>
<p>In order to connect to the gateway machine via ssh, you need to setup
two-factor authentication (2FA, see <a rel="nofollow" class="external free" href="https://docs.mpcdf.mpg.de/faq/2fa.html">https://docs.mpcdf.mpg.de/faq/2fa.html</a>).
For this, tyhe following steps are needed:
</p>
<ul><li>Visit <a rel="nofollow" class="external free" href="https://selfservice.mpcdf.mpg.de">https://selfservice.mpcdf.mpg.de</a> and log in</li>
<li>In the menu bar at the top of the page, click “My account &gt; Security”</li>
<li>Select “Configure 2FA” and provide your password</li>
<li>Choose a primary token type, recommended is an OTP app (OTP one time password)</li>
<li>Scan the QR code with the app on your phone</li>
<li>Validate the token by providing a valid OTP</li>
<li>Choose a secondary token type</li></ul>
<h2><span class="mw-headline" id="ssh_config_made_easy">ssh config made easy</span></h2>
<p>In order to avoid typing in your password repeatedly, you can configure a
ControlMaster setup for ssh (see
<a rel="nofollow" class="external free" href="https://docs.mpcdf.mpg.de/faq/2fa.html#do-i-have-to-type-in-an-otp-every-time-i-access-the-secured-systems">https://docs.mpcdf.mpg.de/faq/2fa.html#do-i-have-to-type-in-an-otp-every-time-i-access-the-secured-systems</a>).
The following snippet can be added to <tt>~/.ssh/config</tt> and should work for Linux
and MacOS (replace YOUR_USER_NAME with your user name):
</p>
<pre># Correctly resolve short names of gateway machines and HPC nodes
Match originalhost gate*,cobra,raven
    CanonicalDomains mpcdf.mpg.de
    CanonicalizeFallbackLocal no
    CanonicalizeHostname yes

# Keep a tunnel open for the day when accessing the gate machines
Match canonical host gate*
    User YOUR_USER_NAME
    Compression yes
    ServerAliveInterval 120
    ControlMaster auto
    ControlPersist 10h
    ControlPath ~/.ssh/master-%C

# Keep a tunnel open for the day when accessing the HPC nodes
Match canonical host cobra*,raven*
    User YOUR_USER_NAME
    Compression yes
    ControlMaster auto
    ControlPersist 10h
    ControlPath ~/.ssh/master-%C
    # OpenSSH &gt;=7.3
    ProxyJump gatezero
    # OpenSSH &lt;=7.2
    #ProxyCommand ssh -W %h:%p gatezero
</pre>
<h2><span class="mw-headline" id="Connect_to_the_supercomputer">Connect to the supercomputer</span></h2>
<p>We will use the supercomputer cobra in this tutorial, so you can use
</p>
<pre>ssh cobra
</pre>
<p>to connect via gatezero to the login node of cobra.
</p><p>More information on cobra can be found at
<a rel="nofollow" class="external free" href="https://docs.mpcdf.mpg.de/doc/computing/cobra-user-guide.html">https://docs.mpcdf.mpg.de/doc/computing/cobra-user-guide.html</a>
</p><p>So please login to cobra. You will find your home directory at
/u/YOUR_USER_NAME, which is on one of two fast parallel file systems (GPFS).
For the purposes of this course, you can store data and run simulations under
this folder. For production runs with serious I/O, please use the ptmp file
system under /ptmp/YOUR_USER_NAME because it is larger and more powerful. Be
aware that files that have not been accessed for more than 12 weeks are deleted
on ptmp.
</p>
<h2><span class="mw-headline" id="Software_environment">Software environment</span></h2>
<p>The software stack on cobra is available via environment modules
(<a rel="nofollow" class="external free" href="https://docs.mpcdf.mpg.de/doc/computing/software/environment-modules.html">https://docs.mpcdf.mpg.de/doc/computing/software/environment-modules.html</a>).
</p>
<ul><li>`module avail` shows available software packages</li>
<li>`module load package_name` loads a module, such that binaries are in the path; for many packages, a variable named &lt;PKG&gt;_HOME (with the package name) is exported to be used for compiling and linking codes</li>
<li>`module unload package_name` unloads a module and cleans the environment variables</li>
<li>`module purge` unloads all modules and creates a clean environment</li></ul>
<p>To manage the plethora of software packages resulting from all the relevant
combinations of compilers and MPI libraries, we have decided to organize the
environment module system for accessing these packages in a natural hierarchical
manner. Compilers (gcc, intel) are located on the uppermost level, depending
libraries (e.g., MPI) on the second level, more depending libraries on a third
level. This means that not all the modules are visible initially: only after
loading a compiler module, will the modules depending on this become available.
Similarly, loading an MPI module in addition will make the modules depending on
the MPI library available.
</p><p><br />
In case you know the name of the module you wish to load, but you are not sure
about the available versions or what dependencies need to be loaded first, you
can try to use the ‘find-module’ command. This tool searches for the MODULENAME
string through a list of all installed modules:
</p>
<pre>find-module MODULENAME
</pre>
<p>Many software packages, such as octopus, are available as modules. Please run
`find-module octopus` to find all octopus modules and the compiler and MPI
modules you need to load in order to make a specific version available.
</p><p>Three modules of octopus are available for combinations of compiler and MPI
packages (here for version 11):
</p>
<ul><li>octopus/11: standard package, compiled against MKL and using FFTs from MKL</li>
<li>octopus-pfft/11: compiled against PFFT and FFTW to make the better-scaling PFFT backend for the FFT Poisson solver available</li>
<li>octopus-gpu/11: version compiled to run on GPUs</li></ul>
<p>To load the default version of octopus/11, run:
</p>
<pre>module purge
module load intel/19.1.3 impi/2019.9 octopus/11
</pre>
<p>Now, octopus is available in the path, so you can confirm the octopus version with
</p>
<pre>oct-help -v
</pre>
<p>This should print out the version of octopus:
</p>
<pre>octopus 11.1 (git commit )
</pre>
<span class="noprint"><hr />
<p>- Next <a href="/wiki/Tutorial:Slurm_usage" title="Tutorial:Slurm usage">Slurm usage</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Running_Octopus_on_HPC_systems" title="Tutorial Series:Running Octopus on HPC systems">Running Octopus on HPC systems</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155109
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.051 seconds
Real time usage: 0.058 seconds
Preprocessor visited node count: 166/1000000
Preprocessor generated node count: 537/1000000
Post‐expand include size: 686/2097152 bytes
Template argument size: 719/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 979/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   26.644      1 Template:Tutorial_foot
100.00%   26.644      1 -total
 97.64%   26.016      4 Template:If
 67.72%   18.044      5 Template:P2
 36.75%    9.793      3 Template:P1
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:3067-0!canonical and timestamp 20230412155109 and revision id 11409
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:MPCDF_Systems&amp;oldid=11409">http:///mediawiki/index.php?title=Tutorial:MPCDF_Systems&amp;oldid=11409</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AMPCDF+Systems&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:MPCDF_Systems" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:MPCDF_Systems&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:MPCDF_Systems">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:MPCDF_Systems&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:MPCDF_Systems&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:MPCDF_Systems" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:MPCDF_Systems" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:MPCDF_Systems&amp;oldid=11409" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:MPCDF_Systems&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 6 September 2021, at 09:19.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.051","walltime":"0.058","ppvisitednodes":{"value":166,"limit":1000000},"ppgeneratednodes":{"value":537,"limit":1000000},"postexpandincludesize":{"value":686,"limit":2097152},"templateargumentsize":{"value":719,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":979,"limit":5000000},"timingprofile":["100.00%   26.644      1 Template:Tutorial_foot","100.00%   26.644      1 -total"," 97.64%   26.016      4 Template:If"," 67.72%   18.044      5 Template:P2"," 36.75%    9.793      3 Template:P1"]},"cachereport":{"timestamp":"20230412155109","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":112});});</script>
</body>
</html>
