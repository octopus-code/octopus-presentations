<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Tutorial:RDMFT - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:RDMFT","wgTitle":"Tutorial:RDMFT","wgCurRevisionId":11183,"wgRevisionId":0,"wgArticleId":3026,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:RDMFT","wgRelevantArticleId":3026,"wgRequestId":"8f7a40c90c5fb2f4d76c73ea","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_RDMFT rootpage-Tutorial_RDMFT skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Tutorial:RDMFT</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Tutorial:RDMFT" title="Tutorial:RDMFT">Tutorial:RDMFT</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">In this tutorial, you will learn how to do a Reduced Density Matrix Functional Theory (RDMFT) calculation with {{octopus}}. 

In contrast to density functional theory or Hartree-Fock theory, here we do '''not''' try to find one optimal slater-determinant (single-reference) to describe the ground-state of a given many-body system, but instead approximate the one-body reduced density matrix (1RDM) of the system. Thus, the outcome of an RDMFT minimization is a set of eigen-vectors, so called ''natural orbitals'' (NOs,) and eigenvalues, so-called ''natural occupation numbers'' (NONs), of the 1RDM. One important aspect of this is that we need to use ''more'' orbitals than the number of electrons. This additional freedom allows to include static correlation in the description of the systems and hence, we can describe settings or processes that are very difficult for single-reference methods. One such process is the '''dissociation of a molecule''', which we utilize as example for this tutorial: You will learn how to do a properly converged dissociation study of H&lt;math>_2&lt;/math>. For further information about RDMFT, we can recommend e.g. chapter ''Reduced Density Matrix Functional Theory (RDMFT) and Linear Response Time-Dependent RDMFT (TD-RDMFT)'' in the book of Ferre, N., Filatov, M. &amp; Huix-Rotllant, M. ''Density-Functional Methods for Excited States'' (Springer, 2015). doi:10.1007/978-3-319-22081-9 

== Basic Steps of an RDMFT Calculation ==

The first thing to notice for RDMFT calculations is that, we will explicitly make use of a '''basis set''', which is in contrast to the minimization over the full real-space grid that is performed normally with {{octopus}}. Consequently, we always need to do a preliminary calculation to generate our basis set. We will exemplify this for the hydrogen molecule in equilibrium position. Create the following {{file|inp}} file:

 {{variable|CalculationMode|Calculation_Modes}} = gs
 {{variable|TheoryLevel|Hamiltonian}} = independent_particles
 
 {{variable|Dimensions}} = 3
 {{variable|Radius|Mesh}} = 8
 {{variable|Spacing|Mesh}} = 0.15
 
 # distance between H atoms
 d = 1.4172 # (equilibrium bond length H2)
 
 %{{variable|Coordinates|System}}
  'H' | 0 | 0 | -d/2
  'H' | 0 | 0 | d/2
 %
 
 {{variable|ExtraStates|States}} = 14

You should be already familiar with all these variables, so there is no need to explain them again, but we want to stress the two important points here: 
# We chose {{variable|TheoryLevel|Hamiltonian}} = independent_particles, which you should always use as a basis in an RDMFT calculation (see actual octopus paper?)
# We set the {{variable|ExtraStates|States}} variable, which controls the size of the basis set that will be used in the RDMFT calculation. Thus in RDMFT, we have with the '''basis set''' a '''new numerical parameter''' besides the {{variable|Radius|Mesh}} and the {{variable|Spacing|Mesh}} that needs to be converged for a successful calculation. The size of the basis set M is just the number of the orbitals for the ground state (number of electrons divided by 2) plus the number of extra states.
# All the numerical parameters '''depend on each other''': If we want to include many {{variable|ExtraStates|States}}  to have a sufficiently large basis, we will need also a larger {{variable|Radius|Mesh}} and especially a smaller {{variable|Spacing|Mesh}} at a certain point. The reason is that the additional states will have function values bigger than zero in a larger region than the bound states. Additionally, all states are orthogonal to each other, and thus the grid needs to resolve more nodes with an increasing number of {{variable|ExtraStates|States}}.

With this basis, we can now do our first rdmft calculation. For that, you just need to change the {{variable|TheoryLevel|Hamiltonian}} to 'rdmft' and '''add the part {{variable|ExperimentalFeatures}} = yes'''. In the standard out there will be some new information:
* Calculating Coulomb and exchange matrix elements in basis --this may take a while--
* Occupation numbers: they are not zero or one/two here but instead fractional!
* '''anything else important?'''

== Basis Set Convergence ==

Now, we need to converge our calculation for the three numerical parameters. This needs to be done in some iterative way, which means we first set {{variable|ExtraStates|States}} and then do convergence series for {{variable|Radius|Mesh}} and {{variable|Spacing|Mesh}} as we know it. Then, we utilize the converged values and perform a series for {{variable|ExtraStates|States}}. Having converged {{variable|ExtraStates|States}}, we start again with the series for {{variable|Radius|Mesh}} and {{variable|Spacing|Mesh}} and see if the energy (or better the electron density) still changes considerably. If yes, this meas that the higher lying {{variable|ExtraStates|States}} where net well captured by the simulation box and we need to adjust the parameters again. We go on like this until convergence of all parameters. This sound like a lot of work, but don't worry, if you have done such convergence studies a few times, you will get a feeling for good values.

So let us do one iteration as example. We start with a guess for {{variable|ExtraStates|States}}, which should not be too small, say 14 (thus M=1+14=15) and converge {{variable|Radius|Mesh}}  and {{variable|Spacing|Mesh}}. To save time, we did this for you and found the values
{{variable|Radius|Mesh}} = 8
{{variable|Spacing|Mesh}} = 0.15.
If you feel insecure with such convergence studies, you should have a look in the corresponding tutorial: [[Tutorial:Total_energy_convergence]]

Now let us perform a basis-set series. For that, we first create reference basis by repeating the above calculation with say {{variable|ExtraStates|States}} = 29. We copy the restart folder in a new folder that is called 'basis' and execute the following script (if you rename anything you need to change the respective part in the script):
  
 #!/bin/bash
 
 series=ES-series
 outfile="$series.log"
 echo "#ES    Energy    1. NON  2.NON" > $outfile
 list="4 9 14 19 24 29"
 
 export OCT_PARSE_ENV=1 
 for param in $list 
 do
   folder="$series-$param"
   mkdir $folder
   out_tmp=out-RDMFT-$param
   cd $folder
    # here we specify the basis folder
    cp -r ../basis/restart .
    # create inp file
    {
 cat &lt;&lt;-EOF
 calculationMode = gs
 TheoryLevel = rdmft
 ExperimentalFeatures = yes
 
 ExtraStates = $param
 
 Dimensions = 3
 
 Radius = 8
 Spacing = 0.15
 
 # distance between H atoms
 d = 1.4172 # (equilibrium) 
 
 %Coordinates
   'H' | 0 | 0 | -d/2
   'H' | 0 | 0 | d/2
 %
 
 Output = density
 OutputFormat = axis_x
 EOF
 } > inp
  
   # put the octopus dir here
   [octopus-dir]/bin/octopus > $out_tmp
 
    energy=`grep -a "Total energy" $out_tmp  | tail -1 | awk '{print $3}'`
    seigen=`grep -a " 1  " static/info |  awk '{print $2}'`
    peigen=`grep -a " 2  " static/info |  awk '{print $2}'`
    echo $param $energy $seigen $peigen >> ../$outfile
  cd ..
 done

If everything works out fine, you should find the following output in the 'ES-series.log' file:

 #ES    Energy    1. NON  2.NON
 4 -1.1476150018E+00 1.935750757008 0.032396191164
 9 -1.1498006205E+00 1.932619193929 0.032218954381
 14 -1.1609241676E+00 1.935215440985 0.032526426664
 19 -1.1610006378E+00 1.934832116929 0.032587169713
 24 -1.1622104536E+00 1.932699204653 0.032997371081
 29 -1.1630839347E+00 1.932088486112 0.032929702131

So we see that even with M=30, the calculation is not entirely converged! Thus for a correctly converged result, one would need to further increase the basis and optimize the simulation box. Since the calculations become quite expensive, we would need to do them on a high-performance cluster, which goes beyond the scope of this tutorial. We will thus have to proceed with '''not entirely converged''' parameters in the next section.

== H2 Dissociation ==

In this third and last part of the tutorial, we come back to our original goal: the calculation of the H2 dissociation curve. Now, we want to change the ''d''-parameter in the input file and consequently, we should use an adapted simulation box. Most suited for a dissociation, is the {{variable|BoxShape|Mesh}}= cylinder. However, since this makes the calculations again much more expensive, we just stick to the default choice of {{variable|BoxShape|Mesh}}=minimal.

We choose {{variable|ExtraStates|States}}=14 as a compromise between accuracy and numerical cost and do the d-series with the following script:

 #!/bin/bash
 
 series=dist-series
 outfile="$series.log"
 echo "#ES    Energy    1. NON  2.NON" > $outfile
 list="0.25 0.5 0.75 1.0 1.5 2.0 2.5 3.0 4.0 5.0 6.0 8.0"
 
 export OCT_PARSE_ENV=1 
 for param in $list 
 do
  folder="$series-$param"
  mkdir $folder
  out_tmp=out-$series-$param
  cd $folder
 
  # create inp file
 {
 cat &lt;&lt;-EOF
 calculationMode = gs
 TheoryLevel = rdmft
 ExperimentalFeatures = yes
 
 ExtraStates = 14
 
 Dimensions = 3
 
 Radius = 8
 Spacing = 0.15
 
 # distance between H atoms
 d = $param
 
 %Coordinates
   'H' | 0 | 0 | -d/2
   'H' | 0 | 0 | d/2
 %
 
 Output = density
 OutputFormat = axis_x
 EOF
 } > inp
  
  # put the octopus dir here
  oct_dir=~/octopus-developer/bin  
  
  # generate the basis
  #export OCT_TheoryLevel=independent_particles
  #${oct_dir}/octopus > ${out_tmp}_basis
  
  # do the actual rdmft calculation
  #export OCT_TheoryLevel=rdmft
  #${oct_dir}/octopus > ${out_tmp}
 
  energy=`grep -a "Total energy" $out_tmp  | tail -1 | awk '{print $3}'`
  non1=`grep -a " 1  " static/info |  awk '{print $2}'`
  non2=`grep -a " 2  " static/info |  awk '{print $2}'`
  echo $param $energy $non1 $non2 >> ../$outfile
  cd ..
 done

The execution of the script should take about 30-60 minutes on a normal machine, so now it's time for well-deserved coffee!

After the script has finished, you have find a new log file and if you plot the energy of the files, you should get something of the following shape:
[[File:H2 diss curve.png|thumb|Dissociation curve of the Hydrogen molecule, calculated with RDMFT using the Müller functional.]]

Comments:
* This dissociation limit is not correct. Since the basis set is not converged, we get a too large value of E_diss=-1.009 than the Müller functional obtains for a converged set (E_diss=-1.047 Hartree), which is paradoxically closer to the exact dissociation limit of E_diss=1.0 Hartree because of an error cancellation (too small bases always increase the energy).
* The principal shape is good. Especially, we see that the curve becomes constant for large d, which single-reference methods cannot reproduce. 

&lt;span class=noprint>&lt;hr>
Back to [[Tutorials]]

[[Category:Tutorial]]
[[Category:Advanced]]
[[Category:RDMFT]]
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Code" title="Template:Code">Template:Code</a> (<a href="/mediawiki/index.php?title=Template:Code&amp;action=edit" title="Template:Code">view source</a>) </li><li><a href="/wiki/Template:File" title="Template:File">Template:File</a> (<a href="/mediawiki/index.php?title=Template:File&amp;action=edit" title="Template:File">view source</a>) </li><li><a href="/wiki/Template:Octopus" title="Template:Octopus">Template:Octopus</a> (<a href="/mediawiki/index.php?title=Template:Octopus&amp;action=edit" title="Template:Octopus">view source</a>) </li><li><a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus major version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_major_version&amp;action=edit" title="Template:Octopus major version">view source</a>) </li><li><a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus minor version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_minor_version&amp;action=edit" title="Template:Octopus minor version">view source</a>) </li><li><a href="/wiki/Template:Octopus_version" title="Template:Octopus version">Template:Octopus version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_version&amp;action=edit" title="Template:Octopus version">view source</a>) </li><li><a href="/wiki/Template:Variable" title="Template:Variable">Template:Variable</a> (<a href="/mediawiki/index.php?title=Template:Variable&amp;action=edit" title="Template:Variable">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Tutorial:RDMFT" title="Tutorial:RDMFT">Tutorial:RDMFT</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Tutorial:RDMFT">http:///wiki/Tutorial:RDMFT</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ARDMFT&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:RDMFT" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:RDMFT&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Tutorial:RDMFT">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:RDMFT" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:RDMFT" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":138});});</script>
</body>
</html>
