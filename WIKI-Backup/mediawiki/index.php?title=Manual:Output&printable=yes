<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:Output - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Output","wgTitle":"Manual:Output","wgCurRevisionId":8424,"wgRevisionId":8424,"wgArticleId":1813,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Output","wgRelevantArticleId":1813,"wgRequestId":"b4f1c7b85b89700214d2e603","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.cite.styles":"ready","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["ext.cite.ux-enhancements","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.cite.styles%7Cext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Output rootpage-Manual_Output skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:Output</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>At first you may be quite happy that you have mastered the input file, and Octopus runs without errors. However, eventually you (or your thesis advisor) will want to learn something about the system you have managed to describe to Octopus.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Ground-State_DFT"><span class="tocnumber">1</span> <span class="toctext">Ground-State DFT</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Time-Dependent_DFT"><span class="tocnumber">2</span> <span class="toctext">Time-Dependent DFT</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Optical_Properties"><span class="tocnumber">2.1</span> <span class="toctext">Optical Properties</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Linear-Response_Theory"><span class="tocnumber">2.2</span> <span class="toctext">Linear-Response Theory</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Electronic_Excitations_by_Means_of_Time-Propagation"><span class="tocnumber">2.3</span> <span class="toctext">Electronic Excitations by Means of Time-Propagation</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-6"><a href="#References"><span class="tocnumber">3</span> <span class="toctext">References</span></a></li>
</ul>
</div>

<h4><span class="mw-headline" id="Ground-State_DFT">Ground-State DFT</span></h4>
<p>Octopus sends some relevant information to the standard output (which you may have redirected to a file).  Here you will see energies and occupations of the eigenstates of your system.  These values and other information can also be found in the file <b><tt>static/info</tt></b>.
</p><p>However Octopus also calculates the wavefunctions of these states and the positions of the nuclei in your system.  Thus it can tell you the density of the dipole moment, the charge density, or the matrix elements of the dipole moment operator between different states.  Look at the values that the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=Output"><code>Output</code></a></span> variable can take to see the possibilities.
</p><p>For example, if you include  
</p>
<pre><tt>
Output = wfs_sqmod + potential 
</tt>
</pre>
<p>in your <b><tt>inp</tt></b> file, Octopus will create separate text files in the directory <b><tt>static</tt></b> with the values of the square modulus of the wave function and the local, classical, Hartree, and exchange/correlation parts of the Kohn-Sham potential at the points in your mesh.  
</p><p>You can specify the formatting details for these input files with the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=OutputFormat"><code>OutputFormat</code></a></span> variable and the other variables in the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=Output"><code>Output</code></a></span> section of the Reference Manual.  For example, you can specify that the file will only contain values along the x, y, or z axis, or in the plane x=0, y=0, or z=0.  You can also set the format to be readable by the graphics programs <a rel="nofollow" class="external text" href="http://www.opendx.org/">OpenDX</a>, <a rel="nofollow" class="external text" href="http://www.gnuplot.info/">gnuplot</a> or <a rel="nofollow" class="external text" href="http://www.mathworks.com/products/matlab/">MatLab</a>.  OpenDX can make plots of iso-surfaces if you have data in three-dimensions.  However gnuplot can only make a 3-d plot of a function of two variables, i.e. if you have the values of a wavefunction in a plane, and 2-d plots of a function of one variable, i.e. the value of the wavefunction along an axis.
</p>
<h4><span class="mw-headline" id="Time-Dependent_DFT">Time-Dependent DFT</span></h4>
<h5><span class="mw-headline" id="Optical_Properties">Optical Properties</span></h5>
<p>A primary reason for using a time-dependent DFT program is to obtain the optical properties of your system.  You have two choices for this, linear-response theory <i>a la</i> <a rel="nofollow" class="external text" href="http://scitation.aip.org/getabs/servlet/GetabsServlet?prog=normal&amp;id=JCPSA6000104000013005134000001&amp;idtype=cvips&amp;gifs=yes">Jamorski, Casida &amp; Salahub</a>
<sup id="cite_ref-1" class="reference"><a href="#cite_note-1">&#91;1&#93;</a></sup>, 
or explicit time-propagation of the system after a perturbation, a la <a rel="nofollow" class="external text" href="http://prola.aps.org/abstract/PRB/v54/i7/p4484_1">Yabana &amp; Bertsch</a>
<sup id="cite_ref-2" class="reference"><a href="#cite_note-2">&#91;2&#93;</a></sup>.  You may wish to read more about these methods in the paper by <a rel="nofollow" class="external text" href="http://www3.interscience.wiley.com/cgi-bin/abstract/112650991/ABSTRACT?CRETRY=1&amp;SRETRY=0">Castro et al.</a><sup id="cite_ref-3" class="reference"><a href="#cite_note-3">&#91;3&#93;</a></sup>
</p>
<h5><span class="mw-headline" id="Linear-Response_Theory">Linear-Response Theory</span></h5>
<p>Linear-response theory is based on the idea that a small (time-dependent) perturbation in an externally applied electric potential 
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/25ad91cf095dd328ec976e84142bc1efc68b4606" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:7.514ex; height:2.843ex;" alt="{\displaystyle \delta v(r,\omega )}" /> will result in a (time-dependent) perturbation of the electronic density 
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/30d00eb2615629e728491aa5939e29d515dcd826" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:7.588ex; height:2.843ex;" alt="{\displaystyle \delta \rho (r,\omega )}" /> which is linearly related to the size of the perturbation:
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/e5ebfc3eb9b80332ba8c643e76bd2ca24d5674e7" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.338ex; width:34.347ex; height:5.676ex;" alt="{\displaystyle \delta \rho (r,\omega )=\int d^{3}r&#39;\chi (r,r&#39;;\omega )\delta v(r,\omega )}" />.  Here, obviously, the time-dependence is Fourier-transformed into a frequency-dependence, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/48eff443f9de7a985bb94ca3bde20813ea737be8" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.446ex; height:1.676ex;" alt="{\displaystyle \omega }" />. The susceptibility,
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/7823eef1318cf5f46d58ce8d9a00b34c03aab95b" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:9.56ex; height:3.009ex;" alt="{\displaystyle \chi (r,r&#39;;\omega )}" />, is a density-density response function, because it is the response of the charge density to a potential that couples to the charge density of the system.  Because of this, it has poles at the excitation energies of the many-body system, meaning that the induced density also has these poles.  One can use this analytical property to find a related operator whose eigenvalues are these many-body excitation energies.  The matrix elements of the operator contain among other things:  1) occupied and unoccupied Kohn-Sham states and energies (from a ground state DFT calculation) and 2) an exchange-correlation kernel, 
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/f6ec55c11a6e25b1763838347a381e549148e6fd" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.671ex; width:33.7ex; height:6.509ex;" alt="{\displaystyle f_{xc}(r,r&#39;,\omega )={{\delta v_{xc}[n(r,\omega )]} \over {\delta n(r&#39;,\omega )}}\mid _{\delta v_{ext}=0}}" />.
</p><p>Casida's equations are a full solution to this problem (for real wavefunctions). The Tamm-Dancoff approximation uses only occupied-unoccupied transitions. The Petersilka approximation uses only the diagonal elements of the Tamm-Dancoff matrix, <i>i.e.</i> there is no mixing of transitions.<sup id="cite_ref-4" class="reference"><a href="#cite_note-4">&#91;4&#93;</a></sup>It takes only a little more time to calculate the whole matrix, so Petersilka is provided mostly for comparison.
</p><p>These methods are clearly much faster (an order of magnitude) than propagating
in time, but it turns out that they are very sensitive to the quality
of the unoccupied states. This means that it is very hard to converge the
excitation energy, because one requires a very large simulation box (much
larger than when propagating in real time).
</p>
<h5><span class="mw-headline" id="Electronic_Excitations_by_Means_of_Time-Propagation">Electronic Excitations by Means of Time-Propagation</span></h5>
<p>See <a href="/wiki/Manual:Time_Dependent#Delta_kick:_Calculating_an_absorption_spectrum" class="mw-redirect" title="Manual:Time Dependent">Manual:Time_Dependent#Delta_kick:_Calculating_an_absorption_spectrum</a>.
</p>
<h4><span class="mw-headline" id="References">References</span></h4>
<div class="mw-references-wrap"><ol class="references">
<li id="cite_note-1"><span class="mw-cite-backlink"><a href="#cite_ref-1">↑</a></span> <span class="reference-text">Christine Jamorski, Mark E. Casida, and Dennis R. Salahub, <i>Dynamic polarizabilities and excitation spectra from a molecular implementation of time-dependent density-functional response theory: N2 as a case study</i>, <a rel="nofollow" class="external text" href="http://dx.doi.org/10.1063/1.471140">J. Chem. Phys.</a> <b></b> 5134-5147 (1996) </span>
</li>
<li id="cite_note-2"><span class="mw-cite-backlink"><a href="#cite_ref-2">↑</a></span> <span class="reference-text">K. Yabana, G. F. Bertsch, <i>Time-dependent local-density approximation in real time</i>, <a rel="nofollow" class="external text" href="http://dx.doi.org/10.1103/PhysRevB.54.4484">Phys. Rev. B</a> <b></b> 4484 - 4487 (1996) </span>
</li>
<li id="cite_note-3"><span class="mw-cite-backlink"><a href="#cite_ref-3">↑</a></span> <span class="reference-text">Alberto Castro, Heiko Appel, Micael Oliveira, Carlo A. Rozzi, Xavier Andrade, Florian Lorenzen, M. A. L. Marques, E. K. U. Gross, Angel Rubio, <i>octopus: a tool for the application of time-dependent density functional theory</i>, <a rel="nofollow" class="external text" href="http://dx.doi.org/10.1002/pssb.200642067">physica status solidi (b)</a> <b>243</b> 2465-2488 (2006) </span>
</li>
<li id="cite_note-4"><span class="mw-cite-backlink"><a href="#cite_ref-4">↑</a></span> <span class="reference-text">Petersilka, M.  and Gossmann, U. J. and Gross, E. K. U., <i>Excitation Energies from Time-Dependent Density-Functional Theory</i>, <a rel="nofollow" class="external text" href="http://dx.doi.org/10.1103/PhysRevLett.76.1212">Phys. Rev. Lett.</a> <b>76</b> 1212--1215 (1996) </span>
</li>
</ol></div>
<p><br />
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:Discretization" title="Manual:Discretization">Manual:Discretization</a> - Next <a href="/wiki/Manual:Troubleshooting" title="Manual:Troubleshooting">Manual:Troubleshooting</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412153941
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.229 seconds
Real time usage: 2.717 seconds
Preprocessor visited node count: 460/1000000
Preprocessor generated node count: 1417/1000000
Post‐expand include size: 1840/2097152 bytes
Template argument size: 1046/2097152 bytes
Highest expansion depth: 8/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 3948/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00% 2683.274      1 -total
  0.98%   26.170      4 Template:Article
  0.86%   23.204      3 Template:Variable
  0.62%   16.640      4 Template:If
  0.47%   12.602      3 Template:Octopus_version
  0.39%   10.417      8 Template:P2
  0.37%    9.997      1 Template:Manual_foot
  0.19%    4.970      1 Template:Foot
  0.16%    4.385      3 Template:Code
  0.15%    4.133      3 Template:File
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1813-0!canonical!math=0 and timestamp 20230412153939 and revision id 8424
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:Output&amp;oldid=8424">http:///mediawiki/index.php?title=Manual:Output&amp;oldid=8424</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AOutput&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Output" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:Output&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:Output">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Output&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Output&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Output" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Output" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:Output&amp;oldid=8424" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Output&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 7 September 2016, at 12:34.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.229","walltime":"2.717","ppvisitednodes":{"value":460,"limit":1000000},"ppgeneratednodes":{"value":1417,"limit":1000000},"postexpandincludesize":{"value":1840,"limit":2097152},"templateargumentsize":{"value":1046,"limit":2097152},"expansiondepth":{"value":8,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":3948,"limit":5000000},"timingprofile":["100.00% 2683.274      1 -total","  0.98%   26.170      4 Template:Article","  0.86%   23.204      3 Template:Variable","  0.62%   16.640      4 Template:If","  0.47%   12.602      3 Template:Octopus_version","  0.39%   10.417      8 Template:P2","  0.37%    9.997      1 Template:Manual_foot","  0.19%    4.970      1 Template:Foot","  0.16%    4.385      3 Template:Code","  0.15%    4.133      3 Template:File"]},"cachereport":{"timestamp":"20230412153941","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":106});});</script>
</body>
</html>
