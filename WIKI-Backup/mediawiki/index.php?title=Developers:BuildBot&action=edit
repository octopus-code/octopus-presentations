<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Developers:BuildBot - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers:BuildBot","wgTitle":"Developers:BuildBot","wgCurRevisionId":11166,"wgRevisionId":0,"wgArticleId":1891,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers:BuildBot","wgRelevantArticleId":1891,"wgRequestId":"45e881bdf4a60f7fa7d3c28e","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_BuildBot rootpage-Developers_BuildBot skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Developers:BuildBot</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Developers:BuildBot" title="Developers:BuildBot">Developers:BuildBot</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">== Generalities ==

[http://buildbot.net/ {{name|Buildbot}}] is a {{name|Python}} program to automate software builds and tests. It operates a server that triggers jobs on a number of workers. These workers may be running on the same machine as the server but also on different ones. This opens the possibility to compile and check on different architectures and operating systems. More information about {{name|Buildbot}} can be found in the [https://docs.buildbot.net/current/manual/introduction.html manual].

The server, which in the {{name|Buildbot}} terminology is called a master, is running on &lt;tt>octopus-code.org&lt;/tt>. We also have several dedicated machines, the workers, that take care of running the builders. Each builder performs a predefined set of tasks. These tasks consist mainly in compiling the source code and running the testsuite, but there are also builders to perform more specific tasks, like generating the code documentation. The builders are triggered by schedulers.

The results of each run are stored and statistically analysed in terms of their average values and deviations thereof. This can help to find systematic errors, or slow systematic drifts in the results, and also to determine the best possible tolerances for given results. This analysis tool can be found under [https://octopus-code.org/testsuite https://octopus-code.org/testsuite]. There also exists a [https://www.youtube.com/watch?v=uJK3QkJxQ0g tutorial video].

== Schedulers ==

There are several schedulers that are configured, each one triggering different types of builders under different circumstances:
* The '''branch_push''' scheduler: it is triggered whenever there is a push event or tag event. It triggers the '''tests''' builders. 
* The '''nightlyPT''' scheduler: every night, if any changes have been made to the develop branch, this scheduler triggers the '''nighthly_tests''' builder.

Whenever the '''tests''' or '''nighthly_tests''' builders fail, an e-mail is sent to &lt;tt>octopus-notify@tddft.org&lt;/tt>. This e-mail contains a link to the output of all commands run by the builder. In addition to this, the '''tests''' builder reports its result back to [http://gitlab.com/octopus-code/octopus GitLab], so one can easily keep track of the status of the tests for each branch. This is particularly useful when deciding whether to accept or not a merge request.

== Workers ==

A list of all the workers can be found in the [http://octopus-code.org/buildbot/#/workers workers] web page. Further information, like the location, OS, etc, of each worker can be obtained by clicking on the worker name.

== Builders ==

A complete list of all the builders can be found in the [http://octopus-code.org/buildbot/#/builders {{name|builders}}] web page.

Among the builders, two of them have the sole purpose of triggering other builders:
* The '''tests''' builder: depending on the event and the branch that triggered this builder, it will in turn trigger different builders.
* The '''nighlty_tests''' builder: it starts several builders that compile the code and run the testsuite (see table bellow).

The builders that perform special tasks (i.e., that do not run the testsuite) are the following:
* The '''documentation''' builder. This builder generates the variables info and the doxygen source code documentation and uploads it to [http://octopus-code.org/ octopus-code.org]. It is triggered when there is a push to the 'develop' and release branches, or when a new tag is created.
* The '''dist''' builder. This builder generates a tarball with the source code and uploads it to [http://octopus-code.org/ octopus-code.org]. It is triggered when there is a push to the a release branches or when a new tag is created.

The following table contains a list of all the remaining builders that run the tests, and these are triggered for all push events but not when a new tag is created:

{|class="wikitable" style="text-align:center"
!Builder/Option !! CC !! CFLAGS !! FC !! FCFLAGS !! MPI !! MPI2 !! OpenMP !! OpenMP/SIMD !! cuda !! OpenCL !! --disable-debug !! maxdim !! BerkeleyGW !! Blacs !! ELPA !! ETSF_IO !! GDLIB !! LibFMM !! Libxc 3+ !! Libxc 4+ !! Netcdf !! NFFT !! NLOPT !! PARMETIS !! PFFT !! PNFFT !! poke !! pspio !! Scalapack !! Sparskit !! Ext. LibISF !! YAML !! CLBLAS !! CLFFT !! cuBLAS !! cuFFT
|-
| [http://octopus-code.org/buildbot/#/builders/distcheck distcheck] || gcc  || -O0 || gfortran  || -O0 || no || no || no || no || no || no || no || 3 || no || no || no || no || no || no || yes || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a foss-2017a] || gcc  || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || yes || no || yes || no || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_debug foss-2017a_debug] || gcc  || -Wall -O2 -march=native -Wextra -pedantic -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -march=native -fbacktrace -fcheck=all -fbounds-check -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 4 || yes || no || yes || yes || yes || no || yes || no || yes || no || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_min foss-2017a_min] || gcc  || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || no || no || no || no || no || no || yes || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_mpi foss-2017a_mpi] || mpicc (gcc) || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || yes || no || yes || no || yes || yes || no || no || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_mpi_debug foss-2017a_mpi_debug] || mpicc (gcc) || -Wall -O2 -march=native -Wextra -pedantic -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace -fcheck=all -fbounds-check -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 4 || yes || yes || yes || yes || yes || no || yes || no || yes || no || yes || yes || no || no || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_mpi_min foss-2017a_mpi_min] || mpicc (gcc) || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || no || no || no || no || no || no || yes || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_mpi_opt foss-2017a_mpi_opt] || mpicc (gcc) || -Wall -O3 -march=native -funroll-loops -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O3 -march=native -fbacktrace -funroll-loops -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || yes || no || yes || no || yes || yes || no || no || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_opt foss-2017a_opt] || gcc  || -Wall -O3 -march=native -funroll-loops -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O3 -march=native -fbacktrace -funroll-loops -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || yes || no || yes || no || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_ppc foss-2017a_ppc] || gcc  || -Wall -O2 -mcpu=native -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -mcpu=native -fbacktrace -fcheck=all -fbounds-check -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || yes || no || yes || no || no || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017a_ppc_mpi foss-2017a_ppc_mpi] || mpicc (gcc) || -Wall -O2 -mcpu=native -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -mcpu=native -fbacktrace -fcheck=all -fbounds-check -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || yes || no || yes || no || no || yes || no || no || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017b foss-2017b] || gcc  || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || no || yes || yes || no || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2017b_mpi foss-2017b_mpi] || mpicc (gcc) || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || no || yes || yes || no || yes || yes || no || no || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2017a intel-2017a] || icc  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || ifort  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || no || no || no || no || no || no || no || 4 || yes || no || yes || yes || no || no || yes || no || yes || no || yes || no || no || no || no || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2017a_impi intel-2017a_impi] || mpiicc (icc) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || mpiifort (ifort) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || yes || yes || no || no || no || no || no || 4 || yes || yes || yes || yes || no || no || yes || no || yes || no || yes || yes || no || no || no || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2017a_impi_omp intel-2017a_impi_omp] || mpiicc (icc) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || mpiifort (ifort) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all -qopenmp -qsmp=omp || yes || yes || yes || no || no || no || no || 4 || yes || yes || yes || yes || no || no || yes || no || yes || no || yes || yes || no || no || no || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2017a_omp intel-2017a_omp] || icc  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || ifort  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all -qopenmp -qsmp=omp || no || no || yes || no || no || no || no || 4 || yes || no || yes || yes || no || no || yes || no || yes || no || yes || no || no || no || no || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2017b intel-2017b] || icc  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || ifort  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || no || no || no || no || no || no || no || 4 || yes || no || yes || yes || no || no || no || yes || yes || no || yes || no || no || no || no || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2017b_impi intel-2017b_impi] || mpiicc (icc) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || mpiifort (ifort) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || yes || yes || no || no || no || no || no || 4 || yes || yes || yes || yes || no || no || no || yes || yes || no || yes || yes || no || no || no || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/pgi-2017.10 pgi-2017.10] || pgcc  || -O2 -Mflushz || pgfortran  || -O2 -Mflushz || no || no || no || no || no || no || no || 4 || no || no || no || yes || no || no || no || yes || yes || no || yes || no || no || no || no || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/pgi-2017.10_mpi pgi-2017.10_mpi] || mpicc (pgcc) || -O2 -Mflushz || mpifort (pgfortran) || -O2 -Mflushz || yes || yes || no || no || no || no || no || 4 || no || no || no || yes || no || no || no || yes || yes || no || yes || yes || no || no || no || yes || no || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2018a foss-2018a] || gcc  || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || no || yes || yes || yes || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2018a_mpi foss-2018a_mpi] || mpicc (gcc) || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || no || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2018b foss-2018b] || gcc  || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || gfortran  || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || no || yes || yes || yes || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2018b_mpi foss-2018b_mpi] || mpicc (gcc) || -Wall -O2 -march=native -ftest-coverage -fprofile-arcs || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace -ftest-coverage -fprofile-arcs || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || no || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2018a intel-2018a] || icc  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || ifort  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || no || no || no || no || no || no || no || 4 || yes || no || yes || yes || no || no || no || yes || yes || no || yes || no || no || no || no || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2018b intel-2018b] || icc  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || ifort  || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || no || no || no || no || no || no || no || 4 || yes || no || yes || yes || no || no || no || yes || yes || no || yes || no || no || no || no || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2018a_impi intel-2018a_impi] || mpiicc (icc) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || mpiifort (ifort) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || yes || yes || no || no || no || no || no || 4 || yes || yes || yes || yes || no || no || no || yes || yes || no || yes || yes || no || no || no || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/intel-2018b_impi intel-2018b_impi] || mpiicc (icc) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -Wall || mpiifort (ifort) || -O3 -xHost -ftz -fp-speculation=safe -fp-model source -traceback -warn all || yes || yes || no || no || no || no || no || 4 || yes || yes || yes || yes || no || no || no || yes || yes || no || yes || yes || no || no || no || yes || yes || yes || yes || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/fosscuda-2018a fosscuda-2018a] || gcc  || -Wall -ftest-coverage -fprofile-arcs -O2 -march=native || gfortran  || -Wall -ftest-coverage -fprofile-arcs -O2 -march=native -fbacktrace || no || no || no || no || yes || no || no || 3 || yes || no || yes || yes || yes || no || no || yes || yes || yes || yes || no || no || no || yes || no || no || yes || no || no || no || no || yes || yes
|-
| [http://octopus-code.org/buildbot/#/builders/fosscuda-2018a_mpi fosscuda-2018a_mpi] || mpicc (gcc) || -Wall -ftest-coverage -fprofile-arcs -O2 -march=native || mpifort (gfortran) || -Wall -ftest-coverage -fprofile-arcs -O2 -march=native -fbacktrace || yes || yes || no || no || yes || no || no || 3 || yes || yes || yes || yes || yes || no || no || yes || yes || yes || yes || yes || no || no || yes || yes || yes || yes || no || no || no || no || yes || yes
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2018a_valgrind foss-2018a_valgrind] || gcc  || -Wall -O2 -march=native || gfortran  || -Wall -O2 -march=native -fbacktrace || no || no || no || no || no || no || no || 3 || yes || no || yes || yes || yes || no || no || yes || yes || yes || yes || no || no || no || yes || yes || no || yes || no || no || no || no || no || no
|-
| [http://octopus-code.org/buildbot/#/builders/foss-2018a_mpi_valgrind foss-2018a_mpi_valgrind] || mpicc (gcc) || -Wall -O2 -march=native || mpifort (gfortran) || -Wall -O2 -march=native -fbacktrace || yes || yes || no || no || no || no || no || 3 || yes || yes || yes || yes || yes || no || no || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || yes || no || no || no || no || no
|}
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Template used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Name" title="Template:Name">Template:Name</a> (<a href="/mediawiki/index.php?title=Template:Name&amp;action=edit" title="Template:Name">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Developers:BuildBot" title="Developers:BuildBot">Developers:BuildBot</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Developers:BuildBot">http:///wiki/Developers:BuildBot</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers%3ABuildBot&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers:BuildBot" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk"><span><a href="/wiki/Talk:Developers:BuildBot" rel="discussion" title="Discussion about the content page [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Developers:BuildBot">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Developers:BuildBot&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:BuildBot&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers:BuildBot" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers:BuildBot" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers:BuildBot&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":131});});</script>
</body>
</html>
