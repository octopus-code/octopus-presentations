<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Information for "Manual:About this manual" - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:About_this_manual","wgTitle":"Manual:About this manual","wgCurRevisionId":2939,"wgRevisionId":0,"wgArticleId":1538,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"info","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:About_this_manual","wgRelevantArticleId":1538,"wgRequestId":"d76984caa364cf473e69b384","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_About_this_manual rootpage-Manual_About_this_manual skin-vector action-info">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Information for "Manual:About this manual"</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><style>.mw-hiddenCategoriesExplanation { display: none; }</style>
<style>.mw-templatesUsedExplanation { display: none; }</style>
<h2 id="mw-pageinfo-header-basic"><span class="mw-headline" id="Basic_information">Basic information</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-pageinfo-display-title"><td style="vertical-align: top;">Display title</td><td>Manual:About this manual</td></tr>
<tr id="mw-pageinfo-default-sort"><td style="vertical-align: top;">Default sort key</td><td>Manual:About this manual</td></tr>
<tr id="mw-pageinfo-length"><td style="vertical-align: top;">Page length (in bytes)</td><td>2,545</td></tr>
<tr id="mw-pageinfo-article-id"><td style="vertical-align: top;">Page ID</td><td>1538</td></tr>
<tr><td style="vertical-align: top;">Page content language</td><td>en - English</td></tr>
<tr id="mw-pageinfo-content-model"><td style="vertical-align: top;">Page content model</td><td>wikitext</td></tr>
<tr id="mw-pageinfo-robot-policy"><td style="vertical-align: top;">Indexing by robots</td><td>Allowed</td></tr>
<tr><td style="vertical-align: top;"><a href="/mediawiki/index.php?title=Special:WhatLinksHere/Manual:About_this_manual&amp;hidelinks=1&amp;hidetrans=1" title="Special:WhatLinksHere/Manual:About this manual">Number of redirects to this page</a></td><td>1</td></tr>
<tr id="mw-pageinfo-contentpage"><td style="vertical-align: top;">Counted as a content page</td><td>Yes</td></tr>
</table>
<h2 id="mw-pageinfo-header-restrictions"><span class="mw-headline" id="Page_protection">Page protection</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-restriction-edit"><td style="vertical-align: top;">Edit</td><td>Allow all users (infinite)</td></tr>
<tr id="mw-restriction-move"><td style="vertical-align: top;">Move</td><td>Allow all users (infinite)</td></tr>
</table>
<a href="/mediawiki/index.php?title=Special:Log&amp;type=protect&amp;page=Manual%3AAbout+this+manual" title="Special:Log">View the protection log for this page.</a>
<h2 id="mw-pageinfo-header-edits"><span class="mw-headline" id="Edit_history">Edit history</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-pageinfo-firstuser"><td style="vertical-align: top;">Page creator</td><td><a href="/wiki/User:Xavier" class="mw-userlink" title="User:Xavier"><bdi>Xavier</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Xavier&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Xavier (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Xavier" class="mw-usertoollinks-contribs" title="Special:Contributions/Xavier">contribs</a>)</span></td></tr>
<tr id="mw-pageinfo-firsttime"><td style="vertical-align: top;">Date of page creation</td><td><a href="/mediawiki/index.php?title=Manual:About_this_manual&amp;oldid=2273" title="Manual:About this manual">23:59, 28 June 2006</a></td></tr>
<tr id="mw-pageinfo-lastuser"><td style="vertical-align: top;">Latest editor</td><td><a href="/wiki/User:Xavier" class="mw-userlink" title="User:Xavier"><bdi>Xavier</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Xavier&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Xavier (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Xavier" class="mw-usertoollinks-contribs" title="Special:Contributions/Xavier">contribs</a>)</span></td></tr>
<tr id="mw-pageinfo-lasttime"><td style="vertical-align: top;">Date of latest edit</td><td><a href="/mediawiki/index.php?title=Manual:About_this_manual&amp;oldid=2939" title="Manual:About this manual">15:36, 4 July 2006</a></td></tr>
<tr id="mw-pageinfo-edits"><td style="vertical-align: top;">Total number of edits</td><td>29</td></tr>
<tr id="mw-pageinfo-authors"><td style="vertical-align: top;">Total number of distinct authors</td><td>1</td></tr>
<tr id="mw-pageinfo-recent-edits"><td style="vertical-align: top;">Recent number of edits (within past 90 days)</td><td>0</td></tr>
<tr id="mw-pageinfo-recent-authors"><td style="vertical-align: top;">Recent number of distinct authors</td><td>0</td></tr>
</table>
<h2 id="mw-pageinfo-header-properties"><span class="mw-headline" id="Page_properties">Page properties</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-pageinfo-templates"><td style="vertical-align: top;">Transcluded templates (10)</td><td><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Code" title="Template:Code">Template:Code</a> (<a href="/mediawiki/index.php?title=Template:Code&amp;action=edit" title="Template:Code">view source</a>) </li><li><a href="/wiki/Template:Command" title="Template:Command">Template:Command</a> (<a href="/mediawiki/index.php?title=Template:Command&amp;action=edit" title="Template:Command">view source</a>) </li><li><a href="/wiki/Template:File" title="Template:File">Template:File</a> (<a href="/mediawiki/index.php?title=Template:File&amp;action=edit" title="Template:File">view source</a>) </li><li><a href="/wiki/Template:Foot" title="Template:Foot">Template:Foot</a> (<a href="/mediawiki/index.php?title=Template:Foot&amp;action=edit" title="Template:Foot">view source</a>) </li><li><a href="/wiki/Template:Manual_foot" title="Template:Manual foot">Template:Manual foot</a> (<a href="/mediawiki/index.php?title=Template:Manual_foot&amp;action=edit" title="Template:Manual foot">view source</a>) </li><li><a href="/wiki/Template:Name" title="Template:Name">Template:Name</a> (<a href="/mediawiki/index.php?title=Template:Name&amp;action=edit" title="Template:Name">view source</a>) </li><li><a href="/wiki/Template:Octopus" title="Template:Octopus">Template:Octopus</a> (<a href="/mediawiki/index.php?title=Template:Octopus&amp;action=edit" title="Template:Octopus">view source</a>) </li><li><a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus major version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_major_version&amp;action=edit" title="Template:Octopus major version">view source</a>) </li><li><a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus minor version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_minor_version&amp;action=edit" title="Template:Octopus minor version">view source</a>) </li><li><a href="/wiki/Template:Octopus_version" title="Template:Octopus version">Template:Octopus version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_version&amp;action=edit" title="Template:Octopus version">view source</a>) </li></ul></td></tr>
<tr id="mw-pageinfo-transclusions"><td style="vertical-align: top;">Page transcluded on (1)</td><td><div class="mw-templatesUsedExplanation"><p>Template used on this page:
</p></div><ul>
<li><a href="/wiki/The_Octopus_Manual" title="The Octopus Manual">The Octopus Manual</a> (<a href="/mediawiki/index.php?title=The_Octopus_Manual&amp;action=edit" title="The Octopus Manual">view source</a>) </li></ul></td></tr>
</table>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Manual:About_this_manual">http:///wiki/Manual:About_this_manual</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AAbout+this+manual&amp;returntoquery=action%3Dinfo" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:About_this_manual" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:About_this_manual&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Manual:About_this_manual">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:About_this_manual&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:About_this_manual&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:About_this_manual" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:About_this_manual" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:About_this_manual&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":194});});</script>
</body>
</html>
