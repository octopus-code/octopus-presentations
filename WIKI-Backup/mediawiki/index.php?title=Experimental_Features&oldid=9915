<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Experimental Features - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Experimental_Features","wgTitle":"Experimental Features","wgCurRevisionId":9915,"wgRevisionId":9915,"wgArticleId":2165,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Experimental_Features","wgRelevantArticleId":2165,"wgRequestId":"d7e2934f818e8ef70f348950","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Experimental_Features rootpage-Experimental_Features skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Experimental Features</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 16:00, 5 April 2018 by <a href="/mediawiki/index.php?title=User:Flick&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:Flick (page does not exist)"><bdi>Flick</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Flick&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Flick (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Flick" class="mw-usertoollinks-contribs" title="Special:Contributions/Flick">contribs</a>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Experimental_Features&amp;diff=prev&amp;oldid=9915" title="Experimental Features">diff</a>) <a href="/mediawiki/index.php?title=Experimental_Features&amp;direction=prev&amp;oldid=9915" title="Experimental Features">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Why_are_some_parts_marked_as_experimental.3F"><span class="tocnumber">1</span> <span class="toctext">Why are some parts marked as experimental?</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#What_can_happen_if_I_use_an_experimental_feature.3F"><span class="tocnumber">2</span> <span class="toctext">What can happen if I use an experimental feature?</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#What_can_I_do_if_I_want_to_use_an_experimental_feature_for_my_paper.3F"><span class="tocnumber">3</span> <span class="toctext">What can I do if I want to use an experimental feature for my paper?</span></a></li>
<li class="toclevel-1 tocsection-4"><a href="#When_will_a_certain_experimental_feature_be_declared_as_non-experimental.3F"><span class="tocnumber">4</span> <span class="toctext">When will a certain experimental feature be declared as non-experimental?</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#How_do_I_use_an_experimental_feature.3F"><span class="tocnumber">5</span> <span class="toctext">How do I use an experimental feature?</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Does_this_mean_that_non-experimental_features_of_octopus_will_always_work_properly.3F"><span class="tocnumber">6</span> <span class="toctext">Does this mean that non-experimental features of octopus will always work properly?</span></a></li>
</ul>
</div>

<h4><span id="Why_are_some_parts_marked_as_experimental?"></span><span class="mw-headline" id="Why_are_some_parts_marked_as_experimental.3F">Why are some parts marked as experimental?</span></h4>
<p>Some features of octopus are declared as experimental, when you try to use them the code will stop with an error. This is done to protect users from parts of Octopus that are not fully implemented or whose results have not been properly validated.
</p>
<h4><span id="What_can_happen_if_I_use_an_experimental_feature?"></span><span class="mw-headline" id="What_can_happen_if_I_use_an_experimental_feature.3F">What can happen if I use an experimental feature?</span></h4>
<p>If you are lucky the code might stop with an error or segmentation fault; if you are unlucky the code will run as usual but the results will be wrong.
</p><p>All results obtained from an Octopus run that uses experimental features should be considered as possibly wrong and should not be used for publications or other scientific presentations.
</p>
<h4><span id="What_can_I_do_if_I_want_to_use_an_experimental_feature_for_my_paper?"></span><span class="mw-headline" id="What_can_I_do_if_I_want_to_use_an_experimental_feature_for_my_paper.3F">What can I do if I want to use an experimental feature for my paper?</span></h4>
<p>First, you should check that you really need that particular feature. If you do, you should contact the octopus developers through the <a href="/wiki/Mailing_lists" title="Mailing lists">octopus-users mailing list</a> to know what the status of that feature is and why it is in experimental status.
</p>
<h4><span id="When_will_a_certain_experimental_feature_be_declared_as_non-experimental?"></span><span class="mw-headline" id="When_will_a_certain_experimental_feature_be_declared_as_non-experimental.3F">When will a certain experimental feature be declared as non-experimental?</span></h4>
<p>It depends. Contact the octopus developers through the <a href="/wiki/Mailing_lists" title="Mailing lists">octopus-users mailing list</a> to inquire.
</p>
<h4><span id="How_do_I_use_an_experimental_feature?"></span><span class="mw-headline" id="How_do_I_use_an_experimental_feature.3F">How do I use an experimental feature?</span></h4>
<p>First, you have to understand what it means to use an experimental feature. Then, you need to set the input variable <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=ExperimentalFeatures"><code>ExperimentalFeatures</code></a></span> to <code>yes</code>.
</p>
<h4><span id="Does_this_mean_that_non-experimental_features_of_octopus_will_always_work_properly?"></span><span class="mw-headline" id="Does_this_mean_that_non-experimental_features_of_octopus_will_always_work_properly.3F">Does this mean that non-experimental features of octopus will always work properly?</span></h4>
<p>No. While the developers try to make sure that all octopus components work as they should and are constantly fixing problems, the code still has bugs that may appear. The user is responsible for validating the results for the particular system or property before using it for predictions.
</p>
<!-- 
NewPP limit report
Cached time: 20230412155643
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.036 seconds
Real time usage: 0.044 seconds
Preprocessor visited node count: 48/1000000
Preprocessor generated node count: 138/1000000
Post‐expand include size: 214/2097152 bytes
Template argument size: 72/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   20.870      1 -total
 97.75%   20.401      1 Template:Variable
 54.01%   11.272      1 Template:Octopus_version
 18.47%    3.855      2 Template:Code
 17.84%    3.723      1 Template:Octopus_major_version
 15.93%    3.325      1 Template:Octopus_minor_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2165-0!canonical and timestamp 20230412155643 and revision id 9915
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Experimental_Features&amp;oldid=9915">http:///mediawiki/index.php?title=Experimental_Features&amp;oldid=9915</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Experimental+Features&amp;returntoquery=oldid%3D9915" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Experimental_Features" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Experimental_Features&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Experimental_Features">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Experimental_Features&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Experimental_Features&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Experimental_Features" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Experimental_Features" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Experimental_Features&amp;oldid=9915&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Experimental_Features&amp;oldid=9915" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Experimental_Features&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 5 April 2018, at 16:00.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.036","walltime":"0.044","ppvisitednodes":{"value":48,"limit":1000000},"ppgeneratednodes":{"value":138,"limit":1000000},"postexpandincludesize":{"value":214,"limit":2097152},"templateargumentsize":{"value":72,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   20.870      1 -total"," 97.75%   20.401      1 Template:Variable"," 54.01%   11.272      1 Template:Octopus_version"," 18.47%    3.855      2 Template:Code"," 17.84%    3.723      1 Template:Octopus_major_version"," 15.93%    3.325      1 Template:Octopus_minor_version"]},"cachereport":{"timestamp":"20230412155643","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":123});});</script>
</body>
</html>
