<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Particle in an octopus - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Particle_in_an_octopus","wgTitle":"Tutorial:Particle in an octopus","wgCurRevisionId":11437,"wgRevisionId":11437,"wgArticleId":2208,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Beginner","Ground State","Model","User-defined Species","Independent Particles","Visualization"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Particle_in_an_octopus","wgRelevantArticleId":2208,"wgRequestId":"791116b7fc792bf199281640","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Particle_in_an_octopus rootpage-Tutorial_Particle_in_an_octopus skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Particle in an octopus</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Octopus can actually run 2D systems where the shape of the simulation box is defined by what is white in an image file. Here is an example of a "particle in an octopus", in which we have a constant potential and an octopus-shaped quantum dot. To run it, you will need to have built the code with the <a rel="nofollow" class="external text" href="https://libgd.github.io">optional library GDLIB</a>.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Input"><span class="tocnumber">1</span> <span class="toctext">Input</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#inp"><span class="tocnumber">1.1</span> <span class="toctext"><b>inp</b></span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#gdlib.png"><span class="tocnumber">1.2</span> <span class="toctext"><b>gdlib.png</b></span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-4"><a href="#Plotting"><span class="tocnumber">2</span> <span class="toctext">Plotting</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Exercises"><span class="tocnumber">3</span> <span class="toctext">Exercises</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Input">Input</span></h2>
<p>For this example we will need two files:
</p>
<h4><span class="mw-headline" id="inp"><b><tt>inp</tt></b></span></h4>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = gs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=FromScratch"><code>FromScratch</code></a></span> = yes
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Dimensions"><code>Dimensions</code></a></span> = 2

%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Species"><code>Species</code></a></span>
"null" | species_user_defined | potential_formula | "0" | valence | 1
%

%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Coordinates"><code>Coordinates</code></a></span>
"null" | 0 | 0
%

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=BoxShape"><code>BoxShape</code></a></span> = box_image
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=BoxShapeImage"><code>BoxShapeImage</code></a></span> = "gdlib.png"

ff = 20
%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Lsize"><code>Lsize</code></a></span>
 135 / ff | 95 / ff
%

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Hamiltonian&amp;name=TheoryLevel"><code>TheoryLevel</code></a></span> = independent_particles
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=SCF&amp;name=ConvEigenError"><code>ConvEigenError</code></a></span> = yes
</pre>
<h4><span class="mw-headline" id="gdlib.png"><b><tt>gdlib.png</tt></b></span></h4>
<p>Make this file available in the run directory. You can download it by clicking on the image bellow. It is also available in the <b><tt>PREFIX/share/octopus</tt></b> directory from your Octopus installation.
</p><p><a href="/wiki/File:Gdlib.png" class="image"><img alt="Gdlib.png" src="/mediawiki/images/e/e4/Gdlib.png" decoding="async" width="135" height="95" /></a>
</p>
<h2><span class="mw-headline" id="Plotting">Plotting</span></h2>
<p>You can obtain the wavefunction by adding this to the input file:
</p>
<pre>%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Output&amp;name=Output"><code>Output</code></a></span>
 wfs | plane_z
%
</pre>
<p>and rerunning. View it in <code>gnuplot</code> with
</p>
<pre>plot 'static/wf-st0001.z=0' u 1:2:3 linetype palette
</pre>
<p>or
</p>
<pre>splot 'static/wf-st0001.z=0' u 1:2:(0):($3*500) with pm3d
</pre>
<p>Where does the wavefunction localize, and why?
</p>
<h2><span class="mw-headline" id="Exercises">Exercises</span></h2>
<ul><li>See how the total energy scales with the size of the system (controlled by the <tt>ff</tt> parameter in the input file). How does it compare to the formula for a particle in a box?</li>
<li>Look at the wavefunctions of the unoccupied states.</li>
<li>Think of a serious application that would use the ability to define the simulation box by an image!</li></ul>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:1D_Helium" title="Tutorial:1D Helium">1D Helium</a> - Next <a href="/wiki/Tutorial:e-H_scattering" title="Tutorial:e-H scattering">e-H scattering</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Model_systems" title="Tutorial Series:Model systems">Model systems</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412154108
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.112 seconds
Real time usage: 0.137 seconds
Preprocessor visited node count: 388/1000000
Preprocessor generated node count: 909/1000000
Post‐expand include size: 2828/2097152 bytes
Template argument size: 1299/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   76.224      1 -total
 48.42%   36.907     11 Template:Variable
 34.39%   26.210      1 Template:Tutorial_foot
 32.95%   25.116      4 Template:If
 23.91%   18.225     11 Template:Octopus_version
 22.63%   17.247      4 Template:P2
 13.06%    9.958      4 Template:P1
  8.62%    6.570     12 Template:Code
  6.08%    4.632      3 Template:File
  5.73%    4.369     11 Template:Octopus_major_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2208-0!canonical and timestamp 20230412154108 and revision id 11437
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Particle_in_an_octopus&amp;oldid=11437">http:///mediawiki/index.php?title=Tutorial:Particle_in_an_octopus&amp;oldid=11437</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Beginner" title="Category:Beginner">Beginner</a></li><li><a href="/wiki/Category:Ground_State" title="Category:Ground State">Ground State</a></li><li><a href="/wiki/Category:Model" title="Category:Model">Model</a></li><li><a href="/wiki/Category:User-defined_Species" title="Category:User-defined Species">User-defined Species</a></li><li><a href="/wiki/Category:Independent_Particles" title="Category:Independent Particles">Independent Particles</a></li><li><a href="/wiki/Category:Visualization" title="Category:Visualization">Visualization</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AParticle+in+an+octopus&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Particle_in_an_octopus" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Particle_in_an_octopus&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Particle_in_an_octopus">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Particle_in_an_octopus&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Particle_in_an_octopus&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Particle_in_an_octopus" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Particle_in_an_octopus" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Particle_in_an_octopus&amp;oldid=11437" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Particle_in_an_octopus&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 9 September 2021, at 14:56.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.112","walltime":"0.137","ppvisitednodes":{"value":388,"limit":1000000},"ppgeneratednodes":{"value":909,"limit":1000000},"postexpandincludesize":{"value":2828,"limit":2097152},"templateargumentsize":{"value":1299,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   76.224      1 -total"," 48.42%   36.907     11 Template:Variable"," 34.39%   26.210      1 Template:Tutorial_foot"," 32.95%   25.116      4 Template:If"," 23.91%   18.225     11 Template:Octopus_version"," 22.63%   17.247      4 Template:P2"," 13.06%    9.958      4 Template:P1","  8.62%    6.570     12 Template:Code","  6.08%    4.632      3 Template:File","  5.73%    4.369     11 Template:Octopus_major_version"]},"cachereport":{"timestamp":"20230412154108","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":114});});</script>
</body>
</html>
