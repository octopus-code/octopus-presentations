<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Tutorial:Band structure unfolding - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Band_structure_unfolding","wgTitle":"Tutorial:Band structure unfolding","wgCurRevisionId":11579,"wgRevisionId":0,"wgArticleId":3008,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Band_structure_unfolding","wgRelevantArticleId":3008,"wgRequestId":"0cfbb9652fb77a129a959b88","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Band_structure_unfolding rootpage-Tutorial_Band_structure_unfolding skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Tutorial:Band structure unfolding</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Tutorial:Band_structure_unfolding" title="Tutorial:Band structure unfolding">Tutorial:Band structure unfolding</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">

In this tutorial, we look at how to perform a band-structure unfolding using Octopus. This calculation is done in several steps and each one is described below.

== Supercell ground-state ==

The first thing to do is to compute the ground state of a supercell. In this example we will use bulk silicon. The input file is similar to the one used in the [[Tutorial:Getting started with periodic systems|Getting started with periodic systems]] tutorial, but in the present case we will use a supercell composed of 8 atoms. Here is the corresponding input file:

 {{variable|CalculationMode|Calculation_Modes}} = gs
 
 {{variable|PeriodicDimensions|System}} = 3
 
 {{variable|Spacing|Mesh}} = 0.5
 
 a = 10.18
 %{{variable|LatticeParameters|Mesh}}
  a | a  | a
 90 | 90 |90
 %
 
 %{{variable|ReducedCoordinates|System}}
  "Si" | 0.0         | 0.0       | 0.0 
  "Si" |   1/2       | 1/2       | 0.0
  "Si" |   1/2       | 0.0       | 1/2
  "Si" |   0.0       | 1/2       | 1/2
  "Si" |   1/4       | 1/4       | 1/4
  "Si" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4
  "Si" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2
  "Si" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2
 %
 
 nk = 4
 %{{variable|KPointsGrid|Mesh}}
   nk |  nk |  nk
 %
 {{variable|KPointsUseSymmetries|Mesh}} = yes

All these variables should be familiar from other tutorials. Now run {{octopus}} using this input file to obtain the ground-state of the supercell.

== Unfolding setup == 

After obtaining the ground state of the supercell, we now define the primitive cell on which we want to unfold our supercell and the specific ''k''-points path that we are interested in. To do this, we take the previous input file and add some new lines:

 {{variable|CalculationMode|Calculation_Modes}} = gs
 
 {{variable|PeriodicDimensions|System}} = 3
 
 {{variable|Spacing|Mesh}} = 0.5
 
 a = 10.18
 %{{variable|LatticeParameters|Mesh}}
  a | a  | a
 90 | 90 |90
 %
 
 %{{variable|ReducedCoordinates|System}}
  "Si" | 0.0         | 0.0       | 0.0 
  "Si" |   1/2       | 1/2       | 0.0
  "Si" |   1/2       | 0.0       | 1/2
  "Si" |   0.0       | 1/2       | 1/2
  "Si" |   1/4       | 1/4       | 1/4
  "Si" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4
  "Si" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2
  "Si" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2
 %
 
 nk = 4
 %{{variable|KPointsGrid|Mesh}}
   nk |  nk |  nk
 %
 {{variable|KPointsUseSymmetries|Mesh}} = yes
 
 ExperimentalFeatures = yes
 {{variable|UnfoldMode|Utilities::oct-unfold}} = unfold_setup
 %{{variable|UnfoldLatticeParameters|Utilities::oct-unfold}}
   a | a | a
 %
 %{{variable|UnfoldLatticeVectors|Utilities::oct-unfold}}
  0.  | 0.5 | 0.5
  0.5 | 0.  | 0.5
  0.5 | 0.5 | 0.0  
 %
 %{{variable|UnfoldKPointsPath|Utilities::oct-unfold}}
  4 | 4 | 8
  0.5 | 0.0 | 0.0 # L point
  0.0 | 0.0 | 0.0 # Gamma point
  0.0 | 0.5 | 0.5 # X point
  1.0 | 1.0 | 1.0 # Another Gamma point
 %

Lets see more in detail the input variables that were added:

* &lt;tt>{{variable|UnfoldMode|Utilities::oct-unfold}} = unfold_setup&lt;/tt>: this variable instructs the utility &lt;tt>oct-unfold&lt;/tt> in which mode we are running. As a first step, we are running in the &lt;tt>unfold_setup&lt;/tt> mode, which generates some files that will be necessary for the next steps.

* &lt;tt>{{variable|UnfoldLatticeParameters|Utilities::oct-unfold}}&lt;/tt> specifies the lattice parameters of the primitive cell. This variable is similar to {{variable|LatticeParameters|Mesh}}.

* &lt;tt>{{variable|UnfoldLatticeVectors|Utilities::oct-unfold}}&lt;/tt> specifies the lattice vectors of the primitive cell. This variable is similar to {{variable|LatticeVectors|Mesh}}.

* &lt;tt>{{variable|UnfoldKPointsPath|Utilities::oct-unfold}}&lt;/tt> specifies the ''k''-points path. The coordinates are indicated as reduced coordinates of the primitive lattice. This variable is similar to {{variable|KPointsPath|Mesh}}.

Now run the &lt;tt>oct-unfold&lt;/tt> utility. You will obtain two files. The first one ({{file|unfold_kpt.dat}}) contains the list of ''k''-points of the specified ''k''-point path, but expressed in reduced coordinates of the supercell. These are the ''k''-points for which we need to evaluate the wavefunctions of the supercell. The second file ({{file|unfold_gvec.dat}}) contains the list of reciprocal lattice vectors that relate the ''k''-points of the primitive cell to the one in the supercell.

== Obtaining the wavefunctions ==

In order to perform the unfolding, we need the wavefunctions in the supercell at specific k-points. These points are described in the {{file|unfold_kpt.dat}} file that we obtained in the previous step. To get the wavefunctions, we need to run a non self-consistent calculation. Here is the corresponding input file:

 {{variable|CalculationMode|Calculation_Modes}} = unocc
 
 {{variable|PeriodicDimensions|System}} = 3
 
 {{variable|Spacing|Mesh}} = 0.5
 
 a = 10.18
 %{{variable|LatticeParameters|Mesh}}
  a | a  | a
 90 | 90 |90
 %
 
 %{{variable|ReducedCoordinates|System}}
  "Si" | 0.0         | 0.0       | 0.0 
  "Si" |   1/2       | 1/2       | 0.0
  "Si" |   1/2       | 0.0       | 1/2
  "Si" |   0.0       | 1/2       | 1/2
  "Si" |   1/4       | 1/4       | 1/4
  "Si" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4
  "Si" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2
  "Si" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2
 %
 
 ExperimentalFeatures = yes
 {{variable|UnfoldMode|Utilities::oct-unfold}} = unfold_setup
 %{{variable|UnfoldLatticeParameters|Utilities::oct-unfold}}
   a | a | a
 %
 %{{variable|UnfoldLatticeVectors|Utilities::oct-unfold}}
  0.  | 0.5 | 0.5
  0.5 | 0.  | 0.5
  0.5 | 0.5 | 0.0  
 %
 %{{variable|UnfoldKPointsPath|Utilities::oct-unfold}}
  4 | 4 | 8
  0.5 | 0.0 | 0.0 # L point
  0.0 | 0.0 | 0.0 # Gamma point
  0.0 | 0.5 | 0.5 # X point
  1.0 | 1.0 | 1.0 # Another Gamma point
 %
 
 include unfold_kpt.dat
 
 {{variable|ExtraStates|States}} = 6
 {{variable|ExtraStatesToConverge|States}} = 4

In this input file we have changed the calculation mode ({{variable|CalculationMode|Calculation_Modes}} = unocc) and replaced all the ''k''-points related variables (%{{variable|KPointsGrid|Mesh}}, %{{variable|KPointsPath|Mesh}}, and %{{variable|KPoints|Mesh}}) by the line:

  include unfold_kpt.dat

When doing a normal band structure calculation one normally also wants to have some extra states, therefore we have used the {{variable|ExtraStates|States}} and {{variable|ExtraStatesToConverge|States}} variables, as explained in the [[Tutorial:Getting started with periodic systems|Getting started with periodic systems]] tutorial. In this particular case, we are requesting to converge 4 unoccupied states, which all correspond to the first valence band in the folded primitive cell, as the supercell is 4 times larger than the initial cell.

Now run {{octopus}} on this input file. Note that after you have performed this step, you won't be able to start a time-dependent calculation from this folder. Indeed, the original ground-state wavefunctions will be replaced by the ones from this last calculation, which are incompatible. Therefore it might be a good idea to create a backup of the restart information before performing this step.

== Unfolding ==

Now that we have computed the states for the required ''k''-points, we can finally compute the spectral function for each of the ''k''-points of the specified ''k''-point path. This is done by changing unfolding mode to be &lt;tt>{{variable|UnfoldMode|Utilities::oct-unfold}} = unfold_run&lt;/tt>, so the final input file should look like this:

 {{variable|CalculationMode|Calculation_Modes}} = unocc
 
 {{variable|PeriodicDimensions|System}} = 3
 
 {{variable|Spacing|Mesh}} = 0.5
 
 a = 10.18
 %{{variable|LatticeParameters|Mesh}}
  a | a  | a
 90 | 90 |90
 %
 
 %{{variable|ReducedCoordinates|System}}
  "Si" | 0.0         | 0.0       | 0.0 
  "Si" |   1/2       | 1/2       | 0.0
  "Si" |   1/2       | 0.0       | 1/2
  "Si" |   0.0       | 1/2       | 1/2
  "Si" |   1/4       | 1/4       | 1/4
  "Si" |   1/4 + 1/2 | 1/4 + 1/2 | 1/4
  "Si" |   1/4 + 1/2 | 1/4       | 1/4 + 1/2
  "Si" |   1/4       | 1/4 + 1/2 | 1/4 + 1/2
 %
 
 ExperimentalFeatures = yes
 {{variable|UnfoldMode|Utilities::oct-unfold}} = unfold_run
 %{{variable|UnfoldLatticeParameters|Utilities::oct-unfold}}
   a | a | a
 %
 %{{variable|UnfoldLatticeVectors|Utilities::oct-unfold}}
  0.  | 0.5 | 0.5
  0.5 | 0.  | 0.5
  0.5 | 0.5 | 0.0  
 %
 %{{variable|UnfoldKPointsPath|Utilities::oct-unfold}}
  4 | 4 | 8
  0.5 | 0.0 | 0.0 # L point
  0.0 | 0.0 | 0.0 # Gamma point
  0.0 | 0.5 | 0.5 # X point
  1.0 | 1.0 | 1.0 # Another Gamma point
 %
 
 include unfold_kpt.dat
 
 {{variable|ExtraStates|States}} = 6
 {{variable|ExtraStatesToConverge|States}} = 4

This will produce the spectral function for the full path ({{file|static/ake.dat}}) and for each individual points of the path ({{file|static/ake_XXX.dat}}). The content of the {{file|static/ake.dat}} file should look like this:
&lt;pre>
#Energy Ak(E)
#Number of points in energy window  1020
 0.000000000000E+00 -2.942706167586E-01  9.233964264948E-02
 0.000000000000E+00 -2.936502506800E-01  9.335548313377E-02
 0.000000000000E+00 -2.930298846013E-01  9.439013038656E-02
 0.000000000000E+00 -2.924095185227E-01  9.544407253720E-02
...
&lt;/pre>
The first column is the coordinate along the ''k''-point path, the second column is the energy eigenvalue and the last column is the spectral function. There are several ways to plot the information contained in this file. One possibility is to plot it as a heat map. For example, if you are using &lt;tt>gnuplot&lt;/tt>, you can try the following command:

 plot 'static/ake.dat' u 1:2:3 w image

The unfolded bandstructure of silicon is shown below. How does it compare to the bandstructure compute in the [[Tutorial:Getting started with periodic systems|Getting started with periodic systems]]?

[[Image:Si_unfolded.png|thumb|420px|Unfolded band structure of bulk silicon.]]

Note that it is possible to change the energy range and the energy resolution for the unfolded band structure. This is done by specifying the variables {{variable|UnfoldMinEnergy|Utilities::oct-unfold}}, {{variable|UnfoldMaxEnergy|Utilities::oct-unfold}}, and {{variable|UnfoldEnergyStep|Utilities::oct-unfold}}. It is also important to note here that the code needs to read all the unoccupied wavefunctions and therefore might need a large amount of memory, so make sure enough memory is available.

{{Tutorial_foot|series=Periodic systems|prev=Optical_spectra_of_solids|next=}}

[[Category:Advanced]]
[[Category:Ground State]]
[[Category:Bulk]]
[[Category:oct-unfold]]
[[Category:Band Structure]]
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Code" title="Template:Code">Template:Code</a> (<a href="/mediawiki/index.php?title=Template:Code&amp;action=edit" title="Template:Code">view source</a>) </li><li><a href="/wiki/Template:File" title="Template:File">Template:File</a> (<a href="/mediawiki/index.php?title=Template:File&amp;action=edit" title="Template:File">view source</a>) </li><li><a href="/wiki/Template:If" title="Template:If">Template:If</a> (<a href="/mediawiki/index.php?title=Template:If&amp;action=edit" title="Template:If">view source</a>) </li><li><a href="/wiki/Template:Octopus" title="Template:Octopus">Template:Octopus</a> (<a href="/mediawiki/index.php?title=Template:Octopus&amp;action=edit" title="Template:Octopus">view source</a>) </li><li><a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus major version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_major_version&amp;action=edit" title="Template:Octopus major version">view source</a>) </li><li><a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus minor version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_minor_version&amp;action=edit" title="Template:Octopus minor version">view source</a>) </li><li><a href="/wiki/Template:Octopus_version" title="Template:Octopus version">Template:Octopus version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_version&amp;action=edit" title="Template:Octopus version">view source</a>) </li><li><a href="/wiki/Template:P1" title="Template:P1">Template:P1</a> (<a href="/mediawiki/index.php?title=Template:P1&amp;action=edit" title="Template:P1">view source</a>) </li><li><a href="/wiki/Template:P2" title="Template:P2">Template:P2</a> (<a href="/mediawiki/index.php?title=Template:P2&amp;action=edit" title="Template:P2">view source</a>) </li><li><a href="/wiki/Template:Tutorial_foot" title="Template:Tutorial foot">Template:Tutorial foot</a> (<a href="/mediawiki/index.php?title=Template:Tutorial_foot&amp;action=edit" title="Template:Tutorial foot">view source</a>) </li><li><a href="/wiki/Template:Variable" title="Template:Variable">Template:Variable</a> (<a href="/mediawiki/index.php?title=Template:Variable&amp;action=edit" title="Template:Variable">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Tutorial:Band_structure_unfolding" title="Tutorial:Band structure unfolding">Tutorial:Band structure unfolding</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Tutorial:Band_structure_unfolding">http:///wiki/Tutorial:Band_structure_unfolding</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ABand+structure+unfolding&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Band_structure_unfolding" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Band_structure_unfolding&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Tutorial:Band_structure_unfolding">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Tutorial:Band_structure_unfolding&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Band_structure_unfolding&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Band_structure_unfolding" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Band_structure_unfolding" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Band_structure_unfolding&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":148});});</script>
</body>
</html>
