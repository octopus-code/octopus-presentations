<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers Manual:Exchange and Correlation - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers_Manual:Exchange_and_Correlation","wgTitle":"Developers Manual:Exchange and Correlation","wgCurRevisionId":7637,"wgRevisionId":7637,"wgArticleId":2253,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers_Manual:Exchange_and_Correlation","wgRelevantArticleId":2253,"wgRequestId":"faf356dd37d27a9fc3c1299a","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Manual_Exchange_and_Correlation rootpage-Developers_Manual_Exchange_and_Correlation skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers Manual:Exchange and Correlation</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 22:38, 20 March 2015 by <a href="/wiki/User:Dstrubbe" class="mw-userlink" title="User:Dstrubbe"><bdi>Dstrubbe</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Dstrubbe&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Dstrubbe (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Dstrubbe" class="mw-usertoollinks-contribs" title="Special:Contributions/Dstrubbe">contribs</a>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;diff=prev&amp;oldid=7637" title="Developers Manual:Exchange and Correlation">diff</a>) <a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;direction=prev&amp;oldid=7637" title="Developers Manual:Exchange and Correlation">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Octopus uses <a href="/wiki/Libxc" title="Libxc">libxc</a>, which holds the parametrizations of the LDA, GGA, etc. functionals.
</p><p><b>WARNING: This page is out of date.</b>
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#xc_module"><span class="tocnumber">1</span> <span class="toctext">xc module</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Introduction"><span class="tocnumber">1.1</span> <span class="toctext">Introduction</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Types"><span class="tocnumber">1.2</span> <span class="toctext">Types</span></a>
<ul>
<li class="toclevel-3 tocsection-4"><a href="#xc_type"><span class="tocnumber">1.2.1</span> <span class="toctext">xc_type</span></a></li>
</ul>
</li>
<li class="toclevel-2 tocsection-5"><a href="#Constants"><span class="tocnumber">1.3</span> <span class="toctext">Constants</span></a>
<ul>
<li class="toclevel-3 tocsection-6"><a href="#OEP_levels"><span class="tocnumber">1.3.1</span> <span class="toctext">OEP levels</span></a></li>
</ul>
</li>
<li class="toclevel-2 tocsection-7"><a href="#Subroutines"><span class="tocnumber">1.4</span> <span class="toctext">Subroutines</span></a>
<ul>
<li class="toclevel-3 tocsection-8"><a href="#xc_init"><span class="tocnumber">1.4.1</span> <span class="toctext">xc_init</span></a></li>
<li class="toclevel-3 tocsection-9"><a href="#xc_write_info"><span class="tocnumber">1.4.2</span> <span class="toctext">xc_write_info</span></a></li>
<li class="toclevel-3 tocsection-10"><a href="#xc_end"><span class="tocnumber">1.4.3</span> <span class="toctext">xc_end</span></a></li>
<li class="toclevel-3 tocsection-11"><a href="#subroutine_xc_get_vxc"><span class="tocnumber">1.4.4</span> <span class="toctext">subroutine xc_get_vxc</span></a></li>
</ul>
</li>
</ul>
</li>
<li class="toclevel-1 tocsection-12"><a href="#xc_functl_module"><span class="tocnumber">2</span> <span class="toctext">xc_functl module</span></a>
<ul>
<li class="toclevel-2 tocsection-13"><a href="#Introduction_2"><span class="tocnumber">2.1</span> <span class="toctext">Introduction</span></a></li>
<li class="toclevel-2 tocsection-14"><a href="#Types_2"><span class="tocnumber">2.2</span> <span class="toctext">Types</span></a>
<ul>
<li class="toclevel-3 tocsection-15"><a href="#xc_functl_type"><span class="tocnumber">2.2.1</span> <span class="toctext">xc_functl_type</span></a></li>
</ul>
</li>
<li class="toclevel-2 tocsection-16"><a href="#Subroutines_2"><span class="tocnumber">2.3</span> <span class="toctext">Subroutines</span></a>
<ul>
<li class="toclevel-3 tocsection-17"><a href="#xc_functl_init_exchange"><span class="tocnumber">2.3.1</span> <span class="toctext">xc_functl_init_exchange</span></a></li>
<li class="toclevel-3 tocsection-18"><a href="#xc_functl_init_correlation"><span class="tocnumber">2.3.2</span> <span class="toctext">xc_functl_init_correlation</span></a></li>
<li class="toclevel-3 tocsection-19"><a href="#xc_functl_write_info"><span class="tocnumber">2.3.3</span> <span class="toctext">xc_functl_write_info</span></a></li>
<li class="toclevel-3 tocsection-20"><a href="#xc_functl_end"><span class="tocnumber">2.3.4</span> <span class="toctext">xc_functl_end</span></a></li>
</ul>
</li>
</ul>
</li>
</ul>
</div>

<h1><span class="mw-headline" id="xc_module">xc module</span></h1>
<h2><span class="mw-headline" id="Introduction">Introduction</span></h2>
<p>This module takes care of <i>most</i> of the problem of calculating the exchange-correlation potential and energy functionals. Unfortunately, it cannot handle the whole problem, as would be desirable, because the OEP functionals have to be handled within the hamiltonian module (to obtain an OEP functional one needs to apply the Hamiltonian). Since Fortran 90 is unable to handle circular dependencies, obtaining OEP functionals (or obtaining the SIC through the OEP method) is done in the <tt>xc_oep</tt> subroutine  in the hamiltonian module. We can consider <tt>xc_oep</tt> to be within the <tt>xc</tt> module <i>in spirit</i>.
</p>
<h2><span class="mw-headline" id="Types">Types</span></h2>
<h3><span class="mw-headline" id="xc_type">xc_type</span></h3>
<pre>type(xc_functl_type)&#160;:: functl(2)
integer              :: sic_correction
type(xc_functl_type)&#160;:: sic_aux(2)
integer              :: mGGA_implementation
integer              :: oep_level
FLOAT                :: oep_mixing
logical              :: nlcc
</pre>
<p>This data type holds all the necessary information to fully
characterize the exchange and correlation functional used in an
<a href="/wiki/Octopus" class="mw-redirect" title="Octopus">octopus</a> calculation. Its components are the following:
</p>
<ul><li><tt>functl(2)</tt>: The first element of this two-element array is the <tt>xc_functl_type</tt> structure that holds the information about the exchange functional; the second element holds the correlation functional.</li></ul>
<ul><li><tt>sic_correction</tt> determines wether self-interaction corrections (SIC) are to be applied or not. It only holds if any of the <tt>functl</tt> components is holding a functional of the <tt>XC_FAMILY_LDA</tt> or <tt>XC_FAMILY_GGA</tt> families. The possible values are: 0 (do not apply SIC corrections), or anything else</li></ul>
<p>(do apply SIC corrections).
</p>
<ul><li><tt>sic_aux(2)</tt>: These are two <tt>xc_functl_type</tt> structures, which hold auxiliary exchange and correlation functionals, necessary to builid the SIC. [WARNING: I believe that this could be avoided]</li></ul>
<ul><li><tt>mGGA_implementation</tt>: The mGGA functional is in fact an orbital-dependent functional. It may then also use the OEP formalism. The precise way in which the mGGA functional is implemented is then decided by this variable, which may take two values:
<ul><li><tt>mGGA_implementation = 1</tt>: GEA-like implementation (not working).</li>
<li><tt>mGGA_implementation = 2</tt>: OEP-like implementation (not yet implemented).</li></ul></li></ul>
<ul><li><tt>oep_level</tt>: The optimized effective potential method is applied in three cases: (i) in case a SIC is applied to a LDA or GGA functional; (ii) in case the exact exchange functional is requested; and (iii) in case the mGGA is to be implemented through the OEP (<tt>mGGA_implementation = 2</tt>). However, several possible choices are then possible to approximate (or calculate exactly) the OEP.</li></ul>
<ul><li><tt>oep_mixing</tt></li>
<li><tt> nlcc</tt></li></ul>
<h2><span class="mw-headline" id="Constants">Constants</span></h2>
<h3><span class="mw-headline" id="OEP_levels">OEP levels</span></h3>
<ul><li><tt>XC_OEP_NONE = 0</tt></li>
<li><tt>XC_OEP_SLATER = 1</tt></li>
<li><tt>XC_OEP_KLI = 2</tt></li>
<li><tt>XC_OEP_CEDA = 3</tt></li>
<li><tt>XC_OEP_FULL = 4</tt></li></ul>
<p><br />
</p>
<h2><span class="mw-headline" id="Subroutines">Subroutines</span></h2>
<h3><span class="mw-headline" id="xc_init">xc_init</span></h3>
<pre>type(xc_type), intent(out)&#160;:: xcs
logical,       intent(in)  :: nlcc
integer,       intent(in)  :: spin_channels
</pre>
<p>This subroutine initializes a <tt>xc_type</tt> variable, the <tt>xcs</tt> argument, maiinly by reading in variables from the input file. Two input arguments need, however, to be passed in: 
</p>
<ul><li><tt>nlcc</tt>, used to fill in <tt>xc_type%nlcc</tt> (see <a href="/wiki/Xc_module#Xc_type" class="mw-redirect" title="Xc module">type xc_type</a>).</li>
<li><tt>spin_channels</tt>, which is needed to call <tt>xc_functl_init_exchange</tt> and <tt>xc_functl_init_correlation</tt> (see <tt>xc_functl</tt>). It should be equal to one if the calculations are unpolarized, and equal to two if the calculations are spin-polarized, or based in two-spinors.</li></ul>
<p>This routine is in charge of calling <a href="/wiki/Xc_functl_module#exc_functl_init_exchange" title="Xc functl module">exc_functl_init_exchange</a> and <a href="/wiki/Xc_functl_module#xc_functl_init_correlation" title="Xc functl module">xc_functl_init_correlation</a>.
</p><p>It also reads some extra variables from the input file:
</p>
<ul><li><tt>MGGAimplementation</tt>: it is used to fill <tt>xcs%mGGA_implementation</tt>.</li>
<li><tt>SICCorrection</tt>: used to fill <tt>xcs%sic_correction</tt>.</li>
<li><tt>OEP_level</tt>: used to fill <tt>xcs%oep_level</tt>.</li>
<li><tt>OEP_mixing</tt>: used to fill <tt>xcs%oep_mixing</tt>.</li></ul>
<h3><span class="mw-headline" id="xc_write_info">xc_write_info</span></h3>
<pre>type(xc_type), intent(in)&#160;:: xcs
integer,       intent(in)&#160;:: iunit
</pre>
<h3><span class="mw-headline" id="xc_end">xc_end</span></h3>
<pre>type(xc_type), intent(inout)&#160;:: xcs
</pre>
<h3><span class="mw-headline" id="subroutine_xc_get_vxc">subroutine xc_get_vxc</span></h3>
<pre>type(xc_type),        intent(in)    :: xcs
type(mesh_type),      intent(in)    :: m
type(f_der_type),     intent(inout)&#160;:: f_der
type(states_type),    intent(in)    :: st
FLOAT,                intent(inout)&#160;:: vxc(:,:), ex, ec
FLOAT,                intent(in)    :: ip, qtot
logical, optional,    intent(in)    :: aux
</pre>
<p>This routine fills the exchange and correlation potential <tt>vxc</tt> (as well as the exchange and correlation energies, <tt>ex</tt> and <tt>ec</tt>. It takes care of the "simple" functionals, i.e. LDA, GGA and mGGA; The SIC corrections to these functionals or the exact exchange OEP is handled in the <tt>h_xc_OEP</tt> subroutine, which should be called after <tt>xc_get_vxc</tt>.
</p><p>The input variables are:
</p>
<ul><li><tt>xcs</tt>: This is the <tt>xc_type</tt> variable that holds all the information about the exchange and correlation functional. It should have been initialized previously by the <tt>xc_init</tt> subroutine.</li>
<li><tt>m</tt>: The mesh where all the system functions live.</li>
<li><tt>f_der</tt>: This is the <tt>f_der_type</tt> structure: the structure that knows how to do the derivatives. It is defined <tt>inout</tt> for technical reasons, but it should not change.</li>
<li><tt>st</tt>: The <tt>states_type</tt> structure that holds the density and the Kohn-Sham states.</li>
<li><tt>ip</tt>: The ionization potential of the system, or some reasonable estimation for it, necessary only in case the van Leeuween &amp; Baerends potential is to be used.</li>
<li><tt>qtot</tt>: The total electron number of the system, also only necessary in case the van Leeuween &amp; Baerends potential is to be used.</li>
<li><tt>aux</tt>: This is an optional flag:
<ul><li>If it is passed, and it is set to <tt>.true.</tt>, the exchange and correlation functional used will be those held in <tt>xcs%sic_aux</tt>. This is done in order to calculate the self-interaction corrections.</li>
<li>Otherwise, they will be those held in <tt>xcs%functl</tt>.</li></ul></li>
<li><tt>vxc</tt>, <tt>ex</tt>, <tt>ec</tt>: the values of these three variables on entry is added to the calculated values.</li></ul>
<p>The output variables are:
</p>
<ul><li><tt>ex</tt>: The exchange energy.</li>
<li><tt>ec</tt>: The correlation energy.</li>
<li><tt>vxc(:,&#160;:)</tt>: The dimension of this array are <tt>(m%np, st%d%nspin)</tt>. On output,</li></ul>
<p>it holds the exchange and correlation potential - added to whatever it held before.
</p><p><br />
</p>
<h1><span class="mw-headline" id="xc_functl_module">xc_functl module</span></h1>
<h2><span class="mw-headline" id="Introduction_2">Introduction</span></h2>
<p>This the is lower level module used by <a href="/wiki/Developers_Manual:Exchange_and_Correlation#xc_module" title="Developers Manual:Exchange and Correlation">Developers_Manual:Exchange and Correlation#xc module</a>.
</p>
<h2><span class="mw-headline" id="Types_2">Types</span></h2>
<h3><span class="mw-headline" id="xc_functl_type">xc_functl_type</span></h3>
<pre>integer&#160;:: family
integer&#160;:: id
integer&#160;:: spin_channels
integer(POINTER_SIZE)&#160;:: conf
integer(POINTER_SIZE)&#160;:: info
</pre>
<p>The integer <tt>family</tt> may take one of the four following families:
</p>
<ul><li><tt>XC_FAMILY_LDA  = 1</tt> - For LDA functionals.</li>
<li><tt>XC_FAMILY_GGA  = 2</tt> - For GGA functionals.</li>
<li><tt>XC_FAMILY_MGGA = 3</tt> - For MGGA functionals.</li>
<li><tt>XC_FAMILY_OEP  = 4</tt> - For OEP functionals (i.e. orbital dependent functionals).</li></ul>
<p>This value is set in either the <tt>xc_functl_init_exchange</tt> and <tt>xc_functl_init_correlation</tt>, depending on the functional requested, read from the input file, and which in turns sets the <tt>id</tt> integer identifier.
</p><p>The integer <tt>id</tt> is the identifier for the functional. Note that it does not fully identify the exchange and correlation functional - SIC corrections can be added to, for example, one LDA functional. This possibility is handled in the <a href="/wiki/Xc_module" class="mw-redirect" title="Xc module">xc module</a>. The values that this variable can take are defined in <a href="/wiki/Libxc#Constants" title="Libxc">libxc</a>.
</p><p>The <tt>spin_channels</tt> integer may be one, in case the calculation is of spin-unpolarized type (i.e. restricted to paired electrons in closed shells), or two, in case the calculation is spin-polarized, or it is a two-spinors like calculation.
</p><p>The <tt>integer(POINTER_SIZE)</tt>, <tt>conf</tt>, and <tt>info</tt> are the pointers needed to use the subroutines in the <a href="/wiki/Libxc" title="Libxc">libxc</a> module.
</p><p><br />
</p>
<h2><span class="mw-headline" id="Subroutines_2">Subroutines</span></h2>
<h3><span class="mw-headline" id="xc_functl_init_exchange">xc_functl_init_exchange</span></h3>
<pre>type(xc_functl_type), intent(out)&#160;:: functl
integer,              intent(in)  :: spin_channels
</pre>
<p>This subroutine initializes one <tt>xc_functl_type</tt> structure (argument <tt>functl</tt>), so that it holds information about one exchange functional. Its only input is <tt>spin_channels</tt> - which is used to fill in <tt>functl%spin_channels</tt>, whereas the rest of the needed information is obtained through the input file.
</p><p>The input file variable <tt>XFunctional</tt> is read, and used to fill in <tt>functl%id</tt>. According to its value <tt>functl%family</tt> is then set by the code. The values given in the input file for <tt>XFunctional</tt> must then be any of the accepted values for <tt>functl%id</tt> - in the exchange case. Some mnemonic variables are provided in the <tt>SHARE/octopus/variables</tt> directory.
</p><p>Besides filling the <tt>xc_functl_type</tt> structure <tt>functl</tt>, the main task of this subroutine is to call either <tt>xc_lda_init</tt>, <tt>xc_gga_init</tt> or <tt>xc_mgga_init</tt> in module <a href="/wiki/Libxc" title="Libxc">libxc</a>.
</p>
<h3><span class="mw-headline" id="xc_functl_init_correlation">xc_functl_init_correlation</span></h3>
<pre>type(xc_functl_type), intent(out)&#160;:: functl
integer,              intent(in)  :: spin_channels
</pre>
<p>This subroutine initializes one <tt>xc_functl_type</tt> structure (argument <tt>functl</tt>), so that it holds information about one correlation functional. Its only input is <tt>spin_channels</tt> - which is used to fill in <tt>functl%spin_channels</tt>, whereas the rest of the needed information is obtained through the input file.
</p><p>The input file variable <tt>CFunctional</tt> is read, and used to fill in <tt>functl%id</tt>. According to its value <tt>functl%family</tt> is then set by the code. The values given in the input file for <tt>CFunctional</tt> must then be any of the accepted values for <tt>functl%id</tt> - in the correlation case. Some mnemonic variables are provided in the <tt>SHARE/octopus/variables</tt> directory.
</p><p>Besides filling the <tt>xc_functl_type</tt> structure <tt>functl</tt>, the main task of this subroutine is to call either <tt>xc_lda_init</tt>, <tt>xc_gga_init</tt>, or <tt>xc_mgga_init</tt> in <a href="/wiki/Libxc" title="Libxc">libxc</a>.
</p>
<h3><span class="mw-headline" id="xc_functl_write_info">xc_functl_write_info</span></h3>
<pre>type(xc_functl_type), intent(in)&#160;:: functl
integer,              intent(in)&#160;:: iunit
</pre>
<p>It outputs to the file opened in unit <tt>iunit</tt> information about the functional initialized in <tt>functl</tt>.
</p>
<h3><span class="mw-headline" id="xc_functl_end">xc_functl_end</span></h3>
<pre>type(xc_functl_type), intent(inout)&#160;:: functl
</pre>
<p>This subroutine does the necessary cleaning of a <tt>xc_fucntl_type</tt> variable that is not going to be used any more.
</p>
<!-- 
NewPP limit report
Cached time: 20230412154255
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.075 seconds
Real time usage: 0.079 seconds
Preprocessor visited node count: 163/1000000
Preprocessor generated node count: 284/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 1343/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2253-0!canonical and timestamp 20230412154255 and revision id 7637
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;oldid=7637">http:///mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;oldid=7637</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers+Manual%3AExchange+and+Correlation&amp;returntoquery=oldid%3D7637" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers_Manual:Exchange_and_Correlation" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers_Manual:Exchange_and_Correlation&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers_Manual:Exchange_and_Correlation">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers_Manual:Exchange_and_Correlation" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers_Manual:Exchange_and_Correlation" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;oldid=7637&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;oldid=7637" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers_Manual:Exchange_and_Correlation&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 20 March 2015, at 22:38.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.075","walltime":"0.079","ppvisitednodes":{"value":163,"limit":1000000},"ppgeneratednodes":{"value":284,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":1343,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412154255","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":125});});</script>
</body>
</html>
