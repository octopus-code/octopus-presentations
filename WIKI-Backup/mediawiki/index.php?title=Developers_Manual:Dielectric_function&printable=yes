<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers Manual:Dielectric function - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers_Manual:Dielectric_function","wgTitle":"Developers Manual:Dielectric function","wgCurRevisionId":7472,"wgRevisionId":7472,"wgArticleId":1961,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers_Manual:Dielectric_function","wgRelevantArticleId":1961,"wgRequestId":"2382dc0fb69f39837728a51a","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Manual_Dielectric_function rootpage-Developers_Manual_Dielectric_function skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers Manual:Dielectric function</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>This is based on G. F. Bertsch <i>et al.</i> <a rel="nofollow" class="external text" href="http://link.aps.org/abstract/PRB/v62/p7998">Phys. Rev. B <b>62</b>, 7998 (2000)</a>.
</p><p>For the moment we will only consider the terms of the Lagrangian with <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/2b0ccd94e930d427d3aa6307c4eb5853125e5ef1" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; margin-right: -0.387ex; width:3.027ex; height:2.343ex;" alt="{\displaystyle A^{\alpha }\!}" />.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Standard_formulation"><span class="tocnumber">1</span> <span class="toctext">Standard formulation</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Equation_of_motion_for"><span class="tocnumber">1.1</span> <span class="toctext">Equation of motion for</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#The_coupling_with_the_TDKS_equation"><span class="tocnumber">1.2</span> <span class="toctext">The coupling with the TDKS equation</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-4"><a href="#Notation"><span class="tocnumber">2</span> <span class="toctext">Notation</span></a></li>
</ul>
</div>

<h3><span class="mw-headline" id="Standard_formulation">Standard formulation</span></h3>
<p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/57518df0992ec66fd4ce11fd295b3aa86e1586a2" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:70.056ex; height:6.676ex;" alt="{\displaystyle L=\int d^{3}r\sum _{k}{\frac {1}{2}}\left(i\partial ^{\alpha }\phi _{k}^{*}(r)-{\frac {1}{c}}A^{\alpha }\phi _{k}^{*}(r)\right)\left(-i\partial ^{\alpha }\phi _{k}(r)-{\frac {1}{c}}A^{\alpha }\phi _{k}(r)\right)}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b2d4551ce6bfac7c779500f26a10013818986588" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:48.044ex; height:6.343ex;" alt="{\displaystyle +\int d^{3}r\,d^{3}r&#39;\,\sum _{k}e^{i/cA^{\alpha }(r^{\alpha }-{r&#39;}^{\alpha })}\phi _{k}^{*}(r)V(r,r&#39;)\phi _{k}(r&#39;)}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/50dc28ebba4d8c6737c12c74dd160c3b0602b4a5" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.171ex; width:13.319ex; height:5.676ex;" alt="{\displaystyle -{\frac {\Omega }{8\pi c^{2}}}{\dot {A}}^{\alpha }{\dot {A}}^{\alpha }}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/de24d87ef0d6773ad2b319cadad5363207ced7ce" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:26.14ex; height:6.343ex;" alt="{\displaystyle -i\int d^{3}r\,\sum _{k}\phi _{k}^{*}(r)\partial _{t}\phi _{k}(r)}" />
</p>
<h4><span class="mw-headline" id="Equation_of_motion_for">Equation of motion for <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/2b0ccd94e930d427d3aa6307c4eb5853125e5ef1" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; margin-right: -0.387ex; width:3.027ex; height:2.343ex;" alt="{\displaystyle A^{\alpha }\!}" /></span></h4>
<p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/d55dc2185a4ddfd16b1eea4ce1a96c7344106ceb" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:21.244ex; height:6.509ex;" alt="{\displaystyle {\frac {d}{dt}}{\frac {\partial L}{\partial {\dot {A}}^{\beta }}}=-{\frac {\Omega }{4\pi c^{2}}}{\ddot {A}}^{\beta }}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/8062171ee1496e9cb8e9ed5e7f3ef6d53f41be1d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:110.907ex; height:6.676ex;" alt="{\displaystyle {\frac {\partial L}{\partial A^{\beta }}}=\int d^{3}r\sum _{k}{\frac {1}{2}}\left[\left(-{\frac {1}{c}}\delta ^{\alpha \beta }\phi _{k}^{*}(r)\right)\left(-i\partial ^{\alpha }\phi _{k}(r)-{\frac {1}{c}}{}A^{\alpha }\phi _{k}(r)\right)+\left(i\partial ^{\alpha }\phi _{k}^{*}(r)-{\frac {1}{c}}A^{\alpha }\phi _{k}^{*}(r)\right)\left(-{\frac {1}{c}}\delta ^{\alpha \beta }\phi _{k}(r)\right)\right]}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/4c3d4c1e3a5e2c71c8cd9fd9c5459b0ebbc49ec8" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:61.408ex; height:6.343ex;" alt="{\displaystyle +\int d^{3}r\,d^{3}r&#39;\,\sum _{k}{\frac {i}{c}}\left(r^{\beta }-{r&#39;}^{\beta }\right)e^{i/cA^{\alpha }(r^{\alpha }-{r&#39;}^{\alpha })}\phi _{k}^{*}(r)V(r,r&#39;)\phi _{k}(r&#39;)}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/9e6d04db1dbf8d30cf9508251df3179fc5fb13cd" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:94.569ex; height:6.676ex;" alt="{\displaystyle {\frac {\partial L}{\partial A^{\beta }}}=\int d^{3}r\sum _{k}{\frac {1}{2}}\left[{\frac {i}{c}}\phi _{k}^{*}(r)\partial ^{\beta }\phi _{k}(r)+{\frac {1}{c^{2}}}\phi _{k}^{*}(r)\phi _{k}(r)A^{\beta }-{\frac {i}{c}}\phi _{k}(r)\partial ^{\beta }\phi _{k}^{*}(r)+{\frac {1}{c^{2}}}A^{\beta }\phi _{k}^{*}(r)\phi _{k}(r)\right]}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b8ff251b3662efa7b69a6661cc549977b0505153" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:68.239ex; height:6.343ex;" alt="{\displaystyle +{\frac {i}{c}}\int d^{3}r\,d^{3}r&#39;\,\sum _{k}e^{i/cA^{\alpha }(r^{\alpha }-{r&#39;}^{\alpha })}\phi _{k}^{*}(r)\left[r^{\beta }{}V(r,r&#39;)-V(r,r&#39;){r&#39;}^{\beta }\right]\phi _{k}(r&#39;)}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/6bbab0d81e513de77e4f0d091b2678fa65e16a32" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:63.35ex; height:6.509ex;" alt="{\displaystyle {\frac {\partial L}{\partial A^{\beta }}}={\frac {i}{c}}\int d^{3}r\sum _{k}\phi _{k}^{*}(r)\partial ^{\beta }\phi _{k}(r)+{\frac {1}{c^{2}}}A^{\beta }\int d^{3}r\sum _{k}\phi _{k}^{*}(r)\phi _{k}(r)}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/e95b5b4a2d30fb1e60b94e86c2f59aed37597857" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:72.628ex; height:6.343ex;" alt="{\displaystyle +{\frac {i}{c}}\int d^{3}r\,d^{3}r&#39;\,\sum _{k}e^{i/cA^{\alpha }{r^{\alpha }}}\phi _{k}^{*}(r)\left[r^{\beta }{}V(r,r&#39;)-V(r,r&#39;){r&#39;}^{\beta }\right]e^{-i/cA^{\alpha }{r&#39;}^{\alpha }}\phi _{k}(r&#39;)}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/7b2260cea9e90dfa6f56fad1a55228edceb08720" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:107.196ex; height:6.343ex;" alt="{\displaystyle {\ddot {A}}^{\beta }=-i{\frac {4\pi {c}}{\Omega }}\int d^{3}r\sum _{k}\phi _{k}^{*}(r)\partial ^{\beta }\phi _{k}(r)-{\frac {4\pi {N}}{\Omega }}A^{\beta }-i{\frac {4\pi {c}}{\Omega }}\int d^{3}r\,d^{3}r&#39;\,\sum _{k}e^{i/cA^{\alpha }{r^{\alpha }}}\phi _{k}^{*}(r)\left[{\hat {r}}^{\beta }{},{\hat {V}}\right]e^{-i/cA^{\alpha }{r&#39;}^{\alpha }}\phi _{k}(r&#39;)}" />
</p><p>Finally:
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/1afd869135af5ccc9ae1f044ba8b5c33606630d1" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:90.15ex; height:6.676ex;" alt="{\displaystyle {\ddot {A}}^{\beta }={\frac {4\pi {c}}{i\Omega }}\int d^{3}r\sum _{k}\phi _{k}^{*}(r)\left\{\partial ^{\beta }\phi _{k}(r)+e^{i/cA^{\alpha }{r^{\alpha }}}\int d^{3}r&#39;\,\left[{\hat {r}}^{\beta }{},{\hat {V}}\right]e^{-i/cA^{\alpha }{r&#39;}^{\alpha }}\phi _{k}(r&#39;)\right\}-{\frac {4\pi {N}}{\Omega }}A^{\beta }}" />
</p>
<h4><span class="mw-headline" id="The_coupling_with_the_TDKS_equation">The coupling with the TDKS equation</span></h4>
<p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/4318359c5f76d7374f39452d0ae0f22b8e0a9547" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:92.194ex; height:6.676ex;" alt="{\displaystyle {\frac {\delta L}{\delta \phi _{m}^{*}(r&#39;)}}=\int d^{3}r\sum _{k}{\frac {1}{2}}\left(-i\delta (r-r&#39;)\delta _{km}\partial ^{\alpha }-{\frac {1}{c}}A^{\alpha }\delta (r-r&#39;)\delta _{km}\right)\left(-i\partial ^{\alpha }\phi _{k}(r)-{\frac {1}{c}}A^{\alpha }\phi _{k}(r)\right)}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/4942d2568c55af4bac5468433396c8f2e2b6b7c4" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.005ex; width:56.481ex; height:6.343ex;" alt="{\displaystyle +\int d^{3}r\,d^{3}r&#39;&#39;\,\sum _{k}e^{i/cA^{\alpha }(r^{\alpha }-{r&#39;&#39;}^{\alpha })}\delta (r-r&#39;)\delta _{km}V(r,r&#39;&#39;)\phi _{k}(r&#39;&#39;)}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/58d1b7f68c1d05de76564d4423a56be25549add8" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.838ex; width:56.803ex; height:6.509ex;" alt="{\displaystyle {\frac {\delta L}{\delta \phi _{k}^{*}(r)}}={\frac {1}{2}}\left(-i\partial ^{\alpha }-{\frac {1}{c}}A^{\alpha }\right)\left(-i\partial ^{\alpha }\phi _{k}(r)-{\frac {1}{c}}A^{\alpha }\phi _{k}(r)\right)}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/262ed489a2c91450c0b194e30ab2c04ca292f8da" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.338ex; width:35.262ex; height:5.676ex;" alt="{\displaystyle +\int \,d^{3}r&#39;\,e^{i/cA^{\alpha }(r^{\alpha }-{r&#39;}^{\alpha })}V(r,r&#39;)\phi _{k}(r&#39;)}" />
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/a0d95678c902e4261eb1b20d14acb8e5b92d75da" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.838ex; width:58.666ex; height:6.343ex;" alt="{\displaystyle {\frac {\delta L}{\delta \phi _{k}^{*}(r)}}=-{\frac {1}{2}}\partial ^{\alpha }\partial ^{\alpha }\phi _{k}(r)+{\frac {1}{2c^{2}}}A^{\alpha }A^{\alpha }\phi _{k}(r)+{\frac {i}{c}}{A}^{\alpha }\partial ^{\alpha }\phi _{k}(r)}" />
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/58914d6946b1e1588f5b6620993214449ba84e88" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.338ex; width:39.651ex; height:5.676ex;" alt="{\displaystyle +e^{i/cA^{\alpha }r^{\alpha }}\int \,d^{3}r&#39;\,V(r,r&#39;)e^{-i/cA^{\alpha }{r&#39;}^{\alpha }}\phi _{k}(r&#39;)}" />
</p>
<h3><span class="mw-headline" id="Notation">Notation</span></h3>
<ul><li>Greek superscript indexes: spatial coordinates with implicit sum.</li>
<li><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/75072415890364919d87f2e93f66995c90d69e0d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; margin-right: -0.283ex; width:1.574ex; height:2.176ex;" alt="{\displaystyle \Omega \!}" />: cell volume</li>
<li><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b4df206628eed61b48a8c51b432eca0c6520179e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; margin-right: -0.387ex; width:2.064ex; height:2.176ex;" alt="{\displaystyle N\!}" />: number of electrons</li></ul>
<!-- 
NewPP limit report
Cached time: 20230412154255
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.472 seconds
Real time usage: 10.195 seconds
Preprocessor visited node count: 208/1000000
Preprocessor generated node count: 480/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 3/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 10234/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1961-0!canonical!math=0 and timestamp 20230412154244 and revision id 7472
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers_Manual:Dielectric_function&amp;oldid=7472">http:///mediawiki/index.php?title=Developers_Manual:Dielectric_function&amp;oldid=7472</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers+Manual%3ADielectric+function&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers_Manual:Dielectric_function" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers_Manual:Dielectric_function&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers_Manual:Dielectric_function">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:Dielectric_function&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers_Manual:Dielectric_function&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers_Manual:Dielectric_function" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers_Manual:Dielectric_function" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers_Manual:Dielectric_function&amp;oldid=7472" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers_Manual:Dielectric_function&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 28 January 2015, at 11:02.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.472","walltime":"10.195","ppvisitednodes":{"value":208,"limit":1000000},"ppgeneratednodes":{"value":480,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":3,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":10234,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412154255","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":108});});</script>
</body>
</html>
