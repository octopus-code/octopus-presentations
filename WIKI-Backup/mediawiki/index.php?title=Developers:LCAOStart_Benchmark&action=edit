<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Developers:LCAOStart Benchmark - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers:LCAOStart_Benchmark","wgTitle":"Developers:LCAOStart Benchmark","wgCurRevisionId":8008,"wgRevisionId":0,"wgArticleId":2284,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers:LCAOStart_Benchmark","wgRelevantArticleId":2284,"wgRequestId":"00c9c5c26b8645d33a0ab9e2","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_LCAOStart_Benchmark rootpage-Developers_LCAOStart_Benchmark skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Developers:LCAOStart Benchmark</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Developers:LCAOStart_Benchmark" title="Developers:LCAOStart Benchmark">Developers:LCAOStart Benchmark</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">This is a benchmark of the different LCAOStart options.

= Technical details =

== SVN revision ==

This benchmark was done using SVN revision [http://www.tddft.org/trac/octopus/changeset/14733 14733]. The source was modified to allow freezing the density/KS potential during the first 3 SCF iterations for some of the runs.

== Environment/Hardware ==

GCC 4.8.3 20140911

Intel(R) Core(TM) i7 CPU X 980 @ 3.33GHz

= Test set =

== Systems ==
The test set consists of 11 systems:
# Alanine
# Alanine dimer
# Alanine trimer
# Alanine tetramer
# Ascidiacyclamide
# Benzene
# Betaine (zwitterion - separated formal charges)
# Methane
# Nitrogen-substituted graphene nanoflake model
# Oxyluciferin anion
# Tetraazacubane

== Input files ==
All the input files shared the same skeleton:
&lt;pre>
CalculationMode = gs
Units = eV_Angstrom
ExperimentalFeatures = yes
FromScratch = yes

XYZCoordinates = "adjusted.xyz"
MaximumIter = -1
&lt;/pre>
For most systems the Spacing and Radius were set to:
&lt;pre>
Spacing = 0.2
Radius = 4.0
&lt;/pre>
with the exceptions of methane:
&lt;pre>
Spacing = 0.25
Radius = 3.5
&lt;/pre>
and benzene:
&lt;pre>
Spacing = 0.15
Radius = 5.0
&lt;/pre>

All systems are neutral, with the exception of oxyluciferin:
&lt;pre>
ExcessCharge = -1
&lt;/pre>
and the graphene nanoflake:
&lt;pre>
ExcessCharge = 1
&lt;/pre>
The graphene nanoflake was considered to be spin-polarized.

All the geometries can be found [[media:Lcaostart_bench_xyz.tgz‎ |here]].

All the different options for {{variable|LCAOStart|SCF}} were benchmarked for three different cases: CG eigensolver, RMMDIIS eigensolver with no extra states, and RMMDISS eigensolver with 20% extra states.

= Results =

== Summary ==

{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Average relative differences of the number of SCF iterations, in percentage, with respect to the lcao_none option with the potential updated every SCF iteration. 
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| CG
|| 0.0 || 28.1 || 0.8 || 48.1 || 24.3 || 38.9 || 32.3 || 67.7
|- align="center"
| RMMDIIS
|| 0.0 || 82.3 || -34.7 || -28.7 || -4.2 || 94.1 || -36.9 || -33.2
|- align="center"
| RMMDIIS + 20% extra states
|| 0.0 || 32.4 || -37.8 || -32.0 || -0.4 || 34.2 || -23.1 || -30.2
|}


{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Average relative differences of the total number of matrix-vector products, in percentage, with respect to the lcao_none option with the potential updated every SCF iteration. 
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| CG
|| 0.0 || 29.2 || 0.2 || 44.6 || 22.9 || 43.1 || 28.7 || 63.2
|- align="center"
| RMMDIIS
|| 0.0 || 85.7 || -35.6 || -29.5 || -4.2 || 98.0 || -38.0 || -34.2
|- align="center"
| RMMDIIS + 20% extra states
|| 0.0 || 34.2 || -39.6 || -33.5 || -0.1 || 36.4 || -24.1 || -31.4
|}

== Number of SCF iterations ==

{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Number of SCF iterations using the CG eigensolver.
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| 1ALA
|| 12 || 12 || 11 || 14 || 14 || 14 || 14 || 16
|- align="center"
| 2ALA
|| 16 || 19 || 14 || 19 || 18 || 16 || 16 || 24
|- align="center"
| 3ALA
|| 27 || 44 || 41 || 43 || 39 || 70 || 54 || 126
|- align="center"
| 4ALA
|| 12 || 12 || 11 || 12 || 15 || 14 || 14 || 14
|- align="center"
| ASC
|| 13 || 13 || 13 || 12 || 16 || 16 || 16 || 17
|- align="center"
| benzene
|| 12 || 20 || 11 || 9 || 14 || 19 || 14 || 12
|- align="center"
| betaine
|| 13 || 17 || 12 || 12 || 15 || 18 || 15 || 15
|- align="center"
| methane
|| 8 || 8 || 8 || 8 || 11 || 11 || 11 || 11
|- align="center"
| N-GNF
|| 38 || 82 || 45 || 222 || 49 || 58 || 75 || 94
|- align="center"
| oxyluciferin
|| 14 || 16 || 13 || 14 || 18 || 15 || 16 || 18
|- align="center"
| tetraazacubane
|| 11 || 11 || 10 || 10 || 13 || 13 || 13 || 13
|}



{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Number of SCF iterations using the RMMDIIS eigensolver with no extra states.
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| 1ALA
|| 62 || 97 || 40 || 51 || 77 || 72 || 42 || 43
|- align="center"
| 2ALA
|| 108 || 197 || 92 || 83 || 112 || 140 || 74 || 77
|- align="center"
| 3ALA
|| 178 || 111 || 83 || 106 || 172 || 143 || 98 || 91
|- align="center"
| 4ALA
|| 59 || 59 || 45 || 34 || 76 || 118 || 38 || 40
|- align="center"
| ASC
|| 107 || 80 || 55 || 50 || 98 || 47 || 45 || 47
|- align="center"
| benzene
|| 50 || 185 || 33 || 35 || 51 || 213 || 33 || 33
|- align="center"
| betaine
|| 60 || 372 || 30 || 42 || 54 || 441 || 40 || 40
|- align="center"
| methane
|| 43 || 43 || 43 || 43 || 29 || 29 || 29 || 29
|- align="center"
| N-GNF
|| 389 || 203 || 323 || 371 || 151 || 231 || 320 || 411
|- align="center"
| oxyluciferin
|| 112 || 95 || 48 || 46 || 126 || 95 || 53 || 55
|- align="center"
| tetraazacubane
|| 51 || 103 || 27 || 43 || 50 || 98 || 34 || 39
|}


{| align="center" border="1" cellpadding="2" cellspacing="0"

|+ Number of SCF iterations using the RMMDIIS eigensolver with 20% extra states.
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| 1ALA
|| 43 || 42 || 26 || 28 || 47 || 40 || 27 || 25
|- align="center"
| 2ALA
|| 56 || 49 || 29 || 41 || 54 || 44 || 31 || 37
|- align="center"
| 3ALA
|| 62 || 69 || 35 || 60 || 55 || 53 || 102 || 59
|- align="center"
| 4ALA
|| 40 || 40 || 28 || 28 || 39 || 38 || 27 || 22
|- align="center"
| ASC
|| 41 || 39 || 22 || 22 || 43 || 34 || 22 || 23
|- align="center"
| benzene
|| 40 || 183 || 33 || 36 || 42 || 182 || 33 || 39
|- align="center"
| betaine
|| 44 || 45 || 24 || 22 || 42 || 57 || 29 || 23
|- align="center"
| methane
|| 22 || 2 || 22 || 22 || 30 || 30 || 30 || 30
|- align="center"
| N-GNF
|| 82 || 77 || 26 || 24 || 53 || 78 || 28 || 25
|- align="center"
| oxyluciferin
|| 53 || 51 || 26 || 23 || 50 || 54 || 23 || 25
|- align="center"
| tetraazacubane
|| 34 || 39 || 25 || 26 || 35 || 41 || 27 || 25
|}

== Number of matrix-vector products ==

{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Total number of matrix-vector products using the CG eigensolver.
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| 1ALA
|| 4719 || 4685 || 4518 || 5480 || 5666 || 5658 || 5512 || 6383
|- align="center"
| 2ALA
|| 11227 || 12889 || 9712 || 13473 || 12797 || 11697 || 11536 || 17374
|- align="center"
| 3ALA
|| 27141 || 43993 || 36040 || 37398 || 38999 || 73498 || 52410 || 121933
|- align="center"
| 4ALA
|| 16495 || 16236 || 15231 || 15818 || 20331 || 19623 || 19218 || 19469
|- align="center"
| ASC
|| 42632 || 42120 || 41272 || 40410 || 51658 || 51848 || 51374 || 56978
|- align="center"
| benzene
|| 4393 || 7721 || 3755 || 3248 || 4919 || 7220 || 4550 || 3960
|- align="center"
| betaine
|| 6624 || 9640 || 6318 || 6334 || 8022 || 9911 || 7527 || 7759
|- align="center"
| methane
|| 687 || 687 || 687 || 687 || 777 || 777 || 777 || 777
|- align="center"
| N-GNF
|| 107563 || 223023 || 142762 || 603383 || 154009 || 196033 || 232098 || 265523
|- align="center"
| oxyluciferin
|| 12743 || 14625 || 11902 || 13554 || 16082 || 14187 || 14191 || 16267
|- align="center"
| tetraazacubane
|| 4894 || 5096 || 4481 || 4372 || 5567 || 5749 || 5315 || 5348
|}


{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Total number of matrix-vector products using the RMMDIIS eigensolver with no extra states.
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| 1ALA
|| 7542 || 11952 || 4770 || 6156 || 9432 || 8802 || 5022 || 5148
|- align="center"
| 2ALA
|| 23712 || 43648 || 20128 || 18112 || 24608 || 30880 || 16096 || 16768
|- align="center"
| 3ALA
|| 56626 || 35052 || 26036 || 33442 || 54694 || 45356 || 30866 || 28612
|- align="center"
| 4ALA
|| 23880 || 23880 || 18000 || 13380 || 31020 || 48660 || 15060 || 15900
|- align="center"
| ASC
|| 104228 || 77390 || 52540 || 47570 || 95282 || 44588 || 42600 || 44588
|- align="center"
| benzene
|| 5025 || 19200 || 3240 || 3450 || 5130 || 22140 || 3240 || 3240
|- align="center"
| betaine
|| 9720 || 62136 || 4680 || 6696 || 8712 || 73728 || 6360 || 6360
|- align="center"
| methane
|| 1144 || 1144 || 1144 || 1144 || 752 || 752 || 752 || 752
|- align="center"
| N-GNF
|| 400784 || 208088 || 332408 || 382136 || 154216 || 237096 || 329300 || 423576
|- align="center"
| oxyluciferin
|| 30760 || 26000 || 12840 || 12280 || 34680 || 26000 || 14240 || 14800
|- align="center"
| tetraazacubane
|| 6840 || 14120 || 3480 || 5720 || 6700 || 13420 || 4460 || 5160
|}


{| align="center" border="1" cellpadding="2" cellspacing="0"
|+ Total number of matrix-vector products using the RMMDIIS eigensolver with 20% extra states.
|-
| || colspan="4" | Potential updated every SCF iteration || colspan="4" | Potential fixed during the first 3 SCF iterations
|-
! System 
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
! scope="col" style="width: 80px;" | lcao_none
! scope="col" style="width: 80px;" | lcao_simple
! scope="col" style="width: 80px;" | lcao_states
! scope="col" style="width: 80px;" | lcao_full
|- align="center"
| 1ALA
|| 6292 || 6138 || 3674 || 3982 || 6908 || 5830 || 3828 || 3520
|- align="center"
| 2ALA
|| 14326 || 12464 || 7144 || 10336 || 13794 || 11134 || 7676 || 9272
|- align="center"
| 3ALA
|| 23045 || 25740 || 12650 || 22275 || 20350 || 19580 || 38445 || 21890
|- align="center"
| 4ALA
|| 19080 || 19080 || 13032 || 13032 || 18576 || 18072 || 12528 || 10008
|- align="center"
| ASC
|| 46240 || 43860 || 23630 || 23630 || 48620 || 37910 || 23630 || 24820
|- align="center"
| benzene
|| 4770 || 22788 || 3888 || 4266 || 5022 || 22788 || 3888 || 4644
|- align="center"
| betaine
|| 8497 || 8700 || 4437 || 4031 || 8091 || 11136 || 5452 || 4234
|- align="center"
| methane
|| 695 || 695 || 695 || 695 || 975 || 975 || 975 || 975
|- align="center"
| N-GNF
|| 99502 || 93272 || 29726 || 27234 || 63368 || 94518 || 32218 || 28480
|- align="center"
| oxyluciferin
|| 17088 || 16416 || 8016 || 7008 || 16080 || 17424 || 7008 || 7680
|- align="center"
| tetraazacubane
|| 5352 || 6192 || 3840 || 4008 || 5520 || 6528 || 4176 || 3840
|}
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Code" title="Template:Code">Template:Code</a> (<a href="/mediawiki/index.php?title=Template:Code&amp;action=edit" title="Template:Code">view source</a>) </li><li><a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus major version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_major_version&amp;action=edit" title="Template:Octopus major version">view source</a>) </li><li><a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus minor version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_minor_version&amp;action=edit" title="Template:Octopus minor version">view source</a>) </li><li><a href="/wiki/Template:Octopus_version" title="Template:Octopus version">Template:Octopus version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_version&amp;action=edit" title="Template:Octopus version">view source</a>) </li><li><a href="/wiki/Template:Variable" title="Template:Variable">Template:Variable</a> (<a href="/mediawiki/index.php?title=Template:Variable&amp;action=edit" title="Template:Variable">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Developers:LCAOStart_Benchmark" title="Developers:LCAOStart Benchmark">Developers:LCAOStart Benchmark</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Developers:LCAOStart_Benchmark">http:///wiki/Developers:LCAOStart_Benchmark</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers%3ALCAOStart+Benchmark&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers:LCAOStart_Benchmark" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers:LCAOStart_Benchmark&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Developers:LCAOStart_Benchmark">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Developers:LCAOStart_Benchmark&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:LCAOStart_Benchmark&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers:LCAOStart_Benchmark" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers:LCAOStart_Benchmark" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers:LCAOStart_Benchmark&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":156});});</script>
</body>
</html>
