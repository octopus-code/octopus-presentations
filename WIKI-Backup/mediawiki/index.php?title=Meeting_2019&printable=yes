<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Meeting 2019 - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Meeting_2019","wgTitle":"Meeting 2019","wgCurRevisionId":11004,"wgRevisionId":11004,"wgArticleId":2998,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Meeting_2019","wgRelevantArticleId":2998,"wgRequestId":"a49fb0393372826b3b6f29f8","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Meeting_2019 rootpage-Meeting_2019 skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Meeting 2019</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Octopus_developers_workshop_-_11-12_June_2019"><span class="tocnumber">1</span> <span class="toctext">Octopus developers workshop - 11-12 June 2019</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#List_of_participants"><span class="tocnumber">1.1</span> <span class="toctext">List of participants</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Program"><span class="tocnumber">1.2</span> <span class="toctext">Program</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Discussions"><span class="tocnumber">1.3</span> <span class="toctext">Discussions</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Minutes"><span class="tocnumber">1.4</span> <span class="toctext">Minutes</span></a></li>
<li class="toclevel-2 tocsection-6"><a href="#Practical_information"><span class="tocnumber">1.5</span> <span class="toctext">Practical information</span></a></li>
</ul>
</li>
</ul>
</div>

<h1><span class="mw-headline" id="Octopus_developers_workshop_-_11-12_June_2019">Octopus developers workshop - 11-12 June 2019</span></h1>
<p>The fourth Octopus developers workshop will take place at the Max Planck Institute for the Dynamics and Structure of Matter, in Hamburg, Germany from the 11 to the 12 of June 2019.
</p>
<h2><span class="mw-headline" id="List_of_participants">List of participants</span></h2>
<table>
<tbody><tr>
<td>
<ol><li>Angel Rubio</li>
<li>Micael Oliveira</li>
<li>Martin Lueders</li>
<li>Nicolas Tancogne-Dejean</li>
<li>Florian Eich (Tuesday 100%, Wednesday 50%)</li>
<li>Alberto Castro</li>
<li>Sebastian Ohlmann</li>
<li>Heiko Appel</li>
<li>Shunsuke Sato</li>
<li>Florian Buchholz</li>
<li>Xavier Andrade</li>
<li>Carlo Andrea Rozzi</li>
<li>Umberto De Giovannini</li>
<li>Hannes Huebener</li>
<li>Enrico Ronca</li>
<li>Lukas Windgaetter</li>
<li>Dong bin Shin</li>
<li>Christian Schaefer</li>
<li>Franco Bonafé</li>
<li>Iris Theophilou</li>
<li>Henning Glawe</li>
<li>Davis Welakuh</li></ol>
</td>
<td>
<div class="center"><div class="thumb tnone"><div class="thumbinner" style="width:602px;"><a href="/wiki/File:IMG_20190611_194517.jpg" class="image"><img alt="IMG 20190611 194517.jpg" src="/mediawiki/images/thumb/0/0a/IMG_20190611_194517.jpg/600px-IMG_20190611_194517.jpg" decoding="async" width="600" height="450" class="thumbimage" srcset="/mediawiki/images/thumb/0/0a/IMG_20190611_194517.jpg/900px-IMG_20190611_194517.jpg 1.5x, /mediawiki/images/thumb/0/0a/IMG_20190611_194517.jpg/1200px-IMG_20190611_194517.jpg 2x" /></a>  <div class="thumbcaption"><div class="magnify"><a href="/wiki/File:IMG_20190611_194517.jpg" class="internal" title="Enlarge"></a></div></div></div></div></div>
</td>
<td>
</td></tr></tbody></table>
<h2><span class="mw-headline" id="Program">Program</span></h2>
<table>
<tbody><tr>
<th>
</th>
<th>Tuesday
</th>
<th>Wednesday
</th></tr>
<tr style="vertical-align:top;">
<th>Morning
</th>
<td>09:00 Welcome (Heiko)
<p>09:10 Micael
</p><p><br />
</p><p><br />
</p><p>10:30 Coffee break
</p><p>11:00 Davis
</p><p>11:20 Franco
</p><p>11:40 Heiko
</p><p>12:00 Lunch
</p>
</td>
<td>09:00 Sebastian
<p>09:50 Xavier
</p><p>10:30 Coffee break
</p><p>11:00 Nicolas
</p><p>11:10 Alberto
</p><p>11:50 Carlo
</p><p>12:30 Lunch
</p><p><br />
</p>
</td></tr>
<tr style="vertical-align:top;">
<th>Afternoon
</th>
<td>13:30 Xavier
<p>14:00 Florian E.
</p><p>14:30 Umberto/Hannes
</p><p>15:00 Nicolas
</p><p>15:30 Florian B.
</p><p>16:00 Coffee break
</p><p>16:30 Discussions
</p>
</td>
<td>13:30 Discussions
<p>15:30 Coffee break
</p><p>16:00 Final remarks/Conclusions
</p><p>17:00 Departure
</p>
</td></tr>
<tr style="vertical-align:top;">
<th>Evening
</th>
<td>Workshop dinner
</td>
<td>
</td></tr></tbody></table>
<p>Talks:
</p>
<table>
<tbody><tr>
<td>
<ol><li>State of the <del>Union</del>Octopus (Micael)</li>
<li>Non-linear transport from real-time TDDFT (Xavier)</li>
<li>Magnons from real-space real-time TDDFT (Florian Eich)</li>
<li>TBA (Davis)</li>
<li>Twisted light and local field enhancement (Franco)</li>
<li>Time-dependent density-functional theory for QED: coupled Ehrenfest-Maxwell-Pauli-Kohn-Sham equations (Heiko Appel)</li>
<li>Floquet analysis with Octopus (Umberto/Hannes)</li>
<li>Hybrid functionals and real-time TDDFT in Octopus (Nicolas Tancogne-Dejean)</li>
<li>Conjugate Gradients Algorithm for RDMFT (Florian Buchholz)</li>
<li>Improving ground-state calculations, analyzing testsuite results (Sebastian Ohlmann)</li>
<li>Octopus and GPUs (Xavier)</li>
<li>Working with solids in Octopus: The Delta factor strikes back (Nicolas Tancogne-Dejean)</li>
<li>New utilities in Octopus (Nicolas Tancogne-Dejean)</li>
<li>Commutator-free Magnus expansions for octopus (Alberto)</li>
<li>From static PCM to real-time PCM in octopus (Carlo)</li></ol>
</td></tr></tbody></table>
<h2><span class="mw-headline" id="Discussions">Discussions</span></h2>
<p>These are topics we should discuss during the meeting.
</p>
<ol><li>Next Octopus developers workshop</li>
<li>Scheduling a monthly meeting</li>
<li>Upcoming Octopus paper</li>
<li>Compiler warnings in buildbot tests.</li>
<li>Deprecation of forall in the Fortran standard.</li>
<li>Proposal to make components of derived data types private by default.</li>
<li>Proposal to reduce the default values for the ConvRelDens and EigensolverTol input options.</li>
<li>Introduction of Fortran 2003/2008 OOP features.</li>
<li>Permissions on gitlab group and associated projects.</li>
<li>New release manager.</li>
<li>Proposal to make reviews of merge requests mandatory</li>
<li>Modular parts interchangeable with other codes</li></ol>
<h2><span class="mw-headline" id="Minutes">Minutes</span></h2>
<ul><li><b>Next Octopus developers workshop</b>
<ul><li>Looking for volunteers to organize the next workshop.</li>
<li>Location and dates still to be decided. Suggestions for location: NYC, Jena.</li></ul></li>
<li><b>Scheduling a monthly meeting</b>
<ul><li>We will have a monthly videoconference meeting the first Monday of each he month, at 6 PM (CET/CEST)</li></ul></li>
<li><b>Upcoming Octopus paper</b>
<ul><li>Nicolas: Currently gathering contributions. All contributions should be sent before the end of June.</li>
<li>Micael: Paper will be submitted to NPJ Computational Materials. Lenght is not a problem, but possible lack of novelty might be one.</li></ul></li>
<li><b>Compiler warnings in buildbot tests.</b>
<ul><li>The compilation on the buildbot builders produces a lot of compiler warnings. Some of them are useful and should not be discarded, but they are drowned in a lot of useless warnings.</li>
<li>Compiler flags for warnings should be restricted to the useful ones and the presence of warnings should make the build fail. The failure should come from Buildbot itself, not the compiler, so that we get all the warnings for the full compilation of the code.</li>
<li>To decide the subset of useful warnings we first need to check what warnings we are getting and if we can actually remove them.</li></ul></li>
<li><b>Deprecation of forall in the Fortran standard.</b>
<ul><li>The forall statement has been declared obsolete in the Fortran 2018 standard. From our own experience, forall statements are not really faster than do loops and can even be slower when iterating over more than one index.</li>
<li>Thus no new forall statements should be added to the code and the existing ones should be replace by do loops.</li></ul></li>
<li><b>Proposal to make components of derived data types private by default.</b>
<ul><li>There is currently no simple and reliable way of checking if a component of a derived data type is used outside the module where the type is defined (there are many components with identical names and the names of the type instances are arbitrary).</li>
<li>The proposal is to use the same scheme as for the private/public statments in modules: by default all the components of a derived data type are declared private and the components that are used outside the module are explicitly declared to be public. An exception to this rule is if all or the almost all the components of a derived data type are public, in which case a comment stating that components are public by default is added and the the private components are indicated explicitly.</li></ul></li>
<li><b>Proposal to reduce the default values for the ConvRelDens and EigensolverTol input options.</b>
<ul><li>The current default values for ConvRelDens (1e-5) and EigensolverTolerace (1e-6) tend to produce differences in total energies and other quantities between identical runs performed on different machines or with different parallelization strategies that are higher than what one is usually aiming at.</li>
<li>After some discussion about the best way to tackle this, the following suggestion was agreed on:
<ul><li>Reduce ConvRelDens default to 1e-6 and EigensolverTolerace default to 1e-7</li>
<li>Change the default for ConvEnergy from 0 (not used) to some suitable value (0.1 miliHartree?)</li>
<li>Change the SCF stopping criteria such that the code exits the SCF loop only if the convergence criteria are met two consecutive iterations</li></ul></li></ul></li>
<li><b>Introduction of Fortran 2003/2008 OOP features.</b>
<ul><li>Compiler support for Fortran 2003 OOP features is now quite mature.</li>
<li>These features would be very useful for the planned multi-system support.</li>
<li>Need to ensure that these are correctly supported by all compilers of interest.</li></ul></li>
<li><b>Permissions on gitlab group and associated projects.</b>
<ul><li>There is no policy about the group/project to which new members are added and the corresponding permissions.</li>
<li>Currently the senior developers are part of the octopus-code group, which gives them access to all the projects within the group. By default other developers are added to the octopus-code/octopus project. Developers are added to the octopus-code/octopus-private project and to the buildbot subgroup projects upon request.</li>
<li>It was agreed to make this our official policy regarding gitlab user permissions.</li></ul></li>
<li><b>New release manager.</b>
<ul><li>Micael has been doing the release for almost five years.</li>
<li>It would be good if the associated workload was shared with the other developers.</li>
<li>There is currently no one else with experience in doing the releases with git and gitflow.</li>
<li>It was agreed to rotate the role of release manager among the developers, such that one person takes the role for the duration of one release cycle (one year, roughly). This should be done with the support of the previous release manager, so that the knowledge is smoothly passed on.</li>
<li>Martin has volunteered to be the new release manager.</li></ul></li>
<li><b>Proposal to make reviews of merge requests mandatory</b>
<ul><li>Merge request reviews have become an important tool to improve the code quality and in preventing the introduction of bugs. Most merge request are currently reviewed, but not all. The proposal is to make them mandatory by using the approval system of gitlab.</li>
<li>Concerns were raised that this could slow down development or that it could become a bureaucratic procedure.</li>
<li>There was an agreement that reviews should be performed in a reasonable amount (eg. one week) of time and developers should be able to go on with the merge if this time is exceeded.</li>
<li>Code reviews should definitely not be a bureaucratic procedure. The purpose of the code review is to improve the code in a constructive way, not to hinder development.</li>
<li>Code reviewers should look for things like code correctness, compliance with coding standards, existence and appropriateness of regression tests, etc.</li></ul></li>
<li><b>Modular parts interchangeable with other codes</b>
<ul><li>Xavier will be part of a project that will develop code in C++. Some of it could be reused in Octopus. There were no objections to this.</li>
<li>Using git subtrees could make it easier to share code with other projects. We could already try to do this with the Octopus parser.</li></ul></li>
<li><b>Git workflow</b>
<ul><li>Micael presented the current workflow used (gitflow with a couple of tweaks, mainly regarding the hotfix releases).</li>
<li>Advantages, disadvanges and pitfalls of the current workflow were discussed. The main problem seems to be the necessity of branching hotfixes from the master branch, instead of develop.</li>
<li>A compromise was found by allowing developers to create the bugfix branches from develop and let the release manager handle any required rebase.</li>
<li>If a bug fix is to be applied to master, but not to develop, then this needs to be clearly indicated in the corresponding merge request.</li></ul></li>
<li><b>Octopus webpage</b>
<ul><li>The webpage starts to show its age. The wiki is not really useful anymore, as there have been very few contributions from external users over the last couple of years.</li>
<li>An alternative would be to use a markdown based static page, as done for Libxc. This would allow to automatize some of the content updates. It would also allow to distribute some of the contents, like the manual, with the code sources.</li>
<li>The "Articles published with Octopus" page on the wiki is not up to date. Maybe it would be better to delete the page. On the other hand, the "How do I cite Octopus?" page should be always up to date.</li></ul></li></ul>
<h2><span class="mw-headline" id="Practical_information">Practical information</span></h2>
<p>The meeting will be at the <a rel="nofollow" class="external text" href="https://goo.gl/maps/pVDNKmmgEDK2">MPSD</a> in Seminar Room SR I. You can find information how to reach it <a rel="nofollow" class="external text" href="http://www.mpsd.mpg.de/en/institute/directions">here</a>.
</p>
<!-- 
NewPP limit report
Cached time: 20230412154605
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.052 seconds
Real time usage: 0.058 seconds
Preprocessor visited node count: 22/1000000
Preprocessor generated node count: 28/1000000
Post‐expand include size: 0/2097152 bytes
Template argument size: 0/2097152 bytes
Highest expansion depth: 2/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%    0.000      1 -total
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2998-0!canonical and timestamp 20230412154605 and revision id 11004
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Meeting_2019&amp;oldid=11004">http:///mediawiki/index.php?title=Meeting_2019&amp;oldid=11004</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Meeting+2019&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Meeting_2019" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Meeting_2019&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Meeting_2019">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Meeting_2019&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Meeting_2019&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Meeting_2019" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Meeting_2019" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Meeting_2019&amp;oldid=11004" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Meeting_2019&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 25 July 2019, at 09:30.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.052","walltime":"0.058","ppvisitednodes":{"value":22,"limit":1000000},"ppgeneratednodes":{"value":28,"limit":1000000},"postexpandincludesize":{"value":0,"limit":2097152},"templateargumentsize":{"value":0,"limit":2097152},"expansiondepth":{"value":2,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%    0.000      1 -total"]},"cachereport":{"timestamp":"20230412154605","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":109});});</script>
</body>
</html>
