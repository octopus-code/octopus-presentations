<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers:Workflow - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers:Workflow","wgTitle":"Developers:Workflow","wgCurRevisionId":10778,"wgRevisionId":10778,"wgArticleId":2907,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers:Workflow","wgRelevantArticleId":2907,"wgRequestId":"e0bbc1e2ca604b4401c23acb","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Workflow rootpage-Developers_Workflow skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers:Workflow</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>In Octopus the development and releases are done using Gitflow. This workflow was first introduced <a rel="nofollow" class="external text" href="https://nvie.com/posts/a-successful-git-branching-model/">here</a>.
</p><p>In Gitflow there are two eternal branches, <b><tt>master</tt></b> and <b><tt>develop</tt></b>. These are called main branches in the original Gitflow description. There are also three types of short-lived branches called supporting branches. These are the feature, release and hotfix branches. For a detailed explanation of the purpose of each type of branch and of the workflow used to create and merge the short-lived branches, please read the Gitflow description from the above link carefully. Nevertheless, there are some points that need to be stressed, while some special cases need to be explained more in detail. 
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#What_main_branch_should_be_used_to_create_the_supporting_branches"><span class="tocnumber">1</span> <span class="toctext">What main branch should be used to create the supporting branches</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#What_to_do_when_finding_a_bug"><span class="tocnumber">2</span> <span class="toctext">What to do when finding a bug</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Possible_issues_and_special_cases"><span class="tocnumber">3</span> <span class="toctext">Possible issues and special cases</span></a>
<ul>
<li class="toclevel-2 tocsection-4"><a href="#Bug_fixes_that_have_significantly_diverged_between_master_and_develop"><span class="tocnumber">3.1</span> <span class="toctext">Bug fixes that have significantly diverged between <b>master</b> and <b>develop</b></span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Incorporating_fixes_into_develop_before_the_hotfix_branch_is_finished"><span class="tocnumber">3.2</span> <span class="toctext">Incorporating fixes into develop before the hotfix branch is finished</span></a></li>
</ul>
</li>
</ul>
</div>

<h3><span class="mw-headline" id="What_main_branch_should_be_used_to_create_the_supporting_branches">What main branch should be used to create the supporting branches</span></h3>
<p>One common mistake when using Gitflow is to create a support branch from the wrong main branch, so here is a quick recap: 
</p>
<ul><li>Feature: The feature branches are used to add new features to <b><tt>develop</tt></b>, so they must be created from <b><tt>develop</tt></b>.</li>
<li>Release: The release branches need to include all the new features since the previous release, so they must be created from the <b><tt>develop</tt></b> branch.</li>
<li>Hotfix: The purpose of a hotfix branch is to create a new release including fixes for one or more bugs found in the previous release, but without including any new features. The <b><tt>develop</tt></b> branch includes new features, so it cannot be used for this. Therefore these branches must be created from the <b><tt>master</tt></b> branch.</li></ul>
<h3><span class="mw-headline" id="What_to_do_when_finding_a_bug">What to do when finding a bug</span></h3>
<p>If you are working on the <b><tt>develop</tt></b> branch or a feature branch, the first thing to do is check if the bug is also present in <b><tt>master</tt></b>. This will be the most likely situation unless you are working on some feature that was only recently added. 
</p>
<ul><li>The bug is <b>not</b> present in <b><tt>master</tt></b>. In this case there is no need for a hotfix branch. Just fix the bug using a feature branch. This can be either a new branch just for the bugfix, or an existing feature branch that is somehow related to the bug. Please do not use a completely unrelated branch to include the bug fix. It is also better if you apply the fix in a commit (or a series of commits if the fix is particularly complex) without including other changes not related to the fix. Make sure that in this case the branch does not contain the word <b><tt>hotfix</tt></b>, otherwise the <a rel="nofollow" class="external text" href="http://octopus-code.org/buildbot/">Buildbot</a> will treat the branch as if it was going to be merged into the <b><tt>master</tt></b> branch, not <b><tt>develop</tt></b>.</li></ul>
<ul><li>The bug is present both in <b><tt>master</tt></b> <b>and</b> <b><tt>develop</tt></b>. In this case create a hotfix branch from <b><tt>master</tt></b>. If there is already a hotfix branch, you can either commit your changes directly there, or create a new branch (name it as you like) and put in a merge request with the existing hotfix branch as target. The hotfix branch then needs to be merged both in <b><tt>master</tt></b> and <b><tt>develop</tt></b>, so that the bug fix is applied to both. It is very important that you do not create two different branches to fix the same bug, one based on <b><tt>develop</tt></b> and the other on <b><tt>master</tt></b>, because this can have serious consequences when the next major release is done, as the commits in <b><tt>develop</tt></b> will be merged into <b><tt>master</tt></b>.</li></ul>
<ul><li>The bug is <b>only</b> present in <b><tt>master</tt></b>. This case is usually treated like if the bug also existed in <b><tt>develop</tt></b>. If the bug is not present in <b><tt>develop</tt></b>, it means that the lines of code where the bug was found have changed significantly in the <b><tt>develop</tt></b> branch. Therefore, the most likely outcome of merging the hotfix branch into <b><tt>develop</tt></b> is a merge conflict. In these circumstances these conflicts will most likely be trivial to solve. In any case, it is always worth trying to merge locally the fix into <b><tt>develop</tt></b> to see what happens beforehand.</li></ul>
<h3><span class="mw-headline" id="Possible_issues_and_special_cases">Possible issues and special cases</span></h3>
<h4><span class="mw-headline" id="Bug_fixes_that_have_significantly_diverged_between_master_and_develop">Bug fixes that have significantly diverged between <b><tt>master</tt></b> and <b><tt>develop</tt></b></span></h4>
<p>What if there is a bug that is present both in <b><tt>master</tt></b> and <b><tt>develop</tt></b>, but the code where the bug is found has significantly diverged between the two branches? If the code in <b><tt>master</tt></b> and <b><tt>develop</tt></b> have diverged to a point where they require two completely different sets of changes to fix the bug, treat this as two different bugs, one that only exists in <b><tt>master</tt></b>, and one that only exists in <b><tt>develop</tt></b>. Nevertheless, git is surprisingly good at applying the same patch to two different branches if the differences between the two branches are not too complex. So, before deciding to treat a case like this as two different bugs, you might want to try first to commit the fix to an hotfix branch created from <b><tt>master</tt></b>, and then locally merging it into <b><tt>develop</tt></b> (make sure you know how to remove a commit by resetting the HEAD of a branch in git before attempting this).
</p>
<h4><span class="mw-headline" id="Incorporating_fixes_into_develop_before_the_hotfix_branch_is_finished">Incorporating fixes into develop before the hotfix branch is finished</span></h4>
<p>In the way Gitflow was originally designed, hotfix branches are supposed to only exist for a very short period of time. For a code like Octopus, which is usually not a critical component of the users' system, we might want to accumulate a few bug fixes in the hotfix branch before doing a release. This is to avoid too frequent releases, and the associated work load. 
</p><p>So what happens if one wants to include some fix from the hotfix branch into <b><tt>develop</tt></b> before the hotfix branch is finished? As it happens, one can merge the hotfix branch into <b><tt>develop</tt></b> at any time, and as often as necessary, so this is not really an issue. One simply needs to create merge requests to merge the hotfix branch into <b><tt>develop</tt></b> as necessary. Note that this does <b>not</b> apply when merging the hotfix branch into <b><tt>master</tt></b>!
</p>
<!-- 
NewPP limit report
Cached time: 20230412154223
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.063 seconds
Real time usage: 0.069 seconds
Preprocessor visited node count: 268/1000000
Preprocessor generated node count: 814/1000000
Post‐expand include size: 1006/2097152 bytes
Template argument size: 276/2097152 bytes
Highest expansion depth: 3/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   30.097      1 -total
 39.76%   11.968     42 Template:File
 12.23%    3.680      2 Template:Octopus
 11.28%    3.394      1 Template:Buildbot
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2907-0!canonical and timestamp 20230412154223 and revision id 10778
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers:Workflow&amp;oldid=10778">http:///mediawiki/index.php?title=Developers:Workflow&amp;oldid=10778</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers%3AWorkflow&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers:Workflow" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers:Workflow&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers:Workflow">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:Workflow&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:Workflow&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers:Workflow" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers:Workflow" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers:Workflow&amp;oldid=10778" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers:Workflow&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 12 March 2019, at 01:21.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.063","walltime":"0.069","ppvisitednodes":{"value":268,"limit":1000000},"ppgeneratednodes":{"value":814,"limit":1000000},"postexpandincludesize":{"value":1006,"limit":2097152},"templateargumentsize":{"value":276,"limit":2097152},"expansiondepth":{"value":3,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   30.097      1 -total"," 39.76%   11.968     42 Template:File"," 12.23%    3.680      2 Template:Octopus"," 11.28%    3.394      1 Template:Buildbot"]},"cachereport":{"timestamp":"20230412154223","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":125});});</script>
</body>
</html>
