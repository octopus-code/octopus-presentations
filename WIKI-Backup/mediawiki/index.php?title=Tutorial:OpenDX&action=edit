<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Tutorial:OpenDX - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:OpenDX","wgTitle":"Tutorial:OpenDX","wgCurRevisionId":11356,"wgRevisionId":0,"wgArticleId":2951,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:OpenDX","wgRelevantArticleId":2951,"wgRequestId":"9d18138e6f5a014eaf2532a6","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_OpenDX rootpage-Tutorial_OpenDX skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Tutorial:OpenDX</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Tutorial:OpenDX" title="Tutorial:OpenDX">Tutorial:OpenDX</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">''Note: the OpenDX program is no longer supported and the following tutorial should be considered obsolete. We recommend you to use one of the alternatives mentioned in this [[Tutorial:Visualization|tutorial]].''

== Input ==

At this point, the input should be quite familiar to you:

 {{variable|CalculationMode|Calculation_Modes}} = gs
 {{variable|UnitsOutput|Execution}} = eV_Angstrom
 
 {{variable|Radius|Mesh}} = 5*angstrom
 {{variable|Spacing|Mesh}} = 0.15*angstrom
 
 {{variable|Output|Output}} = wfs + density + elf + potential
 {{variable|OutputFormat|Output}} = cube + xcrysden + dx + axis_x + plane_z
 
 {{variable|XYZCoordinates|System}} = "benzene.xyz"

Coordinates are in this case given in the file {{file|benzene.xyz}} file. This file should look like:

 12
    Geometry of benzene (in Angstrom)
 C  0.000  1.396  0.000
 C  1.209  0.698  0.000
 C  1.209 -0.698  0.000
 C  0.000 -1.396  0.000
 C -1.209 -0.698  0.000
 C -1.209  0.698  0.000
 H  0.000  2.479  0.000
 H  2.147  1.240  0.000
 H  2.147 -1.240  0.000
 H  0.000 -2.479  0.000
 H -2.147 -1.240  0.000
 H -2.147  1.240  0.000

Note that we asked octopus to output the wavefunctions (&lt;tt>wfs&lt;/tt>), the density, the electron localization function (&lt;tt>elf&lt;/tt>) and the Kohn-Sham potential. We ask Octopus to generate this output in several formats, that are requires for the different visualization tools that we mention below. If you want to save some disk space you can just keep the option that corresponds to the program you will use. Take a look at the documentation on variables {{variable|Output|Output}} and {{variable|OutputFormat|Output}} for the full list of possible quantities to output and formats to visualize them in.

=== OpenDX ===

[http://www.opendx.org OpenDX] is a very powerful, although quite complex, program for the 3D visualization of data. It is completely general-purpose, and uses a visual language to process the data to be plotted. Don't worry, as you will probably not need to learn this language. We have provided a program ({{file|mf.net}} and {{file|mf.cfg}}) that you can get from the {{file|SHARE/util}} directory in the {{octopus}} installation (probably it is located in {{file|/usr/share/octopus/util}}). 

Before starting, please make sure you have it installed together with the Chemistry extensions. You may find some instructions [[Releases#OpenDX|here]]. Note that openDX is far from trivial to install and to use. However, the outcomes are really spectacular, so if you are willing to suffer a little at the beginning, we are sure that your efforts will be compensated.

If you run {{octopus}} with this input file, afterward you will find in the {{file|static}} directory several files with the extension {{file|.dx}}. These files contain the wave-functions, the density, etc. in a format that can be understood by [http://www.opendx.org/ Open DX] (you will need the chemistry extensions package installed on top of DX, you can find it in our [[releases]] page). 

So, let us start. First copy the files {{file|mf.net}} and {{file|mf.cfg}} to your open directory, and open dx with the command line

  > dx

If you followed the instructions given in [[Releases#OpenDX|here]] you should see a window similar to the one shown on the right.
&lt;gallery>
  Image:Tutorial_dx_1.png|Screenshot of the Open DX main menu
  Image:Tutorial_dx_2.png|Main dialog of mf.net
  Image:Tutorial_dx_3.png|Isosurfaces dialog
  Image:Tutorial_dx_4.png|Geometry dialog
&lt;/gallery>

Now select '''&lt;tt>Run Visual Programs...&lt;/tt>''', select the {{file|mf.net}} file, and click on '''&lt;tt>OK&lt;/tt>'''. This will open the main dialog of our application. You will no longer need the main DX window, so it is perhaps better to minimize it to get it out of the way. Before continuing, go to the menu '''&lt;tt>Execute > Execute on Change&lt;/tt>''', which instructs to open DX to recalculate out plot whenever we make a change in any parameters.

[[Image:Tutorial_dx_5.png|thumb|350px]]

Now, let us choose a file to plot. As you can see, the field &lt;tt>File&lt;/tt> in the mains dialog is &lt;tt>NULL&lt;/tt>. If you click on the three dots next to the field a file selector will open. Just select the file {{file|static/density-1.dx}} and press OK. Now we will instruct open DX to draw a iso-surface. Click on &lt;tt>Isosurfaces&lt;/tt> and a new dialog box will open (to close that dialog box, please click again on &lt;tt>Isosurfaces&lt;/tt> in the main dialog!) In the end you may have lots of windows lying around, so try to be organized. Then click on &lt;tt>Show Isosurface 1&lt;/tt> - an image should appear with the density of benzene. You can zoom, rotate, etc the picture if you follow the menu '''&lt;tt>Options > View control...&lt;/tt>''' in the menu of the image panel. You can play also around in the isosurfaces dialog with the &lt;tt>Isosurface value&lt;/tt> in order to make the picture more appealing, change the color, the opacity, etc. You can even plot two isosurfaces with different values.

We hope you are not too disappointed with the picture you just plotted. Densities are usually quite boring (even if the Hohenberg-Kohn theorem proves that they know everything)! You can try to plot other quantities like the electronic localization function, the wave-functions, etc, just by choosing the appropriate file in the main dialog. You can try it later: for now we will stick to the "boring density".

Let us now add a structure. Close the isosurfaces dialog '''by clicking again in &lt;tt>Isosurfaces&lt;/tt> in the main dialog box''', and open the &lt;tt>Geometry&lt;/tt> dialog. In the &lt;tt>File&lt;/tt> select the file {{file|benzene.xyz}}, and click on &lt;tt>Show Geometry&lt;/tt>. The geometry should appear beneath the isosurface. You can make bonds appear and disappear by using the slide &lt;tt>Bonds length&lt;/tt>,  change its colors, etc. in this dialog.

To finish, let us put some "color" in our picture. Close the &lt;tt>Geometry&lt;/tt> dialog (you know how), and open again the isosurfaces dialog. Choose &lt;tt>Map isosurface&lt;/tt> and in the &lt;tt>File&lt;/tt> field choose the file {{file|static/vh.dx}}. There you go, you should have a picture of an isosurface of benzene colored according to the value of the electrostatic potential! Cool, no? If everything went right, you should end with a picture like the one on the right.

As an exercise you can try to plot the wave-functions, etc, make slabs, etc. Play around. As usual some things do not work perfectly, but most of it does!

[[Category:Obsolete]]
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Code" title="Template:Code">Template:Code</a> (<a href="/mediawiki/index.php?title=Template:Code&amp;action=edit" title="Template:Code">view source</a>) </li><li><a href="/wiki/Template:File" title="Template:File">Template:File</a> (<a href="/mediawiki/index.php?title=Template:File&amp;action=edit" title="Template:File">view source</a>) </li><li><a href="/wiki/Template:Octopus" title="Template:Octopus">Template:Octopus</a> (<a href="/mediawiki/index.php?title=Template:Octopus&amp;action=edit" title="Template:Octopus">view source</a>) </li><li><a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus major version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_major_version&amp;action=edit" title="Template:Octopus major version">view source</a>) </li><li><a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus minor version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_minor_version&amp;action=edit" title="Template:Octopus minor version">view source</a>) </li><li><a href="/wiki/Template:Octopus_version" title="Template:Octopus version">Template:Octopus version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_version&amp;action=edit" title="Template:Octopus version">view source</a>) </li><li><a href="/wiki/Template:Variable" title="Template:Variable">Template:Variable</a> (<a href="/mediawiki/index.php?title=Template:Variable&amp;action=edit" title="Template:Variable">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Tutorial:OpenDX" title="Tutorial:OpenDX">Tutorial:OpenDX</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Tutorial:OpenDX">http:///wiki/Tutorial:OpenDX</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AOpenDX&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:OpenDX" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:OpenDX&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Tutorial:OpenDX">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:OpenDX" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:OpenDX" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:OpenDX&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":145});});</script>
</body>
</html>
