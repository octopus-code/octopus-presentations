<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Developers:Preparing Release - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Developers:Preparing_Release","wgTitle":"Developers:Preparing Release","wgCurRevisionId":11146,"wgRevisionId":11146,"wgArticleId":2172,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Developers:Preparing_Release","wgRelevantArticleId":2172,"wgRequestId":"3acacda5dd4a439c3aa25c64","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Developers_Preparing_Release rootpage-Developers_Preparing_Release skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Developers:Preparing Release</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><h2><span class="mw-headline" id="Major_releases">Major releases</span></h2>
<p>For a new major release (<i>e.g.</i> 12.0):
</p>
<ul><li>Create a release branch from <b><tt>develop</tt></b>. The branch should be named <b><tt>release-12.0</tt></b>.</li>
<li>Update the version number in <code>AC_INIT</code> in <b><tt>configure.ac</tt></b> and push the changes to the git repository.</li>
<li>The buildbot will automatically create from the release branch a tarball (<a rel="nofollow" class="external free" href="http://www.tddft.org/programs/octopus/down.php?file=release-12.0/octopus-12.0.tar.gz">http://www.tddft.org/programs/octopus/down.php?file=release-12.0/octopus-12.0.tar.gz</a>), the variable documentation (<a rel="nofollow" class="external free" href="http://octopus-code.org/doc/release-12.0/html/vars.php">http://octopus-code.org/doc/release-12.0/html/vars.php</a>), and the doxygen documentation (<a rel="nofollow" class="external free" href="http://octopus-code.org/doc/release-12.0/doxygen_doc/index.html">http://octopus-code.org/doc/release-12.0/doxygen_doc/index.html</a>).</li>
<li>Distribute the tarball among the developers for testing and correct any problems encountered.</li>
<li>Update the <b><tt>PACKAGING</tt></b> file. (This require to update the periodic_systems/09-etsf_io.test)</li>
<li>Update the <b><tt>debian/changelog</tt></b> file.</li>
<li>Once everybody is happy with the tarball, merge the release branch into the <b><tt>master</tt></b> branch.</li>
<li>Add a new tag on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/tags/new">Gitlab</a>:
<ul><li>Create the new tag from the <b><tt>master</tt></b> branch.</li>
<li>Tag should be named 12.0.</li>
<li>Add a link to <a href="/wiki/Octopus_12" title="Octopus 12">Octopus 12</a> in the release notes.</li>
<li>The buildbot will automatically create from the tag a tarball (<a rel="nofollow" class="external free" href="http://octopus-code.org/down.php?file=12.0/octopus-12.0.tar.gz">http://octopus-code.org/down.php?file=12.0/octopus-12.0.tar.gz</a>), the variable documentation (<a rel="nofollow" class="external free" href="http://octopus-code.org/doc/12.0/html/vars.php">http://octopus-code.org/doc/12.0/html/vars.php</a>), and the doxygen documentation (<a rel="nofollow" class="external free" href="http://octopus-code.org/doc/12.0/doxygen_doc/index.html">http://octopus-code.org/doc/12.0/doxygen_doc/index.html</a>).</li></ul></li>
<li>Merge the release branch into <b><tt>develop</tt></b>. This might require to resolve some conflicts, as the <b><tt>develop</tt></b> branch might have diverged.</li>
<li>Update the codename for the development version in AC_INIT in <b><tt>configure.ac</tt></b> on the <b><tt>develop</tt></b> branch and on the <a href="/wiki/FAQ" title="FAQ">FAQ</a> page.</li>
<li>Update the <a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus_major_version</a> and the <a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus_minor_version</a> wiki templates.</li>
<li>Add a new page for the release to the wiki. <i>e.g.</i> <a href="/wiki/Octopus_12" title="Octopus 12">Octopus 12</a></li>
<li>Update the <a href="/wiki/Changes" title="Changes">Changes</a> wiki page, referring to the merge requests on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/-/merge_requests?scope=all&amp;utf8=✓&amp;state=merged">Gitlab</a>.</li>
<li>Update <a href="/wiki/Manual:Updating_to_a_new_version" title="Manual:Updating to a new version">Manual:Updating to a new version</a>, referring to <a href="/wiki/Developers:Big_changes" title="Developers:Big changes">Developers:Big changes</a>.</li>
<li>Update the wiki <a href="/wiki/Main_Page" title="Main Page">Main_Page</a>, so it shows this latest release as well as the last bugfix release of the previous major version.</li>
<li>Add the release to News on the <a href="/wiki/Main_Page" title="Main Page">Main_Page</a>.</li>
<li>Add the release to <a href="/wiki/Releases" title="Releases">Releases</a>.</li>
<li>Send an email announcement. Sample e-mail below.</li>
<li>Post it on the Octopus Facebook page! <a rel="nofollow" class="external free" href="https://www.facebook.com/octopus.code">https://www.facebook.com/octopus.code</a></li>
<li>Produce new <b><tt>.deb</tt></b> files for Linux installation, upload them, and add to <a href="/wiki/Octopus_12" title="Octopus 12">Octopus 12</a>.</li>
<li>Check and update <a href="/wiki/Manual" title="Manual">Manual</a> and <a href="/wiki/Tutorials" title="Tutorials">Tutorials</a>. (this takes time, it is better to do it after the rest of the release)</li></ul>
<h3><span class="mw-headline" id="Sample_email">Sample email</span></h3>
<p>To: octopus-users@tddft.org, octopus-announce@tddft.org 
</p><p>Subject: Octopus 12.0 released
</p><p>Dear Octopus users,
</p><p>We are pleased to announce that we have just released Octopus 12.0.
</p><p>The source code, documentation and information for this new release can be obtained from:
</p><p><a rel="nofollow" class="external free" href="http://octopus-code.org/wiki/Octopus_12">http://octopus-code.org/wiki/Octopus_12</a>
</p><p>Best regards,
</p><p>The Octopus development team
</p>
<h2><span class="mw-headline" id="Minor_releases">Minor releases</span></h2>
<p>For a new minor release (<i>e.g.</i> 12.1):
</p>
<ul><li>Create a hotfix branch from <b><tt>master</tt></b>. The branch should be named <b><tt>hotfix-12.1</tt></b>.</li>
<li>Create a merge request on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/merge_request/new">Gitlab</a> having the hotfix branch as source and the <b><tt>master</tt></b> branch as target.</li>
<li>Once all the bug fixes have been merged into the hotfix branch, update the version number in <code>AC_INIT</code> in <b><tt>configure.ac</tt></b>.</li>
<li>Update the <b><tt>PACKAGING</tt></b> file.</li>
<li>Update the <b><tt>debian/changelog</tt></b> file.</li>
<li>Merge the hotfix branch into <b><tt>master</tt></b>.</li>
<li>Add a new tag on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/merge_requests/new">Gitlab</a>:
<ul><li>Create the new tag from the <b><tt>master</tt></b> branch.</li>
<li>Tag should be named 12.1.</li>
<li>The buildbot will automatically create from the tag a tarball (<a rel="nofollow" class="external free" href="http://octopus-code.org/down.php?file=12.1/octopus-12.1.tar.gz">http://octopus-code.org/down.php?file=12.1/octopus-12.1.tar.gz</a>), and the variable documentation (<a rel="nofollow" class="external free" href="http://octopus-code.org/doc/12.1/html/vars.php">http://octopus-code.org/doc/12.1/html/vars.php</a>).</li></ul></li>
<li>Create a new merge request on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/merge_requests/new">Gitlab</a> having the hotfix branch as source and the <b><tt>develop</tt></b> branch as target.</li>
<li>Merge the hotfix branch into <b><tt>develop</tt></b>. There should be a conflict in the <b><tt>configure.ac</tt></b> file, because of the changed version number. Keep the one coming from <b><tt>develop</tt></b>. Other conflicts might occur.</li>
<li>Update the <a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus_minor_version</a> wiki template.</li>
<li>Add the new release to the "Other releases in this series" section of the release page, <i>e.g.</i> <a href="/wiki/Octopus_12" title="Octopus 12">Octopus 12</a>. There's no need to update the download links, this is done through the templates.</li>
<li>Update the <a href="/wiki/Changes" title="Changes">Changes</a> wiki page, referring to the merge requests on <a rel="nofollow" class="external text" href="https://gitlab.com/octopus-code/octopus/-/merge_requests?scope=all&amp;utf8=✓&amp;state=merged">Gitlab</a>.</li>
<li>Add the release to News on the <a href="/wiki/Main_Page" title="Main Page">Main_Page</a>.</li></ul>
<!-- 
NewPP limit report
Cached time: 20230412154221
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.064 seconds
Real time usage: 0.071 seconds
Preprocessor visited node count: 197/1000000
Preprocessor generated node count: 690/1000000
Post‐expand include size: 682/2097152 bytes
Template argument size: 224/2097152 bytes
Highest expansion depth: 3/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 0/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   28.887      1 -total
 30.61%    8.843     24 Template:File
 16.27%    4.701     24 Template:Octopus_major_version
 13.33%    3.850      2 Template:Code
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:2172-0!canonical and timestamp 20230412154221 and revision id 11146
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Developers:Preparing_Release&amp;oldid=11146">http:///mediawiki/index.php?title=Developers:Preparing_Release&amp;oldid=11146</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Developers%3APreparing+Release&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Developers:Preparing_Release" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Developers:Preparing_Release&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Developers:Preparing_Release">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:Preparing_Release&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Developers:Preparing_Release&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Developers:Preparing_Release" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Developers:Preparing_Release" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Developers:Preparing_Release&amp;oldid=11146" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Developers:Preparing_Release&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 17 July 2020, at 13:55.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.064","walltime":"0.071","ppvisitednodes":{"value":197,"limit":1000000},"ppgeneratednodes":{"value":690,"limit":1000000},"postexpandincludesize":{"value":682,"limit":2097152},"templateargumentsize":{"value":224,"limit":2097152},"expansiondepth":{"value":3,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":0,"limit":5000000},"timingprofile":["100.00%   28.887      1 -total"," 30.61%    8.843     24 Template:File"," 16.27%    4.701     24 Template:Octopus_major_version"," 13.33%    3.850      2 Template:Code"]},"cachereport":{"timestamp":"20230412154221","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":122});});</script>
</body>
</html>
