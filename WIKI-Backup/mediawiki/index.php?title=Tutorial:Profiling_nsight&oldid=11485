<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Profiling nsight - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Profiling_nsight","wgTitle":"Tutorial:Profiling nsight","wgCurRevisionId":11485,"wgRevisionId":11485,"wgArticleId":3077,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Expert"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Profiling_nsight","wgRelevantArticleId":3077,"wgRequestId":"9562dcfedaf25409a2fb1e06","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Profiling_nsight rootpage-Tutorial_Profiling_nsight skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Profiling nsight</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 09:01, 22 September 2021 by <a href="/mediawiki/index.php?title=User:Sohlmann&amp;action=edit&amp;redlink=1" class="new mw-userlink" title="User:Sohlmann (page does not exist)"><bdi>Sohlmann</bdi></a> <span class="mw-usertoollinks">(<a href="/wiki/User_talk:Sohlmann" class="mw-usertoollinks-talk" title="User talk:Sohlmann">talk</a> | <a href="/wiki/Special:Contributions/Sohlmann" class="mw-usertoollinks-contribs" title="Special:Contributions/Sohlmann">contribs</a>)</span> <span class="comment">(<span dir="auto"><span class="autocomment"><a href="#Slurm_script_for_Raven">→‎Slurm script for Raven</a></span></span>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;diff=prev&amp;oldid=11485" title="Tutorial:Profiling nsight">diff</a>) <a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;direction=prev&amp;oldid=11485" title="Tutorial:Profiling nsight">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>In this tutorial, we want to profile the GPU version of octopus using NVIDIA
Nsight Systems.  This allows us to see which parts are executed on the GPU and
which parts might still be executed on the CPU or where transfers lead to
inefficiencies.
</p><p><br />
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Prerequisites"><span class="tocnumber">1</span> <span class="toctext">Prerequisites</span></a>
<ul>
<li class="toclevel-2 tocsection-2"><a href="#Compiling_octopus"><span class="tocnumber">1.1</span> <span class="toctext">Compiling octopus</span></a></li>
<li class="toclevel-2 tocsection-3"><a href="#Nsight_systems"><span class="tocnumber">1.2</span> <span class="toctext">Nsight systems</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-4"><a href="#Run_the_profiling"><span class="tocnumber">2</span> <span class="toctext">Run the profiling</span></a></li>
<li class="toclevel-1 tocsection-5"><a href="#Analyze_the_results"><span class="tocnumber">3</span> <span class="toctext">Analyze the results</span></a></li>
<li class="toclevel-1 tocsection-6"><a href="#Next_steps"><span class="tocnumber">4</span> <span class="toctext">Next steps</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Slurm_script_for_Raven"><span class="tocnumber">5</span> <span class="toctext">Slurm script for Raven</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Prerequisites">Prerequisites</span></h2>
<h3><span class="mw-headline" id="Compiling_octopus">Compiling octopus</span></h3>
<p>Make sure that octopus has been compiled with CUDA and NVTX support. For this, you need to add
the options <tt>--enable-cuda --enable-nvtx</tt> at the configure step. Then, it should be listed in
the `Configuration options` section of the Octopus standard output. For example: 
</p>
<pre>   Configuration options  : maxdim3 openmp mpi **cuda** sse2 avx
</pre>
<p>If you compile the parallel MPI version, the configure script will automatically detect if
CUDA-aware MPI is supported (works for OpenMPI). For the beginning, you don't necessarily
need this feature, but it is very important for obtaining the best performances in the case domain-parallel runs.
</p><p>Please refer to the Octopus installation guide for detailed compilation instructions.
</p>
<h3><span class="mw-headline" id="Nsight_systems">Nsight systems</span></h3>
<p>We use NVIDIA Nsight Systems for the profiling which is a low overhead
performance analysis tool, designed to analyze program on GPUs.
To collect the profiling data, you need
to have Nsight Systems CLI installed on your target machine. Please refer to
the <a rel="nofollow" class="external text" href="https://docs.nvidia.com/nsight-systems/InstallationGuide/index.html">NVIDIA Nsight Systems installation guide</a> for
more details. If you want to run Nsight Systems inside a container, please
refer to <a rel="nofollow" class="external text" href="https://developer.nvidia.com/blog/nvidia-nsight-systems-containers-cloud/">this guide</a>.
Many of the NVIDIA GPU Cloud catalog already include the Nsight Systems.
</p><p>Nsight systems is available on MPCDF systems as a module and can be loaded as:
<tt>module load nsight_systems/2021</tt>
</p><p>In order to analyze the results, you need to use the Nsight systems GUI. For
this, you have several options:
</p>
<ul><li>Run the GUI on the remote machine (e.g. login node of the cluster) using X forwarding (i.e. ssh -XC login_node). This can be laggy due to the connection.</li>
<li>Run the GUI locally; for this you need to install Nsight systems on your laptop (see <a rel="nofollow" class="external free" href="https://docs.nvidia.com/nsight-systems/InstallationGuide/index.html">https://docs.nvidia.com/nsight-systems/InstallationGuide/index.html</a>)</li>
<li>If you can use MPCDF services, you can use the remote visualization service to get an X session with access to your files (<a rel="nofollow" class="external free" href="https://rvs.mpcdf.mpg.de/">https://rvs.mpcdf.mpg.de/</a>)</li></ul>
<h2><span class="mw-headline" id="Run_the_profiling">Run the profiling</span></h2>
<p>Use the following input file
</p>
<pre> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = gs
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=FromScratch"><code>FromScratch</code></a></span> = yes
  
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span> = "1ala.xyz"
 
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 4.0*angstrom
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.4*angstrom
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=SCF&amp;name=Eigensolver"><code>Eigensolver</code></a></span> = rmmdiis
 
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=ProfilingMode"><code>ProfilingMode</code></a></span> = prof_time
</pre>
<p>together with the file <tt>1ala.xyz</tt>:
</p>
<pre> 23
 units: A
      N                   -1.801560    0.333315   -1.308298
      C                   -1.692266    1.069227    0.012602
      C                   -0.217974    1.151372    0.425809
      O                    0.256888    2.203152    0.823267
      C                   -2.459655    0.319513    1.077471
      H                   -1.269452    0.827043   -2.046396
      H                   -1.440148   -0.634968   -1.234255
      H                   -2.791116    0.267637   -1.602373
      H                   -2.104621    2.114111   -0.129280
      H                   -2.391340    0.844513    2.046396
      H                   -2.090378   -0.708889    1.234538
      H                   -3.530691    0.246022    0.830204
      N                    0.476130   -0.012872    0.356408
      C                    1.893957   -0.046600    0.735408
      C                    2.681281    0.990593   -0.107455
      O                    3.486946    1.702127    0.516523
      O                    2.498931    1.021922   -1.333241
      C                    2.474208   -1.425485    0.459844
      H                    0.072921   -0.880981    0.005916
      H                    1.975132    0.211691    1.824463
      H                    1.936591   -2.203152    1.019733
      H                    3.530691   -1.461320    0.761975
      H                    2.422706   -1.683153   -0.610313
</pre>
<p>It is important that you enable the time profiling in your Octopus input file
using <tt>ProfilingMode = prof_time</tt>. This is required for activating the
NVTX markers and ranges.
</p><p>Now you can run octopus with the profiler as follows:
</p>
<pre>nsys profile -t cuda,nvtx,mpi octopus
</pre>
<p>This will run octopus on one core. To use it on several cores (and potentially also on several GPUs), you can use:
</p>
<pre>nsys profile -t cuda,nvtx,mpi srun -n 2 octopus
</pre>
<p>This will run octopus on 2 cores using srun (use this inside a slurm script).
You might need to adapt this to your MPI launcher (e.g. mpiexec) and number of
MPI tasks.
</p><p>Octopus will run as usual and at the end of the run a report file with `.qdrep`
extension will be generated. If you run nsys more often, it will create files
with increasing numbers to avoid overwriting old results.
</p><p>By default, the Nsight systems will use `/tmp/nvidia` to store temporary files
during data collection. In case you are running the profiler on a diskless
machine (such as MPCDF’s Raven cluster) you must set the TMPDIR enviroment
variable in your job sumbission script to another fast storage device instead.
For example: <tt>export TMPDIR=/ptmp/$USER/nvidia</tt>
</p>
<h2><span class="mw-headline" id="Analyze_the_results">Analyze the results</span></h2>
<p>Inspect the generated report file. The best way of inspecting the report file
is using the Nsight Systems GUI (<tt>nsys-ui</tt>) which is included in the
Nsight Systems installation. As explained above, you can start it locally or remotely on
the cluster you ran the simulation on.
</p><p>After running the `nsys-ui`, open the report file
and you will see the timeline view of the report.
You can expand the rows for NVTX to see the annotation of the code ranges.
This is very useful because it shows you a timeline with all profiling
regions of the code which is very instructive.
</p><p>Here is an example screenshot:
</p><p><a href="/wiki/File:Nsys_octopus.png" class="image"><img alt="Nsys octopus.png" src="/mediawiki/images/thumb/4/44/Nsys_octopus.png/400px-Nsys_octopus.png" decoding="async" width="400" height="409" srcset="/mediawiki/images/thumb/4/44/Nsys_octopus.png/600px-Nsys_octopus.png 1.5x, /mediawiki/images/thumb/4/44/Nsys_octopus.png/800px-Nsys_octopus.png 2x" /></a>
</p><p>In the row "memory", you can see the transfers from and to the GPU with some
details about their speed. In the row kernels, you see which kernels run on
the GPU and also some details about their launching parameters. It is a good
sign if you see that kernels are continuously running on the GPU and if there 
are basically no data transfers. If either no kernel is running or there are a
lot of data transfers, it is usually a sign of bad performance. Then you
can look at the NVTX regions where no kernels are running on the GPU to
find out which parts are not yet ported.
Note that the names of the different regions correspond to the profiling regions of the internal Octopus profilling tool. 
</p><p>Check out the <a rel="nofollow" class="external text" href="https://docs.nvidia.com/nsight-systems/UserGuide/index.html#report">Nsight Systems documentation</a>
for detailed information on using the Nsight Systems GUI.
</p>
<h2><span class="mw-headline" id="Next_steps">Next steps</span></h2>
<p>Now, profile and analyze a td run which is usually more efficient on GPUs. For this, use the following input file:
</p>
<pre> <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = td
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=FromScratch"><code>FromScratch</code></a></span> = yes
  
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=XYZCoordinates"><code>XYZCoordinates</code></a></span> = "1ala.xyz"
  
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 4.0*angstrom
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.4*angstrom
 
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=ProfilingMode"><code>ProfilingMode</code></a></span> = prof_time
 
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDPropagator"><code>TDPropagator</code></a></span> = aetrs
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDMaxSteps"><code>TDMaxSteps</code></a></span> = 20
 <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDTimeStep"><code>TDTimeStep</code></a></span> = 0.05
</pre>
<p>and run octopus again with Nsight systems to get a profile that you can analyze in the GUI.
</p>
<h2><span class="mw-headline" id="Slurm_script_for_Raven">Slurm script for Raven</span></h2>
<p>To run octopus on the MPCDF system Raven, you can use the following slurm script which will use one node with 4 GPUs:
</p>
<pre>#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./log.%j
#SBATCH -D ./
#SBATCH -J octopus
#SBATCH --constraint="gpu"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=18
#SBATCH --gres=gpu:a100:4
#SBATCH --time=00:30:00

# For pinning threads correctly:
export OMP_PLACES=cores
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

module purge
module load gcc/10 cuda/11.2 openmpi_gpu/4 octopus-gpu/develop
srun octopus

# to run with nsight, uncomment the following
#module load nsight_systems/2021
#export TMPDIR=/ptmp/$USER/nvidia
#mkdir -p $TMPDIR
#nsys profile -t cuda,nvtx,mpi srun octopus
</pre>
<p>To run only on one GPU, you can use:
</p>
<pre>#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./log.%j
#SBATCH -D ./
#SBATCH -J octopus
#SBATCH --constraint="gpu"
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=18
#SBATCH --gres=gpu:a100:1
#SBATCH --mem=125000
#SBATCH --time=00:30:00

# For pinning threads correctly:
export OMP_PLACES=cores
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

module purge
module load gcc/10 cuda/11.2 openmpi_gpu/4 octopus-gpu/develop
srun octopus

# to run with nsight, uncomment the following
#module load nsight_systems/2021
#export TMPDIR=/ptmp/$USER/nvidia
#mkdir -p $TMPDIR
#nsys profile -t cuda,nvtx,mpi srun octopus
</pre>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Profiling" title="Tutorial:Profiling">Profiling</a> - 
</p><p>Back to <a href="/wiki/Tutorial_Series:Running_Octopus_on_HPC_systems" title="Tutorial Series:Running Octopus on HPC systems">Running Octopus on HPC systems</a>
</p>
<span class="noprint"><hr />
<p>Back to <a href="/wiki/Tutorials" title="Tutorials">Tutorials</a>
</p></span></span>
<!-- 
NewPP limit report
Cached time: 20230412155132
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.127 seconds
Real time usage: 0.168 seconds
Preprocessor visited node count: 515/1000000
Preprocessor generated node count: 1107/1000000
Post‐expand include size: 3453/2097152 bytes
Template argument size: 1385/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 2764/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   79.816      1 -total
 55.93%   44.637     16 Template:Variable
 31.61%   25.232      1 Template:Tutorial_foot
 30.79%   24.573      4 Template:If
 26.58%   21.218     16 Template:Octopus_version
 21.46%   17.129      5 Template:P2
 11.37%    9.076      3 Template:P1
  9.39%    7.491     16 Template:Code
  5.69%    4.544     16 Template:Octopus_major_version
  5.52%    4.406     16 Template:Octopus_minor_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:3077-0!canonical and timestamp 20230412155132 and revision id 11485
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;oldid=11485">http:///mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;oldid=11485</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Expert" title="Category:Expert">Expert</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AProfiling+nsight&amp;returntoquery=oldid%3D11485" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Profiling_nsight" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Profiling_nsight&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Profiling_nsight">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Profiling_nsight" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Profiling_nsight" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;oldid=11485&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;oldid=11485" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Profiling_nsight&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 22 September 2021, at 09:01.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.127","walltime":"0.168","ppvisitednodes":{"value":515,"limit":1000000},"ppgeneratednodes":{"value":1107,"limit":1000000},"postexpandincludesize":{"value":3453,"limit":2097152},"templateargumentsize":{"value":1385,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":2764,"limit":5000000},"timingprofile":["100.00%   79.816      1 -total"," 55.93%   44.637     16 Template:Variable"," 31.61%   25.232      1 Template:Tutorial_foot"," 30.79%   24.573      4 Template:If"," 26.58%   21.218     16 Template:Octopus_version"," 21.46%   17.129      5 Template:P2"," 11.37%    9.076      3 Template:P1","  9.39%    7.491     16 Template:Code","  5.69%    4.544     16 Template:Octopus_major_version","  5.52%    4.406     16 Template:Octopus_minor_version"]},"cachereport":{"timestamp":"20230412155132","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":132});});</script>
</body>
</html>
