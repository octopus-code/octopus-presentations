<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:Building from scratch - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Building_from_scratch","wgTitle":"Manual:Building from scratch","wgCurRevisionId":8456,"wgRevisionId":8456,"wgArticleId":1988,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Building_from_scratch","wgRelevantArticleId":1988,"wgRequestId":"1fb63e10b7d44aa29de652db","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Building_from_scratch rootpage-Manual_Building_from_scratch skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:Building from scratch</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 15:35, 7 September 2016 by <a href="/wiki/User:Micael" class="mw-userlink" title="User:Micael"><bdi>Micael</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Micael&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Micael (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Micael" class="mw-usertoollinks-contribs" title="Special:Contributions/Micael">contribs</a>)</span> <span class="comment">(<span dir="auto"><span class="autocomment"><a href="#Compilation_of_Octopus">→‎Compilation of Octopus</a></span></span>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;diff=prev&amp;oldid=8456" title="Manual:Building from scratch">diff</a>) <a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;direction=prev&amp;oldid=8456" title="Manual:Building from scratch">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Compiling Octopus on some systems might be difficult, this is mainly because some libraries are required and they have to be all compiled with same Fortran compiler. This is a guide on how to compile Octopus and the required libraries starting from scratch (a Fortran compiler is required) in a standard Unix system.
</p><p><b>NOTE: This page should be considered a last resort. On most systems you will be able to install many of these dependencies from a package manager or other pre-built binaries, which will save you a lot of effort.</b>
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Prerequisites"><span class="tocnumber">1</span> <span class="toctext">Prerequisites</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Compiler_flags"><span class="tocnumber">2</span> <span class="toctext">Compiler flags</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Compilation_of_libraries"><span class="tocnumber">3</span> <span class="toctext">Compilation of libraries</span></a>
<ul>
<li class="toclevel-2 tocsection-4"><a href="#BLAS"><span class="tocnumber">3.1</span> <span class="toctext">BLAS</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#LAPACK"><span class="tocnumber">3.2</span> <span class="toctext">LAPACK</span></a></li>
<li class="toclevel-2 tocsection-6"><a href="#GSL"><span class="tocnumber">3.3</span> <span class="toctext">GSL</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#FFTW_3"><span class="tocnumber">3.4</span> <span class="toctext">FFTW 3</span></a></li>
<li class="toclevel-2 tocsection-8"><a href="#LibXC"><span class="tocnumber">3.5</span> <span class="toctext">LibXC</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-9"><a href="#Compilation_of_Octopus"><span class="tocnumber">4</span> <span class="toctext">Compilation of Octopus</span></a></li>
</ul>
</div>

<h3><span class="mw-headline" id="Prerequisites">Prerequisites</span></h3>
<p>You will need the following:
</p>
<ul><li>A standard Unix/Linux system, where you want to install Octopus.</li>
<li>A basic knowledge on how to use the system (manage files, edit text files, extract tar archives, etc.).</li>
<li>A directory where we will install the created binaries, this can be a specially created directory or even your <code>$HOME</code>. From now, we will assume that the directory is <tt><b>&lt;basedir&gt;</b></tt>, whenever it appears you should replace it with the actual directory you are using. <i>Note that the files that you will download and the directories with the sources, including the ones of Octopus, don't need to be placed in this directory.</i></li>
<li>A working C compiler, gcc is the ideal choice for x86/x86_64 systems. For other platforms you might want to choose a compiler that produces more optimized code. We will assume that the command to run the compiler is <tt><b>&lt;cc&gt;</b></tt>.</li>
<li>A working Fortran 95 compiler, if you don't have one you can probably get <a rel="nofollow" class="external text" href="http://gcc.gnu.org/fortran/">gfortran</a> or <a rel="nofollow" class="external text" href="http://www.g95.org/">g95</a>. We will assume that the command to run the Fortran compiler is <tt><b>&lt;fc&gt;</b></tt>.</li>
<li>If you are compiling for a dual 32/64 bits architecture, it is a good idea to tell the compilers explicitly what type of binaries you want, many problems are caused by unadvertedly mixing 32 and 64 bits code. For Octopus normally you will want a 64 bits binary (<code>-m64</code> flag in most compilers). Since this flag should be passed always we recommend you to include it in the compiler command used in this guide (sometimes optimization flags are left out), so for example <tt><b>&lt;cc&gt;</b></tt> could be <tt><i>gcc -m64</i></tt>.</li></ul>
<h3><span class="mw-headline" id="Compiler_flags">Compiler flags</span></h3>
<p>Since probably you want Octopus to run fast, you will probably would to set optimization flags for both the C and Fortran compilers on your system (at least -O3). In general Octopus should run fine with aggressive optimization options. We will assume now that you have found a set of flags for <tt><b>&lt;cc&gt;</b></tt> and <tt><b>&lt;fc&gt;</b></tt>, we will call them <tt><b>&lt;cflags&gt;</b></tt> and <tt><b>&lt;fcflags&gt;</b></tt>. For example <tt><b>&lt;cflags&gt;</b></tt> could be <tt><i>-O3 -march=native</i></tt>.
</p><p><i>Note: if <tt><b>&lt;cc&gt;</b></tt>, <tt><b>&lt;fc&gt;</b></tt>, <tt><b>&lt;cflags&gt;</b></tt> or <tt><b>&lt;fcflags&gt;</b></tt> contain spaces you should not enclose them in "". We will put the "" explicitly when it is necessary.</i>
</p>
<h3><span class="mw-headline" id="Compilation_of_libraries">Compilation of libraries</span></h3>
<p>First we will compile and install the libraries required by Octopus.
</p>
<h4><span class="mw-headline" id="BLAS">BLAS</span></h4>
<p>The first library we will compile is blas. We will use the generic reference implementation, this version is not highly optimized but it is free software and simple to install. So after you have a working version of Octopus you may want to use a more optimized blas implementation as <a rel="nofollow" class="external text" href="http://www.tacc.utexas.edu/resources/software/#blas">libgoto</a>, <a rel="nofollow" class="external text" href="http://math-atlas.sourceforge.net">ATLAS</a>, MKL, ACML, ESSL, etc.
</p>
<ul><li>Download the package from the netlib site: <a rel="nofollow" class="external free" href="http://www.netlib.org/blas/blas.tgz">http://www.netlib.org/blas/blas.tgz</a></li>
<li>Extract the package and enter into the newly created <b><tt>BLAS</tt></b> directory.</li>
<li>Edit the <b><tt>make.inc</tt></b> file and modify the definition of the fortran compiler that now should be:</li></ul>
<pre>FORTRAN  = <tt><b>&lt;fc&gt;</b></tt>
OPTS     = <tt><b>&lt;fcflags&gt;</b></tt>
DRVOPTS  = $(OPTS)
NOOPT    =
LOADER   = <tt><b>&lt;fc&gt;</b></tt>
LOADOPTS =
</pre>
<ul><li>Now type <tt><i>make</i></tt> to compile, this will take a while.</li>
<li>One compilation is finished, create the directory <tt><b>&lt;basedir&gt;</b></tt><b><tt>/lib</tt></b></li></ul>
<pre>mkdir <tt><b>&lt;basedir&gt;</b></tt>/lib
</pre>
<p>and copy the file <b><tt>blas_LINUX.a</tt></b> to <tt><b>&lt;basedir&gt;</b></tt><b><tt>/lib/libblas.a</tt></b>
</p>
<pre>cp blas_LINUX.a <tt><b>&lt;basedir&gt;</b></tt>/lib/libblas.a
</pre>
<p><i>Note: the generated file librrary will be always called <b><tt>blas_LINUX.a</tt></b> independently of the operating system you are using, this is just a name.</i>
</p>
<h4><span class="mw-headline" id="LAPACK">LAPACK</span></h4>
<p>We will use the open source reference implementation of Lapack.
</p>
<ul><li>Get the Lapak package from netlib: <a rel="nofollow" class="external free" href="http://www.netlib.org/lapack/lapack.tgz">http://www.netlib.org/lapack/lapack.tgz</a></li>
<li>Extract the archive and enter the newly created lapack directory.</li>
<li>Copy <b><tt>make.inc.example</tt></b> to <b><tt>make.inc</tt></b>:</li></ul>
<pre>cp make.inc.example make.inc
</pre>
<ul><li>Edit <b><tt>make.inc</tt></b> to indicate the compiler the will be used. The relevant part of the file should look like:</li></ul>
<pre>FORTRAN  = <tt><b>&lt;fc&gt;</b></tt>
OPTS     = <tt><b>&lt;fcflags&gt;</b></tt>
DRVOPTS  = $(OPTS)
NOOPT    =
LOADER   = <tt><b>&lt;fc&gt;</b></tt>
LOADOPTS =
</pre>
<ul><li>Now build the library with</li></ul>
<pre>make lib
</pre>
<ul><li>Copy the newly created library <b><tt>lapack_LINUX.a</tt></b> to <b><tt>&lt;basedir&gt;/lib/liblapack.a</tt></b> .</li></ul>
<h4><span class="mw-headline" id="GSL">GSL</span></h4>
<ul><li>Get the source of the latest version of GSL from <a rel="nofollow" class="external free" href="ftp://ftp.gnu.org/gnu/gsl/">ftp://ftp.gnu.org/gnu/gsl/</a> , currently it is GSL 1.16: <a rel="nofollow" class="external free" href="ftp://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz">ftp://ftp.gnu.org/gnu/gsl/gsl-1.16.tar.gz</a> .</li>
<li>Extract the archive and enter the newly created directory.</li>
<li>Run the configure script with:</li></ul>
<pre>./configure CC="<tt><b>&lt;cc&gt;</b></tt>" --prefix=<tt><b>&lt;basedir&gt;</b></tt> --disable-shared --enable-static
</pre>
<ul><li>Compile and install:</li></ul>
<pre>make
make install
</pre>
<h4><span class="mw-headline" id="FFTW_3">FFTW 3</span></h4>
<ul><li>Get the sources for the latest version of <a rel="nofollow" class="external text" href="http://www.fftw.org/">FFTW</a>, currently <a rel="nofollow" class="external free" href="http://www.fftw.org/fftw-3.3.4.tar.gz">http://www.fftw.org/fftw-3.3.4.tar.gz</a> .</li>
<li>Extract the archive and enter the newly created directory.</li>
<li>Configure:</li></ul>
<pre>./configure  --prefix=<tt><b>&lt;basedir&gt;</b></tt> CC="<tt><b>&lt;cc&gt;</b></tt>" CFLAGS="<tt><b>&lt;cflags&gt;</b></tt>" F77="<tt><b>&lt;fc&gt;</b></tt>" F77FLAGS="<tt><b>&lt;fcflags&gt;</b></tt>"
</pre>
<ul><li>Compile and install:</li></ul>
<pre>make
make install
</pre>
<h4><span class="mw-headline" id="LibXC">LibXC</span></h4>
<ul><li>Get Libxc, currently <a rel="nofollow" class="external free" href="http://www.tddft.org/programs/octopus/down.php?file=libxc/libxc-2.2.0.tar.gz">http://www.tddft.org/programs/octopus/down.php?file=libxc/libxc-2.2.0.tar.gz</a> .</li>
<li>Extract the archive and enter the newly created directory.</li></ul>
<pre>tar -xvzf libxc-2.2.0.tar.gz
cd libxc-2.2.0
</pre>
<ul><li>Configure:</li></ul>
<pre>./configure --prefix=<tt><b>&lt;basedir&gt;</b></tt> CC="<tt><b>&lt;cc&gt;</b></tt>" CFLAGS="<tt><b>&lt;cflags&gt;</b></tt>" FC="<tt><b>&lt;fc&gt;</b></tt>" FCFLAGS="<tt><b>&lt;fcflags&gt;</b></tt>"
</pre>
<ul><li>Compile and install:</li></ul>
<pre>make
make install
</pre>
<h3><span class="mw-headline" id="Compilation_of_Octopus">Compilation of Octopus</span></h3>
<p>After compiling the libraries, now we are ready to compile Octopus. These are the steps you have to follow:
</p>
<ul><li>Download the last version of Octopus: <a rel="nofollow" class="external free" href="http://www.tddft.org/programs/octopus/down.php?file=12.0/octopus-12.0.tar.gz">http://www.tddft.org/programs/octopus/down.php?file=12.0/octopus-12.0.tar.gz</a></li>
<li>Extract the file and enter the newly created directory.</li>
<li>Define the following environment variables to be used by the configure script (we assume that you are using bash, if you are using another shell, the commands should be modified accordingly):</li></ul>
<pre>export LIBS_BLAS=<tt><b>&lt;basedir&gt;</b></tt>/lib/libblas.a
export LIBS_LAPACK=<tt><b>&lt;basedir&gt;</b></tt>/lib/liblapack.a
export LIBS_FFT=<tt><b>&lt;basedir&gt;</b></tt>/lib/libfftw3.a
</pre>
<ul><li>Now call the configure script:</li></ul>
<pre>./configure CC="<tt><b>&lt;cc&gt;</b></tt>" CFLAGS="<tt><b>&lt;cflags&gt;</b></tt>" FC="<tt><b>&lt;fc&gt;</b></tt>" FCFLAGS="<tt><b>&lt;fcflags&gt;</b></tt>" --prefix=<tt><b>&lt;basedir&gt;</b></tt> --with-gsl-prefix=<tt><b>&lt;basedir&gt;</b></tt> --with-libxc-prefix=<tt><b>&lt;basedir&gt;</b></tt>
</pre>
<ul><li>Compile and install</li></ul>
<pre>make
make install
</pre>
<ul><li>If everything went fine, you should have Octopus installed in the <tt><b>&lt;basedir&gt;</b></tt> directory, this means that the executables are in <tt><b>&lt;basedir&gt;</b></tt><b><tt>/bin/</tt></b>. You may want to add this last directory to your path to run octopus commands directly, otherwise you will have to give the full path.</li>
<li>To test the compilation is correct, you can run the testsuite of Octopus, that compares the results obtained with the created version against reference values. To do it, run</li></ul>
<pre>make check
</pre>
<p>All tests should be passed if the compilation is correct. If all of them fail, there is probably a problem with your executable, typically missing dynamic libraries. If
just some fail, there might be a more serious problem, we recommend you to look for help in the <a href="/wiki/Mailing_lists" title="Mailing lists">Mailing lists</a>.
</p><p><br />
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:Updating_to_a_new_version" title="Manual:Updating to a new version">Manual:Updating to a new version</a> - Next <a href="/wiki/Tutorial:Running_Octopus_on_Graphical_Processing_Units_(GPUs)" title="Tutorial:Running Octopus on Graphical Processing Units (GPUs)">Tutorial:Running Octopus on Graphical Processing Units (GPUs)</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412153856
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.146 seconds
Real time usage: 0.190 seconds
Preprocessor visited node count: 415/1000000
Preprocessor generated node count: 1271/1000000
Post‐expand include size: 2050/2097152 bytes
Template argument size: 447/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 17/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  121.092      1 -total
 21.32%   25.817      3 Template:Libxc_version
 11.28%   13.654      2 Template:Octopus_version
  6.86%    8.305      1 Template:Manual_foot
  5.19%    6.283     14 Template:Name
  5.10%    6.178     12 Template:File
  3.82%    4.620      1 Template:Libxc
  3.58%    4.340      3 Template:Command
  3.56%    4.311      1 Template:Octopus_major_version
  3.51%    4.246      1 Template:Foot
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1988-0!canonical and timestamp 20230412153855 and revision id 8456
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:Building_from_scratch&amp;oldid=8456">http:///mediawiki/index.php?title=Manual:Building_from_scratch&amp;oldid=8456</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3ABuilding+from+scratch&amp;returntoquery=oldid%3D8456" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Building_from_scratch" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:Building_from_scratch&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:Building_from_scratch">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Building_from_scratch" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Building_from_scratch" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;oldid=8456&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;oldid=8456" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 7 September 2016, at 15:35.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.146","walltime":"0.190","ppvisitednodes":{"value":415,"limit":1000000},"ppgeneratednodes":{"value":1271,"limit":1000000},"postexpandincludesize":{"value":2050,"limit":2097152},"templateargumentsize":{"value":447,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":17,"limit":5000000},"timingprofile":["100.00%  121.092      1 -total"," 21.32%   25.817      3 Template:Libxc_version"," 11.28%   13.654      2 Template:Octopus_version","  6.86%    8.305      1 Template:Manual_foot","  5.19%    6.283     14 Template:Name","  5.10%    6.178     12 Template:File","  3.82%    4.620      1 Template:Libxc","  3.58%    4.340      3 Template:Command","  3.56%    4.311      1 Template:Octopus_major_version","  3.51%    4.246      1 Template:Foot"]},"cachereport":{"timestamp":"20230412153856","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":120});});</script>
</body>
</html>
