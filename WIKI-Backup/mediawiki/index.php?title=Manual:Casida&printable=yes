<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:Casida - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Casida","wgTitle":"Manual:Casida","wgCurRevisionId":8128,"wgRevisionId":8128,"wgArticleId":1810,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Casida","wgRelevantArticleId":1810,"wgRequestId":"cb4daf9973945aa5f1f3b480","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Casida rootpage-Manual_Casida skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:Casida</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Mark Casida's formulation of Linear-Response TDDFT allows calculations of the excitation energies of a finite system.  For small molecules, this is normally the fastest way to calculate them.
</p><p>To perform a Casida calculation you first need a <a href="/wiki/Manual:Ground_State" title="Manual:Ground State">ground-state</a> calculation and a calculation of unoccupied states; then you run the code with <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span><code>=casida</code>.
</p><p>This is an input file for a linear-response calculation of the nitrogen dimer, found at <b><tt>SHARE/testsuite/linear_response/01-casida.*</tt></b>.
</p>
<pre>                                                                                
%CalculationMode
gs | unocc | casida
"ground_state_" |  "unocc_states_" | "lrtddft_"
1 | 2 | 3
%
                                                                                
FromScratch = yes
                                                                                
bond_length = 2.0744
                                                                                
%Coordinates
"N" |  -bond_length/2 |  0.0 |  0.0 | no
"N" |   bond_length/2 |  0.0 |  0.0 | no
%
                                                                                
%Species
"N" | 14.0067000 | tm2 | 7 | 2 | 0
%
                                                                                
BoxShape = sphere
                                                                                
Radius = 12.0
Spacing = 0.36
                                                                                
SpinComponents = unpolarized
                                                                                
XFunctional = lda_x
CFunctional = lda_c_vwn
                                                                                
MaximumIter = 200
ConvRelDens = 1e-5
                                                                                
LCAOStart = lcao_full
LCAODimension = 18
                                                                                  
EigenSolver = cg_new
EigenSolverInitTolerance = 1e-2
EigenSolverFinalTolerance = 1e-5
EigenSolverFinalToleranceIteration = 6
EigenSolverMaxIter = 20
unocc_states_EigenSolverMaxIter = 1000
                                                                                
TypeOfMixing = broyden
                                                                                
NumberUnoccStates = 9
                                                                                
PoissonSolver = fft_corrected
</pre>
<p><br />
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:Time-Dependent" title="Manual:Time-Dependent">Manual:Time-Dependent</a> - Next <a href="/wiki/Manual:Linear_Response" title="Manual:Linear Response">Manual:Linear_Response</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412154046
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.039 seconds
Real time usage: 0.050 seconds
Preprocessor visited node count: 55/1000000
Preprocessor generated node count: 258/1000000
Post‐expand include size: 565/2097152 bytes
Template argument size: 237/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 1969/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   35.957      1 -total
 52.83%   18.995      1 Template:Variable
 29.82%   10.722      1 Template:Octopus_version
 21.91%    7.878      1 Template:Manual_foot
 21.20%    7.622      2 Template:Code
 11.10%    3.991      1 Template:Foot
 10.18%    3.659      1 Template:File
  9.95%    3.577      1 Template:Eq
  9.23%    3.320      1 Template:Octopus_minor_version
  9.19%    3.304      1 Template:Octopus_major_version
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1810-0!canonical and timestamp 20230412154045 and revision id 8128
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:Casida&amp;oldid=8128">http:///mediawiki/index.php?title=Manual:Casida&amp;oldid=8128</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3ACasida&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Casida" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk"><span><a href="/wiki/Talk:Manual:Casida" rel="discussion" title="Discussion about the content page [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:Casida">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Casida&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Casida&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Casida" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Casida" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:Casida&amp;oldid=8128" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Casida&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 14 January 2016, at 03:31.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.039","walltime":"0.050","ppvisitednodes":{"value":55,"limit":1000000},"ppgeneratednodes":{"value":258,"limit":1000000},"postexpandincludesize":{"value":565,"limit":2097152},"templateargumentsize":{"value":237,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":1969,"limit":5000000},"timingprofile":["100.00%   35.957      1 -total"," 52.83%   18.995      1 Template:Variable"," 29.82%   10.722      1 Template:Octopus_version"," 21.91%    7.878      1 Template:Manual_foot"," 21.20%    7.622      2 Template:Code"," 11.10%    3.991      1 Template:Foot"," 10.18%    3.659      1 Template:File","  9.95%    3.577      1 Template:Eq","  9.23%    3.320      1 Template:Octopus_minor_version","  9.19%    3.304      1 Template:Octopus_major_version"]},"cachereport":{"timestamp":"20230412154046","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":102});});</script>
</body>
</html>
