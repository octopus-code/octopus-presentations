<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Information for "The Octopus Manual" - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"The_Octopus_Manual","wgTitle":"The Octopus Manual","wgCurRevisionId":7171,"wgRevisionId":0,"wgArticleId":1686,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"info","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"The_Octopus_Manual","wgRelevantArticleId":1686,"wgRequestId":"acc58b2d18db8bad01726d82","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-The_Octopus_Manual rootpage-The_Octopus_Manual skin-vector action-info">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Information for "The Octopus Manual"</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><style>.mw-hiddenCategoriesExplanation { display: none; }</style>
<style>.mw-templatesUsedExplanation { display: none; }</style>
<h2 id="mw-pageinfo-header-basic"><span class="mw-headline" id="Basic_information">Basic information</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-pageinfo-display-title"><td style="vertical-align: top;">Display title</td><td>The Octopus Manual</td></tr>
<tr id="mw-pageinfo-default-sort"><td style="vertical-align: top;">Default sort key</td><td>The Octopus Manual</td></tr>
<tr id="mw-pageinfo-length"><td style="vertical-align: top;">Page length (in bytes)</td><td>367</td></tr>
<tr id="mw-pageinfo-article-id"><td style="vertical-align: top;">Page ID</td><td>1686</td></tr>
<tr><td style="vertical-align: top;">Page content language</td><td>en - English</td></tr>
<tr id="mw-pageinfo-content-model"><td style="vertical-align: top;">Page content model</td><td>wikitext</td></tr>
<tr id="mw-pageinfo-robot-policy"><td style="vertical-align: top;">Indexing by robots</td><td>Allowed</td></tr>
<tr><td style="vertical-align: top;"><a href="/mediawiki/index.php?title=Special:WhatLinksHere/The_Octopus_Manual&amp;hidelinks=1&amp;hidetrans=1" title="Special:WhatLinksHere/The Octopus Manual">Number of redirects to this page</a></td><td>0</td></tr>
<tr id="mw-pageinfo-contentpage"><td style="vertical-align: top;">Counted as a content page</td><td>Yes</td></tr>
</table>
<h2 id="mw-pageinfo-header-restrictions"><span class="mw-headline" id="Page_protection">Page protection</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-restriction-edit"><td style="vertical-align: top;">Edit</td><td>Allow all users (infinite)</td></tr>
<tr id="mw-restriction-move"><td style="vertical-align: top;">Move</td><td>Allow all users (infinite)</td></tr>
</table>
<a href="/mediawiki/index.php?title=Special:Log&amp;type=protect&amp;page=The+Octopus+Manual" title="Special:Log">View the protection log for this page.</a>
<h2 id="mw-pageinfo-header-edits"><span class="mw-headline" id="Edit_history">Edit history</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-pageinfo-firstuser"><td style="vertical-align: top;">Page creator</td><td><a href="/wiki/User:Xavier" class="mw-userlink" title="User:Xavier"><bdi>Xavier</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Xavier&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Xavier (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Xavier" class="mw-usertoollinks-contribs" title="Special:Contributions/Xavier">contribs</a>)</span></td></tr>
<tr id="mw-pageinfo-firsttime"><td style="vertical-align: top;">Date of page creation</td><td><a href="/mediawiki/index.php?title=The_Octopus_Manual&amp;oldid=3255" title="The Octopus Manual">00:44, 8 July 2006</a></td></tr>
<tr id="mw-pageinfo-lastuser"><td style="vertical-align: top;">Latest editor</td><td><a href="/wiki/User:Dstrubbe" class="mw-userlink" title="User:Dstrubbe"><bdi>Dstrubbe</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Dstrubbe&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Dstrubbe (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Dstrubbe" class="mw-usertoollinks-contribs" title="Special:Contributions/Dstrubbe">contribs</a>)</span></td></tr>
<tr id="mw-pageinfo-lasttime"><td style="vertical-align: top;">Date of latest edit</td><td><a href="/mediawiki/index.php?title=The_Octopus_Manual&amp;oldid=7171" title="The Octopus Manual">23:43, 3 April 2014</a></td></tr>
<tr id="mw-pageinfo-edits"><td style="vertical-align: top;">Total number of edits</td><td>37</td></tr>
<tr id="mw-pageinfo-authors"><td style="vertical-align: top;">Total number of distinct authors</td><td>4</td></tr>
<tr id="mw-pageinfo-recent-edits"><td style="vertical-align: top;">Recent number of edits (within past 90 days)</td><td>0</td></tr>
<tr id="mw-pageinfo-recent-authors"><td style="vertical-align: top;">Recent number of distinct authors</td><td>0</td></tr>
</table>
<h2 id="mw-pageinfo-header-properties"><span class="mw-headline" id="Page_properties">Page properties</span></h2>
<table class="wikitable mw-page-info">
<tr id="mw-pageinfo-templates"><td style="vertical-align: top;">Transcluded templates (91)</td><td><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Manual" title="Manual">Manual</a> (<a href="/mediawiki/index.php?title=Manual&amp;action=edit" title="Manual">view source</a>) </li><li><a href="/wiki/Manual:About_Octopus" title="Manual:About Octopus">Manual:About Octopus</a> (<a href="/mediawiki/index.php?title=Manual:About_Octopus&amp;action=edit" title="Manual:About Octopus">view source</a>) </li><li><a href="/wiki/Manual:About_this_manual" title="Manual:About this manual">Manual:About this manual</a> (<a href="/mediawiki/index.php?title=Manual:About_this_manual&amp;action=edit" title="Manual:About this manual">view source</a>) </li><li><a href="/wiki/Manual:Advanced_ways_of_running_Octopus" title="Manual:Advanced ways of running Octopus">Manual:Advanced ways of running Octopus</a> (<a href="/mediawiki/index.php?title=Manual:Advanced_ways_of_running_Octopus&amp;action=edit" title="Manual:Advanced ways of running Octopus">view source</a>) </li><li><a href="/wiki/Manual:Appendix:Copying" title="Manual:Appendix:Copying">Manual:Appendix:Copying</a> (<a href="/mediawiki/index.php?title=Manual:Appendix:Copying&amp;action=edit" title="Manual:Appendix:Copying">view source</a>) </li><li><a href="/wiki/Manual:Appendix:Porting_Octopus_and_Platform_Specific_Instructions" title="Manual:Appendix:Porting Octopus and Platform Specific Instructions">Manual:Appendix:Porting Octopus and Platform Specific Instructions</a> (<a href="/mediawiki/index.php?title=Manual:Appendix:Porting_Octopus_and_Platform_Specific_Instructions&amp;action=edit" title="Manual:Appendix:Porting Octopus and Platform Specific Instructions">view source</a>) </li><li><a href="/wiki/Manual:Appendix:Reference_Manual" title="Manual:Appendix:Reference Manual">Manual:Appendix:Reference Manual</a> (<a href="/mediawiki/index.php?title=Manual:Appendix:Reference_Manual&amp;action=edit" title="Manual:Appendix:Reference Manual">view source</a>) </li><li><a href="/wiki/Manual:Building_from_scratch" title="Manual:Building from scratch">Manual:Building from scratch</a> (<a href="/mediawiki/index.php?title=Manual:Building_from_scratch&amp;action=edit" title="Manual:Building from scratch">view source</a>) </li><li><a href="/wiki/Manual:Casida" title="Manual:Casida">Manual:Casida</a> (<a href="/mediawiki/index.php?title=Manual:Casida&amp;action=edit" title="Manual:Casida">view source</a>) </li><li><a href="/wiki/Manual:Deprecated_Utilities" title="Manual:Deprecated Utilities">Manual:Deprecated Utilities</a> (<a href="/mediawiki/index.php?title=Manual:Deprecated_Utilities&amp;action=edit" title="Manual:Deprecated Utilities">view source</a>) </li><li><a href="/wiki/Manual:Discretization" title="Manual:Discretization">Manual:Discretization</a> (<a href="/mediawiki/index.php?title=Manual:Discretization&amp;action=edit" title="Manual:Discretization">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:Examples:Benzene&amp;action=edit&amp;redlink=1" class="new" title="Manual:Examples:Benzene (page does not exist)">Manual:Examples:Benzene</a> (<a href="/mediawiki/index.php?title=Manual:Examples:Benzene&amp;action=edit" class="new" title="Manual:Examples:Benzene (page does not exist)">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:Examples:Hello_world&amp;action=edit&amp;redlink=1" class="new" title="Manual:Examples:Hello world (page does not exist)">Manual:Examples:Hello world</a> (<a href="/mediawiki/index.php?title=Manual:Examples:Hello_world&amp;action=edit" class="new" title="Manual:Examples:Hello world (page does not exist)">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-analyze_projections&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-analyze projections (page does not exist)">Manual:External utilities:oct-analyze projections</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-analyze_projections&amp;action=edit" class="new" title="Manual:External utilities:oct-analyze projections (page does not exist)">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-atomic_occupations" title="Manual:External utilities:oct-atomic occupations">Manual:External utilities:oct-atomic occupations</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-atomic_occupations&amp;action=edit" title="Manual:External utilities:oct-atomic occupations">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-casida_spectrum" title="Manual:External utilities:oct-casida spectrum">Manual:External utilities:oct-casida spectrum</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-casida_spectrum&amp;action=edit" title="Manual:External utilities:oct-casida spectrum">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-center-geom" title="Manual:External utilities:oct-center-geom">Manual:External utilities:oct-center-geom</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-center-geom&amp;action=edit" title="Manual:External utilities:oct-center-geom">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-check_deallocs&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-check deallocs (page does not exist)">Manual:External utilities:oct-check deallocs</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-check_deallocs&amp;action=edit" class="new" title="Manual:External utilities:oct-check deallocs (page does not exist)">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-conductivity&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-conductivity (page does not exist)">Manual:External utilities:oct-conductivity</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-conductivity&amp;action=edit" class="new" title="Manual:External utilities:oct-conductivity (page does not exist)">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-convert" title="Manual:External utilities:oct-convert">Manual:External utilities:oct-convert</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-convert&amp;action=edit" title="Manual:External utilities:oct-convert">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-dielectric-function&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-dielectric-function (page does not exist)">Manual:External utilities:oct-dielectric-function</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-dielectric-function&amp;action=edit" class="new" title="Manual:External utilities:oct-dielectric-function (page does not exist)">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-display_partitions" title="Manual:External utilities:oct-display partitions">Manual:External utilities:oct-display partitions</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-display_partitions&amp;action=edit" title="Manual:External utilities:oct-display partitions">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-harmonic-spectrum" title="Manual:External utilities:oct-harmonic-spectrum">Manual:External utilities:oct-harmonic-spectrum</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-harmonic-spectrum&amp;action=edit" title="Manual:External utilities:oct-harmonic-spectrum">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-help" title="Manual:External utilities:oct-help">Manual:External utilities:oct-help</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-help&amp;action=edit" title="Manual:External utilities:oct-help">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-infrared_spectrum&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-infrared spectrum (page does not exist)">Manual:External utilities:oct-infrared spectrum</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-infrared_spectrum&amp;action=edit" class="new" title="Manual:External utilities:oct-infrared spectrum (page does not exist)">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-local_multipoles&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-local multipoles (page does not exist)">Manual:External utilities:oct-local multipoles</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-local_multipoles&amp;action=edit" class="new" title="Manual:External utilities:oct-local multipoles (page does not exist)">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-oscillator-strength&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-oscillator-strength (page does not exist)">Manual:External utilities:oct-oscillator-strength</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-oscillator-strength&amp;action=edit" class="new" title="Manual:External utilities:oct-oscillator-strength (page does not exist)">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-photoelectron_spectrum&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-photoelectron spectrum (page does not exist)">Manual:External utilities:oct-photoelectron spectrum</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-photoelectron_spectrum&amp;action=edit" class="new" title="Manual:External utilities:oct-photoelectron spectrum (page does not exist)">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-propagation_spectrum" title="Manual:External utilities:oct-propagation spectrum">Manual:External utilities:oct-propagation spectrum</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-propagation_spectrum&amp;action=edit" title="Manual:External utilities:oct-propagation spectrum">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-run_periodic_table" title="Manual:External utilities:oct-run periodic table">Manual:External utilities:oct-run periodic table</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_periodic_table&amp;action=edit" title="Manual:External utilities:oct-run periodic table">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-run_regression_test.pl" title="Manual:External utilities:oct-run regression test.pl">Manual:External utilities:oct-run regression test.pl</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_regression_test.pl&amp;action=edit" title="Manual:External utilities:oct-run regression test.pl">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-run_testsuite.sh" title="Manual:External utilities:oct-run testsuite.sh">Manual:External utilities:oct-run testsuite.sh</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-run_testsuite.sh&amp;action=edit" title="Manual:External utilities:oct-run testsuite.sh">view source</a>) </li><li><a href="/mediawiki/index.php?title=Manual:External_utilities:oct-vdW_c6&amp;action=edit&amp;redlink=1" class="new" title="Manual:External utilities:oct-vdW c6 (page does not exist)">Manual:External utilities:oct-vdW c6</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-vdW_c6&amp;action=edit" class="new" title="Manual:External utilities:oct-vdW c6 (page does not exist)">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-vibrational_spectrum" title="Manual:External utilities:oct-vibrational spectrum">Manual:External utilities:oct-vibrational spectrum</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-vibrational_spectrum&amp;action=edit" title="Manual:External utilities:oct-vibrational spectrum">view source</a>) </li><li><a href="/wiki/Manual:External_utilities:oct-xyz-anim" title="Manual:External utilities:oct-xyz-anim">Manual:External utilities:oct-xyz-anim</a> (<a href="/mediawiki/index.php?title=Manual:External_utilities:oct-xyz-anim&amp;action=edit" title="Manual:External utilities:oct-xyz-anim">view source</a>) </li><li><a href="/wiki/Manual:Geometry_Optimization" title="Manual:Geometry Optimization">Manual:Geometry Optimization</a> (<a href="/mediawiki/index.php?title=Manual:Geometry_Optimization&amp;action=edit" title="Manual:Geometry Optimization">view source</a>) </li><li><a href="/wiki/Manual:Ground_State" title="Manual:Ground State">Manual:Ground State</a> (<a href="/mediawiki/index.php?title=Manual:Ground_State&amp;action=edit" title="Manual:Ground State">view source</a>) </li><li><a href="/wiki/Manual:Hamiltonian" title="Manual:Hamiltonian">Manual:Hamiltonian</a> (<a href="/mediawiki/index.php?title=Manual:Hamiltonian&amp;action=edit" title="Manual:Hamiltonian">view source</a>) </li><li><a href="/wiki/Manual:Input_file" title="Manual:Input file">Manual:Input file</a> (<a href="/mediawiki/index.php?title=Manual:Input_file&amp;action=edit" title="Manual:Input file">view source</a>) </li><li><a href="/wiki/Manual:Installation" title="Manual:Installation">Manual:Installation</a> (<a href="/mediawiki/index.php?title=Manual:Installation&amp;action=edit" title="Manual:Installation">view source</a>) </li><li><a href="/wiki/Manual:Linear_Response" title="Manual:Linear Response">Manual:Linear Response</a> (<a href="/mediawiki/index.php?title=Manual:Linear_Response&amp;action=edit" title="Manual:Linear Response">view source</a>) </li><li><a href="/wiki/Manual:Optimal_Control" title="Manual:Optimal Control">Manual:Optimal Control</a> (<a href="/mediawiki/index.php?title=Manual:Optimal_Control&amp;action=edit" title="Manual:Optimal Control">view source</a>) </li><li><a href="/wiki/Manual:Output" title="Manual:Output">Manual:Output</a> (<a href="/mediawiki/index.php?title=Manual:Output&amp;action=edit" title="Manual:Output">view source</a>) </li><li><a href="/wiki/Manual:Physical_System" title="Manual:Physical System">Manual:Physical System</a> (<a href="/mediawiki/index.php?title=Manual:Physical_System&amp;action=edit" title="Manual:Physical System">view source</a>) </li><li><a href="/wiki/Manual:Running_Octopus" title="Manual:Running Octopus">Manual:Running Octopus</a> (<a href="/mediawiki/index.php?title=Manual:Running_Octopus&amp;action=edit" title="Manual:Running Octopus">view source</a>) </li><li><a href="/wiki/Manual:Specific_architectures" title="Manual:Specific architectures">Manual:Specific architectures</a> (<a href="/mediawiki/index.php?title=Manual:Specific_architectures&amp;action=edit" title="Manual:Specific architectures">view source</a>) </li><li><a href="/wiki/Manual:Symmetry" title="Manual:Symmetry">Manual:Symmetry</a> (<a href="/mediawiki/index.php?title=Manual:Symmetry&amp;action=edit" title="Manual:Symmetry">view source</a>) </li><li><a href="/wiki/Manual:Time-Dependent" title="Manual:Time-Dependent">Manual:Time-Dependent</a> (<a href="/mediawiki/index.php?title=Manual:Time-Dependent&amp;action=edit" title="Manual:Time-Dependent">view source</a>) </li><li><a href="/wiki/Manual:Troubleshooting" title="Manual:Troubleshooting">Manual:Troubleshooting</a> (<a href="/mediawiki/index.php?title=Manual:Troubleshooting&amp;action=edit" title="Manual:Troubleshooting">view source</a>) </li><li><a href="/wiki/Manual:Units" title="Manual:Units">Manual:Units</a> (<a href="/mediawiki/index.php?title=Manual:Units&amp;action=edit" title="Manual:Units">view source</a>) </li><li>This list may be incomplete.</li></ul></td></tr>
</table>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/The_Octopus_Manual">http:///wiki/The_Octopus_Manual</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=The+Octopus+Manual&amp;returntoquery=action%3Dinfo" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/The_Octopus_Manual" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:The_Octopus_Manual&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/The_Octopus_Manual">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=The_Octopus_Manual&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=The_Octopus_Manual&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/The_Octopus_Manual" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/The_Octopus_Manual" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=The_Octopus_Manual&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":249});});</script>
</body>
</html>
