<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:Scaling - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Scaling","wgTitle":"Tutorial:Scaling","wgCurRevisionId":11413,"wgRevisionId":11413,"wgArticleId":3071,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Scaling","wgRelevantArticleId":3071,"wgRequestId":"5159f6c1afaeaf6eb60d1b1e","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Scaling rootpage-Tutorial_Scaling skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:Scaling</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Octopus can be used on large clusters and supercomputers, but to do this
efficiently, it is important to understand how to judge the efficiency of its
parallelization scheme. To achieve this, we will conduct a
<b>scaling analysis</b> in this tutorial, for which we will run Octopus for a
certain system with different numbers of processes and compare the timings. This
kind of analysis is also called "strong scaling".
</p><p><br />
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Introduction"><span class="tocnumber">1</span> <span class="toctext">Introduction</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Scaling_analysis_for_domain_parallelization"><span class="tocnumber">2</span> <span class="toctext">Scaling analysis for domain parallelization</span></a>
<ul>
<li class="toclevel-2 tocsection-3"><a href="#Baseline:_serial_run"><span class="tocnumber">2.1</span> <span class="toctext">Baseline: serial run</span></a></li>
<li class="toclevel-2 tocsection-4"><a href="#Scaling_runs"><span class="tocnumber">2.2</span> <span class="toctext">Scaling runs</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Analysis"><span class="tocnumber">2.3</span> <span class="toctext">Analysis</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-6"><a href="#Scaling_analysis_for_states_parallelization"><span class="tocnumber">3</span> <span class="toctext">Scaling analysis for states parallelization</span></a></li>
<li class="toclevel-1 tocsection-7"><a href="#Combined_states_and_domain_parallelization"><span class="tocnumber">4</span> <span class="toctext">Combined states and domain parallelization</span></a></li>
<li class="toclevel-1 tocsection-8"><a href="#K-point_parallelization"><span class="tocnumber">5</span> <span class="toctext">K-point parallelization</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#Production_runs"><span class="tocnumber">6</span> <span class="toctext">Production runs</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#Memory_usage"><span class="tocnumber">7</span> <span class="toctext">Memory usage</span></a></li>
<li class="toclevel-1 tocsection-11"><a href="#Guidelines"><span class="tocnumber">8</span> <span class="toctext">Guidelines</span></a></li>
<li class="toclevel-1 tocsection-12"><a href="#Code_snippets"><span class="tocnumber">9</span> <span class="toctext">Code snippets</span></a></li>
</ul>
</div>

<h2><span class="mw-headline" id="Introduction">Introduction</span></h2>
<p>A code like octopus is parallelized to utilize more computing resources at the
same time, thus reducing the time to solution for calculations. Naively, one
could expect that using twice the number of CPU cores for a calculation should
cut the time needed by a factor of two. However, not all parts of a program are
typically running in parallel and also there is some overhead associated with
the parallelization scheme itself, such as communication between processes.
Hence, the time <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/247ad91284a62fe8ea7485ec968d276a4336910f" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:5.509ex; height:2.843ex;" alt="{\displaystyle T(N)}" /> for the execution of a certain computation on
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/f5e3890c981ae85503089652feb48b191b57aae3" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.064ex; height:2.176ex;" alt="{\displaystyle N}" /> processors will depend on the fraction of serial work
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/01d131dfd7673938b947072a13a9744fe997e632" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.09ex; height:1.676ex;" alt="{\displaystyle s}" /> because only the parallel fraction <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/4e15ff2771f19c0892a98d79616a2b2d2a18094f" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.505ex; width:5.093ex; height:2.343ex;" alt="{\displaystyle 1-s}" /> can be sped
up by a factor of <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/f5e3890c981ae85503089652feb48b191b57aae3" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:2.064ex; height:2.176ex;" alt="{\displaystyle N}" />:
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/ce73691d6c84fec6c904129229e68996329ae7b4" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -1.838ex; width:27.684ex; height:5.176ex;" alt="{\displaystyle T(N)=sT(1)+{\frac {1-s}{N}}T(1)}" />
</p><p>The <i>speed-up</i> <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/4611d85173cd3b508e67077d4a1252c9c05abca2" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:1.499ex; height:2.176ex;" alt="{\displaystyle S}" /> is defined as the ratio <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/c447c54ef9ffa20376e33dcb518d08f04425073e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:11.28ex; height:2.843ex;" alt="{\displaystyle T(1)/T(N)}" />.
Plugging in the formula from above yields
</p><p><img src="https://wikimedia.org/api/rest_v1/media/math/render/png/e01b9aa5fc81c0ebc4d9d2e1d1cbee9dfb4c5c2d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -3.505ex; width:34.197ex; height:7.343ex;" alt="{\displaystyle S={\frac {T(1)}{T(N)}}={\frac {1}{s+{\frac {1-s}{N}}}}\quad \xrightarrow {N\to \infty } {\frac {1}{s}}}" />
</p><p>This relation is called <i>Amdahl's law</i>. It means that the achievable speed-up
can never be larger than the inverse of the serial fraction. If the serial
fraction is 50%, the speed-up cannot be larger than 2; if the serial fraction is
10%, the speed-up cannot be larger than 10; if the serial fraction is 1%, the
speed-up cannot be larger than 100. As you can see, the serial fraction must be
very small to achieve large speed-ups: for achieving a speed-up of 10000, the
serial fraction must be less than <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/9588d1bcf3d738e9ed5c044770795e06f7e4eb28" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:4.658ex; height:2.676ex;" alt="{\displaystyle 10^{-4}}" />.
</p><p>From this, it also follows that it one should not use more cores for a
computation than the maximum speed-up that can be reached. If, for example, the
maximum speed-up is 10, it does not make sense to run this calculation on 100
cores - this would be inefficient and waste resources.
</p><p>To judge the efficiency of the parallelization, one can use the
<i>parallel efficiency</i> which is defined as the ratio of the observed speed-up
to the ideal speed-up, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/0907f5b6784460728909c023b1a7349d6e78b668" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.338ex; width:9.917ex; height:5.843ex;" alt="{\displaystyle \epsilon ={\frac {S}{S_{\text{ideal}}}}}" />. As an
example, when comparing a run on 4 cores to a run on 1 core, for which the
speed-up is 3, the ideal speed-up would be 4 and thus the parallel efficiency is
<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/b2d16e075eea2b4ad558fac15988510c9208036c" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:14.889ex; height:2.843ex;" alt="{\displaystyle \epsilon =3/4=75\%}" /> in this case. As a rule of thumb, efficiencies
above 70% are acceptable.
</p><p>In practice, one usually does not know the serial fraction. Thus, one executes
the code for a short test case for several numbers of processes to obtain the
speed-up, scaling curve, and efficiency. From this, one can then infer up to
which point the parallelization is still efficient and one should choose this
number of processors for subsequent runs to efficiently use the computing
resources.
</p><p>If in doubt, it is usually more efficient to use slightly less resources, but
run several simulations at the same time (which is often required) - this will
use the resources efficiently and still provide a small total time to solution
because several calculations can run in parallel.
</p><p>Be aware that on supercomputers, the available computing time is shared between
the members of a project or between the members of a group. If everyone runs
their code efficiently, the number of simulations that can be done by all
members of the group is maximized and thus more papers can be produced!
</p><p><br />
</p>
<h2><span class="mw-headline" id="Scaling_analysis_for_domain_parallelization">Scaling analysis for domain parallelization</span></h2>
<p>As a first example, we will do a strong scaling analysis for the same input as
in the <a href="/wiki/Tutorial:Parallelization_in_octopus" title="Tutorial:Parallelization in octopus">previous tutorial</a>.
Save the following as <b>inp</b> file:
</p>
<pre>CalculationMode = gs
FromScratch = yes

XYZCoordinates = "1ala.xyz"

Spacing = 0.2*angstrom
Radius = 4.0*angstrom

TDPropagator = aetrs
TDMaxSteps = 20
TDTimeStep = 0.05
</pre>
<p>and the following as "1ala.xyz":
</p>
<pre>  23
 units: A
      N                   -1.801560    0.333315   -1.308298
      C                   -1.692266    1.069227    0.012602
      C                   -0.217974    1.151372    0.425809
      O                    0.256888    2.203152    0.823267
      C                   -2.459655    0.319513    1.077471
      H                   -1.269452    0.827043   -2.046396
      H                   -1.440148   -0.634968   -1.234255
      H                   -2.791116    0.267637   -1.602373
      H                   -2.104621    2.114111   -0.129280
      H                   -2.391340    0.844513    2.046396
      H                   -2.090378   -0.708889    1.234538
      H                   -3.530691    0.246022    0.830204
      N                    0.476130   -0.012872    0.356408
      C                    1.893957   -0.046600    0.735408
      C                    2.681281    0.990593   -0.107455
      O                    3.486946    1.702127    0.516523
      O                    2.498931    1.021922   -1.333241
      C                    2.474208   -1.425485    0.459844
      H                    0.072921   -0.880981    0.005916
      H                    1.975132    0.211691    1.824463
      H                    1.936591   -2.203152    1.019733
      H                    3.530691   -1.461320    0.761975
      H                    2.422706   -1.683153   -0.610313
</pre>
<p>Then, run the ground state calculation or reuse the ground state from the
previous tutorial. For running the calculations, you can use the batch scripts
from the previous tutorial.
</p><p>For this analysis, we will only look at the parallelization using MPI and
disregard the OpenMP parallelization for the time being. Usually, these two
components can be examined independent of each other, keeping the other fixed.
</p>
<h3><span class="mw-headline" id="Baseline:_serial_run">Baseline: serial run</span></h3>
<p>As a baseline, we need to run the TD calculation on one processor. Change the
CalculationMode variable to td and submit a batch script to execute it on one
core. Check the parallelization section in the output and make sure that it is
executed in serial. 
</p><p>From the output of the timestep information, you can again get an average of the
elapsed time per timestep:
</p>
<pre>********************* Time-Dependent Simulation **********************
  Iter           Time        Energy   SC Steps    Elapsed Time

**********************************  **********************************
      1       0.050000   -109.463446         1         1.060
      2       0.100000   -109.463446         1         1.093
      3       0.150000   -109.463446         1         1.064
      4       0.200000   -109.463446         1         1.032
      5       0.250000   -109.463446         1         0.940
      6       0.300000   -109.463446         1         0.957
      7       0.350000   -109.463446         1         0.901
      8       0.400000   -109.463446         1         0.860
      9       0.450000   -109.463446         1         0.880
     10       0.500000   -109.463446         1         0.958
     11       0.550000   -109.463446         1         0.846
     12       0.600000   -109.463446         1         0.855
     13       0.650000   -109.463446         1         0.826
     14       0.700000   -109.463446         1         0.805
     15       0.750000   -109.463446         1         0.805
     16       0.800000   -109.463446         1         0.832
     17       0.850000   -109.463446         1         0.814
     18       0.900000   -109.463446         1         0.788
     19       0.950000   -109.463446         1         0.812
     20       1.000000   -109.463446         1         0.897
</pre>
<p>In this case, the average of the 20 time steps would be about 0.90s.
</p><p><br />
</p>
<h3><span class="mw-headline" id="Scaling_runs">Scaling runs</span></h3>
<p>To analyze the scaling, we will run TD calculations with a logarithmic spacing
in the number of processors, using 2, 4, 8, and 16 cores. To make sure that
domain parallelization is used, add the following to the <b>inp</b> file:
</p>
<pre>ParStates = 1
ParDomains = auto
</pre>
<p>Submit a job script with for each of the core numbers and wait until they are
finished. Then, extract the average time for one time step as we did above for
the serial run.
</p><p>With these numbers, you can fill the following table:
</p>
<table>
<tbody><tr>
<td>Cores</td>
<td>1</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>16
</td></tr>
<tr>
<td>Time</td>
<td>0.90</td>
<td>0.57</td>
<td>0.34</td>
<td>0.21</td>
<td>0.11
</td></tr></tbody></table>
<p>The numbers you get from your own measurement can deviate, but overall the
result should be similar.
</p><p><br />
</p>
<h3><span class="mw-headline" id="Analysis">Analysis</span></h3>
<p>Using the times you measured, compute the speed-up and the parallel efficiency
as introduced earlier in the tutorial. You should get a table that is similar to
the following:
</p>
<table>
<tbody><tr>
<td>Cores</td>
<td>1</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>16
</td></tr>
<tr>
<td>Time</td>
<td>0.90</td>
<td>0.57</td>
<td>0.34</td>
<td>0.21</td>
<td>0.11
</td></tr>
<tr>
<td>Speed-up</td>
<td>1.00</td>
<td>1.58</td>
<td>2.65</td>
<td>4.29</td>
<td>8.18
</td></tr>
<tr>
<td>Ideal Speed-up</td>
<td>1</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>16
</td></tr>
<tr>
<td>Efficiency</td>
<td>1</td>
<td>0.79</td>
<td>0.66</td>
<td>0.54</td>
<td>0.51
</td></tr></tbody></table>
<p>What do these numbers tell us? First of all, the parallel efficiency is almost
80% when using 2 cores, so that is still fine. Going to more cores, the
efficiency drops. Thus, for this system it is inefficient to use more than 2
cores for the domain parallelization.
</p><p>A standard way of visualizing scaling data is to display a log-log plot of the
speed-up vs. the number of cores. A little python script for doing this can be
found at the end of the tutorial. When the speed-up is near the line of the
ideal speed-up, the efficiency is good, but once it drops below, the scaling
breaks down.
</p><p>Why does the efficiency drop for more than 2 cores? As you might remember from the
<a href="/wiki/Tutorial:Parallelization_in_octopus" title="Tutorial:Parallelization in octopus">previous tutorial on parallelization</a>,
the parallelization in domains is only efficient, when the number of inner
points is large enough compared to the number of ghost points. So let's compare
the ratio of ghost points to local points, which should be less than about 25%
as a rule of thumb. For this, you need to look at the information on the mesh
partitioning, which should look similar to the following on 2 cores:
</p>
<pre>      Partition quality:    0.244738E-08

                 Neighbours         Ghost points
      Average  :          1                14293
      Minimum  :          1                14150
      Maximum  :          1                14435

      Nodes in domain-group      1
        Neighbours     :         1        Local points    :     70175
        Ghost points   :     14435        Boundary points&#160;:     27217
      Nodes in domain-group      2
        Neighbours     :         1        Local points    :     70154
        Ghost points   :     14150        Boundary points&#160;:     27859
</pre>
<p>For this case, the ratio would be about 14435/70175=21%.
</p><p>What are the ratios you get for the other core numbers? Can they explain the
drop in efficiency?
</p><p>(as a reference, the ratios should be roughly:
2 cores: 21%; 4: 35%; 8: up to 70% 16: up to 111%)
</p><p><br />
</p>
<h2><span class="mw-headline" id="Scaling_analysis_for_states_parallelization">Scaling analysis for states parallelization</span></h2>
<p>Let's do a similar analysis, but for states parallelization. Change the
parallelization options to
</p>
<pre>ParStates = auto
ParDomains = 1
</pre>
<p>to make sure only states parallelization is used. Then submit batch jobs to run
the code on 1, 2, 4, 8, and 16 cores. Gather the timings as in the previous
section and create a table of the timings; also compute the speed-up and
parallel efficiency for all runs. The table should be similar to (the timings
can vary, but the speed-up and efficiency should be similar):
</p>
<table>
<tbody><tr>
<td>Cores</td>
<td>1</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>16
</td></tr>
<tr>
<td>Time</td>
<td>0.67</td>
<td>0.36</td>
<td>0.23</td>
<td>0.14</td>
<td>0.15
</td></tr>
<tr>
<td>Speed-up</td>
<td>1.00</td>
<td>1.86</td>
<td>2.91</td>
<td>4.79</td>
<td>4.47
</td></tr>
<tr>
<td>Ideal Speed-up</td>
<td>1</td>
<td>2</td>
<td>4</td>
<td>8</td>
<td>16
</td></tr>
<tr>
<td>Efficiency</td>
<td>1</td>
<td>0.93</td>
<td>0.73</td>
<td>0.60</td>
<td>0.28
</td></tr></tbody></table>
<p>As one can see, the efficiency is above 70% only up to 4 cores; above the
efficiency drops. Thus, for this system, state parallelization should only used
up to 4 cores.
</p><p>Now the question is: why does the scaling break down above 4 processes and
especially above 8 processes? Let's look at the parallelization output from the
log of the 8-core run:
</p>
<pre>Info: Parallelization in states
Info: Node in group    0 will manage      4 states:     1 -      4
Info: Node in group    1 will manage      4 states:     5 -      8
Info: Node in group    2 will manage      4 states:     9 -     12
Info: Node in group    3 will manage      4 states:    13 -     16
Info: Node in group    4 will manage      4 states:    17 -     20
Info: Node in group    5 will manage      4 states:    21 -     24
Info: Node in group    6 will manage      4 states:    25 -     28
Info: Node in group    7 will manage      4 states:    29 -     32
</pre>
<p>The system has 32 states, thus each core processes 4 states. In the previous
tutorial, it was indicated as a rule of thumb that 4 states per process is the
minimum to be efficient, also in terms of vectorization. As one can see, the
calculation time does not even decrease when going to 16 cores, where each
core processes only 2 states. However, on 4 cores, each process has 8 states and
that is more efficient.
</p>
<h2><span class="mw-headline" id="Combined_states_and_domain_parallelization">Combined states and domain parallelization</span></h2>
<p>Now run the system again, combining states and domain parallelization, with 2
cores each and with 2 and 4 cores. Extract the timings and compute the speed-up
and parallel efficiency. What is the most efficient way to run this system?
</p>
<h2><span class="mw-headline" id="K-point_parallelization">K-point parallelization</span></h2>
<p>We don't treat k-point parallelization in detail here, because it is only
relevant for solids which are covered in a later tutorial. But as the
parallelization is quite trivial, you can scale the processes up to using only 1
k point per process. It is most efficient when the distribution of k points to
processes is balanced (e.g. all cores have 2 k-points instead of some having 2
and some having only 1).
</p>
<h2><span class="mw-headline" id="Production_runs">Production runs</span></h2>
<p>Usually, production runs are larger and require more resources. Thus, one
usually requests full nodes and then the scaling analysis is done using 1, 2, 4,
8, ... full nodes and the speed-up is computed relative to one node. Other than
that, the analysis is the same as outlined above.
</p><p>For one of the systems you use in production runs: how many cores or nodes do
you usually use? Is that efficient? Run a quick scaling analysis on 3 or 4
different node counts to estimate the parallel efficiency. Check the guidelines
for the different parallelization strategies.
</p><p><br />
</p>
<h2><span class="mw-headline" id="Memory_usage">Memory usage</span></h2>
<p>Another reason to use more nodes besides the larger compute power is the larger
memory that might be needed. If the memory needed for the states is too large to
fit into the main memory of one node, more nodes need to be used to distribute
the storage of the states across nodes.
</p><p>There is a section in the output that gives approximate memory requirements. For
the example from the previous sections, it looks as follows:
</p>
<pre>****************** Approximate memory requirements *******************
Mesh
  global  :       2.9 MiB
  local   :       7.4 MiB
  total   :      10.3 MiB

States
  real    :      47.1 MiB (par_kpoints + par_states + par_domains)
  complex&#160;:      94.1 MiB (par_kpoints + par_states + par_domains)

**********************************************************************
</pre>
<p>This indicates that mesh object takes about 10 MiB on each core and that the
states in total require 94 MiB for complex numbers (which are used for TD runs).
To estimate the total amount of memory needed, one can add the memory for the
states to the memory of mesh times the number of processes because the mesh
information is needed on each process. If this is larger than the memory per
node (which can usually be found in the documentation of the corresponding
cluster or supercomputer), one can estimate the number of nodes needed by
dividing the total memory needed by the memory per node.
</p><p>Sometimes, supercomputers also offer compute nodes with more memory per node,
then it can also be an option to request those nodes.
</p>
<h2><span class="mw-headline" id="Guidelines">Guidelines</span></h2>
<p>Here is a brief summary of guidelines:
</p>
<ul><li>do a scaling run for new systems and compute parallel efficiency</li>
<li>parallel efficiency should be &gt;70%</li>
<li>in doubt use less cores and run several simulations in parallel</li>
<li>for states parallelization: minimum 4-8 states per core</li>
<li>for domain parallelization: ratio ghost/local points should be &lt;25%</li>
<li>for k-point parallelization: up to 1 k point per core</li>
<li>for k-point and states parallelization: a balanced distribution is more efficient</li>
<li>do not waste resources; leads to more science being done by all!</li></ul>
<h2><span class="mw-headline" id="Code_snippets">Code snippets</span></h2>
<p>You can use the following python script to create a scaling plot as a log-log
plot of speed-up vs. core number:
</p>
<pre>import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
# number of cores
cores = np.array([1, 2, 4, 8, 16])
# enter the times you measured here
times = np.array([0.90, 0.57, 0.34, 0.21, 0.11])
speedup = times[0]/times
ideal_speedup = cores/cores[0]
plt.loglog(cores, speedup, label="speed-up")
plt.loglog(cores, ideal_speedup, label="ideal")
plt.xlabel("Cores")
plt.ylabel("Speed-up")
plt.legend()
# some formatting
plt.gca().set_xticks(cores)
plt.gca().set_xticklabels(cores)
plt.gca().set_yticks(cores)
plt.gca().set_yticklabels(cores)
plt.gca().xaxis.set_tick_params(which='minor', size=0)
plt.gca().yaxis.set_tick_params(which='minor', size=0)
plt.savefig("speedup.pdf")
</pre>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Tutorial:Parallelization_in_octopus" title="Tutorial:Parallelization in octopus">Parallelization in octopus</a> - Next <a href="/wiki/Tutorial:Octopus_on_GPUs" title="Tutorial:Octopus on GPUs">Octopus on GPUs</a>
</p><p>Back to <a href="/wiki/Tutorial_Series:Running_Octopus_on_HPC_systems" title="Tutorial Series:Running Octopus on HPC systems">Running Octopus on HPC systems</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155139
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.306 seconds
Real time usage: 5.921 seconds
Preprocessor visited node count: 353/1000000
Preprocessor generated node count: 869/1000000
Post‐expand include size: 1274/2097152 bytes
Template argument size: 1343/2097152 bytes
Highest expansion depth: 14/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 8682/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%   28.409      1 Template:Tutorial_foot
100.00%   28.409      1 -total
 93.71%   26.623      4 Template:If
 65.50%   18.607      4 Template:P2
 37.35%   10.611      4 Template:P1
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:3071-0!canonical!math=0 and timestamp 20230412155133 and revision id 11413
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:Scaling&amp;oldid=11413">http:///mediawiki/index.php?title=Tutorial:Scaling&amp;oldid=11413</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Category</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3AScaling&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Scaling" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Scaling&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:Scaling">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Scaling&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Scaling&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Scaling" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Scaling" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:Scaling&amp;oldid=11413" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Scaling&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 7 September 2021, at 11:17.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.306","walltime":"5.921","ppvisitednodes":{"value":353,"limit":1000000},"ppgeneratednodes":{"value":869,"limit":1000000},"postexpandincludesize":{"value":1274,"limit":2097152},"templateargumentsize":{"value":1343,"limit":2097152},"expansiondepth":{"value":14,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":8682,"limit":5000000},"timingprofile":["100.00%   28.409      1 Template:Tutorial_foot","100.00%   28.409      1 -total"," 93.71%   26.623      4 Template:If"," 65.50%   18.607      4 Template:P2"," 37.35%   10.611      4 Template:P1"]},"cachereport":{"timestamp":"20230412155139","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":123});});</script>
</body>
</html>
