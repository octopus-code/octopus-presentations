<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Manual:Updating to a new version - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Updating_to_a_new_version","wgTitle":"Manual:Updating to a new version","wgCurRevisionId":11359,"wgRevisionId":0,"wgArticleId":1957,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Updating_to_a_new_version","wgRelevantArticleId":1957,"wgRequestId":"cbcfe81025501d9561b6fe69","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Updating_to_a_new_version rootpage-Manual_Updating_to_a_new_version skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Manual:Updating to a new version</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Manual:Updating_to_a_new_version" title="Manual:Updating to a new version">Manual:Updating to a new version</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">This page lists the issues that may appear when updating to a new version of {{octopus}} and how to solve them.

==== Octopus 11 ====

===== Variables =====

* The syntax of the Output and TDOutput variables has been changed for greater flexibility.
* The LSize variable cannot be used anymore for periodic systems. Instead, the LatticeParameters variable is mandatory for periodic systems.

===== Execution =====

* Runs that do not use the GPU will now by default fail if the code was compiled with GPU support. It is possible to override this behavior using the AllowCPUonly input variable.

===== Installation =====

* This version supports Libxc 5.1. Support for Libxc 3 and Libxc 5.0 has been removed. Note that if you are using Libxc 5.1, the library needs to be compiled with support for third order derivatives in order for Octopus to be able to perform response calculations using the Casida or the Sternheimer methods.

==== Octopus 10 ====

===== Execution =====

* The default convergence criteria have been changed: {{code|1=ConvRelDens = 1e-6}} and {{code|1=EigensolverTolerance = 1e-7}}.
* The SCF loop is terminated only if the convergence criteria are fulfilled twice in subsequent iterations.

==== Octopus 8 ====

===== Variables =====

* The {{variable|Species|System}} block can now take a {{emph|set}} option to choose which pseudopotential set to use.
* The default for {{variable|XCFunctional|Hamiltonian}} is now taken, when possible, from the pseudopotentials.

==== Octopus 7 ====

===== Variables =====

* The UnitsInput and Units variables have been removed. Input file values are now always in atomic units, unless explicitly stated otherwise in the corresponding variable description. A few constants are available to help using eV/Angstrom units in the input file, such that one can write {{code|1=Spacing = 0.25*angstrom}}.
* Now by default the code assumes XYZ files to be in Angstrom. This can be changed by using the {{variable|UnitsXYZFiles|Execution}} variable.

==== Octopus 6 ====

===== Installation =====

* A compiler supporting Fortran 2003 iso_c_binding is now required.
* The executable is always called {{file|octopus}}, no longer {{file|octopus_mpi}} when compiled in parallel.
* This version supports [[libxc 3.0.0]]. Although it is still possible to use [[libxc 2.0.0]] or any of the 2.x versions, updating to [[libxc 3.0.0]] is highly recommended.

===== Variables =====

* There is a new format for the {{variable|Species|System}} block and the names of the species options have been revised.
* The variable OutputHow has been renamed to {{variable|OutputFormat|Output}}.
* The variable ParallelizationStrategy and ParallelizationGroupRanks have been replaced with {{variable|ParDomains|Execution}}, {{variable|ParStates|Execution}}, {{variable|ParKPoints|Execution}}, and {{variable|ParOther|Execution}}.

===== Utilities =====

The {{file|oct-rotatory_strength}} utility has been removed. This utility is replaced by a new {{variable|PropagationSpectrumType|Utilities}} in {{file|oct-propagation_spectrum}}. 

==== Octopus 5.0 ====

===== Restart files =====

This version breaks the backwards compatibility of the restart files. Nevertheless, it is not too difficult to update the restart files generated with a previous version. Here is a summary of the changes and how to update the files:

* Casida restart data now goes to a directory {{file|restart/casida}}, and a file {{file|kernel}} or {{file|kernel_triplet}}. One then needs to create the directory and rename the files.
* The format of the {{file|occs}} file changed with the addition of an extra field for the imaginary part of the eigenvalues. To update the file one needs to add a column of zeros by hand after the column of eigenvalues.
* The line starting with {{emph|fft_alpha}} was removed from the {{file|mesh}} file.

Note that the mesh partition restart is also not compatible any more. In this case the partition needs to be recalculated, which is usually not a problem.

===== Variables =====

Some variables changed name or changed format. If an obsolete variable is found, {{octopus}} will stop and will tell you the variable that replaces it.

The datasets feature was removed. The input file of a calculation using datasets needs to be split into several independent input files.

==== Octopus 4.1 ====

===== Installation =====

This version requires [[libxc 2.0.0]] or higher, so it might be necessary to update libxc before installing {{octopus}}. 

===== Restart files =====
Casida restart file is incompatible with that generated by version 4.0. The new format avoids problems with restarting with a different number of states.

==== Octopus 4.0 ====

===== Installation =====

* Libxc is now an independent library. To compile {{octopus}} 4.0.0 you will have to compile [[libxc 1.1.0]] first. These are the short instructions to compile it (we assume that libxc will be installed in $DIR, this can be the same directory where {{octopus}} is going to be installed):
 tar -xvzf libxc-1.1.0.tar.gz
 cd libxc-1.1.0 
 ./configure --prefix=$DIR
 make
 make install
Now, when configuring {{octopus}} pass the option --with-libxc-prefix=$DIR.

* The configure option for the location of netcdf and etsf_io are --with-netcdf-prefix and --with-etsf-io-prefix.

===== Variables =====

* TDEvolutionMethod is now called {{variable|TDPropagator|Time-Dependent}}.

===== Utilities =====

* {{file|oct-cross_section}} was renamed to {{file|oct-propagation_spectrum}}.
* {{file|oct-broad}} was renamed to {{file|oct-casida_spectrum}}.
* The format for {{file|oct-help}} has changed. Now {{file|-s}} is used instead of {{file|search}}, {{file|-p}} instead of {{file|show}}, and  {{file|-l}} instead of {{file|list}}.

==== Octopus 3.0 ====

===== Input variables =====

Some variables changed name or changed format. If an obsolete variable is found, {{octopus}} will stop and will tell you the variable that replaces it.

* {{variable|Units|Execution}}, {{variable|UnitsInput|Execution}} and {{variable|UnitsOutput|Execution}} now take a named option as argument instead of a string. So

 Units = "eVA"

should be replaced by
 
 Units = eV_Angstrom

The code will stop if the old format is encountered.

* XFunctional and CFunctional were replaced by {{variable|XCFunctional|Hamiltonian}}, for example

 XFunctional = lda_x
 CFunctional = lda_c_vwn

must be replaced with

 XCFunctional = lda_x + lda_c_vwn

* TDLasers was replaced by {{variable|TDExternalFields|Time-Dependent}}.
* Some options for {{variable|CalculationMode|Calculation_Modes}} were renamed.

===== Output directories =====

Some directories in {{octopus}} output were renamed, restart files now are stored under {{file|restart/}} instead of {{file|tmp/}}. Files previously found under {{file|status/}} are now located in {{file|exec/}}.

===== Restart file format =====

{{octopus}} writes restart files in a binary format, this format has been updated and improved. As a result {{octopus}} is no longer able to restart automatically the calculation of a run performed with older versions. 

===== Recovering old restart files =====

If you really need to recover your old restart files, first you have to generate NetCDF restart files with your old version of {{octopus}}. Then you will have to rename the {{file|tmp/}} directory to {{file|restart/}} and remove the {{file|restart_}} prefix from its subdirectories, for example {{file|tmp/restart_gs/}} now should be called {{file|restart/gs/}}.

Now you can run {{octopus}} 3.0 and it will find your restart information.


{{manual_foot|prev=Manual:Deprecated Utilities|next=Manual:Building from scratch}}
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:Code" title="Template:Code">Template:Code</a> (<a href="/mediawiki/index.php?title=Template:Code&amp;action=edit" title="Template:Code">view source</a>) </li><li><a href="/wiki/Template:Emph" title="Template:Emph">Template:Emph</a> (<a href="/mediawiki/index.php?title=Template:Emph&amp;action=edit" title="Template:Emph">view source</a>) </li><li><a href="/wiki/Template:File" title="Template:File">Template:File</a> (<a href="/mediawiki/index.php?title=Template:File&amp;action=edit" title="Template:File">view source</a>) </li><li><a href="/wiki/Template:Foot" title="Template:Foot">Template:Foot</a> (<a href="/mediawiki/index.php?title=Template:Foot&amp;action=edit" title="Template:Foot">view source</a>) </li><li><a href="/wiki/Template:Manual_foot" title="Template:Manual foot">Template:Manual foot</a> (<a href="/mediawiki/index.php?title=Template:Manual_foot&amp;action=edit" title="Template:Manual foot">view source</a>) </li><li><a href="/wiki/Template:Octopus" title="Template:Octopus">Template:Octopus</a> (<a href="/mediawiki/index.php?title=Template:Octopus&amp;action=edit" title="Template:Octopus">view source</a>) </li><li><a href="/wiki/Template:Octopus_major_version" title="Template:Octopus major version">Template:Octopus major version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_major_version&amp;action=edit" title="Template:Octopus major version">view source</a>) </li><li><a href="/wiki/Template:Octopus_minor_version" title="Template:Octopus minor version">Template:Octopus minor version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_minor_version&amp;action=edit" title="Template:Octopus minor version">view source</a>) </li><li><a href="/wiki/Template:Octopus_version" title="Template:Octopus version">Template:Octopus version</a> (<a href="/mediawiki/index.php?title=Template:Octopus_version&amp;action=edit" title="Template:Octopus version">view source</a>) </li><li><a href="/wiki/Template:Variable" title="Template:Variable">Template:Variable</a> (<a href="/mediawiki/index.php?title=Template:Variable&amp;action=edit" title="Template:Variable">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Manual:Updating_to_a_new_version" title="Manual:Updating to a new version">Manual:Updating to a new version</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Manual:Updating_to_a_new_version">http:///wiki/Manual:Updating_to_a_new_version</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AUpdating+to+a+new+version&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Updating_to_a_new_version" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:Updating_to_a_new_version&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Manual:Updating_to_a_new_version">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Manual:Updating_to_a_new_version&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Updating_to_a_new_version&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Updating_to_a_new_version" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Updating_to_a_new_version" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Updating_to_a_new_version&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":136});});</script>
</body>
</html>
