<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Manual:Input file - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Manual:Input_file","wgTitle":"Manual:Input file","wgCurRevisionId":11351,"wgRevisionId":11351,"wgArticleId":1343,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Manual:Input_file","wgRelevantArticleId":1343,"wgRequestId":"b12258c9084be653d3fc42d1","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.toc.styles":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.toc","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cmediawiki.toc.styles%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Manual_Input_file rootpage-Manual_Input_file skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Manual:Input file</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"><div class="mw-revision"><div id="mw-revision-info">Revision as of 11:36, 1 September 2021 by <a href="/wiki/User:Micael" class="mw-userlink" title="User:Micael"><bdi>Micael</bdi></a> <span class="mw-usertoollinks">(<a href="/mediawiki/index.php?title=User_talk:Micael&amp;action=edit&amp;redlink=1" class="new mw-usertoollinks-talk" title="User talk:Micael (page does not exist)">talk</a> | <a href="/wiki/Special:Contributions/Micael" class="mw-usertoollinks-contribs" title="Special:Contributions/Micael">contribs</a>)</span></div><div id="mw-revision-nav">(<a href="/mediawiki/index.php?title=Manual:Input_file&amp;diff=prev&amp;oldid=11351" title="Manual:Input file">diff</a>) <a href="/mediawiki/index.php?title=Manual:Input_file&amp;direction=prev&amp;oldid=11351" title="Manual:Input file">← Older revision</a> | Latest revision (diff) | Newer revision → (diff)</div></div></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>Octopus uses a single input file from which to read user instructions to know what to calculate and how. This page explains how to generate that file and what is the general format. The Octopus parser is a library found in the <b><tt>liboct_parser</tt></b> directory of the source, based on bison and C. You can find two (old) separate release versions of it at the bottom of the <a href="/wiki/Releases" title="Releases">Releases</a> page.
</p>
<div id="toc" class="toc"><input type="checkbox" role="button" id="toctogglecheckbox" class="toctogglecheckbox" style="display:none" /><div class="toctitle" lang="en" dir="ltr"><h2>Contents</h2><span class="toctogglespan"><label class="toctogglelabel" for="toctogglecheckbox"></label></span></div>
<ul>
<li class="toclevel-1 tocsection-1"><a href="#Input_file"><span class="tocnumber">1</span> <span class="toctext">Input file</span></a></li>
<li class="toclevel-1 tocsection-2"><a href="#Scalar_Variables"><span class="tocnumber">2</span> <span class="toctext">Scalar Variables</span></a></li>
<li class="toclevel-1 tocsection-3"><a href="#Mathematical_expressions"><span class="tocnumber">3</span> <span class="toctext">Mathematical expressions</span></a>
<ul>
<li class="toclevel-2 tocsection-4"><a href="#Arithmetic_operators"><span class="tocnumber">3.1</span> <span class="toctext">Arithmetic operators</span></a></li>
<li class="toclevel-2 tocsection-5"><a href="#Logical_operators"><span class="tocnumber">3.2</span> <span class="toctext">Logical operators</span></a></li>
<li class="toclevel-2 tocsection-6"><a href="#Functions"><span class="tocnumber">3.3</span> <span class="toctext">Functions</span></a></li>
<li class="toclevel-2 tocsection-7"><a href="#References"><span class="tocnumber">3.4</span> <span class="toctext">References</span></a></li>
</ul>
</li>
<li class="toclevel-1 tocsection-8"><a href="#Predefined_variables"><span class="tocnumber">4</span> <span class="toctext">Predefined variables</span></a></li>
<li class="toclevel-1 tocsection-9"><a href="#Blocks"><span class="tocnumber">5</span> <span class="toctext">Blocks</span></a></li>
<li class="toclevel-1 tocsection-10"><a href="#Comments"><span class="tocnumber">6</span> <span class="toctext">Comments</span></a></li>
<li class="toclevel-1 tocsection-11"><a href="#Includes"><span class="tocnumber">7</span> <span class="toctext">Includes</span></a></li>
<li class="toclevel-1 tocsection-12"><a href="#Environment_variables"><span class="tocnumber">8</span> <span class="toctext">Environment variables</span></a></li>
<li class="toclevel-1 tocsection-13"><a href="#Default_values"><span class="tocnumber">9</span> <span class="toctext">Default values</span></a></li>
<li class="toclevel-1 tocsection-14"><a href="#Documentation"><span class="tocnumber">10</span> <span class="toctext">Documentation</span></a></li>
<li class="toclevel-1 tocsection-15"><a href="#Experimental_features"><span class="tocnumber">11</span> <span class="toctext">Experimental features</span></a></li>
<li class="toclevel-1 tocsection-16"><a href="#Good_practices"><span class="tocnumber">12</span> <span class="toctext">Good practices</span></a></li>
</ul>
</div>

<h3><span class="mw-headline" id="Input_file">Input file</span></h3>
<p>Input options should be in a file called <b><tt>inp</tt></b>, in the directory Octopus is run from. This is a plain ASCII text file, to create or edit it you can use any text editor like emacs, vi, jed, pico, gedit, etc..
</p><p>At the beginning of the program, the parser reads the input file, parses it, and generates a list of variables that will be read by Octopus (note that the input is case-independent). There are two kind of variables: scalar values (strings or numbers), and blocks (that you may view as matrices).
</p>
<h3><span class="mw-headline" id="Scalar_Variables">Scalar Variables</span></h3>
<p>A scalar variable <code>var</code> can be defined by:
</p>
<pre> <code> var = exp</code>
</pre>
<p><code>var</code> can contain any alphanumeric character plus _, and <code>exp</code> can be a quote-delimited string, a number (integer, real, or complex), a variable name, or a mathematical expression. Complex numbers are defined as <tt>{real, imag}</tt>. Real numbers can use scientific notation with <tt>e</tt> or <tt>E</tt> (no <tt>d</tt> or <tt>D</tt>, Fortran people), such as <tt>6.02e23</tt>. Variable names are not case-sensitive, and you must not redefine a previously defined symbol -- especially not the reserved variables <tt>x, y, z, r, w, t</tt> which are used in space- or time-dependent expressions, where <tt>w</tt> is the 4th space coordinate when operating in 4D.
</p>
<h3><span class="mw-headline" id="Mathematical_expressions">Mathematical expressions</span></h3>
<p>The parser can interpret expressions in the input file, either to assign the result to a variable, or for defining functions such as a potential in the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Species"><code>Species</code></a></span> block or a time-dependent function in the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Time-Dependent&amp;name=TDFunctions"><code>TDFunctions</code></a></span> block. The arguments can be numbers or other variables.
</p>
<h4><span class="mw-headline" id="Arithmetic_operators">Arithmetic operators</span></h4>
<dl><dt><code>a+b</code></dt>
<dd>addition</dd>
<dt><code>a-b</code></dt>
<dd>subtraction</dd>
<dt><code>-a</code></dt>
<dd>unary minus</dd>
<dt><code>a*b</code></dt>
<dd>multiplication</dd>
<dt><code>a/b</code></dt>
<dd>division</dd>
<dt><code>a^b</code></dt>
<dd>exponentiation</dd></dl>
<h4><span class="mw-headline" id="Logical_operators">Logical operators</span></h4>
<p>Logical operation will return 0 for false or 1 for true. You can exploit this to define a piecewise expression, e.g. <tt>"2 * (x &lt;= 0) - 3 * (x &gt; 0)"</tt> (although the <tt>step</tt> function may also be used). The comparison operators (except <tt>==</tt>) use only the real part of complex numbers.
</p>
<dl><dt><code>a &lt; b</code></dt>
<dd>less than</dd>
<dt><tt>a &lt;= b</tt></dt>
<dd>less than or equal to (<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/440568a09c3bfdf0e1278bfa79eb137c04e94035" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.505ex; width:1.808ex; height:2.176ex;" alt="{\displaystyle \leq }" />)</dd>
<dt><code>a &gt; b</code></dt>
<dd>greater than</dd>
<dt><tt>a &gt;= b</tt></dt>
<dd>greater than or equal to (<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/bcef7c0e95bb77a35fd1a874ca91f425215f3c26" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.505ex; width:1.808ex; height:2.176ex;" alt="{\displaystyle \geq }" />)</dd>
<dt><tt>a == b</tt></dt>
<dd>equal to</dd>
<dt><code>a &amp;&amp; b</code></dt>
<dd>logical and</dd>
<dt><tt>a || b</tt></dt>
<dd>logical or</dd>
<dt><code>!a</code></dt>
<dd>logical not</dd></dl>
<h4><span class="mw-headline" id="Functions">Functions</span></h4>
<dl><dt><code>sqrt(x)</code></dt>
<dd>The square root of <code>x</code>.</dd>
<dt><code>exp(x)</code></dt>
<dd>The exponential of <code>x</code>.</dd>
<dt><code>log(x)</code> or <code>ln(x)</code></dt>
<dd>The natural logarithm of <code>x</code>.</dd>
<dt><code>log10(x)</code></dt>
<dd>Base 10 logarithm of <code>x</code>.</dd>
<dt><code>logb(x, b)</code></dt>
<dd>Base <code>b</code> logarithm of <code>x</code>.</dd></dl>
<dl><dt><code>{x, y&#125;</code></dt>
<dd>The complex number <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/ceb1c6ce62a20dbfe9cb3d82dca889577b469703" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:6.128ex; height:2.509ex;" alt="{\displaystyle x+iy}" />.</dd>
<dt><code>arg(z)</code></dt>
<dd>Argument of the complex number <code>z</code>, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/5d8bb9d3cf9c1a82f1666e4e72b6702b0cee4b21" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:6.134ex; height:2.843ex;" alt="{\displaystyle \arg(z)}" />, where <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/f93df0041afc9711e1a89de53076512aa53ad81d" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:18.611ex; height:2.843ex;" alt="{\displaystyle -\pi &lt;\arg(z)&lt;=\pi }" />.</dd>
<dt><code>abs(z)</code></dt>
<dd>Magnitude of the complex number <code>z</code>, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/28fd4d7dcabf618d707c21bd08306c7b3aa8b68e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:2.382ex; height:2.843ex;" alt="{\displaystyle |z|}" />.</dd>
<dt><code>abs2(z)</code></dt>
<dd>Magnitude squared of the complex number <code>z</code>, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/bf1302d25887ea6ddc11bb5c3482f14016dc6f76" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:3.436ex; height:3.343ex;" alt="{\displaystyle |z|^{2}}" />.</dd>
<dt><code>logabs(z)</code></dt>
<dd>Natural logarithm of the magnitude of the complex number <code>z</code>, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/332cfaf8e82131a31cb8a5d8f8cc1e8b8b6a5236" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:5.741ex; height:2.843ex;" alt="{\displaystyle \log |z|}" />. It allows an accurate evaluation of <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/332cfaf8e82131a31cb8a5d8f8cc1e8b8b6a5236" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:5.741ex; height:2.843ex;" alt="{\displaystyle \log |z|}" /> when <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/28fd4d7dcabf618d707c21bd08306c7b3aa8b68e" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:2.382ex; height:2.843ex;" alt="{\displaystyle |z|}" /> is close to one. The direct evaluation of <code>log(abs(z))</code> would lead to a loss of precision in this case.</dd>
<dt><code>conjg(z)</code></dt>
<dd>Complex conjugate of the complex number <code>z</code>, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/d3321be6fb7bbb29e3a9004308a0e00fa2f01bcb" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:11.371ex; height:2.676ex;" alt="{\displaystyle z^{*}=x-iy}" />.</dd>
<dt><code>inv(z)</code></dt>
<dd>Inverse, or reciprocal, of the complex number <code>z</code>, <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/77d1e07260b02ccd399515032c2e82e17e7b17bf" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.505ex; width:13.372ex; height:6.009ex;" alt="{\displaystyle {\frac {1}{z}}={\frac {x-iy}{x^{2}+y^{2}}}}" />.</dd></dl>
<dl><dt><code>sin(x)</code>, <code>cos(x)</code>, <code>tan(x)</code>, <code>cot(x)</code>, <code>sec(x)</code>, <code>csc(x)</code></dt>
<dd>The sine, cosine, tangent, cotangent, secant and cosecant of <code>x</code>.</dd>
<dt><code>asin(x)</code>, <code>acos(x)</code>, <code>atan(x)</code>, <code>acot(x)</code>, <code>asec(x)</code>, <code>acsc(x)</code></dt>
<dd>The inverse (arc-) sine, cosine, tangent, cotangent, secant and cosecant of <code>x</code>.</dd>
<dt><code>atan2(x,y)</code></dt>
<dd>= <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/55b3e484153864414f694bb198a66d4274677246" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.838ex; width:9.979ex; height:2.843ex;" alt="{\displaystyle \mathrm {atan} (y/x)}" />.</dd>
<dt><code>sinh(x)</code>, <code>cosh(x)</code>, <code>tanh(x)</code>, <code>coth(x)</code>, <code>sech(x)</code>, <code>csch(x)</code></dt>
<dd>The hyperbolic sine, cosine, tangent, cotangent, secant and cosecant of <code>x</code>.</dd>
<dt><code>asinh(x)</code>, <code>acosh(x)</code>, <code>atanh(x)</code>, <code>acoth(x)</code>, <code>asech(x)</code>, <code>acsch(x)</code></dt>
<dd>The inverse hyperbolic sine, cosine, tangent, cotangent, secant and cosecant of <code>x</code>.</dd></dl>
<dl><dt><code>min(x, y)</code></dt>
<dd>The minimum of <code>x</code> and <code>y</code>.</dd>
<dt><code>max(x, y)</code></dt>
<dd>The maximum of <code>x</code> and <code>y</code>.</dd>
<dt><code>step(x)</code></dt>
<dd>The Heaviside step function in <code>x</code>. This can be used for piecewise-defined functions.</dd>
<dt><code>erf(x)</code></dt>
<dd>The error function <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/62b1b792f7919660b9cf91353195c4f62e1ffab7" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -2.838ex; width:23.637ex; height:6.343ex;" alt="{\displaystyle \mathrm {erf} (x)={\frac {2}{\sqrt {\pi }}}\int _{0}^{x}dte^{-t^{2}}}" />.</dd></dl>
<dl><dt><code>realpart(z)</code></dt>
<dd>The real part of the complex number <code>z</code>.</dd>
<dt><code>imagpart(z)</code></dt>
<dd>The imaginary part of the complex number <code>z</code>.</dd>
<dt><code>floor(x)</code></dt>
<dd>The largest integer less than the real number <code>x</code>.</dd>
<dt><code>ceiling(x)</code></dt>
<dd>The smallest integer greater than the real number <code>x</code>.</dd></dl>
<p>These mathematical operations are all based on the GSL library and are defined in <code>symbols.c</code> and <code>grammar.y</code>.
</p>
<h4><span class="mw-headline" id="References">References</span></h4>
<ul><li><a rel="nofollow" class="external free" href="https://www.gnu.org/software/gsl/manual/html_node/Properties-of-complex-numbers.html">https://www.gnu.org/software/gsl/manual/html_node/Properties-of-complex-numbers.html</a></li>
<li><a rel="nofollow" class="external free" href="https://www.gnu.org/software/gsl/manual/html_node/Complex-arithmetic-operators.html">https://www.gnu.org/software/gsl/manual/html_node/Complex-arithmetic-operators.html</a></li>
<li><a rel="nofollow" class="external free" href="https://www.gnu.org/software/gsl/manual/html_node/Error-Function.html">https://www.gnu.org/software/gsl/manual/html_node/Error-Function.html</a></li>
<li><a rel="nofollow" class="external free" href="https://www.gnu.org/software/libc/manual/html_node/Rounding-Functions.html">https://www.gnu.org/software/libc/manual/html_node/Rounding-Functions.html</a></li>
<li><a rel="nofollow" class="external free" href="https://www.gnu.org/software/gsl/manual/html_node/Representation-of-complex-numbers.html">https://www.gnu.org/software/gsl/manual/html_node/Representation-of-complex-numbers.html</a></li></ul>
<h3><span class="mw-headline" id="Predefined_variables">Predefined variables</span></h3>
<p>There are some predefined constants for your convenience:
</p>
<dl><dt><code>pi</code></dt>
<dd><code>3.141592653589793</code>.</dd>
<dt><code>e</code></dt>
<dd>The base of the natural logarithms.</dd>
<dt><code>false</code> or <code>no</code></dt>
<dd>False.</dd>
<dt><code>true</code> or <code>yes</code></dt>
<dd>True.</dd>
<dt><code>i</code></dt>
<dd>The imaginary unit <img src="https://wikimedia.org/api/rest_v1/media/math/render/png/add78d8608ad86e54951b8c8bd6c8d8416533d20" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.338ex; width:0.802ex; height:2.176ex;" alt="{\displaystyle i}" />, <i>i.e.</i> <code>{0, 1</code>}</dd></dl>
<p>Since version 7.0 there are also some predefined units that can be found by searching for the decimal point <code>.</code> in <code>share/variables</code>:
</p>
<dl><dt><code>angstrom</code></dt>
<dd><code>1.8897261328856432</code>.</dd>
<dt><code>pm</code> or <code>picometer</code></dt>
<dd><code>0.018897261328856432</code>.</dd>
<dt><code>nm</code> or <code>nanometer</code></dt>
<dd><code>18.897261328856432</code>.</dd>
<dt><code>ry</code> or <code>rydberg</code></dt>
<dd><code>0.5</code>.</dd>
<dt><code>eV</code> or <code>electronvolt</code></dt>
<dd><code>0.03674932539796232</code>.</dd>
<dt><code>invcm</code></dt>
<dd><code>4.5563353e-06</code>.</dd>
<dt><code>kelvin</code></dt>
<dd><code> 3.1668105e-06</code>.</dd>
<dt><code>kjoule_mol</code></dt>
<dd><code> 0.00038087988</code>.</dd>
<dt><code>kcal_mol</code></dt>
<dd><code> 0.0015936014</code>.</dd>
<dt><code>as</code> or <code>attosecond</code></dt>
<dd><code>0.0413413737896</code>.</dd>
<dt><code>fs</code> or <code>femtosecond</code></dt>
<dd><code>41.3413737896</code>.</dd>
<dt><code>ps</code> or <code>picosecond</code></dt>
<dd><code>41341.3737896</code>.</dd>
<dt><code>c</code></dt>
<dd><code>137.035999139</code>.</dd></dl>
<h3><span class="mw-headline" id="Blocks">Blocks</span></h3>
<p>Blocks are defined as a collection of values, organised in row and column format.
The syntax is the following:
</p>
<pre> <code>%var</code>
 <code>  exp | exp | exp | ...</code>
 <code>  exp | exp | exp | ...</code>
 <code>  ...</code>
 <code>%</code>
</pre>
<p>Rows in a block are separated by a newline, while columns are separated by the character | or by a tab. There may be any number of lines and any number of columns in a block. Note also that each line can have a different number of columns. Values in a block don't have to be of the same type.
</p>
<h3><span class="mw-headline" id="Comments">Comments</span></h3>
<p>Everything following the character <code>#</code> until the end of the line is considered a comment and is simply cast into oblivion.
</p>
<h3><span class="mw-headline" id="Includes">Includes</span></h3>
<p>With <code>include FILENAME</code> it is possible to include external files into the input file. Here is a simple example to illustrate the usage of this command:
</p>
<table class="wikitable" style="background:white;">
<tbody><tr>
<th>inp
</th>
<th>extra.inp
</th></tr>
<tr>
<td>
<pre>a = 1
b = "string"

include extra.inp
</pre>
</td>
<td>
<pre>c = true
</pre>
</td></tr></tbody></table>
<h3><span class="mw-headline" id="Environment_variables">Environment variables</span></h3>
<p>You can also <a href="/wiki/Manual:Advanced_ways_of_running_Octopus#Passing_arguments_from_environment_variables" title="Manual:Advanced ways of running Octopus"> set variables using the environment</a>, which can be helpful in scripting.
</p>
<h3><span class="mw-headline" id="Default_values">Default values</span></h3>
<p>If Octopus tries to read a variable that is not defined in the input file, it automatically assigns to it a default value (there are some cases where Octopus cannot find a sensible default value and it will stop with an error). All variables read (present or not in the input file) are output to the file <b><tt>exec/parser.log</tt></b>. The variable that are not defined in the input file will have a <tt>#default</tt> comment to it. If you are not sure of what the program is reading, just take a look at it.
</p><p>We recommend you to keep the variables in the input file to a minimum: <i>do not write a variable that will be assigned its default value</i>. The default can change in newer versions of Octopus and old values might cause problems. Besides that, your input files become difficult to read and understand.
</p>
<h3><span class="mw-headline" id="Documentation">Documentation</span></h3>
<p>Each input variable has (or should have) its own documentation explaining what it does and the valid values it may take. This documentation can be obtained <a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php">online</a> or it can also be accessed by the <a href="/wiki/Manual:External_utilities:oct-help" title="Manual:External utilities:oct-help">oct-help</a> command.
</p>
<h3><span class="mw-headline" id="Experimental_features">Experimental features</span></h3>
<p>Even in the stable releases of Octopus there are many features that are being developed and are not suitable for production runs. To protect users from inadvertly using these parts they are declared as <i>Experimental</i>.
</p><p>When you try to use one of these experimental functionalities Octopus will stop with an error. If you want to use it you need to set the variable <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=ExperimentalFeatures"><code>ExperimentalFeatures</code></a></span> to <code>yes</code>. Now Octopus will only emit a warning.
</p><p>By setting <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Execution&amp;name=ExperimentalFeatures"><code>ExperimentalFeatures</code></a></span> to <code>yes</code> you will be allowed to use parts of the code that are not complete or not well tested and most likely produce wrong results. If you want to use them for production runs you should contact the Octopus developers first.
</p>
<h3><span class="mw-headline" id="Good_practices">Good practices</span></h3>
<p>In order to ensure compatibility with newer versions of Octopus and avoid problems, keep in mind the following rules of good practice when writing input files:
</p>
<ul><li>Although input variables that take an option as an input can also take a number, the number representation makes the input file less readable and it is likely to change in the future. So <b>avoid using numbers instead of values</b>. For example</li></ul>
<pre>UnitsOutput = ev_angstrom
</pre>
<p><i>must always</i> be used instead of
</p>
<pre>UnitsOutput = 3
</pre>
<ul><li><b>Do not include variables that are not required in the input file</b>, especially declarations of values that are just a copy of the default value. This makes the input file longer, less readable and, since defaults are likely to change, it makes more probable that your input file will have problems with newer versions of Octopus. Instead rely on default values.</li></ul>
<ul><li><b>Avoid duplicating information in the input file</b>. Use your own variables and the mathematical-interpretation capabilities for that. For example, you should use:</li></ul>
<pre>m = 0.1
c = 137.036
E = m*c^2
</pre>
<p>instead of
</p>
<pre>m = 0.1
c = 137.036
E = 1877.8865
</pre>
<p>In the second case, you might change the value of <code>m</code> (or <code>c</code> if you are a cosmologist) while forgetting to update <code>E</code>, ending up with an inconsistent file.
</p>
<span class="noprint"><hr />
<p>Previous <a href="/wiki/Manual:Installation" title="Manual:Installation">Manual:Installation</a> - Next <a href="/wiki/Manual:Running_Octopus" title="Manual:Running Octopus">Manual:Running Octopus</a>
</p><p>Back to <a href="/wiki/Manual" title="Manual">Manual</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412153843
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.523 seconds
Real time usage: 6.817 seconds
Preprocessor visited node count: 1286/1000000
Preprocessor generated node count: 3551/1000000
Post‐expand include size: 4144/2097152 bytes
Template argument size: 1411/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 4092/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00% 6699.997      1 -total
  0.54%   36.437    147 Template:Code
  0.32%   21.748      4 Template:Variable
  0.21%   14.187      5 Template:Octopus_version
  0.15%   10.103      1 Template:Manual_foot
  0.12%    8.314      6 Template:Code_line
  0.08%    5.070      6 Template:Name
  0.07%    4.917      1 Template:Foot
  0.07%    4.438      3 Template:File
  0.06%    4.197     11 Template:Octopus
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:1343-0!canonical!math=0 and timestamp 20230412153836 and revision id 11351
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Manual:Input_file&amp;oldid=11351">http:///mediawiki/index.php?title=Manual:Input_file&amp;oldid=11351</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Manual%3AInput+file&amp;returntoquery=oldid%3D11351" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Manual:Input_file" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Manual:Input_file&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Manual:Input_file">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Input_file&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Manual:Input_file&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Manual:Input_file" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Manual:Input_file" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-print"><a href="/mediawiki/index.php?title=Manual:Input_file&amp;oldid=11351&amp;printable=yes" rel="alternate" title="Printable version of this page [p]" accesskey="p">Printable version</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Manual:Input_file&amp;oldid=11351" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Manual:Input_file&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 1 September 2021, at 11:36.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.523","walltime":"6.817","ppvisitednodes":{"value":1286,"limit":1000000},"ppgeneratednodes":{"value":3551,"limit":1000000},"postexpandincludesize":{"value":4144,"limit":2097152},"templateargumentsize":{"value":1411,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":4092,"limit":5000000},"timingprofile":["100.00% 6699.997      1 -total","  0.54%   36.437    147 Template:Code","  0.32%   21.748      4 Template:Variable","  0.21%   14.187      5 Template:Octopus_version","  0.15%   10.103      1 Template:Manual_foot","  0.12%    8.314      6 Template:Code_line","  0.08%    5.070      6 Template:Name","  0.07%    4.917      1 Template:Foot","  0.07%    4.438      3 Template:File","  0.06%    4.197     11 Template:Octopus"]},"cachereport":{"timestamp":"20230412153843","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":125});});</script>
</body>
</html>
