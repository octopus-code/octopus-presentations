<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>View source for Tutorial:Slurm usage - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:Slurm_usage","wgTitle":"Tutorial:Slurm usage","wgCurRevisionId":11399,"wgRevisionId":0,"wgArticleId":3068,"wgIsArticle":false,"wgIsRedirect":false,"wgAction":"edit","wgUserName":null,"wgUserGroups":["*"],"wgCategories":[],"wgBreakFrames":true,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:Slurm_usage","wgRelevantArticleId":3068,"wgRequestId":"025cc191c97874670a9f60e6","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["mediawiki.action.edit.collapsibleFooter","site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=mediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,nofollow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_Slurm_usage rootpage-Tutorial_Slurm_usage skin-vector action-edit">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">View source for Tutorial:Slurm usage</h1>
	
	<div id="bodyContent" class="mw-body-content">
		
		<div id="contentSub">← <a href="/wiki/Tutorial:Slurm_usage" title="Tutorial:Slurm usage">Tutorial:Slurm usage</a></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text"><p>You do not have permission to edit this page, for the following reasons:
</p>
<ul class="permissions-errors">
<li>The action you have requested is limited to users in the group: Administrators.</li>
<li>You must confirm your email address before editing pages.
Please set and validate your email address through your <a href="/wiki/Special:Preferences" title="Special:Preferences">user preferences</a>.</li>
</ul><hr />
<p>You can view and copy the source of this page.
</p><textarea readonly="" accesskey="," id="wpTextbox1" cols="80" rows="25" style="" class="mw-editfont-monospace" lang="en" dir="ltr" name="wpTextbox1">== HPC systems ==
 
HPC systems provide large complex shared resources to many parallel users,
therefore central workload management is mandatory for users and admins.
This is typically achieved by using a resource manager and a job scheduler.
The resource manager knows about all the resources available on a system
and monitors the availability and load of each node. It basically manages all
resources like CPUs, memory, and GPUs in a cluster. The job scheduler
assigns compute tasks to resources and manages queues of compute tasks and
their priority. It is typically configured for best possible utilization of the
resources by given typical workloads.


== Slurm  ==

Slurm is a resource manager and job scheduler that is used by the majority of
the TOP 500 HPC systems and that is used on all HPC systems and clusters at
MPCDF. It is open source software with commercial support (Documentation:
https://slurm.schedmd.com, MPCDF HPC documentation:
https://docs.mpcdf.mpg.de/doc/computing/).

Some slurm terminology:
* Job: Reservation of resources on the system to run job steps
* Job step: program/command to be run within a job, initiated via `srun`
* Node: physical multi-core shared-memory computer, a cluster is composed of many nodes
* CPU: single processing unit (core), a node contains multiple CPUs
* Task: process (i.e. instance of a program being executed), may use one or more CPU up to all CPUs available a node, a job step may run multiple tasks in parallel over several nodes
* Partition: a “queue”, where to run jobs, defines specific resource limits or access control


== Slurm commands ==

Important slurm commands are:
* `sinfo`: show state of partitions and resources managed by slurm
* `sbatch job_script.sh`: submit a job script for later execution, obtain job_id
* `scancel job_id`: cancel a job, or send signals to tasks
* `squeue`: show state of jobs or job steps in priority order
* `srun executable`: Initiate job step, launch executable (typically used in job scripts)
* `sacct`: show information for finished jobs

You can get a list of waiting and running jobs of yourself with `squeue --me`.

You can display a concise list of partitions with `sinfo -s` (A/I/O/T means
allocated/idle/offline/total).


== Slurm jobs ==

Slurm jobs are submitted by users from the login node and then scheduled by
slurm to be executed on the compute nodes of the cluster.

Any slurm job requires:
# Specification of the resources – “what does the job need?”
#* Duration
#* Number of CPUs
#* Amount of memory
#* GPUs
#* other resources or constraints
# Definition of the job steps – “what should the job do?”
#* commands/programs to be executed via `srun`
#* typically, first some module are loaded, then the program is executed using srun

All this information is bundled in job scripts that are submitted to slurm
utilizing the `sbatch` command.


== Submitting a first job script ==

Let's create a job script to run a simple octopus calculation.

First, generate the input file called '''inp''' with a text editor (the same as
in the very first tutorial):
&lt;pre>
CalculationMode = gs

%Coordinates
 'H' | 0 | 0 | 0
%
&lt;/pre>
Second, create a job script file called **job_script.sh**:
&lt;pre>
#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J octopus_course
#
# Reservation:
#SBATCH --reservation=mpsd_course
#
# Number of MPI Tasks, e.g. 1:
#SBATCH --ntasks=1
#SBATCH --ntasks-per-core=1
# Memory usage [MB] of the job is required, 2200 MB per task:
#SBATCH --mem=2200
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# Wall clock limit:
#SBATCH --time=00:01:00

# Run the program:
module purge
module load intel/19.1.3 impi/2019.9 octopus/11
srun octopus
&lt;/pre>
For this job, output will be written to tjob.out.XXX (XXX is the job id), error
output to tjob.err.XXX. The job will run using one MPI task on one core,
requesting 2200 MB of memory. You can change the `--mail-type` option to `all`
and the `--mail-user` option to your email address to get email notifications
about changes in the job status (i.e. when the job starts and ends). The job
requests a time of one minute (`--time` option). In the script, a octopus
module is loaded and the octopus executable is started with srun. The
`--reservation` option is only needed for this course to use dedicated
resources reserved for us.

Now submit the job with `sbatch job_script.sh`. You should see an output like
&lt;pre>
Submitted batch job XXX
&lt;/pre>
where XXX is the job id. You can check the status by running `squeue --me`.

Once the calculation has finished, you can check the output by opening the file
tjob.out.XXX. Moreover, you should see the folders and files that octopus has
created, as in the first tutorial.


== More job script examples ==

To submit a parallel job on a few cores, but still on one node (cobra has 40
cores per node), you can use the options `--ntasks=8` and `--mem=17600` to run
on 8 cores, for example.

To run octopus on a full node (or several full nodes) in pure MPI mode, please
use the following job script:
&lt;pre>
#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J octopus_course
#
# Reservation:
#SBATCH --reservation=mpsd_course
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=40
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# Wall clock limit:
#SBATCH --time=00:01:00

# Run the program:
module purge
module load intel/19.1.3 impi/2019.9 octopus/11
srun octopus
&lt;/pre>
Save this file as '''job_script_mpi.sh''' and submit it with `sbatch
job_script_mpi.sh`.  This will run octopus on all 40 cores of one node. To run
on several nodes, adapt the `--nodes` option accordingly.

To run octopus in hybrid mode (MPI + OpenMP), which is suitable for large
grids, you can employ the following script:
&lt;pre>
#!/bin/bash -l
# Standard output and error:
#SBATCH -o ./tjob.out.%j
#SBATCH -e ./tjob.err.%j
# Initial working directory:
#SBATCH -D ./
# Job Name:
#SBATCH -J octopus_course
#
# Reservation:
#SBATCH --reservation=mpsd_course
#
#
# Number of nodes and MPI tasks per node:
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=10
# for OpenMP:
#SBATCH --cpus-per-task=4
#
#SBATCH --mail-type=none
#SBATCH --mail-user=userid@example.mpg.de
#
# Wall clock limit:
#SBATCH --time=00:01:00

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
# For pinning threads correctly:
export OMP_PLACES=cores

# Run the program:
module purge
module load intel/19.1.3 impi/2019.9 octopus/11
srun octopus
&lt;/pre>
This will run octopus on one full node, using 10 MPI ranks with 4 OpenMP
threads each. Exporting the environment variables is necessary to ensure
correct pinning of all processes and threads.

{{Tutorial_foot|series=Running Octopus on HPC systems|prev=MPCDF Systems|next=Parallelization in octopus}}
</textarea><div class="templatesUsed"><div class="mw-templatesUsedExplanation"><p>Templates used on this page:
</p></div><ul>
<li><a href="/wiki/Template:If" title="Template:If">Template:If</a> (<a href="/mediawiki/index.php?title=Template:If&amp;action=edit" title="Template:If">view source</a>) </li><li><a href="/wiki/Template:P1" title="Template:P1">Template:P1</a> (<a href="/mediawiki/index.php?title=Template:P1&amp;action=edit" title="Template:P1">view source</a>) </li><li><a href="/wiki/Template:P2" title="Template:P2">Template:P2</a> (<a href="/mediawiki/index.php?title=Template:P2&amp;action=edit" title="Template:P2">view source</a>) </li><li><a href="/wiki/Template:Tutorial_foot" title="Template:Tutorial foot">Template:Tutorial foot</a> (<a href="/mediawiki/index.php?title=Template:Tutorial_foot&amp;action=edit" title="Template:Tutorial foot">view source</a>) </li></ul></div><p id="mw-returnto">Return to <a href="/wiki/Tutorial:Slurm_usage" title="Tutorial:Slurm usage">Tutorial:Slurm usage</a>.</p>
</div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///wiki/Tutorial:Slurm_usage">http:///wiki/Tutorial:Slurm_usage</a>"</div>
		
		<div id="catlinks" class="catlinks catlinks-allhidden" data-mw="interface"></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ASlurm+usage&amp;returntoquery=action%3Dedit" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:Slurm_usage" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:Slurm_usage&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible"><span><a href="/wiki/Tutorial:Slurm_usage">Read</a></span></li><li id="ca-viewsource" class="collapsible selected"><span><a href="/mediawiki/index.php?title=Tutorial:Slurm_usage&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:Slurm_usage&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:Slurm_usage" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:Slurm_usage" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:Slurm_usage&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":132});});</script>
</body>
</html>
