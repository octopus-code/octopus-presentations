<!DOCTYPE html>
<html class="client-nojs" lang="en" dir="ltr">
<head>
<meta charset="UTF-8"/>
<title>Tutorial:RDMFT - OctopusWiki</title>
<script>document.documentElement.className = document.documentElement.className.replace( /(^|\s)client-nojs(\s|$)/, "$1client-js$2" );</script>
<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":false,"wgNamespaceNumber":0,"wgPageName":"Tutorial:RDMFT","wgTitle":"Tutorial:RDMFT","wgCurRevisionId":11183,"wgRevisionId":11183,"wgArticleId":3026,"wgIsArticle":true,"wgIsRedirect":false,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Tutorial","Advanced","RDMFT"],"wgBreakFrames":false,"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRelevantPageName":"Tutorial:RDMFT","wgRelevantArticleId":3026,"wgRequestId":"514d97791b34cdc02daa8ef7","wgCSPNonce":false,"wgIsProbablyEditable":false,"wgRelevantPageIsProbablyEditable":false,"wgRestrictionEdit":[],"wgRestrictionMove":[]});mw.loader.state({"site.styles":"ready","noscript":"ready","user.styles":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.math.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"});mw.loader.implement("user.tokens@0tffind",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"editToken":"+\\","patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});RLPAGEMODULES=["site","mediawiki.page.startup","mediawiki.page.ready","mediawiki.searchSuggest","skins.vector.js"];mw.loader.load(RLPAGEMODULES);});</script>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=ext.math.styles%7Cmediawiki.legacy.commonPrint%2Cshared%7Cmediawiki.skinning.interface%7Cskins.vector.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<script async="" src="/mediawiki/load.php?lang=en&amp;modules=startup&amp;only=scripts&amp;printable=1&amp;skin=vector"></script>
<meta name="ResourceLoaderDynamicStyles" content=""/>
<link rel="stylesheet" href="/mediawiki/load.php?lang=en&amp;modules=site.styles&amp;only=styles&amp;printable=1&amp;skin=vector"/>
<meta name="generator" content="MediaWiki 1.33.0"/>
<meta name="robots" content="noindex,follow"/>
<link rel="shortcut icon" href="/favicon.ico"/>
<link rel="search" type="application/opensearchdescription+xml" href="/mediawiki/opensearch_desc.php" title="OctopusWiki (en)"/>
<link rel="EditURI" type="application/rsd+xml"/>
<link rel="alternate" type="application/atom+xml" title="OctopusWiki Atom feed" href="/mediawiki/index.php?title=Special:RecentChanges&amp;feed=atom"/>
<!--[if lt IE 9]><script src="/mediawiki/load.php?lang=en&amp;modules=html5shiv&amp;only=scripts&amp;skin=vector&amp;sync=1"></script><![endif]-->
</head>
<body class="mediawiki ltr sitedir-ltr mw-hide-empty-elt ns-0 ns-subject page-Tutorial_RDMFT rootpage-Tutorial_RDMFT skin-vector action-view">
<div id="mw-page-base" class="noprint"></div>
<div id="mw-head-base" class="noprint"></div>
<div id="content" class="mw-body" role="main">
	<a id="top"></a>
	
	<div class="mw-indicators mw-body-content">
</div>

	<h1 id="firstHeading" class="firstHeading" lang="en">Tutorial:RDMFT</h1>
	
	<div id="bodyContent" class="mw-body-content">
		<div id="siteSub" class="noprint">From OctopusWiki</div>
		<div id="contentSub"></div>
		
		
		
		<div id="jump-to-nav"></div>
		<a class="mw-jump-link" href="#mw-head">Jump to navigation</a>
		<a class="mw-jump-link" href="#p-search">Jump to search</a>
		<div id="mw-content-text" lang="en" dir="ltr" class="mw-content-ltr"><div class="mw-parser-output"><p>In this tutorial, you will learn how to do a Reduced Density Matrix Functional Theory (RDMFT) calculation with Octopus. 
</p><p>In contrast to density functional theory or Hartree-Fock theory, here we do <b>not</b> try to find one optimal slater-determinant (single-reference) to describe the ground-state of a given many-body system, but instead approximate the one-body reduced density matrix (1RDM) of the system. Thus, the outcome of an RDMFT minimization is a set of eigen-vectors, so called <i>natural orbitals</i> (NOs,) and eigenvalues, so-called <i>natural occupation numbers</i> (NONs), of the 1RDM. One important aspect of this is that we need to use <i>more</i> orbitals than the number of electrons. This additional freedom allows to include static correlation in the description of the systems and hence, we can describe settings or processes that are very difficult for single-reference methods. One such process is the <b>dissociation of a molecule</b>, which we utilize as example for this tutorial: You will learn how to do a properly converged dissociation study of H<img src="https://wikimedia.org/api/rest_v1/media/math/render/png/98f5231ce90e0471b98ebc29a79b1cc769e992bd" class="mwe-math-fallback-image-inline" aria-hidden="true" style="vertical-align: -0.671ex; width:1.054ex; height:1.676ex;" alt="{\displaystyle _{2}}" />. For further information about RDMFT, we can recommend e.g. chapter <i>Reduced Density Matrix Functional Theory (RDMFT) and Linear Response Time-Dependent RDMFT (TD-RDMFT)</i> in the book of Ferre, N., Filatov, M. &amp; Huix-Rotllant, M. <i>Density-Functional Methods for Excited States</i> (Springer, 2015). doi:10.1007/978-3-319-22081-9 
</p>
<h2><span class="mw-headline" id="Basic_Steps_of_an_RDMFT_Calculation">Basic Steps of an RDMFT Calculation</span></h2>
<p>The first thing to notice for RDMFT calculations is that, we will explicitly make use of a <b>basis set</b>, which is in contrast to the minimization over the full real-space grid that is performed normally with Octopus. Consequently, we always need to do a preliminary calculation to generate our basis set. We will exemplify this for the hydrogen molecule in equilibrium position. Create the following <b><tt>inp</tt></b> file:
</p>
<pre><span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Calculation_Modes&amp;name=CalculationMode"><code>CalculationMode</code></a></span> = gs
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Hamiltonian&amp;name=TheoryLevel"><code>TheoryLevel</code></a></span> = independent_particles

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Section&amp;name=Dimensions"><code>Dimensions</code></a></span> = 3
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 8
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.15

# distance between H atoms
d = 1.4172 # (equilibrium bond length H2)

%<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=System&amp;name=Coordinates"><code>Coordinates</code></a></span>
 'H' | 0 | 0 | -d/2
 'H' | 0 | 0 | d/2
%

<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> = 14
</pre>
<p>You should be already familiar with all these variables, so there is no need to explain them again, but we want to stress the two important points here: 
</p>
<ol><li>We chose <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Hamiltonian&amp;name=TheoryLevel"><code>TheoryLevel</code></a></span> = independent_particles, which you should always use as a basis in an RDMFT calculation (see actual octopus paper?)</li>
<li>We set the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> variable, which controls the size of the basis set that will be used in the RDMFT calculation. Thus in RDMFT, we have with the <b>basis set</b> a <b>new numerical parameter</b> besides the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> and the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> that needs to be converged for a successful calculation. The size of the basis set M is just the number of the orbitals for the ground state (number of electrons divided by 2) plus the number of extra states.</li>
<li>All the numerical parameters <b>depend on each other</b>: If we want to include many <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span>  to have a sufficiently large basis, we will need also a larger <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> and especially a smaller <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> at a certain point. The reason is that the additional states will have function values bigger than zero in a larger region than the bound states. Additionally, all states are orthogonal to each other, and thus the grid needs to resolve more nodes with an increasing number of <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span>.</li></ol>
<p>With this basis, we can now do our first rdmft calculation. For that, you just need to change the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Hamiltonian&amp;name=TheoryLevel"><code>TheoryLevel</code></a></span> to 'rdmft' and <b>add the part <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Section&amp;name=ExperimentalFeatures"><code>ExperimentalFeatures</code></a></span> = yes</b>. In the standard out there will be some new information:
</p>
<ul><li>Calculating Coulomb and exchange matrix elements in basis --this may take a while--</li>
<li>Occupation numbers: they are not zero or one/two here but instead fractional!</li>
<li><b>anything else important?</b></li></ul>
<h2><span class="mw-headline" id="Basis_Set_Convergence">Basis Set Convergence</span></h2>
<p>Now, we need to converge our calculation for the three numerical parameters. This needs to be done in some iterative way, which means we first set <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> and then do convergence series for <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> as we know it. Then, we utilize the converged values and perform a series for <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span>. Having converged <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span>, we start again with the series for <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> and see if the energy (or better the electron density) still changes considerably. If yes, this meas that the higher lying <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> where net well captured by the simulation box and we need to adjust the parameters again. We go on like this until convergence of all parameters. This sound like a lot of work, but don't worry, if you have done such convergence studies a few times, you will get a feeling for good values.
</p><p>So let us do one iteration as example. We start with a guess for <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span>, which should not be too small, say 14 (thus M=1+14=15) and converge <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span>  and <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span>. To save time, we did this for you and found the values
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Radius"><code>Radius</code></a></span> = 8
<span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=Spacing"><code>Spacing</code></a></span> = 0.15.
If you feel insecure with such convergence studies, you should have a look in the corresponding tutorial: <a href="/wiki/Tutorial:Total_energy_convergence" title="Tutorial:Total energy convergence">Tutorial:Total_energy_convergence</a>
</p><p>Now let us perform a basis-set series. For that, we first create reference basis by repeating the above calculation with say <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span> = 29. We copy the restart folder in a new folder that is called 'basis' and execute the following script (if you rename anything you need to change the respective part in the script):
</p>
<pre>#!/bin/bash

series=ES-series
outfile="$series.log"
echo "#ES    Energy    1. NON  2.NON" &gt; $outfile
list="4 9 14 19 24 29"

export OCT_PARSE_ENV=1 
for param in $list 
do
  folder="$series-$param"
  mkdir $folder
  out_tmp=out-RDMFT-$param
  cd $folder
   # here we specify the basis folder
   cp -r ../basis/restart .
   # create inp file
   {
cat &lt;&lt;-EOF
calculationMode = gs
TheoryLevel = rdmft
ExperimentalFeatures = yes

ExtraStates = $param

Dimensions = 3

Radius = 8
Spacing = 0.15

# distance between H atoms
d = 1.4172 # (equilibrium) 

%Coordinates
  'H' | 0 | 0 | -d/2
  'H' | 0 | 0 | d/2
%

Output = density
OutputFormat = axis_x
EOF
} &gt; inp
 
  # put the octopus dir here
  [octopus-dir]/bin/octopus &gt; $out_tmp

   energy=`grep -a "Total energy" $out_tmp  | tail -1 | awk '{print $3}'`
   seigen=`grep -a " 1  " static/info |  awk '{print $2}'`
   peigen=`grep -a " 2  " static/info |  awk '{print $2}'`
   echo $param $energy $seigen $peigen &gt;&gt; ../$outfile
 cd ..
done
</pre>
<p>If everything works out fine, you should find the following output in the 'ES-series.log' file:
</p>
<pre>#ES    Energy    1. NON  2.NON
4 -1.1476150018E+00 1.935750757008 0.032396191164
9 -1.1498006205E+00 1.932619193929 0.032218954381
14 -1.1609241676E+00 1.935215440985 0.032526426664
19 -1.1610006378E+00 1.934832116929 0.032587169713
24 -1.1622104536E+00 1.932699204653 0.032997371081
29 -1.1630839347E+00 1.932088486112 0.032929702131
</pre>
<p>So we see that even with M=30, the calculation is not entirely converged! Thus for a correctly converged result, one would need to further increase the basis and optimize the simulation box. Since the calculations become quite expensive, we would need to do them on a high-performance cluster, which goes beyond the scope of this tutorial. We will thus have to proceed with <b>not entirely converged</b> parameters in the next section.
</p>
<h2><span class="mw-headline" id="H2_Dissociation">H2 Dissociation</span></h2>
<p>In this third and last part of the tutorial, we come back to our original goal: the calculation of the H2 dissociation curve. Now, we want to change the <i>d</i>-parameter in the input file and consequently, we should use an adapted simulation box. Most suited for a dissociation, is the <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=BoxShape"><code>BoxShape</code></a></span>= cylinder. However, since this makes the calculations again much more expensive, we just stick to the default choice of <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=Mesh&amp;name=BoxShape"><code>BoxShape</code></a></span>=minimal.
</p><p>We choose <span class="plainlinks"><a rel="nofollow" class="external text" href="http://octopus-code.org/doc/12.0/html/vars.php?section=States&amp;name=ExtraStates"><code>ExtraStates</code></a></span>=14 as a compromise between accuracy and numerical cost and do the d-series with the following script:
</p>
<pre>#!/bin/bash

series=dist-series
outfile="$series.log"
echo "#ES    Energy    1. NON  2.NON" &gt; $outfile
list="0.25 0.5 0.75 1.0 1.5 2.0 2.5 3.0 4.0 5.0 6.0 8.0"

export OCT_PARSE_ENV=1 
for param in $list 
do
 folder="$series-$param"
 mkdir $folder
 out_tmp=out-$series-$param
 cd $folder

 # create inp file
{
cat &lt;&lt;-EOF
calculationMode = gs
TheoryLevel = rdmft
ExperimentalFeatures = yes

ExtraStates = 14

Dimensions = 3

Radius = 8
Spacing = 0.15

# distance between H atoms
d = $param

%Coordinates
  'H' | 0 | 0 | -d/2
  'H' | 0 | 0 | d/2
%

Output = density
OutputFormat = axis_x
EOF
} &gt; inp
 
 # put the octopus dir here
 oct_dir=~/octopus-developer/bin  
 
 # generate the basis
 #export OCT_TheoryLevel=independent_particles
 #${oct_dir}/octopus &gt; ${out_tmp}_basis
 
 # do the actual rdmft calculation
 #export OCT_TheoryLevel=rdmft
 #${oct_dir}/octopus &gt; ${out_tmp}

 energy=`grep -a "Total energy" $out_tmp  | tail -1 | awk '{print $3}'`
 non1=`grep -a " 1  " static/info |  awk '{print $2}'`
 non2=`grep -a " 2  " static/info |  awk '{print $2}'`
 echo $param $energy $non1 $non2 &gt;&gt; ../$outfile
 cd ..
done
</pre>
<p>The execution of the script should take about 30-60 minutes on a normal machine, so now it's time for well-deserved coffee!
</p><p>After the script has finished, you have find a new log file and if you plot the energy of the files, you should get something of the following shape:
</p>
<div class="thumb tright"><div class="thumbinner" style="width:302px;"><a href="/wiki/File:H2_diss_curve.png" class="image"><img alt="" src="/mediawiki/images/thumb/e/ed/H2_diss_curve.png/300px-H2_diss_curve.png" decoding="async" width="300" height="300" class="thumbimage" srcset="/mediawiki/images/thumb/e/ed/H2_diss_curve.png/450px-H2_diss_curve.png 1.5x, /mediawiki/images/thumb/e/ed/H2_diss_curve.png/600px-H2_diss_curve.png 2x" /></a>  <div class="thumbcaption"><div class="magnify"><a href="/wiki/File:H2_diss_curve.png" class="internal" title="Enlarge"></a></div>Dissociation curve of the Hydrogen molecule, calculated with RDMFT using the Müller functional.</div></div></div>
<p>Comments:
</p>
<ul><li>This dissociation limit is not correct. Since the basis set is not converged, we get a too large value of E_diss=-1.009 than the Müller functional obtains for a converged set (E_diss=-1.047 Hartree), which is paradoxically closer to the exact dissociation limit of E_diss=1.0 Hartree because of an error cancellation (too small bases always increase the energy).</li>
<li>The principal shape is good. Especially, we see that the curve becomes constant for large d, which single-reference methods cannot reproduce.</li></ul>
<span class="noprint"><hr />
<p>Back to <a href="/wiki/Tutorials" title="Tutorials">Tutorials</a>
</p></span>
<!-- 
NewPP limit report
Cached time: 20230412155133
Cache expiry: 86400
Dynamic content: false
CPU time usage: 0.174 seconds
Real time usage: 0.541 seconds
Preprocessor visited node count: 706/1000000
Preprocessor generated node count: 1142/1000000
Post‐expand include size: 5570/2097152 bytes
Template argument size: 1140/2097152 bytes
Highest expansion depth: 4/40
Expensive parser function count: 0/100
Unstrip recursion depth: 0/20
Unstrip post‐expand size: 259/5000000 bytes
-->
<!--
Transclusion expansion time report (%,ms,calls,template)
100.00%  451.052      1 -total
 16.89%   76.173     34 Template:Variable
  7.49%   33.782     34 Template:Octopus_version
  2.74%   12.368     34 Template:Code
  1.29%    5.810     34 Template:Octopus_minor_version
  1.28%    5.779     34 Template:Octopus_major_version
  1.05%    4.734      1 Template:File
  0.81%    3.657      2 Template:Octopus
-->

<!-- Saved in parser cache with key wikidb-oct_:pcache:idhash:3026-0!canonical!math=0 and timestamp 20230412155132 and revision id 11183
 -->
</div></div>
		
		<div class="printfooter">Retrieved from "<a dir="ltr" href="http:///mediawiki/index.php?title=Tutorial:RDMFT&amp;oldid=11183">http:///mediawiki/index.php?title=Tutorial:RDMFT&amp;oldid=11183</a>"</div>
		
		<div id="catlinks" class="catlinks" data-mw="interface"><div id="mw-normal-catlinks" class="mw-normal-catlinks"><a href="/wiki/Special:Categories" title="Special:Categories">Categories</a>: <ul><li><a href="/wiki/Category:Tutorial" title="Category:Tutorial">Tutorial</a></li><li><a href="/wiki/Category:Advanced" title="Category:Advanced">Advanced</a></li><li><a href="/wiki/Category:RDMFT" title="Category:RDMFT">RDMFT</a></li></ul></div></div>
		
		<div class="visualClear"></div>
		
	</div>
</div>

		<div id="mw-navigation">
			<h2>Navigation menu</h2>
			<div id="mw-head">
									<div id="p-personal" role="navigation" aria-labelledby="p-personal-label">
						<h3 id="p-personal-label">Personal tools</h3>
						<ul>
							<li id="pt-login"><a href="/mediawiki/index.php?title=Special:UserLogin&amp;returnto=Tutorial%3ARDMFT&amp;returntoquery=printable%3Dyes" title="You are encouraged to log in; however, it is not mandatory [o]" accesskey="o">Log in</a></li><li id="pt-createaccount"><a href="/wiki/Special:RequestAccount" title="You are encouraged to create an account and log in; however, it is not mandatory">Request account</a></li>						</ul>
					</div>
									<div id="left-navigation">
										<div id="p-namespaces" role="navigation" class="vectorTabs" aria-labelledby="p-namespaces-label">
						<h3 id="p-namespaces-label">Namespaces</h3>
						<ul>
							<li id="ca-nstab-main" class="selected"><span><a href="/wiki/Tutorial:RDMFT" title="View the content page [c]" accesskey="c">Page</a></span></li><li id="ca-talk" class="new"><span><a href="/mediawiki/index.php?title=Talk:Tutorial:RDMFT&amp;action=edit&amp;redlink=1" rel="discussion" title="Discussion about the content page (page does not exist) [t]" accesskey="t">Discussion</a></span></li>						</ul>
					</div>
										<div id="p-variants" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-variants-label">
												<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-variants-label" />
						<h3 id="p-variants-label">
							<span>Variants</span>
						</h3>
						<ul class="menu">
													</ul>
					</div>
									</div>
				<div id="right-navigation">
										<div id="p-views" role="navigation" class="vectorTabs" aria-labelledby="p-views-label">
						<h3 id="p-views-label">Views</h3>
						<ul>
							<li id="ca-view" class="collapsible selected"><span><a href="/wiki/Tutorial:RDMFT">Read</a></span></li><li id="ca-viewsource" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;action=edit" title="This page is protected.&#10;You can view its source [e]" accesskey="e">View source</a></span></li><li id="ca-history" class="collapsible"><span><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;action=history" title="Past revisions of this page [h]" accesskey="h">View history</a></span></li>						</ul>
					</div>
										<div id="p-cactions" role="navigation" class="vectorMenu emptyPortlet" aria-labelledby="p-cactions-label">
						<input type="checkbox" class="vectorMenuCheckbox" aria-labelledby="p-cactions-label" />
						<h3 id="p-cactions-label"><span>More</span></h3>
						<ul class="menu">
													</ul>
					</div>
										<div id="p-search" role="search">
						<h3>
							<label for="searchInput">Search</label>
						</h3>
						<form action="/mediawiki/index.php" id="searchform">
							<div id="simpleSearch">
								<input type="search" name="search" placeholder="Search OctopusWiki" title="Search OctopusWiki [f]" accesskey="f" id="searchInput"/><input type="hidden" value="Special:Search" name="title"/><input type="submit" name="fulltext" value="Search" title="Search the pages for this text" id="mw-searchButton" class="searchButton mw-fallbackSearchButton"/><input type="submit" name="go" value="Go" title="Go to a page with this exact name if it exists" id="searchButton" class="searchButton"/>							</div>
						</form>
					</div>
									</div>
			</div>
			<div id="mw-panel">
				<div id="p-logo" role="banner"><a class="mw-wiki-logo" href="/wiki/Main_Page" title="Visit the main page"></a></div>
						<div class="portal" role="navigation" id="p-navigation" aria-labelledby="p-navigation-label">
			<h3 id="p-navigation-label">Navigation</h3>
			<div class="body">
								<ul>
					<li id="n-mainpage-description"><a href="/wiki/Main_Page" title="Visit the main page [z]" accesskey="z">Main page</a></li><li id="n-recentchanges"><a href="/wiki/Special:RecentChanges" title="A list of recent changes in the wiki [r]" accesskey="r">Recent changes</a></li><li id="n-randompage"><a href="/wiki/Special:Random" title="Load a random page [x]" accesskey="x">Random page</a></li><li id="n-help-mediawiki"><a href="https://www.mediawiki.org/wiki/Special:MyLanguage/Help:Contents">Help about MediaWiki</a></li>				</ul>
							</div>
		</div>
			<div class="portal" role="navigation" id="p-tb" aria-labelledby="p-tb-label">
			<h3 id="p-tb-label">Tools</h3>
			<div class="body">
								<ul>
					<li id="t-whatlinkshere"><a href="/wiki/Special:WhatLinksHere/Tutorial:RDMFT" title="A list of all wiki pages that link here [j]" accesskey="j">What links here</a></li><li id="t-recentchangeslinked"><a href="/wiki/Special:RecentChangesLinked/Tutorial:RDMFT" rel="nofollow" title="Recent changes in pages linked from this page [k]" accesskey="k">Related changes</a></li><li id="t-specialpages"><a href="/wiki/Special:SpecialPages" title="A list of all special pages [q]" accesskey="q">Special pages</a></li><li id="t-permalink"><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;oldid=11183" title="Permanent link to this revision of the page">Permanent link</a></li><li id="t-info"><a href="/mediawiki/index.php?title=Tutorial:RDMFT&amp;action=info" title="More information about this page">Page information</a></li>				</ul>
							</div>
		</div>
				</div>
		</div>
				<div id="footer" role="contentinfo">
						<ul id="footer-info">
								<li id="footer-info-lastmod"> This page was last edited on 14 October 2020, at 15:36.</li>
							</ul>
						<ul id="footer-places">
								<li id="footer-places-privacy"><a href="/wiki/OctopusWiki:Privacy_policy" title="OctopusWiki:Privacy policy">Privacy policy</a></li>
								<li id="footer-places-about"><a href="/wiki/OctopusWiki:About" title="OctopusWiki:About">About OctopusWiki</a></li>
								<li id="footer-places-disclaimer"><a href="/wiki/OctopusWiki:General_disclaimer" title="OctopusWiki:General disclaimer">Disclaimers</a></li>
							</ul>
										<ul id="footer-icons" class="noprint">
										<li id="footer-poweredbyico">
						<a href="//www.mediawiki.org/"><img src="/mediawiki/resources/assets/poweredby_mediawiki_88x31.png" alt="Powered by MediaWiki" srcset="/mediawiki/resources/assets/poweredby_mediawiki_132x47.png 1.5x, /mediawiki/resources/assets/poweredby_mediawiki_176x62.png 2x" width="88" height="31"/></a>					</li>
									</ul>
						<div style="clear: both;"></div>
		</div>
		

<script>(window.RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.174","walltime":"0.541","ppvisitednodes":{"value":706,"limit":1000000},"ppgeneratednodes":{"value":1142,"limit":1000000},"postexpandincludesize":{"value":5570,"limit":2097152},"templateargumentsize":{"value":1140,"limit":2097152},"expansiondepth":{"value":4,"limit":40},"expensivefunctioncount":{"value":0,"limit":100},"unstrip-depth":{"value":0,"limit":20},"unstrip-size":{"value":259,"limit":5000000},"timingprofile":["100.00%  451.052      1 -total"," 16.89%   76.173     34 Template:Variable","  7.49%   33.782     34 Template:Octopus_version","  2.74%   12.368     34 Template:Code","  1.29%    5.810     34 Template:Octopus_minor_version","  1.28%    5.779     34 Template:Octopus_major_version","  1.05%    4.734      1 Template:File","  0.81%    3.657      2 Template:Octopus"]},"cachereport":{"timestamp":"20230412155133","ttl":86400,"transientcontent":false}}});mw.config.set({"wgBackendResponseTime":122});});</script>
</body>
</html>
