%\documentclass[palatino]{beamer}
\documentclass[palatino,handout]{beamer}

%%%%%%%%%%%%%% MACROS AND STUFF %%%%%%%%%%%%%%
\mode<presentation>

\usetheme{Madrid}

\definecolor{mycolor}{RGB}{67,130,176}
\usecolortheme[named=mycolor]{structure}
\setbeamercolor{normal text}{fg=violet!20!black}

\useoutertheme[footline=authortitle,subsection=false]{miniframes}
%\useoutertheme{default}
\setbeamertemplate{navigation symbols}{} 

\usefonttheme[onlymath]{serif}


\usepackage[english]{babel}

\usepackage{graphicx,amsmath,amsfonts,amssymb,mathrsfs,bm}
\usepackage[normalem]{ulem}
\usepackage{pgfpages}
\usepackage{tikz}
\usetikzlibrary{arrows,shadows,decorations.pathmorphing,backgrounds,positioning,fit,petri,calc,shapes}
\usepackage{smartdiagram}

\newenvironment{specialframe}{\begin{frame}[fragile,environment=specialframe]}{\end{frame}}


%%%%%%%%%%%%%% TITLE PAGE %%%%%%%%%%%%%%

\title[]{Octopus Development Roadmap}

\author[M.~Oliveira]{}

\institute[]{AKA Micael's \texorpdfstring{\sout{TODO list}}{} wishlist}

\date[Octopus 2022]{\it January 11, 2022}

\begin{document}

\maketitle


\frame{\frametitle{Disclaimer}

\begin{itemize}
 \item These discussions are NOT about specific scientific projects!
 \item Most likely missing many things. Help me fix that!
 \item Order and grouping of topics is a bit arbitrary
\end{itemize}

}


\frame{\frametitle{Goals}

\begin{center}
 Enable cutting edge research
\end{center}

\vskip0.5cm

\onslide<2->

Octopus should be:
\vskip0.1cm
\begin{enumerate}
 \item Developer friendly (easy to extend and maintain)
 \item Reliable and well tested
 \item Fast
 \item User friendly
\end{enumerate}

}


\frame{\frametitle{Strategy}

\begin{itemize}
 \item Hide complexity
 \item Use external libraries
 \item Stick with standard Fortran as much as possible
 \item Systematically test and validate code
 \item Avoid technical debt
 \item Have complete and well written documentation
\end{itemize}

}

\frame{\frametitle{General stuff}

\begin{itemize}[<+->]
  \item Use more Fortran and less preprocessor:
  \begin{itemize}
   \item Remove MAX\_DIM
   \item Replace FLOAT/CMPLX with real(DP) and complex(DP)
  \end{itemize}
  \item Use Fortran 2003 bindings for C interfaces
  \item Use Fortran 2008 MPI-3 interface
  \item Physical constants, like the electron charge, should be included in the code explicitly
\end{itemize}

}

\frame{\frametitle{Build system}

\begin{itemize}[<+->]
  \item Change macros to have a consistent behavior
  \item Allow usage of Fortran submodules
  \item Allow for use of external Spglib and DFTD3
  \item Fix macro to detect GD library
\end{itemize}

}

\frame{\frametitle{Buildbot}

\begin{itemize}[<+->]
  \item Some jobs get stuck in a deadlock, clogging the tentacles
  \item Fix Intel warnings and add a new builder to enforce this
  \item Catch runtime warnings (e.g., temporary arrays)
  \item Builders for specific Linux distributions and for MacOS
  \item Nightly Valgrind long tests and MPI
  \item Builder to check coding standards
  \item Make on-demand builders more flexible (custom compiler flags, etc)
  \item Rewrite configuration in a more object-oriented way
  \item Centralize toolchain definitions in one JSON file
\end{itemize}

}

\frame{\frametitle{Testsuite}

\begin{itemize}[<+->]
  \item Rewrite all scripts in Python
  \item Improve code-coverage
  \item More unit tests
  \item Run unit tests separately from regression tests
  \item Each unit test should be defined in the Fortran module it is supposed to test
  \item Write total runtime per test to a file
  \item Split tests whenever runs are unrelated or depend on a common step that is very fast to run
  \item Improve app:
  \begin{itemize}
   \item Check for tests with tolerances much larger than dispersion
   \item Check for reference values very different from calculated mean
   \item Detect number of digits that were parsed
   \item Allow to combine results from several runs to determine tolerances?
  \end{itemize}
\end{itemize}

}

\frame{\frametitle{Performance Testsuite}

\begin{itemize}[<+->]
  \item Need to restart development of the app
  \item Decide on metric for performance
  \item Change how jobs are submitted (concurrent repetitions instead of concurrent tests)
  \item Tune parameter space
  \item Add full ground-state and time-dependent runs
\end{itemize}

}


\frame{\frametitle{Webpage}

\begin{itemize}[<+->]
  \item Switch to new page ASAP
  \item New tutorials
\end{itemize}

}



\frame{\frametitle{Grids}

\begin{itemize}[<+->]
  \item Reorganize cube, grid, mesh and submesh into an hierarchy of classes:
  \begin{enumerate}
   \item Simple grid (list of coordinates + indexing)
   \item Submesh (simple grid + map to parent grid)
   \item Distributed grid
   \item Grid + finite differences
   \item Cube (grid with special point ordering)
   \item Multigrids
  \end{enumerate}
  \item Fix, test and benchmark curvilinear grids
  \item Implement mesh to mesh transfers
  \item Finish cleanup of boxes, systems of coordinates, lattice vectors, etc.
\end{itemize}

}


\frame{\frametitle{Batches and mesh functions}

\begin{itemize}[<+->]
  \item Batchify all code that can be batchified
  \item Isolate batches from the rest of the code
  \item Make mesh functions a class
  \item GPU kernels for mesh functions
  \item The ``dream'': write ``a = b + c''
  \begin{itemize}
   \item without knowing if a, b, c are batches or mesh functions
   \item without knowing if a, b, c are on the GPU or on the CPU
   \item without loss of performance
  \end{itemize}

\end{itemize}

}

\frame{\frametitle{Hamiltonian and operators}

\begin{itemize}[<+->]
  \item Introduce an operator class
  \item The Hamiltonian is an operator made of a list of operators
  \item Operators would have a ``start'' and ``end'' methods to apply them
  \item Some operators communicate data and must be executed first so that we can overlap communications with computations
  \item Add a way of combining similar operators (e.g., local potential, etc)
\end{itemize}

}




\frame{\frametitle{Pseudopotentials}

 \begin{itemize}[<+->]
 \item LCAO is critical for convergence of some systems
 \item Often we do not have (good) atomic wavefunctions or densities
 \item Couple Octopus with APE (libAPE?) so that we can compute accurate atomic wavefunctions in all cases
 \item Check (again) filtering
 \item Consider reimplementing double-grid
\end{itemize}

}



\frame{\frametitle{Multisystem Implementation: Ions}

\begin{itemize}[<+->]
 \item More work to disentanble ions from electrons
 \begin{itemize}
  \item Remove indirect dependencies of ions in grid
  \item Separate initialization of ions and electrons
  \item Properly implement electron-ion interactions
 \item Move ions out of electrons
 \end{itemize}
 \item Break appart ion dynamics module
  \item Move classical atoms to their own system (not the same as charged particles!)
  \item What to do with jelliums?
  \item Implement ion-ion and ion-electron interactions
\end{itemize}

}




\frame{\frametitle{Multisystem Implementation: Framework}

\begin{itemize}[<+->]
 \item Attach clocks to energies and use them during update of energies
 \item Interaction extrapolation (handled by the force, potential, etc base classes)
 \item Fix handling of static quantities
 \item Introduce self-consistent propagators
 \item Introduce minimization algorithms:
 \begin{itemize}
  \item SCF
  \item Geometry optimization
 \end{itemize}
 \item Add method to clone systems (important for multi-trajectory)
\end{itemize}

}



\frame{\frametitle{Multisystem Implementation: Output}

\begin{itemize}[<+->]
  \item Specialize output for each system
  \item System-dependent documentation of input options is essential
  \item How to separate output of scalar and vector fields from other quantities?
\end{itemize}

}



\frame{\frametitle{Multisystem Implementation: Parallelization}

\begin{itemize}[<+->]
  \item Introduce new parallelization level
  \item Generalize multicomm (classes?)
  \item Interactions and systems get data through one-sided communications (MPI window)
  \item Each MPI process must instantiate all systems and interactions
  \item ``Dummy'' systems and interactions are place-holders for data received from the ``live'' systems and interactions
  \item Clocks must also be communicated!
\end{itemize}

}



\end{document}
