\frame{\frametitle{Motivation}

  \begin{itemize}[<+->]
   \item After 20 years of development, the current code structure is starting to show its limits
   \item New developments are becoming more difficult
   \item Fortran 2003 introduces lots of new OOP features
   \item Several ``multi-system'' features were very hard to implement and maintain:
   \begin{itemize}
    \item Subsystem DFT
    \item Maxwell solver
    \item Electronic transport
    \item ...
   \end{itemize}
  \end{itemize}

 \onslide<+->
 In 2019 it was decided to introduce a new framework and rewrite large portions of Octopus.

}


\frame{\frametitle{What problem are we trying to solve?}

 \begin{center}
  \includegraphics[width=0.8\textwidth]{img/octopus-interactions}
 \end{center}

}


\begin{specialframe}\frametitle{What problem are we trying to solve?}
 
 \begin{itemize}[<+->]
  \item We want to solve a system of \textbf{coupled} differential equations
  \item How to handle arbitrary numbers of equations?
  \item How to add/remove equations ``on-the-fly'?
  \item How to activate/deactivate couplings ``on-the-fly''?
 \end{itemize}

\end{specialframe}


\begin{specialframe}\frametitle{How to code this?}
 
 The way \alert{NOT} to do it:
 \lstset{language=Fortran}
 \begin{lstlisting}
if (system_A%is_electrons) then
...
else if (system_A%is_ions) then
...
end if

if (system_A%has_interaction_X_with_system_B) then
...
end if

if (system_B%has_interaction_X_with_system_A) then
...
end if

if ((system_A%has_interaction_Y_with_system_B) then
...
end if
 \end{lstlisting}

\end{specialframe}


\frame{\frametitle{Multi-system framework: Key features}

 \begin{itemize}[<+->]
  \item New framework to handle calculations of coupled systems
  \item Allows to define many physical systems simultaneously (electrons, ions, lasers, Maxwell, DFTB+, PCM, etc)
  \item Systems are coupled through interactions (Electron-ion, Lorentz force, dipole coupling, etc)
  \item Calculations modes are now ``algorithms'': a set of state machine atomic operations
  \item The code automatically handles all the interactions/systems
  \item New parallelization level: systems
  \item Current efforts focused on porting SCF and time propagation to new framework
 \end{itemize}

}


\frame{\frametitle{Multi-System Framework: Design}

 \begin{itemize}[<+->]
  \item Focus on extendability and maintainability
  \item Adding new systems, interactions and algorithms should be as simple as possible
  \item Flexible algorithms:
  \begin{itemize}
   \item Time-propagation using different propagators and time-steps for each system
   \item Nested SCF loops
  \end{itemize}
  \item Framework is independent of existing systems and interactions
  \item Systems do not know about each other directly, instead they know interactions
  \item Heavy use of object-oriented programming
 \end{itemize}

}


\frame{\frametitle{Test environment: celestial dynamics}

  \begin{center}
  \includegraphics[width=0.3\textwidth]{img/Moon_apsidal_precession}
 \end{center}

  \begin{itemize}[<+->]
   \item System of Sun, Earth, and Moon as point particles interacting with gravity
   \item Numerical integration of orbits with different algorithms
   \item Fast turnover for code development
  \end{itemize}

}



\begin{specialframe}\frametitle{Test environment: celestial dynamics}

 \begin{block}{\texttt{inp}}
   \begin{tiny}
    \begin{verbatim}
CalculationMode = td
ExperimentalFeatures = yes

%Systems
 "Sun"   | classical_particle
 "Earth" | classical_particle
 "Moon"  | classical_particle
%

%Interactions
 gravity | all_partners
%
InteractionTiming = timing_retarded

#Initial conditions are taken from https://ssd.jpl.nasa.gov/horizons.cgi#top.
# initial condition at time:
# 2458938.500000000 = A.D. 2020-Mar-30 00:00:00.0000 TDB

Earth.ParticleMass = 5.97237e24
%Earth.ParticleInitialPosition
 -147364661998.16476 | -24608859261.610123 | 1665165.2801353487
%
%Earth.ParticleInitialVelocity
 4431.136612956525 | -29497.611635546345 | 0.343475566161544
%
   \end{verbatim}
  \end{tiny}
 \end{block}

\end{specialframe}


\begin{specialframe}\frametitle{Test environment: celestial dynamics}

 \begin{block}{\texttt{inp} (cont.)}
 \begin{tiny}
   \begin{verbatim}   
Moon.ParticleMass = 7.342e22
%Moon.ParticleInitialPosition
 -147236396732.81906 | -24234200672.857853 | -11062799.286082389
%
%Moon.ParticleInitialVelocity
 3484.6397238565924 | -29221.007409082802 | 82.53526338876684
%

Sun.ParticleMass = 1.98855e30
%Sun.ParticleInitialPosition
 0.0 | 0.0 | 0.0
%
%Sun.ParticleInitialVelocity
 0.0 | 0.0 | 0.0
%

TDSystemPropagator = verlet

sampling = 24 # Time-steps per day
days = 3
seconds_per_day = 24*3600
Sun.TDTimeStep = seconds_per_day/sampling
Earth.TDTimeStep = seconds_per_day/sampling/2
Moon.TDTimeStep = seconds_per_day/sampling/4
TDPropagationTime = days*seconds_per_day
   \end{verbatim}
  \end{tiny}
 \end{block}

\end{specialframe}


\begin{specialframe}\frametitle{New multi-system syntax}

 \begin{block}{Systems block}
   \begin{tiny}
    \begin{verbatim}
%Systems
 "Sun"   | classical_particle
 "Earth" | classical_particle
 "Moon"  | classical_particle
%
    \end{verbatim}
   \end{tiny}
 \end{block}

  \onslide<2->
 \begin{block}{Nested systems}
   \begin{tiny}
    \begin{verbatim}
%Systems
 "Sun"   | classical_particle
 "Earth" | multisystem
%

%Earth.Systems
 "Terra" | classical_particle
 "Luna"  | classical_particle
%     
    \end{verbatim}
   \end{tiny}
  \end{block}

\end{specialframe}


\begin{specialframe}\frametitle{New multi-system syntax}

 \begin{block}{Namespaces}
   \begin{tiny}
    \begin{verbatim}
Sun.ParticleMass = 1.98855e30
Earth.Terra.ParticleMass = 5.97237e24
Luna.ParticleMass = 7.342e22     
    \end{verbatim}
   \end{tiny}
 \end{block}

  \onslide<2->
  \begin{block}{Interactions}
   \begin{tiny}
    \begin{verbatim}
%Interactions
 gravity       | all_partners
 coulomb_force | no_partners
%

%SystemA.Interactions
 gravity       | no_partners
 coulomb_force | all_partners 
%

%SystemB.Interactions
 gravity       | only_partners | "SystemA"
 coulomb_force | all_except    | "SystemC"
%
    \end{verbatim}
   \end{tiny}
 \end{block}
 
 \end{specialframe}


\frame{\frametitle{Velocity Verlet}

 \begin{enumerate}[<+->]
  \item Update positions
   \begin{displaymath}
    \mvec{x}(t + \Delta t) = \mvec{x}(t) + \mvec{v}(t) \Delta t + \frac{1}{2}\mvec{a}(t)\Delta t^2 
   \end{displaymath}
  \item Update interactions with all partners (compute $\mvec{F}(\mvec{x}(t+\Delta t))$)
  \item Compute acceleration $\mvec{a}(t + \Delta t)$
  \item Compute velocity
   \begin{displaymath}
    \mvec{v}(t + \Delta t) = \mvec{v}(t) + \frac{1}{2} (\mvec{a}(t) + \mvec{a}(t+\Delta t)) \Delta t
   \end{displaymath}
 \end{enumerate}

}

\frame{\frametitle{Visualizing the multi-system time-stepping algorithm}

 \url{https://octopus-code.org/new-site/develop/developers/code_documentation/propagators/custom_diagram/}

  \begin{center}
  \includegraphics[width=0.8\textwidth]{img/multisystem-diagram}
 \end{center}
 
}


\frame{\frametitle{Celestial orbits}
 
 \begin{center}
  \includegraphics[width=0.8\textwidth]{img/celestial-orbits}
 \end{center}
 
}
