\section{Mesh functions}

\begin{frame}
  \center{\huge{\bf The grid}}
\end{frame}

%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Real-space grid}
\vskip0.2cm
The grid describes a number of things:
\begin{itemize}
  \item the mesh (the actual points in space)
  \item the simulation box (region of space over which the mesh extends)
  \item the derivatives
  \item the stencil
\end{itemize}
\vspace{0.5cm}
\pause
{\scriptsize
\begin{verbatim}
type grid_t
  ! Components are public by default
  type(simul_box_t)                   :: sb            
  type(mesh_t)                        :: mesh          
  type(derivatives_t)                 :: der           
  class(coordinate_system_t), pointer :: coord_system   
  type(stencil_t)                     :: stencil
  type(symmetries_t)                  :: symm
end type grid_t
\end{verbatim}
}
\end{frame}

%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Real-space grid}
The mesh:
\begin{itemize}[<+->]
  \item Usually uniform (curvilinear meshes or double grids are possible)
  \item can be distributed over processes (domain decomposition)
  \item access via linear indices (local and global index)
  \item We need some 'extra points':
  \begin{itemize}
    \item for boundary conditions:\\
    functions on these points are not updated
    \item halo points (ghost points): \\
    when using domain decompositions, each process needs access to neighboring domains.  
  \end{itemize}
\end{itemize}

\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Real-space grid}
Memory layout:
\begin{itemize}[<+->]
  \item mesh sizes:
  \begin{description}
    \item[\texttt{np}] number of local 'inner' points
    \item[\texttt{np\_part}] number of local 'inner' points + 'ghost' points + boundary points
    \item[\texttt{np\_global}] number of global 'inner' points
    \item[\texttt{np\_part\_global}] number of global 'inner' points + boundary points
  \end{description}
  \vspace{0.5cm}
\item ordering:
\begin{itemize}
  \item inner points first \texttt{[1:np]}
  \item ghost and boundary points: \texttt{[np+1:np\_part]}
\end{itemize}
\item mesh points:
\texttt{mesh\%x(1:mesh\%np\_part, 1:space\%dim)}
\end{itemize}

\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Real-space grid}
Mesh functions:
\begin{itemize}[<+->]
  \item position dependent quantities are stored as so-called mesh functions.
  \item Examples:  \\[2mm]
  {\scriptsize
  \texttt{rho(1:gr\%mesh\%np, 1:st\%d\%nspin)} (no ghost points needed here) \\[1mm]
  \texttt{hm\%vhartree(1:gr\%mesh\%np\_part)}\\[1mm]
  \texttt{hm\%a\_ind(1:gr\%mesh\%np\_part, 1:space\%dim)}\\[1mm]
  }
  \item wave functions are stored differently $\longrightarrow$ batches
\end{itemize}
\vspace{2mm}
\pause
Operations on mesh functions:
\begin{itemize}[<+->]
  \item local operations: point-wise operation, simple loop
  \item integrations: summation in each domain and reduction over domains
  \item derivatives: need to consider ghost and boundary points
\end{itemize}

\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Real-space grid}
Pre-defined operations on mesh functions:
\begin{description}[<+->]
  \item[dot product] \texttt{X(mf\_dotp)(mesh, f1, f2, reduce, dotu, np)}
  \item[norm] \texttt{X(mf\_nrm2)(mesh, ff, reduce)}
  \item[Laplacian] \texttt{X(derivatives\_lapl)(der, ff, op\_ff, ghost\_update, set\_bc, factor)} 
  \item[gradient] \texttt{X(derivatives\_grad)(der, ff, op\_ff, ghost\_update, set\_bc)} 
\end{description}
\pause
\vspace{0.5cm}
However: We are trying to use batches wherever possible.
\end{frame}

%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Batches}
\begin{itemize}[<+->]
  \item often one has to operate on many mesh functions at once \\(e.g. wave functions)
  \item more effective to swap mesh index and function index: 'packed form':\\
  fast index is now over states.
  \item excerpt from \texttt{batch\_t}: 
{\scriptsize
\begin{verbatim}
!> unpacked variables; linear variables are pointers with different shapes 
FLOAT, pointer, contiguous,     public :: dff(:, :, :)
CMPLX, pointer, contiguous,     public :: zff(:, :, :)
FLOAT, pointer, contiguous,     public :: dff_linear(:, :)
CMPLX, pointer, contiguous,     public :: zff_linear(:, :)
!> packed variables; only rank-2 arrays due to padding to powers of 2
FLOAT, pointer, contiguous,     public :: dff_pack(:, :)
CMPLX, pointer, contiguous,     public :: zff_pack(:, :)
\end{verbatim}
}
\item basic math operations implemented for batches
\item more in upcoming lectures.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
\frametitle{Operators and obervables}
Calculating expectation values:
\begin{itemize}
  \item operators can be expressed in terms of defined math operations
  \item many terms already implemented in the Hamiltonian
  \item usually no need to touch low level routines
\end{itemize}  
\vspace{0.5cm}
\pause
Let's look at some code: contributions to the total energy (\texttt{electrons/energy\_calc.F90})
{\tiny
\begin{verbatim}
  subroutine energy_calc_total(namespace, space, hm, gr, st, iunit, full)

  FLOAT function X(energy_calc_electronic)(namespace, hm, der, st, terms) result(energy)

  subroutine X(calculate_expectation_values)(namespace, hm, der, st, eigen, terms)
\end{verbatim}
}
\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Operators and obervables}
  \tiny
  \begin{verbatim}
subroutine energy_calc_total(namespace, space, hm, gr, st, iunit, full)

type(namespace_t),        intent(in)    :: namespace
type(space_t),            intent(in)    :: space
type(hamiltonian_elec_t), intent(inout) :: hm
type(grid_t),             intent(in)    :: gr
type(states_elec_t),      intent(inout) :: st
integer, optional,        intent(in)    :: iunit
logical, optional,        intent(in)    :: full

  ... 

hm%energy%eigenvalues = states_elec_eigenvalues_sum(st)

if (full_ .or. hm%theory_level == HARTREE .or. hm%theory_level == HARTREE_FOCK &
  .or. hm%theory_level == GENERALIZED_KOHN_SHAM_DFT) then
  
  if (states_are_real(st)) then
    hm%energy%kinetic = denergy_calc_electronic(namespace, hm, gr%der, st, terms=TERM_KINETIC)
    hm%energy%extern_local = denergy_calc_electronic(namespace, hm, gr%der, st, terms=TERM_LOCAL_EXTERNAL)
    hm%energy%extern_non_local = denergy_calc_electronic(namespace, hm, gr%der, st, &
                                                         terms=TERM_NON_LOCAL_POTENTIAL)
    hm%energy%extern = hm%energy%extern_local + hm%energy%extern_non_local
  else
  ... ! same with z prefix 
  end if

end if
  \end{verbatim}  
\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Operators and obervables}
  \tiny
  \begin{verbatim}
FLOAT function X(energy_calc_electronic)(namespace, hm, der, st, terms) result(energy)

  type(namespace_t),        intent(in)    :: namespace
  type(hamiltonian_elec_t), intent(in)    :: hm
  type(derivatives_t),      intent(in)    :: der
  type(states_elec_t),      intent(inout) :: st
  integer,                  intent(in)    :: terms

  R_TYPE, allocatable  :: tt(:, :)

  PUSH_SUB(X(energy_calc_electronic))

  SAFE_ALLOCATE(tt(st%st_start:st%st_end, st%d%kpt%start:st%d%kpt%end))

  call X(calculate_expectation_values)(namespace, hm, der, st, tt, terms = terms)

  energy = states_elec_eigenvalues_sum(st, TOFLOAT(tt))

  SAFE_DEALLOCATE_A(tt)
  POP_SUB(X(energy_calc_electronic))

end function X(energy_calc_electronic)
\end{verbatim}  
\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Operators and obervables}
  \tiny
  \begin{verbatim}
subroutine X(calculate_expectation_values)(namespace, hm, der, st, eigen, terms)

  type(namespace_t),        intent(in)    :: namespace
  type(hamiltonian_elec_t), intent(in)    :: hm
  type(derivatives_t),      intent(in)    :: der
  type(states_elec_t),      intent(inout) :: st
  R_TYPE,                   intent(out)   :: eigen(st%st_start:, st%d%kpt%start:) !< (:st%st_end, :st%d%kpt%end)
  integer, optional,        intent(in)    :: terms
  
  integer :: ik, minst, maxst, ib
  type(wfs_elec_t) :: hpsib
  
  do ik = st%d%kpt%start, st%d%kpt%end
    do ib = st%group%block_start, st%group%block_end
  
      minst = states_elec_block_min(st, ib)
      maxst = states_elec_block_max(st, ib)
  
      call st%group%psib(ib, ik)%copy_to(hpsib)
      call X(hamiltonian_elec_apply_batch)(hm, namespace, der%mesh, st%group%psib(ib, ik), hpsib, terms = terms)
      call X(mesh_batch_dotp_vector)(der%mesh, st%group%psib(ib, ik), hpsib, eigen(minst:maxst, ik), reduce = .false.)
      call hpsib%end()
  
    end do
  end do
  
  if (der%mesh%parallel_in_domains) call der%mesh%allreduce(&
    eigen(st%st_start:st%st_end, st%d%kpt%start:st%d%kpt%end))
  
end subroutine X(calculate_expectation_values)
  \end{verbatim}  
\end{frame}
  
%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Operators and obervables}
  in \texttt{hamiltonian/hamiltonian\_elec\_inc.F90}:
  {\tiny
  \begin{verbatim}

subroutine X(hamiltonian_elec_apply_batch) (hm, namespace, mesh, psib, hpsib, terms, set_bc)

  ... 

  if (bitand(TERM_KINETIC, terms_) /= 0) then
    ASSERT(associated(hm%hm_base%kinetic))
    call profiling_in(prof_kinetic_start, TOSTRING(X(KINETIC_START)))
    call X(derivatives_batch_start)(hm%hm_base%kinetic, hm%der, epsib, hpsib, handle, &
                                    set_bc = .false., factor = -M_HALF/hm%mass)
    call profiling_out(prof_kinetic_start)
  end if

  ... 

  if (bitand(TERM_KINETIC, terms_) /= 0) then
    call profiling_in(prof_kinetic_finish, TOSTRING(X(KINETIC_FINISH)))
    call X(derivatives_batch_finish)(handle)
    call profiling_out(prof_kinetic_finish)
  else
    call batch_set_zero(hpsib)
  end if
\end{verbatim}}
\pause  
split in start and finish routine to enable other operations during communication.
\end{frame}
