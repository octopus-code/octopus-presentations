\section{Calculations}

\begin{frame}
  \center{\huge{\bf How a calculation works...}}
  
\end{frame}
%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{The calculation modes}
  {\scriptsize
  \begin{description}
  \item[gs]    Calculation of the ground state.
  \item[unocc] Calculation of unoccupied/virtual KS states. Can also be used for a non-self-consistent calculation of states at arbitrary k-points, if density.obf from gs is provided in the restart/gs directory.
  \item[td]  Time-dependent calculation (experimental for periodic systems).
  \item[go]  Optimization of the geometry.
  \item[opt\_control] Optimal control.
  \item[em\_resp] Calculation of the electromagnetic response: electric polarizabilities and hyperpolarizabilities and magnetic susceptibilities (experimental for periodic systems).
  \item[casida]  Excitations via Casida linear-response TDDFT; for finite systems only.
  \item[vdw]  Calculate van der Waals coefficients.
  \item[vib\_modes] Calculation of the vibrational modes.
  \item[invert\_ks] Invert the Kohn-Sham equations (experimental).
  \item[recipe] Prints out a tasty recipe.   
  \item[...] and others
  \end{description} 
  } 
\end{frame}

%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{The calculation modes}
  The \texttt{run()} routine:
  {\tiny
\begin{verbatim}
subroutine run(cm):
  integer, intent(in) :: cm 
  ... 
  select case (calc_mode_id)
  case (OPTION__CALCULATIONMODE__GS)                ! ground state
    call ground_state_run(systems, from_scratch)
  case (OPTION__CALCULATIONMODE__UNOCC)             ! unoccupied states
    call unocc_run(systems, from_scratch)
  case (OPTION__CALCULATIONMODE__TD)                ! time propagation
    call time_dependent_run(systems, from_scratch)
  case (OPTION__CALCULATIONMODE__GO)                ! geometry optimization
    call geom_opt_run(systems, from_scratch)
  ...  
  end select
  ... 
end subroutine run
\end{verbatim}
  }
\pause
Concentrate on:
\begin{itemize}
  \item Ground state calculation
  \item Time propagation
\end{itemize}
\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%

\begin{frame}[fragile]
  \frametitle{Ground state calculation (electrons only)}
 
 \vskip0.2cm
  
 \begin{itemize}
  \item Startup:
  \begin{itemize}
    \item initial wave functions:
    \begin{itemize}
      \item Restart
      \item LCAO from diagonalized speudo-wavefunctions
      \item random wave functions
    \end{itemize}
    \item setup initial Hamiltonian
  \end{itemize}
  \pause
  \item SCF cycle:
  \begin{itemize}[<+-|alert@+>]
    \item run the eigenvalue solver
    \item calculate new occupations and new density 
    \item calculate total energy
    \item mix potentials or densities
    \item update Hamiltonian
    \item check convergence criteria
  \end{itemize}
 \end{itemize}
\end{frame}


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
% \begin{frame}
% \frametitle{Ground state calculation (electrons only)}
% Linear Combination of Atomic Orbitals (LCAO):
% \vskip0.2cm
% \begin{itemize}
%   \item KS SCF cycle needs starting point
%   \item Build LCAO out of pseudo-orbitals (from pseudopotentials)
%   \item diagonalize LCAO space \\
%   (usually some extra orbitals added)
%   \item $\Rightarrow$ starting point for density and potential
% \end{itemize}
% \end{frame}

%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Ground state calculation (electrons only)}
  (simplified) SCF cycle: (\texttt{scf/scf.F90})
  \vskip0.2cm
  {\scriptsize
  \begin{itemize}[<+-|alert@+>]
  \item[] \texttt{do iter = 1, scf\%max\_iter}
  \item[] \texttt{\ \ scf\%eigens\%converged = 0 }
  \item[] \texttt{\ \ call eigensolver\_run(scf\%eigens, namespace, gr, st, hm, iter) }
  \item[] \texttt{\ \ call states\_elec\_fermi(st, namespace, gr\%mesh) }
  \item[] \texttt{\ \ call density\_calc(st, gr, st\%rho) }
  \item[] \texttt{\ \ call v\_ks\_calc(ks, namespace, space, hm, st, ions) }
  \item[] \texttt{\ \ call mixfield\_set\_vout(scf\%mixfield, hm\%vhxc) }
  \item[] \texttt{\ \ call energy\_calc\_total(namespace, space, hm, gr, st, iunit = 0) } 
  \item[] \texttt{\ \ call mixing(scf\%smix) } 
  \item[] \texttt{\ \ call mixfield\_get\_vnew(scf\%mixfield, hm\%vhxc) } 
  \item[] \texttt{\ \ call hamiltonian\_elec\_update\_pot(hm, gr\%mesh) } 
  \item[] \texttt{\ \ call mixfield\_set\_vin(scf\%mixfield, hm\%vhxc(1:gr\%mesh\%np, 1:nspin))} 
  \item[] \texttt{\ \ ! check convergence} 
  \item[] \texttt{enddo}
  \end{itemize}
  }
  \end{frame}
  


%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\begin{frame}[fragile]
  \frametitle{Ground state calculation (electrons only)}
  Eigenvalue problem:\\[0.5cm]
  {\scriptsize  \texttt{call eigensolver\_run(scf\%eigens, namespace, gr, st, hm, iter)}
  }
  \pause
  \vskip0.2cm
  \begin{itemize}
    \item Matrix is huge and sparse: no direct diagonalization
    \item Iterative schemes:
    \begin{itemize}
      \item Conjugate gradient: (\texttt{cg}, \texttt{cg\_new})
      \item Pre-conditioned Lanczos (\texttt{plan})
      \item Residual minimization scheme, direct inversion in the iterative subspace (\texttt{rmmdiis})
    \end{itemize}
  \end{itemize}
  \end{frame}


  
%%%%%%%%%%%%%% Slide %%%%%%%%%%%%%%
\frame{\frametitle{Time-dependent calculations (for electrons)}
\vskip0.2cm
  
\begin{itemize}
  \item Startup:
  \begin{itemize}
    \item Restart from ground state calculation
  \end{itemize}
  \pause
  \item propagation:
  \begin{displaymath}
    \varphi_i(\mvec{r},t+\Delta t) = \hat{T} \mathrm{exp} \left\{
    -\I \minttwo{t}{t}{t+\Delta t}
    \hat{H} \varphi_i(\mvec{r}, t) \right\}
  \end{displaymath}
  \pause
  \item different ways to approximate
  \begin{itemize}
    \item the integration
    \begin{itemize}
      \item \texttt{TDPropagator} for electrons
      \item \texttt{TDSystemPropagator} for multisystem framework
    \end{itemize}
    \item the exponential
    \begin{itemize}
      \item \texttt{TDExponentialMethod}
    \end{itemize}
  \end{itemize}    
  \pause
  \item still different implementations for matter (electrons + ions) and the multisystem approach
  (more on new approach later)
  \pause
  \item this will change soon...
\end{itemize}

}
