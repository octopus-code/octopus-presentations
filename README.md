# octopus-presentations

This repository provides a common place to collect and store presentations related to octopus.

# Overview of presentations
 - [2021-03-06 Slide examples](https://octopus-code.gitlab.io/octopus-presentations/2021-03-06-slide-examples)
 - [2021-03-25 MPSD Theory Seminar](https://octopus-code.gitlab.io/octopus-presentations/2021-03-25-MPSD-Theory-Seminar)

## Installation of reveal-md
```console
> mkdir $HOME/.local
> npm install -g --prefix=$HOME/.local puppeteer
> npm install -g --prefix=$HOME/.local reveal-md
```

## Creation of static pages
```console
> cd 2021-03-25-MPSD-Theory-Seminar
> reveal-md --static static --css style.css --theme white presentation.md
```

## Status
[![pipeline status](https://gitlab.com/octopus-code/octopus-presentations/badges/master/pipeline.svg)](https://gitlab.com/octopus-code/octopus-presentations/badges/master/pipeline.svg)

