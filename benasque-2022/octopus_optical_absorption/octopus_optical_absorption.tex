%\documentclass[palatino]{beamer}
\documentclass[palatino,handout]{beamer}

%%%%%%%%%%%%%% MACROS AND STUFF %%%%%%%%%%%%%%
\mode<presentation>

\usetheme{Madrid}

\definecolor{mycolor}{RGB}{67,130,176}
\usecolortheme[named=mycolor]{structure}
\setbeamercolor{normal text}{fg=violet!20!black}

\useoutertheme[footline=authortitle,subsection=false]{miniframes}
%\useoutertheme{default}
\setbeamertemplate{navigation symbols}{} 

\usefonttheme[onlymath]{serif}


\usepackage[english]{babel}

\usepackage{graphicx,amsmath,amsfonts,amssymb,mathrsfs,bm}
\usepackage{pgfpages}
\usepackage{tikz}
\usetikzlibrary{arrows,shadows,decorations.pathmorphing,backgrounds,positioning,fit,petri,calc,shapes}
\usepackage{smartdiagram}

\input{macros.tex}


\title[]{Introduction to Octopus:\\ Optical properties of finite systems}

\author[]{Micael Oliveira}


\date[]{\vskip-0.1cm {Benasque TDDFT School 2022}}

\setbeamertemplate{sidebar left}


\begin{document}

\maketitle

  
\frame{\frametitle{Electronic response of finite systems to external fields}

 The dynamical polarizability is the ratio of the induced dipole moment to the perturbing electric field:
 \begin{displaymath}
  \alpha(\omega) = \frac{\delta \mvec{p}(\omega)}{\mvec{E}(\omega)} = \frac{1}{\mvec{E}(\omega)} \mint{\br} \br \, \delta n(\br, \omega)
 \end{displaymath}
 \onslide<2->
 \vskip0.3cm
 $\alpha(\omega)$ is related to the optical absorption cross-section:
 \begin{displaymath}
  \sigma(\omega) = \frac{4\pi\omega}{c} \Im\left\lbrace \Tr \left[ \alpha(\omega) \right] \right\rbrace
 \end{displaymath}

}

\frame{\frametitle{Real-time TDDFT}

 The dynamical polarizability can be computed by solving  directly the time-dependent Kohn-Sham equations:
 \vskip0.1cm
 \begin{itemize}
  \item<2-> Take the DFT ground state wavefunctions ${\varphi_i(\br)}$.
  \vskip0.1cm
  \item<3-> Excite all the frequencies of the system by applying the
           appropriate instantaneous perturbation $\delta v(\br, t) = -E x_j \delta(t)$.
  \vskip0.1cm
  \item<4-> Use TDDFT to propagate the wavefunctions in time:
   \begin{displaymath}
    \varphi_i(\mvec{r},t+\Delta t) = \hat{T} \mathrm{exp} \left\{ -\I \minttwo{t}{t}{t+\Delta t}
    \hat{H}_\KS \varphi_i(\mvec{r}, t) \right\}
   \end{displaymath}
   and keep track of the density $n(\br,t)$.
   \vskip0.1cm
   \item<5-> Compute the polarizability $\alpha_{ij}(\omega) = \frac{1}{E(\omega)} \mint{\br} x_i \delta n(\br,\omega)$.
 \end{itemize}

}

\frame{\frametitle{Other methods implemented in Octopus}

 Casida equations:
 \begin{itemize}
  \item Pseudo-eigenvalue equation of the form:
  \begin{displaymath}
   \op{R} F_q = \Omega^2_q F_q
 \end{displaymath}                                                                                                                                                                                              
  \item Eigenvalues $\Omega^2_q$ are the square of the excitation energies
  \item Eigenvectors are related to the oscilator strenghts
  \item $\op{R}$ is a matrix that involves pairs of occupied and unoccupied KS states
 \end{itemize}

}

\frame{\frametitle{Other methods implemented in Octopus}

 Sternheimer equations:
 \begin{itemize}
  \item Relies on the calculation of the first order variations of the KS wavefunctions  $\psi'_{m}(\br,\pm\omega)$
  \item Equations have the following form
  \begin{displaymath}
   \left[\op{H}_\KS - \epsilon_m \pm \omega + \I\eta\right] \psi'_{m,i}(\br,\pm\omega) = -\op{P}_c \op{H}'(\pm\omega)\psi_m(\br)\,,                                                                                    
  \end{displaymath}
  \item $\op{H}'$ is the first order variation of the Kohn-Sham Hamiltonian:                                                                           
  \begin{displaymath}
   \op{H}'(\omega) = x_i + \mint{\br'} \frac{\delta n_i(\br',\omega)}{|\br-\br'|} + \mint{\br'} f_\xc(\br,\br',\omega)\delta n_i(\br',\omega)\,.                                                                       
  \end{displaymath}
 \end{itemize}

}

\frame{\frametitle{Pros and cons}

 \begin{center}
  Real time propagation 
 \end{center}

 \begin{small}
  Pros
  \begin{itemize}
   \item Favourable scaling with system size
   \item Does not require the calculation of empty states
   \item Easy to extend to other perturbations/responses
   \item Allows to go beyond linear-response
   \item Only requires knowledge of $v_\xc$
  \end{itemize}
  Cons
  \begin{itemize}
   \item Slow for small systems
  \end{itemize}
 \end{small}

}

\frame{\frametitle{Pros and cons}

 \begin{center}
  Casida equations
 \end{center}

 Pros
 \begin{itemize}
  \item Fast for small systems
 \end{itemize}
 Cons
 \begin{itemize}
  \item Requires calculation of empty states
  \item Requires computation of large matrices
  \item Unfavourable scaling with system size
 \end{itemize}

}

\frame{\frametitle{Pros and cons}

 \begin{center}
  Sternheimer equations
 \end{center}

 Pros
 \begin{itemize}
  \item Favourable scaling with system size
  \item Does not require the calculation of empty states
  \item Allows to go beyond linear-response
 \end{itemize}
 Cons
 \begin{itemize}
  \item Equations needs to be solved one frequency at a time
 \end{itemize}

}

\frame{\frametitle{The tutorials}
 \vskip0.8cm
 
 You can find the tutorials under this link:
 https://octopus-code.org/documentation/12/tutorial/
 \vskip0.8cm

 Optical response series:
 \begin{itemize}
  \item Lesson 1: Optical spectra from time-propagation
  \item Lesson 2: Convergence of the optical spectra
  \item Lesson 3: Optical spectra from Casida
  \item Lesson 4: Optical spectra from Sternheimer
  \item Lesson 5: Triplet excitations
  \item Lesson 6: Use of symmetries in optical spectra from time-propagation
 \end{itemize}

}

\frame{\frametitle{The tutorials}
 \vskip0.8cm
 
 You can find the tutorials under this link:
 https://octopus-code.org/documentation/12/tutorial/
 \vskip0.8cm
 
 \center{\textbf{Have Fun !}}

 \vskip0.8cm

 \begin{center}
  \includegraphics[width=0.40\textwidth]{img/octopusojo}
 \end{center}
}


\end{document}
