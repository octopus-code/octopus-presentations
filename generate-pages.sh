#!/bin/bash
set -x

# Update plugin submodules
git submodule update --init --recursive

# disable this temporarily
exit 0

# ----------------------------------------------------------------------------
function generate_static_page() {
   pushd $1
    have_movies=""
    [ -d movies ] && have_movies="--static-dirs=movies"
    have_template=""
    [ -f lib/template/reveal.html ] && have_template="--template=lib/template/reveal.html"
    if [ ! -f .skip-md-gen ]; then
      echo reveal-md --static ../public/$1 --static-dirs=figures $have_movies $have_template --css style.css presentation.md
    fi
    pushd ../public/$1
      npm install
    popd
    cp -a favicon.ico ../public/$1
  popd

}

# ----------------------------------------------------------------------------
if [ "X$1" != "X" ]; then
  # generate specific static HTML page
    generate_static_page $1
else
  # generate all static HTML pages
  for x in $(ls -d 20??-??-??-*); do
     generate_static_page $x
  done
fi

