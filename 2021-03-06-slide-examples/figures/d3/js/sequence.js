function tagify(name) {
    let re=/ /g
    return name.replace(re,'_')
}

function run(data, my_global_tag) {

    var systems = data.systems;
    var steps = data.steps;
    var interactions = data.interactions;


    var minHeight  = 30;
    var minWidth = 50;
    var stepHeight = 40; // we still might want to make this more flexible, depending on the content of the steps.
    var stepMargin = 10;
    var systemMargin = 20;
    var interactionWidth = 70; // this should be determined from the text width

    console.log('run with '+my_global_tag)

    // declare the arrow heads:
    var svg = d3.select("test3");
    var arrowPoints = [[0, 0], [0, 10], [10, 5]];
    svg
    .append('defs')
    .append('marker')
    .attr('id', 'arrow')
    .attr('viewBox', [0, 0, 10,10])
    .attr('refX', 10)
    .attr('refY', 5)
    .attr('markerWidth', 10)
    .attr('markerHeight', 10)
    .attr('orient', 'auto-start-reverse')
    .append('path')
    .attr('d', d3.line()(arrowPoints))
    .attr('stroke', 'black');


    var header = svg.append('g').attr('id','header').attr('transform','translate( 20, 0 )')
    var main   = svg.append('g').attr('id','main').attr('transform','translate( 20, 100 )');
    var footer = svg.append('g').attr('id','footer').attr('transform','translate( 20, 0 )')
    
    var actors = []
    var offsets = Object();

    console.log("running main script.");

    data.systems.forEach( function(d) {
        d.id = 'system_'+tagify(d.name)
        d.y0 = 0;
        d.show = true;
        actors.push(d); 
        d.interactions.forEach( function(i) {
            i.y0 = 55;
            i.owner = d.name;
            i.name = i.type+':'+i.owner+'=>'+i.partner;
            actors.push(i);
        });
    });


    var counter = 0;
    data.steps.forEach( function(step_) {
        step_.id = 'step_'+counter++;
        step_.show = false; 
        step_.height = minHeight; 
    });

    svg.append('line')
    .attr('x1',10)
    .attr('y1',50)
    .attr('x2',10)
    .attr('y2',50)
    .attr('id','global_time_line')
    .attr('marker-end', 'url(#arrow)')
    .attr('stroke', 'black');


    update();


    function drawSystem(system_) {

    }

    function drawInteraction(system_) {

        console.log(system_)

        var tag = system_.id;

        console.log('draw_interaction: tag: '+tag+' show = '+system_.show);

        var interactions      = header.selectAll('rect#'+tag).data(system_.interactions);
        var interactions_txt1 = header.selectAll('text#'+tag+'_1').data(system_.interactions);
        var interactions_txt2 = header.selectAll('text#'+tag+'_2').data(system_.interactions);

        interactions.enter().append('rect')
        .attr('id',tag)
        .attr('x', s => s.x )
        .attr('y', 40)
        .attr('rx',2)
        .attr('ry',2)
        .attr('width', s=>s.width)
        .attr('height', 40)
        .attr('stroke', 'black')
        .attr('fill', 'rgba(0,255,0,0.2)')

        interactions
        .attr('x', s => 10+ s.x )
        .attr('width', d=>d.width)

        if(system_.show) {

            console.log('drawing interaction for '+system_.name)



            interactions_txt1.enter().append('text')
            .text( function(d) {return d.type})
            .attr('class','small')
            .attr('id',tag+'_1')
            .attr('x', function(s) {return 15+ s.x; } )
            .attr('y', 55)
            .attr('stroke','black')

            interactions_txt1
            .attr('x', function(s) {return 15+ s.x; } )

            interactions_txt2.enter().append('text')
            .text( function(d) {return d.partner})
            .attr('class','small')
            .attr('id',tag+'_2')
            .attr('x', function(s) {return 15+ s.x; } )
            .attr('y', 70)
            .attr('stroke','black')

            interactions_txt2
            .attr('x', function(s) {return 15+ s.x; } )

        } else {
            // header.selectAll('rect#'+tag).remove()
            header.selectAll('text#'+tag+'_1').remove()
            header.selectAll('text#'+tag+'_2').remove()
        }
    }

    function drawStep(step_) {

        var step_id = 'g#'+step_.id;
        var counter = 0;
        step_.steps.forEach( function(sub) {sub.counter=counter++;} )

        console.log('drawStep: '+step_);

        var subs     = main.selectAll(step_id).selectAll('line#sub').data(step_.steps);
        var subs_txt = main.selectAll(step_id).selectAll('text#sub').data(step_.steps);

        if(step_.show) {

            subs.enter().append('line')
                .attr('id','sub')
                .attr('x1',function(d) {return offsets[d.source];} )
                .attr('y1',function(d) {return 15 + stepMargin + stepHeight*d.counter;})
                .attr('x2',function(d) {return offsets[d.dest];} )
                .attr('y2',function(d) {return 15 + stepMargin + stepHeight*d.counter;})
                .attr('stroke','black')
                .attr('marker-end', 'url(#arrow)')

            subs
                .attr('x1',function(d) {return offsets[d.source];} )
                .attr('y1',function(d) {return 15 + stepMargin + stepHeight*d.counter;})
                .attr('x2',function(d) {return offsets[d.dest];} )
                .attr('y2',function(d) {return 15 + stepMargin + stepHeight*d.counter;})
    

            subs_txt.enter().append('text')
                .text( function(x) {return x.label;}).attr('class','small')
                .attr('id','sub')
                .attr('x',function(d) {return (offsets[d.source]+offsets[d.dest])/2 - 10;} )
                .attr('y',function(d) {return 30 + stepMargin + stepHeight*d.counter;})
                .attr('stroke','black')

            subs_txt
                .attr('x',function(d) {return (offsets[d.source]+offsets[d.dest])/2 - 10;} )
                .attr('y',function(d) {return 30 + stepMargin + stepHeight*d.counter;})

        }
        else {
            main.selectAll(step_id).selectAll('line#sub').remove()
            main.selectAll(step_id).selectAll('text#sub').remove()
        }
        main.selectAll(step_id).selectAll('#step_label').text( step_.name )

    }

    function update() {

        // update offsets:

        var y = 0;
        var counter = 0;
        steps.forEach( function(d) {  d.y = y; y+=d.height+5;} );
        y += 120;

        svg.select('#global_time_line')
        .attr('y2',20+y)
        
        var x = 300;
        data.systems.forEach( function(d) {
            d.x = x; 
            d.line = x+10;
            var offset = systemMargin;
            d.interactions.forEach( function(elem) {console.log(elem); 
                    elem.x = x+offset; 
                    elem.width=(d.show?interactionWidth:10); 
                    offset+=elem.width+5; 
                    elem.line = 10 + elem.x + elem.width/2;
                });
            d.width = offset+5;
            x+= offset+10;
        })
        x += 50
    
        actors.forEach( function(d) { offsets[d.name] = d.line; });



        var system_groups_header = header.selectAll('g.system_t').data(systems);

        system_groups_header.enter()
          .append('g')
            .attr('class','system_t')
            .attr('id',function(d) {return d.id;})
            .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
        system_groups_header
            .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
    

        var system_groups_footer = footer.selectAll('g.system_b').data(systems);

        system_groups_footer.enter()
          .append('g')
            .attr('class','system_b')
            .attr('id',function(d) {return d.id;})
            .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
        system_groups_footer
            .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
        
        



        var sysline1 = header.selectAll('g.system_t').selectAll('rect').data(systems);
        var sysline1_txt = header.selectAll('g.system_t').selectAll('text').data(systems);

        var sysline2 = footer.selectAll('g.system_b').selectAll('rect').data(systems);
        var sysline2_txt = footer.selectAll('g.system_b').selectAll('text').data(systems);

        var system_timelines = header.selectAll('line').data(actors);

        system_timelines.enter()
            .append('line')
            .attr('x1',d => d.line)
            .attr('x2',d => d.line)
            .attr('y1',(d=>30+d.y0))
            .attr('y2',y)
            .attr('stroke','black')

        system_timelines
            .attr('x1',d => d.line)
            .attr('x2',d => d.line)
            .attr('y2',y)


        sysline1.enter().append('rect')
            .attr('x', d =>d.x )
            .attr('y', 5)
            .attr('rx',5)
            .attr('ry',5)
            .attr('width', d=>d.width)
            .attr('height', 20)
            .attr('stroke', 'black')
            .attr('fill', 'white')
            .on("click", function(event, d) { console.log(event); clickSystem(event, d, data); })

        sysline1
            .attr('x', d => d.x )
            .attr('width', d=>d.width)


        sysline1_txt.enter().append('text').text( function(d) {return d.name; })
            .attr('x', function(d) {return 20+d.x;} )
            .attr('y', 20)
            .attr('stroke', 'black')
            .attr('visibility', d => (d.show?'visible':'hidden'))
            .on("click", function(event, d) { console.log(event); clickSystem(event, d, data); })


        sysline1_txt
            .attr('x', function(d) {return 20+d.x;} )
            .attr('visibility', d => (d.show?'visible':'hidden'))

        sysline2.enter().append('rect')
            .attr('x', function(d) {return d.x;} )
            .attr('y', y+5)
            .attr('rx',5)
            .attr('ry',5)
            .attr('width', d => d.width)
            .attr('height', 20)
            .attr('stroke', 'black')
            .attr('fill', 'white')
            .on("click", function(event, d) { console.log(event); clickSystem(event, d, data); })


        sysline2_txt.enter().append('text').text( function(d) {return d.name; })
            .attr('x', function(d) {return 20+d.x;} )
            .attr('y', y+20)
            .attr('stroke', 'black')
            .attr('visibility', d => (d.show?'visible':'hidden'))
            .on("click", function(event, d) { console.log(event); clickSystem(event, d, data); })


        sysline2
            .attr('x', function(d) {return d.x;} )
            .attr('y', y+5)
            .attr('width', d=>d.width)

        sysline2_txt
            .attr('x', function(d) {return 20+d.x;} )
            .attr('y', y+20)
            .attr('visibility', d => (d.show?'visible':'hidden'))


        systems.forEach( drawInteraction )


        var step_groups = main.selectAll('g.step').data(steps);

        step_groups.enter().append('g')
            .attr('class','step')
            .attr('id', d => d.id)
            .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
        step_groups
            .attr("transform",function(d) {return 'translate(0, '+d.y+')';})
    

        var boxes = main.selectAll('g.step')

        boxes.selectAll('rect').remove()
        boxes.selectAll('#step_label').remove()

        boxes.append('rect')
            .attr('x',0)
            .attr('y',0)
            .attr('rx',2)
            .attr('ry',2)
            .attr('width',x)
            .attr('height', function(d) {return d.height;})
            .attr('stroke','black')
            .attr('fill',"rgba(250,250,0,0.2)")
            .on('click', function(event, d) {clickStep(event, d, data);})

        boxes
            .attr('height', function(d) {return d.height;})


        boxes
            .append('text')
            .attr('id','step_label')
            .attr('x',5)
            .attr('y',15)
            .on('click', function(event, d) {clickStep(event, d, data);})


        steps.forEach( drawStep ) 
        
        svg.attr('height',y+100)

    }

    
    function clickStep(event, d, data){
        console.log('click Step');
        console.log(d)
        d.show = !d.show;
        d.height = d.show?(minHeight + d.steps.length * stepHeight + 2*stepMargin):minHeight;
        update()
    }
    
    function clickSystem(event, d, data) {
        console.log('click System');
        console.log(d)
        d.show = !d.show;
        d.width = d.show?(minWidth + d.interactions.length * interactionWidth + 2*systemMargin):minWidth;
        update()
    }

}

function entry(filename, global_tag) {
        console.log('trying to load: '+filename);
        d3.json(filename).then( function(d) {run(d, global_tag)} );
}

entry(filename, global_tag)

