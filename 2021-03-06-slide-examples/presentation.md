<!-- .slide: data-background="#ffffff" data-transition="none" -->

<div class="talk-title">
  <h2>Examples for slides</h2>
  <p style="font-size: 0.7em; display: grid;">
    <a href="https://octopus-code.gitlab.io/octopus-presentations/2021-03-25-MPSD-Theory-Seminar/static/">Heiko Appel </a>
  </p>

  <p style="display: grid;">
    Max Planck Institute for the Structure and Dynamics of Matter
    <br>
    Hamburg, Germany
  </p>

  <img  style="height:200px;" src="figures/Octopus_logo.png" />

  For more content visit: <a href="https://gitlab.com/octopus-code/octopus-presentations">https://gitlab.com/octopus-code/octopus-presentations</a>
</div>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

<div class="container">
 <div>Element 1</div>
 <div class="fragment" data-fragment-index="1">Element 2</div>
 <div class="fragment" data-fragment-index="3">Element 3</div>
 <div class="fragment" data-fragment-index="2">Element 4</div>
 <div class="fragment" data-fragment-index="4">Element 5</div>
 <div class="fragment" data-fragment-index="5">Element 6</div>
 <div class="fragment" data-fragment-index="6">Element 7</div>
 <div class="fragment" data-fragment-index="7">Element 8</div>
 <div class="fragment" data-fragment-index="8">Element 9</div>
 <div class="fragment" data-fragment-index="9">Element 10</div>
 <div class="fragment" data-fragment-index="10">Element 11</div>
 <div class="fragment" data-fragment-index="11">Element 12</div>
</div>

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

```mermaid
graph TD;
   A-->B;
   A-->C;
   B-->D;
   C-->D;
```

---

<!-- .slide: class="fig-container" data-file="./figures/d3/collision-detection.html" style="overflow: visible" data-transition="none"-->

---

<!-- .slide: class="fig-container" data-file="./figures/d3/bar-chart.html" style="overflow: visible" data-transition="none" -->

---

<!-- .slide: data-background="#ffffff" data-transition="none" -->

 <div class="col-50 no-margin-top">
   <p class="no-margin-top small">
   <a href='https://bl.ocks.org/olearym/ec27b9ac1bf0da42ed8a0d533181693b' target='_blank'>Modular Multiplication Circle</a> by Megan O'Leary</p>
   <div class="fig-container no-margin-top" data-file="./figures/d3/modular-multiplication.html"></div>
 </div>
